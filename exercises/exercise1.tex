\documentclass[a4paper,12pt]{article}

\usepackage[a4paper,margin=2cm]{geometry}
\usepackage{minted}
\usepackage{hyperref}
\usepackage{framed}

\title{Exercise 1 : the linux page cache}
\author{S\'ebastien Ponce}

\begin{document}

% for exercises output
\newdimen\longline
\longline=\textwidth\advance\longline-6cm
\def\LayoutTextField#1#2{#2}
\def\lbl#1{\hbox to 5cm{#1\hfill\strut}}%
\def\labelline#1#2#3{\lbl{#1}\vbox{\hbox{\TextField[bordercolor=white,name=#2,width=#3]{\null}}\kern2pt\hrule}}
\def\q#1#2{\hbox to \hsize{\labelline{#1}{#2}{\longline}}\vskip1.4ex}

\def\easyin#1#2{
  \begin{framed}
    \begin{Form}
      \q{#1}{#2}
    \end{Form}
  \end{framed}
}

\maketitle

\section{Foreword}

All the exercises of the ``Strategies and technologies for efficient I/O'' track are build on the same ideas :
\begin{itemize}
\item they provide working code and you only have to use and look around. No programming skills required :-)
\item they are mostly written in python, as python code is more readable than C/C$^{++}$ for non experts. And they are as simple as possible so that the code is very readable
\item their only purpose is to demonstrate something. They are in no way supposed to be efficient or well structured. DO NOT REUSE that code in real software ! It is bad, inefficient and more importantly very unsecure.
\item they are designed to mostly work locally (yes, even the networking ones) on a crappy old machine, or from a virtual machine on a laptop. So they can run anywhere, including directly on your laptops. But take care that you may have to install some tools then.
\end{itemize}

The exercises are using the linux command line and the many tools provided in a standard linux distribution. For the ones you do not know, please open the man page and read at least the description. Also feel free to ask any questions.

\section{Goals of the exercise}

\begin{itemize}
\item Exercise the linux page cache
\item See effect of read ahead
\item Use memory mapped file
\end{itemize}

\section {Setup}

\subsection{Basics}

\begin{itemize}
\item open a terminal
\item go to /data, download and untar the exercises
  \begin{minted}{shell}
    cd /data
    wget http://cern.ch/sponce/CSC/exercises.tgz
    tar xvzf exercises.tgz
  \end{minted}
\item go to exercise1
  \begin{minted}{shell}
    cd exercises/exercise1
  \end{minted}
\end{itemize}

\subsection{Create a data file}

\begin{itemize}
\item Use the generateTemperatureFile.py in the tools directory to create a data file. Use 10000 captors and 100 points for a 4MB file.
\begin{minted}{shell}
  python ../tools/generateTemperatureFile.py 10000 100 datafile 
\end{minted}
\item Be patient, it should take around 2mn
\item check that the file is in the kernel cache
\begin{minted}{shell}
  linux-fincore datafile
\end{minted}
\end{itemize}

\section{Understanding the page cache}

\subsection{Efficient access and cache usage}

\begin{itemize}
\item Have a look at the code of countOverheated.py
  \begin{itemize}
  \item see that there is a single seek and a single read for the main part
  \item note the small read of 4 bytes at the start of the file for nbCaptors
  \end{itemize}
\item Drop the datafile from the page cache
  \begin{minted}{shell}
    linux-fadvise datafile POSIX_FADV_DONTNEED 
  \end{minted}
\item check the cache
  \begin{minted}{shell}
    linux-fincore datafile
  \end{minted}
\item start iostat in another shell in order to monitor I/O. ``-d 2'' will display the amount of I/O every 2 seconds. ``-p \textless device\textgreater'' specifies the device. You can find it from the ``mount'' command, looking at the data mountpoint if you not are using the standard virtual machine.
  \begin{minted}{shell}
    iostat -d 2 -p /dev/mapper/fedora-data
  \end{minted}
\item run the code to find overheated devices at a given time
  \begin{minted}{shell}
    python countOverheated.py datafile 40 45
  \end{minted}
\item check the amount of I/O done from disk and the status of the cache, including the list of cached pages. Remember pages are 4 KiB
  \begin{minted}{shell}
    linux-fincore datafile
    linux-fincore -p datafile
  \end{minted}
  \begin{framed}
    \begin{Form}
      \q{Amount of I/O}{q1}
      \q{Percentage of file in cache}{q2}
      \q{Pages in cache}{q3}
    \end{Form}
  \end{framed}
\item compute what was the minimal set of pages that needed to be in the cache
  \begin{framed}
    \begin{Form}
      \q{Minimal pages needed}{q4}
      \q{Associated cached size}{q5}
    \end{Form}
  \end{framed}
\item draw some conclusions on the behavior of the cache, in particular concerning read-ahead
\item keep iostat running and run the code again for the next point in time
\begin{minted}{shell}
  python countOverheated.py datafile 41 45
\end{minted}
\item see in iostat output that nothing was read from disk
\item see with that the cache did not change
\begin{minted}{shell}
  linux-fincore -p datafile
\end{minted}
\end{itemize}

\subsection{scattered reads}

\begin{itemize}
\item Have a look at the code of plotCaptor.py
  \begin{itemize}
  \item see the number of small reads of size 4
  \item note the small read of 4 bytes at the start of the file for nbCaptors
  \end{itemize}
\item Drop the datafile from the page cache
\begin{minted}{shell}
  linux-fadvise datafile POSIX_FADV_DONTNEED 
\end{minted}
\item check the cache
\begin{minted}{shell}
  linux-fincore datafile
\end{minted}
\item start iostat in another shell in order to monitor I/O
\begin{minted}{shell}
  iostat -d 2 -p /dev/mapper/fedora-data
\end{minted}
\item run the plotCaptor program for one captor
\begin{minted}{shell}
  python plotCaptor.py datafile 40
\end{minted}
\item check the amount of I/O done from disk and the status of the cache, including the list of cached pages
\begin{minted}{shell}
  linux-fincore datafile
  linux-fincore -p datafile
\end{minted}
  \begin{framed}
    \begin{Form}
      \q{Amount of I/O}{q6}
      \q{I/O duration}{q7}
      \q{Percentage of file in cache}{q8}
      \q{Pages in cache}{q9}
    \end{Form}
  \end{framed}
\item note that behavior of the page cache : it ``understood'' that we were doing scattered reads. We have still read 10\% if the file for getting 0.01\% of the data
\item try again with 2 captors not close to each other. Do not forget to clean up the cache
\begin{minted}{shell}
  linux-fadvise datafile POSIX_FADV_DONTNEED 
  python plotCaptor.py datafile 40,2040
  linux-fincore datafile
  linux-fincore -p datafile
\end{minted}
  \begin{framed}
    \begin{Form}
      \q{Amount of I/O}{q10}
      \q{I/O duration}{q11}
      \q{Percentage of file in cache}{q12}
      \q{Pages in cache}{q13}
    \end{Form}
  \end{framed}
\item no big surprise here : 20\% of the file was read
\item try again with 3 captors in 3 contiguous pages. Again do not forget to clean up the cache
\begin{minted}{shell}
  linux-fadvise datafile POSIX_FADV_DONTNEED 
  python plotCaptor.py datafile 40,1040,2040
  linux-fincore datafile
  linux-fincore -p datafile
\end{minted}
  \begin{framed}
    \begin{Form}
      \q{Amount of I/O}{q14}
      \q{I/O duration}{q15}
      \q{Percentage of file in cache}{q16}
      \q{Pages in cache}{q17}
    \end{Form}
  \end{framed}
\item see that we've read 66\% of the file rather that 30\%. Obviously the read-ahead has been activated by the fact that we read contiguous pages.
\item also see that we did more user time (even less actually) although we've read more data. Reading continuous blocks without seeking is efficient.
\end{itemize}


\section{Mapped files}

The goal here is to read big portions of a big file and see the improvement brought by mapped memory. We will use the temperature file but extend it to over 1GB by adding zeroes and we will count the number of captors exceeding a certain temperature.

\subsection{Prepare a big data file}

\begin{itemize}
\item copy datafile into bigdatafile and extend it with zeroes using dd
  \begin{minted}{shell}
  cp datafile bigdatafile
  dd if=/dev/zero of=bigdatafile bs=40000 count=1 seek=40000 conv=notrunc
  \end{minted}
\item the file still has 10000 captors, but has now 40000 points (and a bit more), out of which only the first 100 are non zero
\item note by the way how the file system optimizes such sparse file
  \begin{minted}{shell}
    ls -lh bigdatafile
    du -h bigdatafile
    du -h --apparent-size bigdatafile
  \end{minted}  
  \begin{framed}
    \begin{Form}
      \q{File size from ls}{q18}
      \q{File size from du}{q19}
      \q{Apparent file size from du}{q20}
    \end{Form}
  \end{framed}
\end{itemize}

\subsection{Reading the file the C way}

\begin{itemize}
\item have a look at the code in countOverHeated.c. This is C code but should still be quite readable. See how the file is read into a local buffer that is then used for counting the number of overheated captors
\item make sure that bigdatafile is not in the page cache
  \begin{minted}{shell}
    linux-fadvise bigdatafile POSIX_FADV_DONTNEED
    linux-fincore bigdatafile
  \end{minted}
\item compile countOverHeated - you will also compile the Mapped version actually
  \begin{minted}{shell}
    make
  \end{minted}
\item run countOverHeated for 10000 points and see how much time it takes
  \begin{minted}{shell}
    ./countOverheated bigdatafile 4-10004 45
  \end{minted}
\item check that bigdatafile is now partially in the linux page cache. Check that all what we read is there (compute how much we read)
  \begin{minted}{shell}
    linux-fincore bigdatafile
  \end{minted}
  \begin{framed}
    \begin{Form}
      \q{Percentage of file in cache}{q21}
      \q{Expected size in cache}{q22}
      \q{Total time spent}{q23}
    \end{Form}
  \end{framed}
\item run again the same command and see the improvement due to the page cache. Deduce the amount of time it took to fill the cache versus how much the memory allocation + memory copy took
  \begin{minted}{shell}
    ./countOverheated bigdatafile 4-10004 45
  \end{minted}
  \begin{framed}
    \begin{Form}
      \q{New total time spent}{q24}
      \q{Time for filling the cache}{q25}
      \q{Time for allocation/copy}{q26}
    \end{Form}
  \end{framed}
\end{itemize}

\subsection{Memory mapping the file in C}

\begin{itemize}
\item have a look at the code in countOverHeatedMapped.c. See how the file is mapped into memory and never explicitely read.
\item make sure that bigdatafile is not in the page cache
  \begin{minted}{shell}
    linux-fadvise bigdatafile POSIX_FADV_DONTNEED
    linux-fincore bigdatafile
  \end{minted}
\item run the mapped version a first time with the same 10000 points and see how much time it takes
  \begin{minted}{shell}
    ./countOverheatedMapped bigdatafile 4-10004 45
  \end{minted}
  \easyin{Total time spent}{easyin1}
\item see the difference with the non mapped version
  \easyin{Gain}{easyin2}
\item check that bigdatafile is now partially in the linux page cache. Check that all what we use is there
  \begin{minted}{shell}
    linux-fincore bigdatafile
  \end{minted}
\item run again countOverheatedMapped over the same captors
  \begin{minted}{shell}
    ./countOverheatedMapped bigdatafile 4-10004 45
  \end{minted}
  \easyin{New total time spent}{easyin3}
\item see the improvement due to the page cache
  \easyin{Gain}{easyin4}
\end{itemize}

\subsection{Reading the file in python (optional)}

\begin{itemize}
\item go back to the code of countOverheated.py, the one we've used at the beginning. It's the python version of the countOverheated.c file. No memory mapping code there
\item cleanup cache for bigdatafile
\begin{minted}{shell}
  linux-fadvise bigdatafile POSIX_FADV_DONTNEED 
  linux-fincore bigdatafile
\end{minted}
\item run the python version and check timing
\begin{minted}{shell}
  python countOverheated.py bigdatafile 4-10004 45
\end{minted}
  \begin{framed}
    \begin{Form}
      \q{Total time spent}{q27}
      \q{Time spent in extraction}{q28}
    \end{Form}
  \end{framed}
\item see how python is different from C at computing if you do not take care !
\item also see that the timing for extraction is close to the non mapped C case
\item check page cache status for bigdatafile
\begin{minted}{shell}
  linux-fincore bigdatafile
\end{minted}
\easyin{Percentage of file in cache}{easyin5}
\item run the python version again and check benefit of the page cache
\begin{minted}{shell}
  python countOverheated.py bigdatafile 4-10004 45
\end{minted}
  \begin{framed}
    \begin{Form}
      \q{New total time spent}{q29}
      \q{Gain}{q30}
    \end{Form}
  \end{framed}
\item you should see again performance close to C for data extraction (if not slightly better)
\item checking the python code more carefully, you should actually be surprised. Indeed, we have an extra data copy that C does not have to do : creating a python string from the C array of bytes.  
\item let's understand why we do not seem to pay this copy : run again the python version with strace, looking for file handling calls
\begin{minted}{shell}
  strace -e open,openat,read,write,close,mmap,munmap -s 0 \
    python countOverheated.py bigdatafile 4-10004 45
\end{minted}
\item the output is big, but most of it is python starting, you only have to look at the last 20 lines or so. Identify the 'open(``bigdatafile'', O\_RDONLY)' entry and read on.
\item what do you notice about the file access ? mmap is used ! So we do pay the copy to the python string. But we do not pay the copy to memory as we actually use mmap.
\item we can improve and avoid the copy to python by using the mmap module of python.\\ Look at the code of countOverheatedMapped.py. This time, we map the memory directly to python.
\item run countOverheatedMapped.py and check benefit compared to the initial version
\begin{minted}{shell}
  python countOverheatedMapped.py bigdatafile 4-10004 45
\end{minted}
  \begin{framed}
    \begin{Form}
      \q{New total time spent}{q31}
      \q{Gain}{q32}
    \end{Form}
  \end{framed}
  
\end{itemize}

\end{document}
