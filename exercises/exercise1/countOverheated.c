#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <errno.h>
#include <sys/time.h>

void usage(const char* prog, const char* msg) {
  if (msg) printf (msg);
  printf ("%s <fileName> <pointNb>[-pointNb] <temperatureMax>\n", prog);
  exit(1);
}

int main(int argc, char** argv) {
  // check input
  if (argc != 4 ) {
    usage(argv[0], "Wrong nb of arguments");
  }
  // parse arguments
  unsigned int nbPoints = 1;
  int point1 = -1;
  int point2 = -1;
  char* minus = strchr(argv[2], '-');
  if (minus) {
    *minus = 0;
    point1 = atoi(argv[2]);
    point2 = atoi(minus+1);
    nbPoints = point2 - point1;
  } else {
    point1 = atoi(argv[2]);
  }  
  if (point1 < 0) usage(argv[0], "Invalid point1");
  if (nbPoints > 1 && point2 < 0) usage(argv[0], "Invalid point2");
  double maxTmp = atof(argv[3]);
  // open data file
  FILE *f = fopen(argv[1], "r");
  if (!f) {
    printf("Unable to open file %s\n", argv[1]);
    exit(1);
  }
  // get nb captors
  unsigned int nbCaptors;
  int n = fread(&nbCaptors, 4, 1, f);
  if (n != 1) {
    printf("Got error while reading nbCaptors : %d\n", errno);
    exit(1);    
  }
  // read data
  struct timeval before , after;
  gettimeofday(&before , NULL);
  fseek(f, 4+4*nbCaptors*point1, SEEK_SET);
  void* rawData = malloc(4*nbCaptors*nbPoints);
  int rc = fread(rawData, 4*nbCaptors, nbPoints, f);
  if (rc != nbPoints) {
    printf("Point %d not found in file\n", point1);
    exit(1);
  }
  gettimeofday(&after , NULL);
  double duration = (double)after.tv_sec + ((double)after.tv_usec)/1000000
    - (double)before.tv_sec - ((double)before.tv_usec)/1000000;
  printf("Data extraction took %f seconds\n", duration);
  float* data = (float*)rawData;
  // Compute hot points
  unsigned int nbOverheated = 0;
  gettimeofday(&before , NULL);
  {
    unsigned int point, captor;
    for (point = 0; point < nbPoints; point++) {
      for (captor = 0; captor < nbCaptors; captor++) {
        if (*(data+(point*nbCaptors)+captor) > maxTmp) nbOverheated++;
      }
    }
  }
  gettimeofday(&after , NULL);
  duration = (double)after.tv_sec + ((double)after.tv_usec)/1000000
    - (double)before.tv_sec - ((double)before.tv_usec)/1000000;
  printf("Counting overheated %f seconds\n", duration);
  printf("There are %d overheated captors\n", nbOverheated);
  free(rawData);
}
