#!/usr/bin/python
import sys, struct, os, timeit

# the format of the data is binary with the following structure
#   - bytes 0 to 3 : nbCaptors
#   - bytes 4 to 4+4*nbCaptors : values for the first point of each captor (floats)
#   - repeated bunches of 4*nbCaptors bytes

def usage(msg=None):
    if msg:
        print msg
    print "%s <fileName> <pointNb>[-pointNb] <temperatureMax>" % (sys.argv[0])
    sys.exit(1)

# check input
if len(sys.argv) != 4:
    usage('Wrong nb of arguments')
try:
    points = sys.argv[2]
    if '-' in points :
        point1 = int(points[:points.find('-')])
        point2 = int(points[points.find('-')+1:])+1
        nbPoints = point2 - point1
    else:
        point1 = int(sys.argv[2])
        point2 = None
        nbPoints = 1
    if point1 < 0: raise ValueError
    if point2 and point2 < 0: raise ValueError
except ValueError:
    usage('Invalid point nb/range : "%s"' % sys.argv[2])
try:
    maxTmp = float(sys.argv[3])
except ValueError:
    usage('Invalid maximum temperature : "%s"' % sys.argv[3])

# open data file
f = None
try:
    f = open(sys.argv[1])
except IOError:
    print "File %s does not exist" % sys.argv[1]
    sys.exit(1)
nbCaptors = struct.unpack('I', f.read(4))[0]

# read data
hotCaptors = {}
start_time = timeit.default_timer()
f.seek(4+4*nbCaptors*point1)
rawData = f.read(4*nbCaptors*nbPoints)
extractionTime = timeit.default_timer() - start_time
if len(rawData) < 4*nbCaptors*nbPoints:
    print 'Point %d not found in file' % point1
    print 'The file has %d points' % ((os.stat(sys.argv[1]).st_size-4)/4/nbCaptors)
    sys.exit(1)
print 'Data extraction took %fs' % extractionTime

# Compute hot points
start_time = timeit.default_timer()
nbOverheated = 0
for point in range(nbPoints):
    data = struct.unpack(nbCaptors*'f', rawData[(point*nbCaptors)*4:((point+1)*nbCaptors)*4])
    for captor in range(nbCaptors):
        if data[captor] > maxTmp: nbOverheated += 1
computationTime = timeit.default_timer() - start_time
f.close()
print 'Computation took %fs' % computationTime

# print list to hot captor
if hotCaptors:
    print 'There are %d overheated captors' % nbOverheated
