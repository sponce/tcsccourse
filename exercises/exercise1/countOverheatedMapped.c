#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <unistd.h>
#include <sys/time.h>

void usage(const char* prog, const char* msg) {
  if (msg) printf (msg);
  printf ("%s <fileName> <pointNb>[-pointNb] <temperatureMax>\n", prog);
  exit(1);
}

int main(int argc, char** argv) {
  // check input
  if (argc != 4 ) {
    usage(argv[0], "Wrong nb of arguments");
  }
  // parse arguments
  unsigned int nbPoints = 1;
  int point1 = -1;
  int point2 = -1;
  char* minus = strchr(argv[2], '-');
  if (minus) {
    *minus = 0;
    point1 = atoi(argv[2]);
    point2 = atoi(minus+1);
    nbPoints = point2 - point1;
  } else {
    point1 = atoi(argv[2]);
  }  
  if (point1 < 0) usage(argv[0], "Invalid point1");
  if (nbPoints > 1 && point2 < 0) usage(argv[0], "Invalid point2");
  double maxTmp = atof(argv[3]);
  // open data file
  int fd = open(argv[1], O_RDONLY);
  if (fd < 0) {
    printf("Unable to open file %s\n", argv[1]);
    exit(1);
  }
  // get nb captors
  unsigned int nbCaptors;
  int n = read(fd, &nbCaptors, 4);
  if (n != 4) {
    printf("Got error while reading nbCaptors : %d\n", errno);
    exit(1);    
  }
  // map data
  unsigned int offset = 4+4*nbCaptors*point1;
  unsigned int size = nbPoints*nbCaptors*4;
  long pageSize = sysconf(_SC_PAGE_SIZE);
  unsigned int alignedOffset = (offset/pageSize) * pageSize;
  unsigned int alignedSize = size + offset - alignedOffset;
  void* rawData = mmap(0, alignedSize, PROT_READ, MAP_SHARED, fd, alignedOffset);
  if (rawData == MAP_FAILED) {
    printf("Point %d not found in file : error %d\n", point1, errno);
    exit(1);
  }
  float* data = (float*)(rawData+offset-alignedOffset);
  // Compute hot points
  unsigned int nbOverheated = 0;
  struct timeval before , after;
  gettimeofday(&before , NULL);
  {
    unsigned int point, captor;
    for (point = 0; point < nbPoints; point++) {
      for (captor = 0; captor < nbCaptors; captor++) {
        if (*(data+(point*nbCaptors)+captor) > maxTmp) nbOverheated++;
      }
    }
  }
  gettimeofday(&after , NULL);
  double duration = (double)after.tv_sec + ((double)after.tv_usec)/1000000
    - (double)before.tv_sec - ((double)before.tv_usec)/1000000;
  printf("Extraction and counting took %f seconds\n", duration);
  printf("There are %d overheated captors\n", nbOverheated);
  munmap(rawData, alignedSize);
}
