#!/usr/bin/python
import sys, struct, os, timeit
import matplotlib.pyplot as plt

# the format of the data is binary with the following structure
#   - bytes 0 to 3 : nbCaptors
#   - bytes 4 to 4+4*nbCaptors : values for the first point of each captor (floats)
#   - repeated bunches of 4*nbCaptors bytes

def usage(msg=None):
    if msg:
        print msg
    print "%s <fileName> <captorNb>[,...]" % (sys.argv[0])
    sys.exit(1)

if len(sys.argv) != 3:
    usage('Wrong nb of arguments')
    
captors = None
try:
    captors = [int(arg) for arg in sys.argv[2].split(',')]
except ValueError:
    usage('Invalid captor nb : "%s"' % sys.argv[2])

# open data file
f = None
try:
    f = open(sys.argv[1])
except IOError:
    print "File %s does not exist" % sys.argv[1]
    sys.exit(1)
nbCaptors = struct.unpack('I', f.read(4))[0]
nbPoints = (os.stat(sys.argv[1]).st_size-4)/4/nbCaptors

# check captors validity
notExistingCaptors = [p for p in captors if p >= nbCaptors]
if notExistingCaptors:
    print 'WARNING : the following captors did not exist :'
    print '  %s' % str(notExistingCaptors)

# cleanup captor list : remove duplicates and sort
captors = list(set([p for p in captors if p < nbCaptors]))
captors.sort()

# prepare list of values for each captor
values = {k:list([]) for k in captors}

# read data
start_time = timeit.default_timer()
for i in range(nbPoints):
    for p in captors:
        f.seek(4+4*nbCaptors*i+4*p)
        values[p].append(struct.unpack('f', f.read(4))[0])
elapsed = timeit.default_timer() - start_time
print 'Data retrieval took %fs' % elapsed

# draw plot
for p in captors:
    plt.plot(range(nbPoints), values[p], linestyle='-', marker='x')
plt.axis([0, nbPoints, 0, 40])
plt.legend(['pt ' + str(p) for p in captors], loc='lower left')
plt.xlabel('Time')
plt.ylabel('Temperature')
plt.show()
