\documentclass[a4paper,12pt]{article}

\usepackage[a4paper,margin=2cm]{geometry}
\usepackage{minted}
\usepackage{hyperref}
\usepackage{framed}
\usepackage{upquote}

\title{Exercise 2 : checksums, RAID and erasure coding}
\author{S\'ebastien Ponce}

\begin{document}

% for exercises output
\newdimen\longline
\longline=\textwidth\advance\longline-6cm
\def\LayoutTextField#1#2{#2}
\def\lbl#1{\hbox to 5cm{#1\hfill\strut}}%
\def\labelline#1#2#3{\lbl{#1}\vbox{\hbox{\TextField[bordercolor=white,name=#2,width=#3]{\null}}\kern2pt\hrule}}
\def\q#1#2{\hbox to \hsize{\labelline{#1}{#2}{\longline}}\vskip1.4ex}
\def\pq{\mbox{\TextField[borderwidth=0,name=,width=2cm,height=.5cm]{\null}}}

\def\easyin#1#2{
  \begin{framed}
    \begin{Form}
      \q{#1}{#2}
    \end{Form}
  \end{framed}
}

\maketitle

\section{Goals of the exercise}

\begin{itemize}
\item Play with checksums and compare efficiency and robustness
\item Use hand written version of RAID systems and test error recovery
  \begin{itemize}
  \item with striping
  \item with mirroring
  \item with parity
  \end{itemize}
\item Test erasure coding
\end{itemize}

\section {Setup}

\subsection{Basics}

\begin{itemize}
\item open a terminal
\item if you did not download the exercises yet, refer to exercise1 to do it
\item go to the exercise2 directory
  \begin{minted}{shell}
    cd /data/exercises/exercise2
  \end{minted}
\end{itemize}

\subsection{Create data files}

\begin{itemize}
\item Use the generatePatternFile.py in the tools directory to create a data file with a readable pattern.
\begin{minted}{shell}
  python ../tools/generatePatternFile.py patternFile
\end{minted}
\item check the content of the pattern file : numbers from 0 to 9999
\begin{minted}{shell}
  less patternFile
\end{minted}  
\item Use the generateTemperatureFile.py in the tools directory to create a data file. Use 10000 captors and 100 points for a 40MB file.
\begin{minted}{shell}
  python ../tools/generateTemperatureFile.py 10000 100 datafile 
\end{minted}
\item Be patient, it should take around 2mn
\end{itemize}

\section{Checksums}

Go to checksum subdirectory
\begin{minted}{shell}
  cd checksum
\end{minted}

\subsection{computation speed}

\begin{itemize}
\item compute xor32, adler32, md5, sha1 and sha256 checksums for your datafile from memory (as it is in cache)
\begin{minted}{shell}
  ./xor32.py ../datafile
  ./adler32.py ../datafile
  ./md5.py ../datafile
  ./sha1.py ../datafile
  ./sha256.py ../datafile
\end{minted}
\begin{center}
\renewcommand{\arraystretch}{1.6}
\begin{tabular}{|l|c|c|}
  \hline
  & Checksum (first 4 bytes) & Computation time \\
  \hline
  Xor & \pq & \pq \\
  \hline
  Adler & \pq & \pq \\
  \hline
  MD5 & \pq & \pq \\
  \hline
  Sha1 & \pq & \pq \\
  \hline
  Sha256 & \pq & \pq \\
  \hline
\end{tabular}
\end{center}
\item compare timings except for xor that was implemented in python manually
\item note by the way the difference of speed between non optimized python and optimized compiled code !
\end{itemize}

\subsection{error detection}
\begin{itemize}
\item change first byte of the input file (remember previous value ! it's 0x10)
\begin{minted}{shell}
  hexdump -C -n 16 ../datafile
  printf '\x00' | dd of=../datafile bs=1 count=1 conv=notrunc
  hexdump -C -n 16 ../datafile
\end{minted}
\item Recompute all checksums and see the changes in the different algorithms
\begin{minted}{shell}
  ./xor32.py ../datafile
  ./adler32.py ../datafile
  ./md5.py ../datafile
  ./sha1.py ../datafile
  ./sha256.py ../datafile
\end{minted}
\begin{center}
\renewcommand{\arraystretch}{1.6}
\begin{tabular}{|l|c|}
  \hline
  & Checksum (first 4 bytes) \\
  \hline
  Xor & \pq \\
  \hline
  Adler & \pq \\
  \hline
  MD5 & \pq \\
  \hline
  Sha1 & \pq \\
  \hline
  Sha256 & \pq \\
  \hline
\end{tabular}
\end{center}
\item note that xor changes only by one bit and adler has its lower part almost unchanged (again only by one bit). Note how the other ones are completely different although we've changed only one bit of input.
\item put back proper first byte (was 0x10) but exchange the first 4 bytes with the next 4. Take care that the next 4 are specific to your file ! Put the right values in the following code
\begin{minted}{shell}
  hexdump -C -n 16 ../datafile
  printf '\x10' | dd of=../datafile bs=1 count=1 conv=notrunc
  printf '\x73\x5d\xfc\x40\x10\x27\x00\x00' | \
    dd of=../datafile bs=1 count=8 conv=notrunc
  hexdump -C -n 16 ../datafile
\end{minted}
\easyin{Next 4 bytes in my file}{e1}
\item Recompute all checksums and see the changes in the different algorithms
\begin{minted}{shell}
  ./xor32.py ../datafile
  ./adler32.py ../datafile
  ./md5.py ../datafile
  ./sha1.py ../datafile
  ./sha256.py ../datafile
\end{minted}
\begin{center}
\renewcommand{\arraystretch}{1.6}
\begin{tabular}{|l|c|}
  \hline
  & Checksum (first 4 bytes) \\
  \hline
  Xor & \pq \\
  \hline
  Adler & \pq \\
  \hline
  MD5 & \pq \\
  \hline
  Sha1 & \pq \\
  \hline
  Sha256 & \pq \\
  \hline
\end{tabular}
\end{center}
\item note in particular that xor does not detect exchanges and that adler32 has same lower part.
\item repair file so that it's readable. Again, put the right bytes for your case !
\begin{minted}{shell}
  hexdump -C -n 16 ../datafile
  printf '\x10\x27\x00\x00\x73\x5d\xfc\x40' | \
    dd of=../datafile bs=1 count=8 conv=notrunc
  hexdump -C -n 16 ../datafile
\end{minted}
\end{itemize}

\section{RAID levels}

\subsection{Introduction}

We will play here with fake RAID systems, where disks were replaced by directories.
The implementations are done in python and are highly inefficient.

The only point of this exercise is to have simple code that you can understand easily and demonstrates how RAID technologies work.

\subsection{RAID 0 : striping}

\begin{itemize}
\item Go to RAID0 subdirectory
  \begin{minted}{shell}
    cd ../RAID0
  \end{minted}
\item create a RAID setup using the createRAID0 script. Check its code
  \begin{minted}{shell}
    ./createRAID0.py 5
  \end{minted}
\item store the pattern file into the RAID array using copyToRAID0.py. Check its code
  \begin{minted}{shell}
    ./copyToRAID0.py ../patternFile
  \end{minted}
\item have a look at the content of the striped
  \begin{minted}{shell}
    less Disk0/patternFile.0
    less Disk1/patternFile.1
    less Disk2/patternFile.2
  \end{minted}
\item read back the file using readFromRAID0 and check consistency
  \begin{minted}{shell}
    ./readFromRAID0.py patternFile patternFileCopy
    diff ../patternFile patternFileCopy
  \end{minted}
\end{itemize}

\subsection{RAID 1 : mirroring}

\begin{itemize}
\item Go to RAID1 subdirectory
  \begin{minted}{shell}
    cd ../RAID1
  \end{minted}
\item create a RAID setup using the createRAID1 script. Check its code
  \begin{minted}{shell}
    ./createRAID1.py 3
  \end{minted}
\item store a file into the RAID array using copyToRAID1.py. Check its code
  \begin{minted}{shell}
    ./copyToRAID1.py ../patternFile
  \end{minted}
\item check how file was replicated and the cost in space
  \begin{minted}{shell}
    ls -al Disk*
    du -h ../patternFile
    du -hc Disk*/patternFile*
  \end{minted}
  \begin{framed}
    \begin{Form}
      \q{Original file size}{q1}
      \q{Space used by RAID1}{q2}
    \end{Form}
  \end{framed}
\item read back the file and check consistency. Check the read back code
  \begin{minted}{shell}
    ./readFromRAID1.py patternFile patternFileCopy
    diff ../patternFile patternFileCopy
  \end{minted}
\item suppress one copy and check you can still read
  \begin{minted}{shell}
    rm Disk0/patternFile
    ./readFromRAID1.py patternFile patternFileCopy
    diff ../patternFile patternFileCopy
  \end{minted}
\item corrupt another copy and check you can still read. Note the use of checksums
  \begin{minted}{shell}
    vim Disk1/patternFile
    ./readFromRAID1.py patternFile patternFileCopy
    diff ../patternFile patternFileCopy
  \end{minted}
\item ask for a rebuild of the RAID system and check effect
  \begin{minted}{shell}
    ls -l Disk*
    diff ../patternFile Disk1/patternFile
    ./repairFileInRAID1.py patternFile
    ls -l Disk*
    diff ../patternFile Disk1/patternFile
  \end{minted}
\end{itemize}

\subsection{RAID 5 : parity}

\begin{itemize}
\item Go to RAID5 subdirectory
  \begin{minted}{shell}
    cd ../RAID5
  \end{minted}
\item create a RAID setup using the createRAID5 script. Check its code
  \begin{minted}{shell}
    ./createRAID5.py 4
  \end{minted}
\item store a file into the RAID array using copyToRAID5.py. Check its code
  \begin{minted}{shell}
    ./copyToRAID5.py ../patternFile
  \end{minted}
\item check how file was split and how parity was added
  \begin{minted}{shell}
    ls -al Disk*
    less Disk0/patternFile.0
    less Disk2/patternFile.2
    less Disk3/patternFile.3
  \end{minted}
\item check the cost in space
  \begin{minted}{shell}
    du -h ../patternFile
    du -hc Disk*/patternFile*
  \end{minted}
  \begin{framed}
    \begin{Form}
      \q{Original file size}{q3}
      \q{Space used by RAID5}{q4}
    \end{Form}
  \end{framed}
\item read back the file and check consistency. Check the read back code
  \begin{minted}{shell}
    ./readFromRAID5.py patternFile patternFileCopy
    diff ../patternFile patternFileCopy
  \end{minted}
\item suppress one stripe and check you can still read
  \begin{minted}{shell}
    rm Disk0/patternFile.0
    ./readFromRAID5.py patternFile patternFileCopy
    diff ../patternFile patternFileCopy
  \end{minted}
\item ask for a rebuild of the RAID system and check effect
  \begin{minted}{shell}
    ls -l Disk*
    ./repairFileInRAID5.py patternFile
    ls -l Disk*
  \end{minted}
\item corrupt a stripe or its checksum and check you can still read
  \begin{minted}{shell}
    vim Disk1/patternFile.1
    ./readFromRAID5.py patternFile patternFileCopy
    diff ../patternFile patternFileCopy
    ./repairFileInRAID5.py patternFile
  \end{minted}
\end{itemize}

\section{erasure coding (optional)}

We use here an external library to provide erasure coding code.
It's called zfec and has a nice and simple python API (see https://pypi.python.org/pypi/zfec and go to ``Python API'').

\begin{itemize}
\item Go to erasure subdirectory
  \begin{minted}{shell}
    cd ../erasure
  \end{minted}
\item create a erasure coded pool with 3 + 4 disks using the createErasure script. Check its code
  \begin{minted}{shell}
    ./createErasure.py 3 4
  \end{minted}
\item store a file into the erasure coded pool using copyToErasure.py. Check its code
  \begin{minted}{shell}
    ./copyToErasure.py ../patternFile
  \end{minted}
\item check how file was split and how extra chunks were added
  \begin{minted}{shell}
    ls -al Disk*
    less Disk0/patternFile.0
    less Disk2/patternFile.2
    less Disk5/patternFile.5
  \end{minted}
\item check the cost in space
  \begin{minted}{shell}
    du -h ../patternFile
    du -hc Disk*/patternFile*
  \end{minted}
  \begin{framed}
    \begin{Form}
      \q{Original file size}{q5}
      \q{Space used by erasure}{q6}
    \end{Form}
  \end{framed}
\item read back the file and check consistency
  \begin{minted}{shell}
    ./readFromErasure.py patternFile patternFileCopy
    diff ../patternFile patternFileCopy
  \end{minted}
\item suppress one stripe and check you can still read
  \begin{minted}{shell}
    rm Disk0/patternFile.0
    ./readFromErasure.py patternFile patternFileCopy
    diff ../patternFile patternFileCopy
  \end{minted}
\item corrupt another stripe and check you can still read
  \begin{minted}{shell}
    vim Disk1/patternFile.1
    ./readFromErasure.py patternFile patternFileCopy
    diff ../patternFile patternFileCopy
  \end{minted}
\item go for one more deletion and one more corruption. Check you can still read !
  \begin{minted}{shell}
    vim Disk4/patternFile.4
    rm Disk5/patternFile.5
    ./readFromErasure.py patternFile patternFileCopy
    diff ../patternFile patternFileCopy
  \end{minted}
\item ask for a rebuild of the erasure coded pool and check effect
  \begin{minted}{shell}
    ls -l Disk*
    ./repairFileInErasure.py patternFile
    ls -l Disk*
  \end{minted}
\item have a look at the readFromErasure code and see how all this happens
\end{itemize}

\end{document}
