#!/usr/bin/python
import sys, os

def usage(msg=None):
    if msg:
        print msg
    print "%s <fileName>" % (sys.argv[0])
    sys.exit(1)

if len(sys.argv) != 2:
    usage('Wrong nb of arguments')
fileName = sys.argv[1]

# find number of disks in RAID system
try:
    nbDisks = int(open('.nbDisks').read())
except IOError, e:
    print "No RAID system found. Did you use createRAID0 ?"
    sys.exit(-1)
except ValueError, e:
    print "Invalid RAID0 configuration in '.nbDisks' file, please correct"
    sys.exit(-1)

# compute stripe size
try:
    fileSize = os.path.getsize(fileName)
    stripeSize = (fileSize+nbDisks-1) / nbDisks
except OSError:
    print "File %s does not exist" % fileName
    sys.exit(-1)

# create the stripes
f = open(fileName, 'r')
for stripeNb in range(nbDisks):
    g = open('Disk%d/%s.%d' % (stripeNb, os.path.basename(fileName), stripeNb), 'w')
    g.write(f.read(stripeSize))
    g.close()
f.close()

