#!/usr/bin/python
import sys, os

def usage(msg=None):
    if msg:
        print msg
    print "%s <nbDisks>" % (sys.argv[0])
    sys.exit(1)

if len(sys.argv) != 2:
    usage('Wrong nb of arguments')

try:
    nbDisks = int(sys.argv[1])
    if nbDisks < 1: raise ValueError
except ValueError:
    usage('Invalid nb of disks : "%s"' % sys.argv[1])

# check presence of .nbDisks file
if os.path.isfile(".nbDisks"):
    print "it seems that a RAID already exists, giving up"
    print "you may drop it be running 'rm -rf Disk* .nbDisks'"
    sys.exit(-1)
    
# create the "Disks", that is subdirectories
try:
    for i in range(nbDisks):
        os.mkdir('Disk%d'%i)
except OSError, e:
    print "unable to create RAID. Got following error :"
    print e

# create '.nbDisks' file to remember setup
open(".nbDisks", "w").write(str(nbDisks))
