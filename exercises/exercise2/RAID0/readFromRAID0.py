#!/usr/bin/python
import sys, os

def usage(msg=None):
    if msg:
        print msg
    print "%s <fileName> <destFileName>" % (sys.argv[0])
    sys.exit(1)

if len(sys.argv) != 3:
    usage('Wrong nb of arguments')
fileName = sys.argv[1]
dstFileName = sys.argv[2]

# find number of disks in RAID system
try:
    nbDisks = int(open('.nbDisks').read())
except IOError, e:
    print "No RAID system found. Did you use createRAID0 ?"
    sys.exit(-1)
except ValueError, e:
    print "Invalid RAID0 configuration in '.nbDisks' file, please correct"
    sys.exit(-1)

# read the stripes and create dstFile
f = open(dstFileName, 'w')
for stripeNb in range(nbDisks):
    g = open('Disk%d/%s.%d' % (stripeNb, fileName, stripeNb))
    f.write(g.read())
    g.close()
f.close()
print 'File succesfully retrieved'
