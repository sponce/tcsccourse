#!/usr/bin/python
import sys, os, hashlib

def usage(msg=None):
    if msg:
        print msg
    print "%s <fileName>" % (sys.argv[0])
    sys.exit(1)

if len(sys.argv) != 2:
    usage('Wrong nb of arguments')
fileName = sys.argv[1]

# find number of disks in RAID system
try:
    nbDisks = int(open('.nbDisks').read())
except IOError, e:
    print "No RAID system found. Did you use createRAID0 ?"
    sys.exit(-1)
except ValueError, e:
    print "Invalid RAID1 configuration in '.nbDisks' file, please correct"
    sys.exit(-1)

# read file and compute checksum of the file
try:
    buf = open(fileName).read()
    ck = hashlib.md5(buf).hexdigest()
except OSError:
    print "File %s does not exist" % fileName
    sys.exit(-1)

# create the replicas
for copyNb in range(nbDisks):
    g = open('Disk%d/%s' % (copyNb, os.path.basename(fileName)), 'w')
    g.write(buf)
    g.close()
    g = open('Disk%d/.%s.cksum' % (copyNb, os.path.basename(fileName)), 'w')
    g.write(ck)
    g.close()

