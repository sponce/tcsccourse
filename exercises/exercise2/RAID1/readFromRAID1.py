#!/usr/bin/python
import sys, os, hashlib

def usage(msg=None):
    if msg:
        print msg
    print "%s <fileName> <destFileName>" % (sys.argv[0])
    sys.exit(1)

if len(sys.argv) != 3:
    usage('Wrong nb of arguments')
fileName = sys.argv[1]
dstFileName = sys.argv[2]

# find number of disks in RAID system
try:
    nbDisks = int(open('.nbDisks').read())
except IOError, e:
    print "No RAID system found. Did you use createRAID0 ?"
    sys.exit(-1)
except ValueError, e:
    print "Invalid RAID1 configuration in '.nbDisks' file, please correct"
    sys.exit(-1)

# Go through the disks and try to read
# first succesful one wins
success = False
for copyNb in range(nbDisks):
    # open and get content of current copy
    try:
        content = open('Disk%d/%s' % (copyNb, fileName)).read()
    except IOError:
        # file not found : this copy was lost, go to next one
        print 'Note that copy on disk %d has been lost !' % copyNb
        print 'You should run repairFile !\n'
        continue
    # get checksum of current copy    
    try:
        expectedCk = open('Disk%d/.%s.cksum' % (copyNb, fileName)).read()
    except IOError:
        # file not found : the checksum was lost
        print 'Note that copy on disk %d has no checksum !' % copyNb
        print 'You should run repairFile !\n'
        continue
    computedCk = hashlib.md5(content).hexdigest()
    if expectedCk != computedCk:
        # invalid copy, forget it !
        print 'Note that copy on disk %d is corrupted !' % copyNb
        print 'You should run repairFile !\n'
        continue
    # valid copy, write output file
    open(dstFileName, 'w').write(content)
    success = True
    break
if not success:
    print 'Was unable to retrieve file... I fear I lost your data...'
    sys.exit(-1)
print 'File succesfully retrieved'
