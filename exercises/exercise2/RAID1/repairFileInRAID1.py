#!/usr/bin/python
import sys, os, hashlib

def usage(msg=None):
    if msg:
        print msg
    print "%s <fileName>" % (sys.argv[0])
    sys.exit(1)

if len(sys.argv) != 2:
    usage('Wrong nb of arguments')
fileName = sys.argv[1]

# find number of disks in RAID system
try:
    nbDisks = int(open('.nbDisks').read())
except IOError, e:
    print "No RAID system found. Did you use createRAID0 ?"
    sys.exit(-1)
except ValueError, e:
    print "Invalid RAID1 configuration in '.nbDisks' file, please correct"
    sys.exit(-1)

# Go through the disks and try to find a non corrupted copy and its checksum
# first succesful one wins
success = False
for copyNb in range(nbDisks):
    try:
        # open and get content of current copy
        content = open('Disk%d/%s' % (copyNb, fileName)).read()
        expectedCk = open('Disk%d/.%s.cksum' % (copyNb, fileName)).read()
        computedCk = hashlib.md5(content).hexdigest()
        if expectedCk != computedCk:
            # invalid copy, forget it !
            continue
        success = True
        okDisk = copyNb
        break
    except IOError:
        # file not found : this copy was lost or has no checksum, go to next one
        continue
if not success:
    print 'Was unable to repair file... I fear I lost your data...'
    sys.exit(-1)

# Go again through the disks and repair what needs to be
okContent = content
okCk = expectedCk
repaired = False
for copyNb in range(nbDisks):
    # nothing to do for non currupted one
    if copyNb == okDisk: continue
    repairNeeded = False
    # open and get content of current copy
    try:
        content = open('Disk%d/%s' % (copyNb, fileName)).read()
    except IOError:
        # file not found : this copy was lost
        repairNeeded = True
        pb = 'was lost'
    # get checksum of current copy
    try:
        expectedCk = open('Disk%d/.%s.cksum' % (copyNb, fileName)).read()
    except IOError:
        # file not found : this copy was lost
        repairNeeded = True
        pb = 'had no checksum'
    # check copy checksum
    computedCk = hashlib.md5(content).hexdigest()
    if expectedCk != computedCk:
        repairNeeded = True
        pb = 'was corrupted'
    # we need repair !
    if repairNeeded:
        print 'Repairing copy %d (%s)' % (copyNb, pb)
        try:
            os.remove('Disk%d/%s' % (copyNb, fileName))
        except OSError:
            # file already missing
            pass
        try:
            os.remove('Disk%d/.%s.cksum' % (copyNb, fileName))
        except OSError:
            # file already missing
            pass
        g = open('Disk%d/%s' % (copyNb, fileName), 'w')
        g.write(okContent)
        g.close()
        g = open('Disk%d/.%s.cksum' % (copyNb, fileName), 'w')
        g.write(okCk)
        g.close()
        repaired = True

if not repaired:
    print 'Nothing to be repaired, file is sane'
