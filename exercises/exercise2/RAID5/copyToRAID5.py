#!/usr/bin/python
import sys, os, hashlib

def usage(msg=None):
    if msg:
        print msg
    print "%s <fileName>" % (sys.argv[0])
    sys.exit(1)

if len(sys.argv) != 2:
    usage('Wrong nb of arguments')
fileName = sys.argv[1]

# find number of disks in RAID system
try:
    nbDisks = int(open('.nbDisks').read())
    nbStripes = nbDisks - 1
except IOError, e:
    print "No RAID system found. Did you use createRAID0 ?"
    sys.exit(-1)
except ValueError, e:
    print "Invalid RAID5 configuration in '.nbDisks' file, please correct"
    sys.exit(-1)

# compute stripe size
try:
    fileSize = os.path.getsize(fileName)
    stripeSize = (fileSize+nbStripes-1) / nbStripes
except OSError:
    print "File %s does not exist" % fileName
    sys.exit(-1)

# create the stripes and compute the parity
parity = [0 for i in range(stripeSize)]
f = open(fileName, 'r')
for stripeNb in range(nbStripes):
    buf = f.read(stripeSize)
    ck = hashlib.md5(buf).hexdigest()
    g = open('Disk%d/%s.%d' % (stripeNb, os.path.basename(fileName), stripeNb), 'w')
    g.write(buf)
    g.close()
    g = open('Disk%d/.%s.%d.cksum' % (stripeNb, os.path.basename(fileName), stripeNb), 'w')
    g.write(ck)
    g.close()
    buf += '\0'*(stripeSize-len(buf))
    parity = [a ^ ord(b) for a,b in zip(parity,buf)]
f.close()

# create a file containing the length of the file
# this is needed for repair as length cannot be guessed in case the
# last chunk is lost/corrupted
g = open('.%s.size' % (os.path.basename(fileName)), 'w')
g.write(str(fileSize))
g.close()

# create the parity
parity = ''.join([chr(a) for a in parity]) 
g = open('Disk%d/%s.%d' % (nbDisks-1, os.path.basename(fileName), nbDisks-1), 'w')
g.write(parity)
g.close()
ck = hashlib.md5(parity).hexdigest()
g = open('Disk%d/.%s.%d.cksum' % (nbDisks-1, os.path.basename(fileName), nbDisks-1), 'w')
g.write(ck)
g.close()
