#!/usr/bin/python
import sys, os, hashlib

def usage(msg=None):
    if msg:
        print msg
    print "%s <fileName> <destFileName>" % (sys.argv[0])
    sys.exit(1)

if len(sys.argv) != 3:
    usage('Wrong nb of arguments')
fileName = sys.argv[1]
dstFileName = sys.argv[2]

# find number of disks in RAID system
try:
    nbDisks = int(open('.nbDisks').read())
except IOError, e:
    print "No RAID system found. Did you use createRAID0 ?"
    sys.exit(-1)
except ValueError, e:
    print "Invalid RAID5 configuration in '.nbDisks' file, please correct"
    sys.exit(-1)

# Go through the disks and try to read from all disk but parity one
missingOrCorrupted = []
chunks = {}
for chunkNb in range(nbDisks-1):
    # open and get content of current chunk
    try:
        content = open('Disk%d/%s.%d' % (chunkNb, fileName, chunkNb)).read()
    except IOError:
        # file not found : this chunk was lost
        print 'Note that chunk on disk %d has been lost !' % chunkNb
        print 'You should run repairFile !\n'
        missingOrCorrupted.append(chunkNb)
        continue
    # get associated checksum
    try:
        expectedCk = open('Disk%d/.%s.%d.cksum' % (chunkNb, fileName, chunkNb)).read()
    except IOError:
        # file not found : the checksum was lost
        print 'Note that chunk on disk %d has no checksum !' % chunkNb
        print 'You should run repairFile !\n'
        missingOrCorrupted.append(chunkNb)
        continue
    # verify checksum
    computedCk = hashlib.md5(content).hexdigest()
    if expectedCk != computedCk:
        # invalid chunk, forget it !
        print 'Note that chunk on disk %d is corrupted !' % chunkNb
        print 'You should run repairFile !\n'
        missingOrCorrupted.append(chunkNb)
        continue
    # valid chunk, remember content
    chunks[chunkNb] = content

# are we in a desesperate situation ?
if len(missingOrCorrupted) > 1:
    print 'Was unable to retrieve file... I fear I lost your data...'
    print '  there were too many missing/corrupted chunks !'
    sys.exit(-1)

# do we need to use parity ?
if missingOrCorrupted:
    # read parity chunk
    parity = open('Disk%d/%s.%d' % (nbDisks-1, fileName, nbDisks-1)).read()
    expectedCk = open('Disk%d/.%s.%d.cksum' % (nbDisks-1, fileName, nbDisks-1)).read()
    # check its checksum
    computedCk = hashlib.md5(parity).hexdigest()
    if expectedCk != computedCk:
        # invalid chunk, forget it !
        print 'Note that parity chunk on disk %d is corrupted !\n' % (nbDisks-1)
        print 'Was unable to retrieve file... I fear I lost your data...'
        print '  there were too many missing/corrupted chunks !'
        sys.exit(-1)
    # recompute missing chunk
    stripeSize = len(parity)
    missingChunk = [ord(c) for c in parity]
    for k in chunks:
        buf = chunks[k]
        buf += '\0'*(stripeSize-len(buf))
        missingChunk = [a ^ ord(b) for a,b in zip(missingChunk, buf)]
    # in case it's the last chunk, cut it to proper size using the filesize
    # that is stored in special file
    if (missingOrCorrupted[0] == nbDisks-2):
        try:
            g = open('.%s.size' % (fileName))
            fileSize = int(g.read())
            chunkSize = fileSize % stripeSize
            g.close()
            missingChunk = missingChunk[:chunkSize]
        except:
            print 'Was unable to find file size... The file may thus have extra 0 bytes at the end of the file'
    chunks[missingOrCorrupted[0]] = ''.join([chr(a) for a in missingChunk]) 

# write file
f = open(dstFileName, 'w')
for i in range(nbDisks-1):
    f.write(chunks[i])
f.close()
print 'File succesfully retrieved'
