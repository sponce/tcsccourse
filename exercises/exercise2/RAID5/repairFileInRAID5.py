#!/usr/bin/python
import sys, os, hashlib

def usage(msg=None):
    if msg:
        print msg
    print "%s <fileName>" % (sys.argv[0])
    sys.exit(1)

if len(sys.argv) != 2:
    usage('Wrong nb of arguments')
fileName = sys.argv[1]

# find number of disks in RAID system
try:
    nbDisks = int(open('.nbDisks').read())
except IOError, e:
    print "No RAID system found. Did you use createRAID0 ?"
    sys.exit(-1)
except ValueError, e:
    print "Invalid RAID1 configuration in '.nbDisks' file, please correct"
    sys.exit(-1)

# Go through the disks and try to read eveything + checksums
missingOrCorrupted = []
chunks = {}
for chunkNb in range(nbDisks):
    # open and get content of current chunk
    try:
        content = open('Disk%d/%s.%d' % (chunkNb, fileName, chunkNb)).read()
    except IOError:
        # file not found : this chunk was lost
        pb = 'was lost'
        missingOrCorrupted.append(chunkNb)
        continue
    # get associated checksum
    try:
        expectedCk = open('Disk%d/.%s.%d.cksum' % (chunkNb, fileName, chunkNb)).read()
    except IOError:
        # file not found : the checksum was lost. We suppose it's corrupted
        pb = 'had no checksum'
        missingOrCorrupted.append(chunkNb)
        continue
    # verify checksum
    computedCk = hashlib.md5(content).hexdigest()
    if expectedCk != computedCk:
        # corrupted chunk
        pb = 'was corrupted'
        missingOrCorrupted.append(chunkNb)
        continue
    # valid chunk, remember content
    chunks[chunkNb] = content

# are we in a desesperate situation ?
if len(missingOrCorrupted) > 1:
    print 'Was unable to repair file... I fear I lost your data...'
    print '  there were too many missing/corrupted chunks !'
    sys.exit(-1)

# do we have anything to repair ?
if len(missingOrCorrupted) == 0:
    print 'Nothing to be repaired, file is sane'
else:
    # the missing chunk is simply a xor of the other ones
    stripeSize = max([len(chunk) for chunk in chunks.values()])
    missingChunk = [0 for i in range(stripeSize)]
    for k in chunks:
        buf = chunks[k]
        buf += '\0'*(stripeSize-len(buf))
        missingChunk = [a ^ ord(b) for a,b in zip(missingChunk, buf)]
    # in case it's the last chunk, cut it to proper size using the filesize
    # that is stored in special file
    if (missingOrCorrupted[0] == nbDisks-2):
        try:
            g = open('.%s.size' % (fileName))
            fileSize = int(g.read())
            chunkSize = fileSize % stripeSize
            g.close()
            missingChunk = missingChunk[:chunkSize]
        except:
            print 'Was unable to find file size... The repair may thus have added 0 bytes at the end of the file'
    missingChunk = ''.join([chr(a) for a in missingChunk]) 
    # repair
    g = open('Disk%d/%s.%d' % (missingOrCorrupted[0], fileName, missingOrCorrupted[0]), 'w')
    g.write(missingChunk)
    g.close()
    g = open('Disk%d/.%s.%d.cksum' % (missingOrCorrupted[0], fileName, missingOrCorrupted[0]), 'w')
    g.write(hashlib.md5(missingChunk).hexdigest())
    g.close()
    print 'Repaired chunk %d (%s)' % (missingOrCorrupted[0], pb)

