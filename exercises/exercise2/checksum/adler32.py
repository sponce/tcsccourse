#!/usr/bin/python
import sys, zlib, timeit


def usage(msg=None):
    if msg:
        print msg
    print "%s <fileName>" % (sys.argv[0])
    sys.exit(1)

if len(sys.argv) != 2:
    usage('Wrong nb of arguments')

# read file
try:
    f = open(sys.argv[1], "rb")
    buf = f.read()
    f.close()
except IOError:
    print "File %s does not exist" % sys.argv[1]
    sys.exit(1)

# compute checksum
start_time = timeit.default_timer()
ck = zlib.adler32(buf)
elapsed = timeit.default_timer() - start_time

# print output
if ck < 0:
     ck += 2**32;
print hex(ck)[2:10].zfill(8)
print 'Took %fs' % elapsed
