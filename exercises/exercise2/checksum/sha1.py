#!/usr/bin/python
import sys, hashlib, timeit

def usage(msg=None):
    if msg:
        print msg
    print "%s <fileName>" % (sys.argv[0])
    sys.exit(1)

if len(sys.argv) != 2:
    usage('Wrong nb of arguments')

# read file
try:
    f = open(sys.argv[1], "rb")
    buf = f.read()
    f.close()
except IOError:
    print "File %s does not exist" % sys.argv[1]
    sys.exit(1)

# compute checksum
start_time = timeit.default_timer()
ck = hashlib.sha1(buf)
elapsed = timeit.default_timer() - start_time

# print output
print ck.hexdigest()
print 'Took %fs' % elapsed
