#!/usr/bin/python
import sys, timeit

def usage(msg=None):
    if msg:
        print msg
    print "%s <fileName>" % (sys.argv[0])
    sys.exit(1)

if len(sys.argv) != 2:
    usage('Wrong nb of arguments')

# read file
try:
    buf = open(sys.argv[1], "rb").read()
except IOError:
    print "File %s does not exist" % sys.argv[1]
    sys.exit(1)

def getint(n):
    return ord(buf[n]) | (ord(buf[n+1]) << 8) | (ord(buf[n+2]) << 16) | (ord(buf[n+3]) << 24)
    
# compute checksum
start_time = timeit.default_timer()
ck = 0
for n in range(0, len(buf), 4):
    ck ^= getint(n)
elapsed = timeit.default_timer() - start_time

# print output
print hex(ck)[2:].zfill(8)
print 'Took %fs' % elapsed
