#!/usr/bin/python
import sys, os, hashlib, zfec

def usage(msg=None):
    if msg:
        print msg
    print "%s <fileName>" % (sys.argv[0])
    sys.exit(1)

if len(sys.argv) != 2:
    usage('Wrong nb of arguments')
fileName = sys.argv[1]

# find the configuration of the erasure pool
try:
    nbDataChunks = int(open('.nbDataChunks').read())
    nbExtraChunks = int(open('.nbExtraChunks').read())
    nbDisks = nbDataChunks + nbExtraChunks
except IOError, e:
    print "No erasure pool found. Did you use createErasure ?"
    sys.exit(-1)
except ValueError, e:
    print "Invalid erasure configuration in '.nbDataChunks' or '.nbExtraChunks' file, please correct"
    sys.exit(-1)

# compute chunk size
try:
    fileSize = os.path.getsize(fileName)
    chunkSize = (fileSize+nbDataChunks-1) / nbDataChunks
except OSError:
    print "File %s does not exist" % fileName
    sys.exit(-1)

# create the data chunks
f = open(fileName, 'r')
dataChunks = []
for chunkNb in range(nbDataChunks):
    buf = f.read(chunkSize)
    buf += '\0'*(chunkSize-len(buf))
    dataChunks.append(buf)
f.close()

# compute extra chunks using zfec
chunks = zfec.Encoder(nbDataChunks, nbDisks).encode(dataChunks)

# create files on disks
for chunkNb in range(nbDisks):
    ck = hashlib.md5(chunks[chunkNb]).hexdigest()
    g = open('Disk%d/%s.%d' % (chunkNb, os.path.basename(fileName), chunkNb), 'w')
    g.write(chunks[chunkNb])
    g.close()
    g = open('Disk%d/.%s.%d.cksum' % (chunkNb, os.path.basename(fileName), chunkNb), 'w')
    g.write(ck)
    g.close()

# create a file containing the length of the file
# this is needed for repair as length cannot be guessed in case the
# last chunk is lost/corrupted
g = open('.%s.size' % (os.path.basename(fileName)), 'w')
g.write(str(fileSize))
g.close()
