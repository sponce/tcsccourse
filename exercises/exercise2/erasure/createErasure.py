#!/usr/bin/python
import sys, os

def usage(msg=None):
    if msg:
        print msg
    print "%s <nbDataChunks> <nbExtraChunks>" % (sys.argv[0])
    sys.exit(1)

if len(sys.argv) != 3:
    usage('Wrong nb of arguments')

try:
    nbDataChunks = int(sys.argv[1])
    if nbDataChunks < 2: raise ValueError
except ValueError:
    usage('Invalid nb of data chunks : "%s"' % sys.argv[1])

try:
    nbExtraChunks = int(sys.argv[2])
    if nbExtraChunks < 2: raise ValueError
except ValueError:
    usage('Invalid nb of extra chunks : "%s"' % sys.argv[2])

# check presence of .nbDataChunks file
if os.path.isfile(".nbDataChunks"):
    print "it seems that a erasure pool already exists, giving up"
    print "you may drop it be running 'rm -rf Disk* .nb*'"
    sys.exit(-1)
    
# create the "Disks", that is subdirectories
try:
    for i in range(nbDataChunks+nbExtraChunks):
        os.mkdir('Disk%d'%i)
except OSError, e:
    print "unable to create erasure pool. Got following error :"
    print e

# create '.nbDataChunks' and '.nbExtraChunks' files to remember setup
open(".nbDataChunks", "w").write(str(nbDataChunks))
open(".nbExtraChunks", "w").write(str(nbExtraChunks))
