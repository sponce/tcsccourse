#!/usr/bin/python
import sys, os, hashlib, zfec

def usage(msg=None):
    if msg:
        print msg
    print "%s <fileName> <destFileName>" % (sys.argv[0])
    sys.exit(1)

if len(sys.argv) != 3:
    usage('Wrong nb of arguments')
fileName = sys.argv[1]
dstFileName = sys.argv[2]

# find the configuration of the erasure pool
try:
    nbDataChunks = int(open('.nbDataChunks').read())
    nbExtraChunks = int(open('.nbExtraChunks').read())
    nbDisks = nbDataChunks + nbExtraChunks
except IOError, e:
    print "No erasure pool found. Did you use createErasure ?"
    sys.exit(-1)
except ValueError, e:
    print "Invalid erasure configuration in '.nbDataChunks' or '.nbExtraChunks' file, please correct"
    sys.exit(-1)

# Go through the disks and try to read from all disks until we have enough
chunks = []
chunkNbs = []
for chunkNb in range(nbDisks):
    # open and get content of current chunk
    try:
        content = open('Disk%d/%s.%d' % (chunkNb, fileName, chunkNb)).read()
    except IOError:
        # file not found : this chunk was lost
        print 'Note that chunk on disk %d has been lost !' % chunkNb
        print 'You should run repairFile !\n'
        continue
    # get associated checksum
    try:
        expectedCk = open('Disk%d/.%s.%d.cksum' % (chunkNb, fileName, chunkNb)).read()
    except IOError:
        # file not found : the checksum was lost
        print 'Note that chunk on disk %d has no checksum !' % chunkNb
        print 'You should run repairFile !\n'
        continue
    # verify checksum
    computedCk = hashlib.md5(content).hexdigest()
    if expectedCk != computedCk:
        # invalid chunk, forget it !
        print 'Note that chunk on disk %d is corrupted !' % chunkNb
        print 'You should run repairFile !\n'
        continue
    # valid chunk, remember content
    chunks.append(content)
    chunkNbs.append(chunkNb)
    if len(chunks) >= nbDataChunks:
        break

# are we in a desesperate situation ?
if len(chunks) < nbDataChunks:
    print 'Was unable to retrieve file... I fear I lost your data...'
    print '  there were too many missing/corrupted chunks !'
    sys.exit(-1)

# extract data chunks from what we got
# use zfec for that, even is we already have them (case of no
# erasure, no corruptions), the zfec library will be clever
# enough to not do anything in that case
dataChunks = zfec.Decoder(nbDataChunks, nbDisks).decode(chunks, chunkNbs)

# cut the last chunk to proper size using the filesize
# that is stored in special file
try:
    g = open('.%s.size' % (fileName))
    fileSize = int(g.read())
    chunkSize = fileSize % len(chunks[0])
    g.close()
    dataChunks[-1] = dataChunks[-1][:chunkSize]
except:
    print 'Was unable to find file size... The file may thus have extra 0 bytes at the end of the file'

# write file
f = open(dstFileName, 'w')
for chunk in dataChunks:
    f.write(chunk)
f.close()
print 'File succesfully retrieved'
