#!/usr/bin/python
import sys, os, hashlib, zfec

def usage(msg=None):
    if msg:
        print msg
    print "%s <fileName>" % (sys.argv[0])
    sys.exit(1)

if len(sys.argv) != 2:
    usage('Wrong nb of arguments')
fileName = sys.argv[1]

# find the configuration of the erasure pool
try:
    nbDataChunks = int(open('.nbDataChunks').read())
    nbExtraChunks = int(open('.nbExtraChunks').read())
    nbDisks = nbDataChunks + nbExtraChunks
except IOError, e:
    print "No erasure pool found. Did you use createErasure ?"
    sys.exit(-1)
except ValueError, e:
    print "Invalid erasure configuration in '.nbDataChunks' or '.nbExtraChunks' file, please correct"
    sys.exit(-1)

# Go through the disks and try to read everything
chunks = []
chunkNbs = []
pbs = {}
for chunkNb in range(nbDisks):
    # open and get content of current chunk
    try:
        content = open('Disk%d/%s.%d' % (chunkNb, fileName, chunkNb)).read()
    except IOError:
        # file not found : this chunk was lost
        pbs[chunkNb] = 'was lost'
        continue
    # get associated checksum
    try:
        expectedCk = open('Disk%d/.%s.%d.cksum' % (chunkNb, fileName, chunkNb)).read()
    except IOError:
        # file not found : the checksum was lost
        pbs[chunkNb] = 'had no checksum'
        continue
    # verify checksum
    computedCk = hashlib.md5(content).hexdigest()
    if expectedCk != computedCk:
        # invalid chunk, forget it !
        pbs[chunkNb] = 'was corrupted'
        continue
    # valid chunk, remember content
    chunks.append(content)
    chunkNbs.append(chunkNb)

# are we in a desesperate situation ?
if len(chunks) < nbDataChunks:
    print 'Was unable to repair file... I fear I lost your data...'
    print '  there were too many missing/corrupted chunks !'
    sys.exit(-1)

# do we have anything to repair ?
if len(chunks) == nbDisks:
    print 'Nothing to be repaired, file is sane'
else:
    # we do a very stupid repair here, using decode + new encoding and
    # recreating all files on disk
    # it's very inefficient but simple and sufficient for our use case
    # decoding
    dataChunks = zfec.Decoder(nbDataChunks, nbDisks).decode(chunks[:nbDataChunks], chunkNbs[:nbDataChunks])
    # encoding
    chunks = zfec.Encoder(nbDataChunks, nbDisks).encode(dataChunks)
    # recreation of disk files
    for chunkNb in range(nbDisks):
        ck = hashlib.md5(chunks[chunkNb]).hexdigest()
        g = open('Disk%d/%s.%d' % (chunkNb, fileName, chunkNb), 'w')
        g.write(chunks[chunkNb])
        g.close()
        g = open('Disk%d/.%s.%d.cksum' % (chunkNb, fileName, chunkNb), 'w')
        g.write(ck)
        g.close()
    for chunkNb in pbs:
        print 'Repaired chunk %d (%s)' % (chunkNb, pbs[chunkNb])
