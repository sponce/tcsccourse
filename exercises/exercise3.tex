\documentclass[a4paper,12pt]{article}

\usepackage[a4paper,margin=2cm]{geometry}
\usepackage{minted}
\usepackage{hyperref}
\usepackage{framed}
\usepackage{tabularx}
\usepackage{tikz}
\usepackage{pgfplots}

\title{Exercise 3 : asynchronous I/O and caching}
\author{S\'ebastien Ponce}

\begin{document}

% for exercises output
\newdimen\longline
\longline=\textwidth\advance\longline-6cm
\def\LayoutTextField#1#2{#2}
\def\lbl#1{\hbox to 5.2cm{#1\hfill\strut}}%
\def\labelline#1#2#3{\lbl{#1}\vbox{\hbox{\TextField[bordercolor=white,name=#2,width=#3]{\null}}\kern2pt\hrule}}
\def\q#1#2{\hbox to \hsize{\labelline{#1}{#2}{\longline}}\vskip1.4ex}
\def\pq#1{\mbox{\TextField[borderwidth=0,name=#1,width=2cm,height=.5cm]{\null}}}

\def\easyin#1#2{
  \begin{framed}
    \begin{Form}
      \q{#1}{#2}
    \end{Form}
  \end{framed}
}

\maketitle

\section{Goals of the exercise}

\begin{itemize}
\item Do network measurements
\item Show synchronous I/O (in)efficiency
\item Play with asynchronous I/O
\item See client caching benefits
\end{itemize}

\section{Setup}

\subsection {introduction}

In this exercise, we will play with client-server interaction over wide area networks.
The wide area network will actually be faked : we will connect to ourselves via the loopback device and configure it to have WAN behavior by adding some large delay.

Concerning the client-server architecture, we will mainly use 2 kinds :
\begin{itemize}
\item home cooked scripts as in previous exercises to demonstrate concepts
\item the xrootd framework to go further. Xrootd is a powerful framework and tool suite based on the xrootd protocol and that can provide efficient and scalable data transfer solutions.
\end{itemize}
Depending on the exercises, we will either use the xrdcp command line tool that is able to read/write files from/to an xrootd server, or we will interact directly with the client API of xrootd, and more specifically with its python binding.

\subsection{Basics}

\begin{itemize}
\item open a terminal
\item if you did not download the exercises yet, refer to exercise1 to do it
\item go to the exercise3 directory
  \begin{minted}{shell}
    cd /data/exercises/exercise3
  \end{minted}
\item copy the datafile used for exercise1 into the local directory (or recreate it)
  \begin{minted}{shell}
    cp ../exercise1/datafile .
  \end{minted}
\end{itemize}

\section{Setting up Network}

\subsection{Latency of real WAN}

\begin{itemize}
\item use ping to measure the round trip time to the CERN data center
  \begin{minted}{shell}
    ping -c 5 lxplus.cern.ch
  \end{minted}
  \easyin{Ping Time}{e1}
  
\item try to find out what builds up this latency by using mtr
  \begin{minted}{shell}
    mtr lxplus.cern.ch
  \end{minted}
  \begin{itemize}
  \item analyze the physical path of the data
  \item see the number of hops, estimate the time spent in routers/switches
  \item use google maps to have a rough estimate of the cable length (take fastest road length, that is highways, the fibers follow these)
  \item check whether the order of magnitude fits or whether congestions in the network are predominant
  \end{itemize}
  \begin{framed}
    \begin{Form}
      \q{Main cities on the way}{q1}
      \q{Number of hops}{q2}
      \q{Estimated cable length}{q3}
      \q{Estimated Ping time}{q4}
    \end{Form}
  \end{framed}

  
\item you may be surprised that some intermediate routers have longer response time than the final machine. Take into account that this reponse time involves the ``travel'' time through the network plus the time the CPU of the router took to build the response. That router simply had an overloaded CPU, although it was transmitting packets efficiently.
\end{itemize}

\subsection{Faked network}

From now on, we will not use the WAN but fake it. The main reason being that it's not realiable, neither powerful enough.

In order to fake it, we will add delay and throughput restrictions to the local loopback device so that connecting to ourselves looks like connecting to CERN through the wide area network.

\begin{itemize}
\item create some delay of 15ms on the loopback device, with a normal distribution centered on 15ms and having 5ms of sigma
  \begin{minted}{shell}
    sudo tc qdisc add dev lo root handle 1: netem delay 15ms 5ms \
      distribution normal
  \end{minted}
\item check that latency is correct. Remember that you're using the loopback both for sending and receiving back.
  \begin{minted}{shell}
    ping -c 10 localhost
  \end{minted}
  \easyin{Measured Ping Time}{e2}
\item measure the throughput by using iftop in a separate shell
  \begin{itemize}
  \item in one shell 
    \begin{minted}[gobble=6]{shell}
      sudo iftop -i lo -B
    \end{minted}
  \item in a second shell, start a netcat server
    \begin{minted}[gobble=6]{shell}
      nc -l -p 12345 > /dev/null
    \end{minted}    
  \item in a third shell, send zeroes to the netcat server
    \begin{minted}[gobble=6]{shell}
      dd if=/dev/zero bs=4096 count=1048576 | nc localhost 12345
    \end{minted}
  \end{itemize}
  \easyin{Measured bandwidth}{e3}
\item you should get peaks of almost 80MB/s which means that you have an actual network speed of 1Gbit/s and that you get limited by the loopback device
\end{itemize}

\subsection{Setup Xroot}

\begin{itemize}
\item in a separate shell, go to exercise3 directory and launch an xrootd server serving the /data directory
  \begin{minted}{shell}
    cd /data/exercises/exercise3
    xrootd /data
  \end{minted}
\item create an empty big file to be transfered
  \begin{minted}{shell}
    dd if=/dev/zero of=bigfile bs=1024 seek=1048576 count=1
  \end{minted}
\item note by the way how the file system optimizes such 'sparse' file
  \begin{minted}{shell}
    ls -l bigfile
    du -h bigfile
    du -h --apparent-size bigfile
  \end{minted}  
  \begin{framed}
    \begin{Form}
      \q{File size on disk}{q5}
      \q{Apparent file size}{q6}
    \end{Form}
  \end{framed}
\item read the big file through the loop back device and check that network throughput is ok via the iftop (should still be running)
  \begin{minted}{shell}
    xrdcp -f -DICPParallelChunks 1 \
      root://localhost//data/exercises/exercise3/bigfile /dev/null
  \end{minted}
  \easyin{Measured bandwidth (iftop)}{e4}
\item xrdcp also gives some throughput numbers. They should match the iftop ones
  \easyin{Measured bandwidth (xroot)}{e5}
\end{itemize}

\section{synchronous I/O}

In this part, we will exercise the synchronous remote reading of the big file with different parameters.

\subsection{Manual example}

You will find server.py and readFile.py files in the exercise 3 directory. They implement a very simple (and inefficient) client-server mechanism to transfer file pieces.
  
\begin{itemize}
\item Have a look at the code. It's using http protocol and very simple URL parameters to retrieve a piece of a file. See how readFile.py accesses the file, reading one buffer at a time.
\item start the server part in a separate shell
  \begin{minted}{shell}
    cd /data/exercise3
    python server.py
  \end{minted}
\item read the file using readFile with different buffer sizes and write down the speed you get using iftop. Ctrl-C the transfers once the speed is stable, as it would take ages to complete ! In case the reading is stuck, check the server's output. Error handling is not implemented so nicely, so that code stays simple.
  \begin{minted}{shell}
    python readFile.py localhost 10000 bigfile /dev/null
    python readFile.py localhost 100000 bigfile /dev/null
    python readFile.py localhost 1000000 bigfile /dev/null
    python readFile.py localhost 10000000 bigfile /dev/null
    python readFile.py localhost 100000000 bigfile /dev/null
  \end{minted}
\renewcommand{\arraystretch}{2}
  \begin{Form}
    \begin{tabular}{|l|c|c|c|c|c|}
      \hline
      Buffer Size & 10k & 100k & 1m & 10m & 100m \\
      \hline
      Speed & \pq{q1} & \pq{q2} & \pq{q3} & \pq{q4} & \pq{q5} \\
      \hline
    \end{tabular}
  \end{Form}
\end{itemize}

\subsection{Using xrootd}

\begin{itemize}
\item if it is not already running, start an xrootd server in a separate shell
  \begin{minted}{shell}
    xrootd /data
  \end{minted}
\item exercise synchronous read with 10K buffers
  \begin{minted}{shell}    
    xrdcp -DICPParallelChunks 1 -DICPChunkSize 10000 \
      -f root://localhost//data/exercises/exercise3/bigfile \
      /dev/null
  \end{minted}
\item try other buffer sizes (100K, 1M, 10M) like with the manual example. Compare results
  \renewcommand{\arraystretch}{2}
  \begin{Form}
    \vspace{.2cm}
    \begin{tabular}{|l|c|c|c|c|c|}
      \hline
      Buffer Size & 10k & 100k & 1m & 10m & 100m \\
      \hline
      Speed & \pq{q6} & \pq{q7} & \pq{q8} & \pq{q9} & \pq{q10} \\
      \hline
    \end{tabular}
  \end{Form}
\item (optional) analyze your data : compute efficiency (remember optimal speed is ~100MB/s), then $ping~size$ from it and deduce the latency. \\ Does it match with what we set up ? Do not dream to have precise results, an order of magnitude is all you can get.
  
  \begin{Form}
    \vspace{.2cm}
    \begin{tabular}{|l|c|c|c|c|c|}
      \hline
      Buffer Size & 10k & 100k & 1m & 10m & 100m \\
      \hline
      Efficiency & \pq{q11} & \pq{q12} & \pq{q13} & \pq{q14} & \pq{q15} \\
      \hline
      Ping Size & \pq{q16} & \pq{q17} & \pq{q18} & \pq{q19} & \pq{q20} \\
      \hline
    \end{tabular}
  \end{Form}
  \easyin{Computed latency}{e6}
\end{itemize}


\section{asynchronous I/O}

\begin{itemize}
\item if it is not already running, start an xrootd server in a separate shell
  \begin{minted}{shell}
    xrootd /data
  \end{minted}
\item exercise asynchronous read with 10K buffers and different level of parallelism. Forget flying data for the moment.
  \begin{minted}{shell}    
    xrdcp -DICPParallelChunks 4 -DICPChunkSize 10000 \
      -f root://localhost//data/exercises/exercise3/bigfile \
      /dev/null
    xrdcp -DICPParallelChunks 16 -DICPChunkSize 10000 \
      -f root://localhost//data/exercises/exercise3/bigfile \
      /dev/null
    xrdcp -DICPParallelChunks 64 -DICPChunkSize 10000 \
      -f root://localhost//data/exercises/exercise3/bigfile \
      /dev/null
    xrdcp -DICPParallelChunks 255 -DICPChunkSize 10000 \
      -f root://localhost//data/exercises/exercise3/bigfile \
      /dev/null
  \end{minted}
  \renewcommand{\arraystretch}{2}
  \begin{Form}
    \begin{tabular}{|l|c|c|c|c|}
      \hline
      Parallelism & 4 & 16 & 64 & 255 \\
      \hline
      Speed & \pq{q21} & \pq{q22} & \pq{q23} & \pq{q24} \\
      \hline
      Flying Data & \pq{q25} & \pq{q26} & \pq{q27} & \pq{q28} \\
      \hline
    \end{tabular}
  \end{Form}
\item See how asynchronicity and parallelism allows to work around latency issues
\item Try again with different buffer sizes for an average level of parallelization (16)
  \begin{minted}{shell}
    xrdcp -DICPParallelChunks 16 -DICPChunkSize 1000 \
      -f root://localhost//data/exercises/exercise3/bigfile \
      /dev/null
    xrdcp -DICPParallelChunks 16 -DICPChunkSize 100000 \
      -f root://localhost//data/exercises/exercise3/bigfile \
      /dev/null
  \end{minted}
  \renewcommand{\arraystretch}{2}
  \begin{Form}
    \begin{tabular}{|l|c|c|c|}
      \hline
      Buffer Size & 1k & 10k & 100k \\
      \hline
      Speed & \pq{q29} & \pq{q30} & \pq{q31} \\
      \hline
      Flying Data & \pq{q32} & \pq{q33}  & \pq{q34} \\
      \hline
    \end{tabular}
  \end{Form}
\item do the same for 4 and 64 parallel threads
  \begin{minted}{shell}
    xrdcp -DICPParallelChunks 4 -DICPChunkSize 100000 \
      -f root://localhost//data/exercises/exercise3/bigfile \
      /dev/null
    xrdcp -DICPParallelChunks 4 -DICPChunkSize 1000000 \
      -f root://localhost//data/exercises/exercise3/bigfile \
      /dev/null
  \end{minted}
  \begin{Form}
    \begin{tabular}{|l|c|c|c|}
      \hline
      Buffer Size & 10k & 100k & 1m \\
      \hline
      Speed & \pq{q35} & \pq{q36} & \pq{q37} \\
      \hline
      Flying Data & \pq{q38} & \pq{q39} & \pq{q40} \\
      \hline
    \end{tabular}
  \end{Form}
  \begin{minted}{shell}
    xrdcp -DICPParallelChunks 64 -DICPChunkSize 1000 \
      -f root://localhost//data/exercises/exercise3/bigfile \
      /dev/null
    xrdcp -DICPParallelChunks 64 -DICPChunkSize 100000 \
      -f root://localhost//data/exercises/exercise3/bigfile \
      /dev/null
  \end{minted}
  \begin{Form}
    \begin{tabular}{|l|c|c|c|}
      \hline
      Buffer Size & 1k & 10k & 100k \\
      \hline
      Speed & \pq{q41} & \pq{q42} & \pq{q43} \\
      \hline
      Flying Data & \pq{q44} & \pq{q45} & \pq{q46} \\
      \hline
    \end{tabular}
  \end{Form}
\item (optional) compute in each case the amount of ``flying data'' (= chunkSize * nbParallelChunks) and the transfer efficiency. See that  the ``flying data'' size is more or less proportional to the achieved speed and that it is of the order of magnitude of the datasize needed for a similarly efficient synchronous transfer.

  \vspace{.2cm}
\begin{tikzpicture}
  \begin{axis}[
      xlabel=Flying Data(MB),
      ylabel=Speed (MB/s),
      xtick={0,1,2,3,4,5,6,7},
      xmin=0,
      xmax=7,
      ytick={0,20,40,60,80,100},
      ymin=0,
      ymax=100,
      width=14cm,
      height=8cm] 
  \addplot [mark=none,domain=-2:-1] {x};
  \end{axis}
\end{tikzpicture}
\end{itemize}


\section{caching [optional]}

Note that we go back to synchronous I/O here.

\begin{itemize}
\item if it is not already running, start an xrootd server in a separate shell
  \begin{minted}{shell}
    xrootd /data
  \end{minted}
\item check that datafile is in the kernel page cache. If it's not, cat it into /dev/null to make this happen
  \begin{minted}{shell}
    linux-fincore datafile
    cat datafile > /dev/null
    linux-fincore datafile
  \end{minted}
\item Have a look at the code of plotCaptor.py. It is basically the same as in exercise one, except for the file opening/reading that is using the xrootd API to open a remote file
\item start iftop in another shell in order to monitor I/O. Also add show-totals:yes into its config file so that we can see total I/O
  \begin{minted}{shell}
    echo 'show-totals : yes' > .iftoprc
    sudo iftop -i lo -B -c .iftoprc
  \end{minted}
\item launch plotCaptor.py for a given captor and check timing 
  \begin{minted}{shell}
    python plotCaptor.py \
      root://localhost//data/exercises/exercise3/datafile 40
  \end{minted}
  \easyin{Retrieval time}{e7}
\item check how much was read from network. It's in iftop, the first column on the right of 'cum' (for cumulated) at the bottom.
  \easyin{Amount of data read}{e8}
\item relaunch iftop and plotCaptor.py for a given captor and its neighbour
  \begin{itemize}
    \item in separate shell
      \begin{minted}[gobble=6]{shell}
        sudo iftop -i lo -B -c .iftoprc
      \end{minted}
    \item in main shell
      \begin{minted}[gobble=6]{shell}
        python plotCaptor.py \
          root://localhost//data/exercises/exercise3/datafile 40,41
      \end{minted}
  \end{itemize}
\item see that twice more was read from network and the effect on timing
  \begin{framed}
    \begin{Form}
      \q{Amount of data Read}{q7}
      \q{Overall Time}{q8}
    \end{Form}
  \end{framed}
\item check the implementation of plotCaptorCached.py. Actually you could look at the diff with plotCaptor.py, as it's quite limited
  \begin{minted}{shell}
    diff plotCaptor.py plotCaptorCached.py
  \end{minted}
\item use plotCaptorCached.py for a given captor and its neighbour. Relaunch iftop first to have clean totals
  \begin{itemize}
    \item in separate shell
      \begin{minted}[gobble=6]{shell}
        sudo iftop -i lo -B -c .iftoprc
      \end{minted}
    \item in main shell
      \begin{minted}[gobble=6]{shell}
        python plotCaptorCached.py \
          root://localhost//data/exercises/exercise3/datafile 40,41
      \end{minted}
  \end{itemize}
  \begin{framed}
    \begin{Form}
      \q{Overall Time}{q9}
      \q{Amount of data Read}{q10}
    \end{Form}
  \end{framed}
\item see the improvement in timing. It does not take more time than for a single captor. Understand the I/O number : what are we actually reading ?
\item do it again for 10 captors
  \begin{itemize}
    \item in separate shell
      \begin{minted}[gobble=6]{shell}
        sudo iftop -i lo -B -c .iftoprc
      \end{minted}
    \item in main shell
      \begin{minted}[gobble=6]{shell}
        python plotCaptorCached.py \
          root://localhost//data/exercises/exercise3/datafile \
          40,41,42,43,44,45,46,47,48,49
      \end{minted}
  \end{itemize}
  \begin{framed}
    \begin{Form}
      \q{Overall Time}{q11}
      \q{Amount of data Read}{q12}
    \end{Form}
  \end{framed}
\item I/O is unchanged ! (100 values are cached for each read)
\end{itemize}

\end{document}
