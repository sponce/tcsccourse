#!/usr/bin/python
import sys, struct, os, timeit
import matplotlib.pyplot as plt
from XRootD import client

# the format of the data is binary with the following structure
#   - bytes 0 to 3 : nbCaptors
#   - bytes 4 to 4+4*nbCaptors : values for the first point of each captor (floats)
#   - repeated bunches of 4*nbCaptors bytes

def usage(msg=None):
    if msg:
        print msg
    print "%s <fileURL> <captorNb>[,...]" % (sys.argv[0])
    sys.exit(1)

if len(sys.argv) != 3:
    usage('Wrong nb of arguments')

fileURL = sys.argv[1]
captors = None
try:
    captors = [int(arg) for arg in sys.argv[2].split(',')]
except ValueError:
    usage('Invalid captor nb : "%s"' % sys.argv[2])

# open data file
f = client.File()
f.open(fileURL)
status, data = f.read(size=4)
nbCaptors = struct.unpack('I', data)[0]

# check captors validity
notExistingCaptors = [captor for captor in captors if captor >= nbCaptors]
if notExistingCaptors:
    print 'WARNING : the following captors did not exist :'
    print '  %s' % str(notExistingCaptors)

# cleanup captor list : remove duplicates and sort
captors = list(set([captor for captor in captors if captor < nbCaptors]))
captors.sort()

# prepare list of values for each captor
values = {captor:list([]) for captor in captors}

# helper for data reads
def readValue(point, captor):
    status, data = f.read(offset=4+4*nbCaptors*point+4*captor, size=4)
    if not status.ok or len(data) == 0:
        raise EOFError
    return data

# read data
start_time = timeit.default_timer()
nbPoints = 0
try:
    while True:
        for captor in captors:
            values[captor].append(struct.unpack('f', readValue(nbPoints, captor))[0])
        nbPoints += 1
except EOFError:
    # end of the file
    pass
elapsed = timeit.default_timer() - start_time
print 'Data retrieval took %fs' % elapsed

# draw plot
for captor in captors:
    plt.plot(range(nbPoints), values[captor], linestyle='-', marker='x')
plt.axis([0, nbPoints, 0, 40])
plt.legend(['pt ' + str(captor) for captor in captors], loc='lower left')
plt.xlabel('Time')
plt.ylabel('Temperature')
plt.show()
