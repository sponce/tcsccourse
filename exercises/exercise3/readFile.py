#!/usr/bin/python
import sys, urllib

def usage(msg=None):
    if msg:
        print msg
    print "%s <machine> <bufLen> <srcFile> <dstFile>" % (sys.argv[0])
    sys.exit(1)

if len(sys.argv) != 5:
    usage('Wrong nb of arguments')

machine = sys.argv[1]
srcFile = sys.argv[3]
dstFile = sys.argv[4]
# parse bufLen
try:
    bufLen = int(sys.argv[2])
    if bufLen < 0:
        raise ValueError
except ValueError:
    print "Invalid bufLen '%s'" % bufLen
    sys.exit(-1)

# remote read the file using urllib
g = open(dstFile, 'w')
offset = 0
try:
    while True:
        params = urllib.urlencode({'offset': offset, 'length': bufLen, 'src': srcFile})
        f = urllib.urlopen("http://%s:8000/?%s" % (machine, params))
        data = f.read()
        if data:
            g.write(data)
            offset += len(data)
        else:
            # end of file
            break
except IOError:
    print 'Unable to connect to server ! Did you start it ?'
except Exception, e:
    print 'Unexpected error :', e
g.close()
