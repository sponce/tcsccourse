#import BaseHTTPServer
import SimpleHTTPServer, SocketServer

# extension on the BaseHTTPServer class serving out bigdatafile in chunks
class BigDataFileHandler(SimpleHTTPServer.SimpleHTTPRequestHandler):

    def do_GET(self):
        # parse URL and extract offset and length
        # path is ignored, we only serve one file !
        try:
            path, params = self.path.split('?',1)
        except ValueError:
            self.send_error(404, "Missing URL parameters")
            return None
        try:
            params = dict([p.split('=') for p in params.split('&')])
            offset = int(params['offset'])
            length = int(params['length'])
            srcFile = params['src']
        except ValueError:
            self.send_error(404, "Unable to parse URL parameters")
            return None
        
        # open file and seek to offset
        try:
            f = open(srcFile, 'rb')
            f.seek(offset)
        except IOError:
            self.send_error(404, "File bigdatafile not found. Server started in wrong directory ?")
            return None

        # build response
        self.send_response(200)
        self.send_header("Content-type", 'text/plain')
        buf = f.read(length)
        self.send_header("Content-Length", str(len(buf)))
        self.end_headers()
        self.wfile.write(buf)
        f.close()

# start server
httpd = SocketServer.TCPServer(("", 8000), BigDataFileHandler)
print("serving at port", 8000)
httpd.serve_forever()
