\documentclass[a4paper,12pt]{article}

\usepackage[a4paper,margin=2cm]{geometry}
\usepackage{minted}
\usepackage{hyperref}
\usepackage{framed}
\usepackage{tabularx}
\usepackage{tikz}
\usepackage{pgfplots}
\usepackage{tcolorbox}

\newif\haveAnswers

\title{optimization of large code base}
\author{S\'ebastien Ponce}

\begin{document}

\newcolumntype{Y}{>{\centering\arraybackslash}X}

% for exercises output
\newsavebox\mybox
\newlength\mylen
\newlength\fieldlen
\def\LayoutTextField#1#2{#2}

% invert comments to have results filled in the form
\newcommand\FilledTextField[3]{%
  #3
  %\TextField[#1]{#2}
}

\newcommand\easyin[2]{%
  \begin{framed}
    \begin{Form}
      \sbox\mybox{#1}
      \settowidth\mylen{\usebox\mybox}
      \setlength\fieldlen{\linewidth}
      \addtolength\fieldlen{-\mylen}
      \addtolength\fieldlen{-5em}
      \usebox\mybox \hspace{1em}-\hspace{1em}
      \FilledTextField{bordercolor=white,name=#1,width=\fieldlen}{#1}{#2}
    \end{Form}
  \end{framed}
}

\def\pq#1#2{\mbox{\FilledTextField{borderwidth=0,name=#1,width=3cm,height=.5cm}{#1}{#2}}}

\maketitle

\section{Foreword}

In this exercise, we will play with (a small subset of) the LHCb first level trigger code.

The code is reading real LHCb raw data and executing the first phase of the reconstruction, that is the tracking inside the Velo (Vertex Locator) subdetector. The code is (almost) the original code that was there before a huge effort was started in LHCb to optimize and speed up the software in view of the run 3 upgrade.

There are a lot of improvements that can be achieved in this code, and the goal of the exercise is to help you to find some of them, improve the code and measure the gains. In a second step, you will try to run the code in a multi-threaded mode. You will first need to make some fixes on the non-reentrant parts of the code, before you can try to optimize the throughput.

The main tool that we will use is valgrind, the open source suite of tools dedicated at debugging and profiling. We will in particular use callgrind but also the less known helgrind. Finally we'll have a quick view of mutrace, a useful little tool to debug thread contentions.

For this exercise we will mainly be using the physical machines at CERN. This is especially important for the threading part of the exercise where having a good number of cores helps. The exercise is using the linux command line and the many tools provided in a standard linux distribution. For the ones you do not know, please open the man page and read at least the description. Also feel free to ask any questions.

We will also use your local machine for graphical tools. This will require to install some software. It is recommended to work under linux for that, but other operating systems should also be usable if you cannot. Just choose linux if you have a choice.

\section{Goals of the exercise}

\begin{itemize}
  \item measure behavior of a ``large'' application
  \item detect and solve inefficiencies
    \begin{itemize}
    \item unadapted data structures
    \item non rentrant code
    \item thread contention
    \end{itemize}
  \item learn how to use several useful opensource tools
    \begin{itemize}
    \item callgrind
    \item helgrind
    \item kcachegrind
    \item htop
    \item mutrace
    \end{itemize}
\end{itemize}

\section{Setup}

Note that the instructions of this document are linux oriented. In case you're using a different operating system on your local machine, you'll need to adapt some parts. However you should be able to work on any platform.

If not under linux, you may actually skip the sshfs configuration part. You will then have to deal with file transfers by hand and log to lxplus manually before attempting to log to the CERN servers.

\subsection{setup of remote machine}

Check which username and machine were assigned to you.
These machines have with 2 CPUs and 10 physical/20 logical cores per CPU.
%They use NUMA (Non Uniform Memory Access), so in
In order to simplify measurements, each of you has been assignated a given CPU
%numa domain
within his/her machine.
%Practically this means you will use a single CPU and the 20 associated logical cores.
This also avoid interferences with the student working on the other CPU.

Let's now log to provided machines at CERN and set them up.

\begin{itemize}
\item open a terminal and login with ssh
  \begin{minted}{shell}
    ssh userName@itrac<nnnn>.cern.ch
  \end{minted}
\item make sure you're using only your
  CPU. Replace 0-19 by 20-39 if you're using CPU 1
  %NUMA domain. Here replace twice 'x' with 0 or 1 depending on the domain allocated to you
%    numactl -N x -m x $SHELL
  \begin{minted}{shell}
    numactl -C 0-19 $SHELL
  \end{minted}
  Note that you should use this command in any new shell that you would open on the machine
\item create a workspace in /tmp and clone the course's repository there
  \begin{minted}{shell}
    cd /tmp
    mkdir -p <username>; cd <username>
    git clone https://gitlab.cern.ch/sponce/tcsccourse.git
  \end{minted}
\item setup the environment to use a recent gcc compiler, here 13.1.0 and a recent valgrind, here 3.22.0
  \begin{minted}{shell}
    source /cvmfs/sft.cern.ch/lcg/views/LCG_105b/x86_64-el9-gcc13-opt/setup.sh
  \end{minted}
\item go to exercise4 and prepare configure using cmake
  \begin{minted}{shell}
    cd tcsccourse/exercises/exercise4
    mkdir build
    cd build
    cmake -DCMAKE_BUILD_TYPE=Release ..
  \end{minted}
\item open another shell on the same machine and run htop inside it
  \begin{minted}{shell}
    ssh userName@itrac<nnnn>.cern.ch
    htop
  \end{minted}
  \mintinline{shell}{htop} tells you which cores are running something. You should see the cores appearing in 4 columns of 10.
%  First column corresponds to physical cores in CPU 0, second to physical cores in CPU 1, and third and fourth to the logical cores of CPU 0 and 1 respectively. You can this way see the activity on the machine, including from the other student on the same node.
  First column corresponds to physical cores in CPU 0, second to logical cores in CPU 0. Third and fourth columns are the physical and logical cores of CPU 1. You can this way see the activity on the machine, including from the other student on the same node.
\item compile using all cores in original shell, letting htop run
  \begin{minted}{shell}
    make -j 24
  \end{minted}
  You should see the columns corresponding to your CPU
  % domain
  going quite busy. Check these are the expected ones and you did not put the wrong CPU.
  %NUMA domain.
\item prepare a reasonably big input file from the one in the git repository
  \begin{minted}{shell}
    touch LHCbbig.mdf
    for i in `seq 10`; do cat ../LHCbEvents.mdf >> LHCbbig.mdf; done
  \end{minted}
\item run the code and time the original status. Note that you are running single threaded so a single core will get busy, although the core used may fluctuate over time
  \begin{minted}{shell}
    time ./FakeLHCb LHCbbig.mdf ../geoData.bin
  \end{minted}
\end{itemize}

\easyin{Original duration}{21.28s user 0.19s system 99\% cpu 21.569 total}

\subsection{share files with your local machine}

This last step allows to mount the remote machine files on your local machine so that they appear to be local.

\begin{itemize}
\item on your local machine, open a terminal
\item create the same directory\footnote{in case or impossibility, you can use any directory but you will have to use {\ttfamily sed} or any find/replace tool on the callgrind output files later and change all paths from the ones on the CERN machine to the ones on your local machine\label{footnote_1}} in /tmp as you did on the CERN server and mount it via sshfs

  \begin{minted}{shell}
    cd /tmp
    mkdir -p <username>
    sshfs <username>@itrac<nnnn>.cern.ch:/tmp/<username> <username>
  \end{minted}

\item you should now see your remote files as if they were local
  
  \begin{minted}{shell}
    ls <username>
  \end{minted}

\end{itemize}


\section{General exploration with callgrind}

\subsection{Running \mintinline{shell}{callgrind}}

Callgrind allows to record the execution of a program and build statistics on where instructions were spent, in terms of functions and lines of code. By default, it only deals with functions, but the \mintinline{shell}{--dump-instr=yes} allows to also have details per line of code. Last but not least, we will recompile in debug mode so that functions names are readable.

One of the ``features'' of the valgrind family is that it will slow down the execution by a factor 20 typically. So it's a good idea to reduce the amount of events we run under callgrind. The good news is that the mdf format of LHCb allows to just cut the input file blindly :-)
\begin{minted}{shell}
  cmake -DCMAKE_BUILD_TYPE=Debug ..
  make -j 24
  head -c 5000000 ../LHCbEvents.mdf > LHCbsmall.mdf
  valgrind --tool=callgrind --dump-instr=yes ./FakeLHCb LHCbsmall.mdf ../geoData.bin
\end{minted}

After some 73 events, you should get an output file in the current directory called callgrind.out.xxxx where xxxx is the process id. This file is human readable but you want to use \mintinline{shell}{q/kcachegrind} for nice visualization of the data. \mintinline{shell}{qcachegrind} is the QT based gui, while \mintinline{shell}{kcachegrind} uses KDE libraries for the same look and feel. In the rest of the document, I'll use kcachegrind by default although both are working the same.

\subsection{installing \mintinline{shell}{kcachegrind}}

\mintinline{shell}{kcachegrind} is a graphical tool. In order for it to be responsive despite the network latency with CERN, we will run it locally, so you'll have to install it. There are mainly 2 possibilities :
\begin{itemize}
\item for linux users, just use your native package manager (yum/apt/dnf/...) and install either qcachegrind or kcachegrind depending on what is available.
\item for others, miniconda is the way to go :
  \begin{itemize}
  \item go to \url{https://docs.conda.io/en/latest/miniconda.html}
  \item download the suitable installer and run it, answer yes when needed
  \item install qcachegrind using conda :
\begin{minted}{shell}
  ~/miniconda3/bin/conda install qcachegrind
\end{minted}
  \end{itemize}
\end{itemize}

\subsection{Exploring callgrind's output}

Open a new terminal on your local machine and launch kcachegrind on the callgrind output file. Remember it's mounted on your local /tmp thanks to sshfs.

\begin{minted}{shell}
  cd /tmp/username
  kcachegrind tcsccourse/exercises/exercise4/build/callgrind.out.*
\end{minted}

\begin{tcolorbox}[coltitle=black,colframe=gray!50!white,title=Note in case you do not use sshfs]

  In case you could not mount the remote machine files via sshfs, here are some more instructions to get the callgrind output file locally.

  Create the same subdirectory as on the CERN server and clone the git repository of the exercise into it, as you did on the server. Again it is important to have the same path on the local machine and on the server or you won't be able to look at the source code in kcachegrind. If not possible, see note~\ref{footnote_1} on page~\pageref{footnote_1}.

\begin{minted}{shell}
  cd /tmp
  mkdir <username>; cd <username>
  git clone https://gitlab.cern.ch/sponce/tcsccourse.git
\end{minted}

Then get the output file of callgrind from the CERN machine and open kcachegrind locally (not necessarily from the command line).

\begin{minted}{shell}
  scp userName@tcsc-2023-<nn>.cern.ch:/tmp/<username>/
      tcsccourse/exercises/exercise4/build/callgrind.out.* .
  kcachegrind callgrind.out.*
\end{minted}

\end{tcolorbox}

The kcachegrind environment is quite complex. Let's first setup a couple of things :
\begin{itemize}
\item check that ``Instruction Fetch'' is selected in the top bar, on the most right drop-down menu. This decides which type of measurement you want to look at. Note that we are not measuring time here, but number of instructions. In some cases, it can make a difference.
\item check that the ``Relative'', or the ``\%'' button is enabled on the left, still in the top bar. This allows to show percentages relative to the parent instead of raw numbers of instructions
\item select the ``Shorten Templates'' button if the top bar, this will make templated function names more readable
\item make sure functions are listed in ``Incl.'' order in the left pane by clicking on the ``Incl.'' column name. Then get to the top of the list and select the top function (should be ``clone'' or ``start\_thread''). This pane shows where the instructions were spent in terms of functions and the selected item is the baseline for the 2 panes on the right. ``Incl.'' means that it is showing the number of instructions spent in the given function and all the ones called from it. The second column ``Self'' allows to see the instructions spend purely in a given function, excluding the instructions spent in callees
\item select the ``Callee Map'' tab in top right pane. This shows a graphical view of all functions called. Each function is depicted by a rectangle enclosed inside the rectangle of its caller. Each rectangle has its area proportional to the number of instructions spent inside the corresponding function
\item select the ``Call Graph'' tab in the bottom right pane. This shows a graph of all function calls, with edges mentioning the number of calls and rectangles giving the percentage of instructions spent in each function. The color code matches the one of the ``Callee Map'' pane. Also the thickness of the edges is depending on the number of instructions involved in the call. Note that clicking on one function in this pane enlights it in the top right pane, at least in recent versions of the tools.
\item right click in the back of the ``Call Graph'' pane, select ``Graph'' then ``Min node cost'' and finally ``5\%''. This means that the graph will be reduced to functions that use at least 5\% of the instructions of their parent. It basically allows to concentrate on the main functions. Just try ``1\%'' to see the difference.
\end{itemize}

From all this, you should be able to get a rought idea of what the program does and where it is spending time.

\easyin{Which operator() takes most of the instructions ? }{PrPixelTracking}
\easyin{2 main subparts of it}{process/buildHitsFromRawBank and searchByPair}
\easyin{Which percentage of instructions is spent in \mintinline{cpp}{buildHitsFromRawBank} ?}{38.6 \%}


\section{Optimizing single threaded version}

\subsection{Global view}

Let's now concentrate on optimizing \mintinline{cpp}{buildHitsFromRawBank}, called in process. Double click on its box in bottom right pane (or click on it in the left pane) to select it as the base line. The graph and the ``Callee Map'' are now only showing this function (and its callers in the graph)

Check how many instructions are spent in this function by unselecting ``Relative'' or the ``\%'' button in the top bar.

 \easyin{Instructions spent in \mintinline{cpp}{buildHitsFromRawBank} initially}{1 682 260 429}

Go back to ``Relative'' mode and also click the ``Relative to parent'' or ``cross'' button next to it. Now \mintinline{cpp}{buildHitsFromRawBank} is 100\% in the graph and the callees percentages are relative to it.

Look at the calls made by \mintinline{cpp}{buildHitsFromRawBank} in the graph. Check what they all have in common (but one actually).

\easyin{Where do we spend instructions in \mintinline{cpp}{buildHitsFromRawBank} ?}{std::vector}

\subsection{\mintinline{cpp}{std::vector::push_back}}

Let's first analyze the calls to \mintinline{cpp}{std::vector::push_back}, starting with the specific case of \mintinline{cpp}{std::vector<>::push_back(LHCb::VPChannelID const&)}

\easyin{What does \mintinline{cpp}{push_back} call internally ? Why ?}{\mintinline{cpp}{_M_realloc_insert}. Because it has to reallocate the full vector each time the current allocated memory gets full}

Let's find out the corresponding lines of code. Select the ``Source Code'' tab in the top right pane. Then click once on the box of the \mintinline{cpp}{push_back} function in the bottom right pane. It should send you to the right line of code. If not, it's line 412. Take care to not double click. Clicking once shows where the function is called within the source code of the currently selected function (here \mintinline{cpp}{buildHitsFromRawBank}). Clicking twice selects \mintinline{cpp}{push_back} as the reference function, and will thus try to display its source code. But as you do not have the sources of the libstdc++, you will get an error message instead.

\begin{tcolorbox}[coltitle=black,colframe=gray!50!white,title=Good to know]
  If you're searching where a piece of code is located in a git repo, \mintinline{shell}{git grep} is your friend
  \begin{minted}{shell}
> git grep channelIDs.emplace_back
Rec/Pr/PrPixel/src/PrPixelHitManager.cpp:410: channelIDs.emplace_back();
  \end{minted}
\end{tcolorbox}

Now understand the lines of code around (lines 394 to 413 and 368). Find out where \mintinline{cpp}{ChannelIDs} is declared and propose an improvement that would avoid the repeated calls to \mintinline{cpp}{realloc_insert}. For your education, there are typically up to 100 items in ChannelIDs for a regular event and each items holds no more than 100 ids.

\easyin{What could improve the \mintinline{cpp}{push_back} speed ?}{Preallocate the vectors using \mintinline{cpp}{reserve} calls}

While you are at it, looking around the line where \mintinline{cpp}{ChannelIDs} is declared, note that \mintinline{cpp}{xFractions} and \mintinline{cpp}{yFractions} may benefit from the same kind of improvements. And \mintinline{cpp}{hitsXVec}, \mintinline{cpp}{hitsYVec}, \mintinline{cpp}{hitsZVec} or \mintinline{cpp}{hitsIDVec} as well ! Fix them all.

Try to recompile and rerun valgrind. See the improvements in kcachegrind.

\begin{minted}{shell}
  # on the physical machine
  make -j 24
  valgrind --tool=callgrind --dump-instr=yes ./FakeLHCb LHCbsmall.mdf ../geoData.bin
  # on your local machine
  cd /tmp/username
  kcachegrind tcsccourse/exercises/exercise4/build/callgrind.out.yyy
\end{minted}

\easyin{Instruction spent in \mintinline{cpp}{buildHitsFromRawBank} after fixing}{1 297 666 143}
\easyin{Gain in percentage}{23 \%}

Let's check how this translates in execution time. We need to recompile in optimized mode.

\begin{minted}{shell}
  cmake -DCMAKE_BUILD_TYPE=Release ..
  make clean
  make -j 24
  time ./FakeLHCb LHCbbig.mdf ../geoData.bin
\end{minted}

\easyin{New duration}{20.36s user 0.19s system 99\% cpu 20.648 total}
\easyin{Gain of time in percentage}{4.4 \%}

\subsection{[optional] \mintinline{cpp}{std::vector::vector and std::vector::~vector}}

Note : prefer jumping to the threaded part and come back here afterward in doubt.\\

Go back to kcachegrind and look at the new graph view for \mintinline{cpp}{buildHitsFromRawBank} to see whether you can go further. Note the calls to the copy constructor of \mintinline{cpp}{std::vector} (more than 15000 !) and the 30000 calls to the \mintinline{cpp}{std::vector} destructor. Try to identify where they are done. If you do not manage, go to the end of the source code, find the calls to \mintinline{cpp}{storeTriggerClusters} around line 538 and look around.

\easyin{Why is copy constructor of vector called there ?}{Because the method \mintinline{cpp}{storeOfflineClusters} passes argument channelIDs by value}

Analyze the usage of the vector's copies and see whether the copy can be avoided (of course it can !). Do the appropriate fix. Hint : the fix is a single char !

Rebuild and rerun callgrind

\begin{minted}{shell}
  # on the physical machine
  cmake -DCMAKE_BUILD_TYPE=Debug ..
  make -j 24
  valgrind --tool=callgrind --dump-instr=yes ./FakeLHCb LHCbsmall.mdf ../geoData.bin
  # on your local machine
  cd /tmp/username
  kcachegrind tcsccourse/exercises/exercise4/build/callgrind.out.zzz
\end{minted}

Check new number of instructions after this other fix.

\easyin{Instruction spent in \mintinline{cpp}{buildHitsFromRawBank} now}{1 124 525 705}
\easyin{Gain in percentage since beginning}{33.2 \%}

\subsection{[optional][advanced]Going even further}

This part should only be done if you've finished the rest of the exercise, including the threading part.

Let's do yet another cycle. If we look at the new graph in kcachegrind, not much remains with the 5\% cut we've applied. Let's switch to 2\% and see what can be improved.

I can see quite some meat :
\begin{itemize}
\item we've not tackle \mintinline{cpp}{pixel_idx} reservation
\item we can reduce the number of calls to reserve easily
\item some \mintinline{cpp}{push-back} can be replaced by \mintinline{cpp}{emplace_back} calls avoiding copying
\item change of data structures may be useful when you have \mintinline{cpp}{vector<vector<...>>}
\item and probably many others. Try to achieve the highest gain you can !
\end{itemize}

\section{Getting thread safe}

In this part, we will try to run our reconstruction software in multi-threaded mode. All is ready in the main file ``FakeLHCb.cpp'', we only have to change the number of threads and use something higher than 1.

Let's first double check how many cores we have on our machine. Just type lscpu and look at the output.

\begin{minted}{shell}
  lscpu
\end{minted}

\easyin{Number of logical cores on the machine ?}{40}

\easyin{Number of logical cores per CPU
  %NUMA domain
  ?}{20}

Put the number of logical cores per CPU
%domain
in the code as the number of threads to be used, rebuild and run

\begin{minted}{shell}
  vim ../FakeLHCb.cpp # change NBTHREADS to 20
  cmake -DCMAKE_BUILD_TYPE=Debug ..
  make -j 24
  ./FakeLHCb LHCbsmall.mdf ../geoData.bin
\end{minted}

\easyin{Initial behavior of multi-threaded code}{crash, seg fault}

Let's see what helgrind has to say. Helgrind is another tool of the valgrind family able to detect potential race conditions in your code. Note the potential ! It can detect race conditions even when they did not occur. This is done by building a graph of dependencies of all locks and thread unsafe statements and analysing that graph for races.

Usage is as easy as callgrind :

\begin{minted}{shell}
valgrind --tool=helgrind ./FakeLHCb LHCbsmall.mdf ../geoData.bin >& helgrind.log
\end{minted}

The output is be very verbose, hence the redirection to a file. Look at the file produced, starting from the top and read until the first mention of a race condition, something like :

\begin{minted}{shell}
==103497== Possible data race during write of size 8 at 0x7FEFFB168 by thread #3
==103497== Locks held: none
... [full stacktrace]
==103497== This conflicts with a previous write of size 8 by thread #2
==103497== Locks held: none
... [full stacktrace]
\end{minted}

\easyin{Where is our race condition (file and line) ?}{FetchDataFromFile.cpp, line 28}
\easyin{What is not thread safe ?}{reading from input file}

In this particular case, there is no other choice than putting a lock to synchronize accesses to the resource. Use a \mintinline{cpp}{std::mutex} and a \mintinline{cpp}{std::scoped_lock} to solve the issue. Recompile and run again.

\begin{minted}{shell}
  make -j 24
  ./FakeLHCb LHCbsmall.mdf ../geoData.bin
\end{minted}

Was it sufficient ? Of course not. You may replay the same game and solve the next issue, but we will rather go for a more global and more efficient solution : using constness.
\vspace{1em}

Find out the 2 main methods we are calling in a threaded context. Hint : these are the 2 methods called in \mintinline{cpp}{mainLoop} located in the top level file \mintinline{shell}{FakeLHCb.cpp}. Second hint : if you're not familiar with C$^{++}$, you may miss them as they are operators. Ask a teacher if you do not find them easily.

\easyin{2 main methods called in threaded code}{\mintinline{cpp}{FetchDataFromFile::operator()} and \mintinline{cpp}{PrPixelTracking::operator()}}

Once you found them, make them \mintinline{cpp}{const}. Do not forget to change both declaration and implementation. Recompile

\begin{minted}{shell}
  make
\end{minted}

You should get compiler errors. For once, they are welcome as they tell you where the race conditions are located.

Start with the ones in \mintinline{shell}{FetchDataFromFile.cpp}. For this file, you have already solved the thread safety problem with a lock. So you are already thread safe, however you get errors as you're changing the status of members in a const method.

Remember that \mintinline{cpp}{const} only means ``visibly const and race condition free''. We have made sure this is the case via the mutex, so we can make use of \mintinline{cpp}{mutable} for all members used in the mutexed method. Now you can compile again

\begin{minted}{shell}
  make
\end{minted}

Once \mintinline{shell}{FetchDataFromFile.cpp} compiles, look at the other errors, in \mintinline{shell}{PrPixelTracking.cpp}. The compiler should complain that you call a non const method from the \mintinline{cpp}{operator()} that is now const. This means that this method should also become const as it is called in a threaded context. Do so and repeat this process of making const the necessary methods until you find the actual race condition, typically a write access to a class member in a const method. If you are not clear why this is a race condition, ask a teacher.

\easyin{Where is the race condition in PrPixelTracking ?}{\mintinline{cpp}{m_stack} member variable}

The right way to solve this case is clearly not to add a mutex. But let's play the game of learning from our mistakes and do it anyway. So edit PrPixelTracking, and solve the race condition using \mintinline{cpp}{std::mutex} and a \mintinline{cpp}{std::scoped_lock} as was done for FetchDataFromFile.

\vspace{1em}
Recompile and see that the program runs fine now, without any crash

\begin{minted}{shell}
  make -j 24
  ./FakeLHCb LHCbsmall.mdf ../geoData.bin
\end{minted}


\section{analysing thread usage}

We will now analyze the efficiency of our multithreaded approach.
As we know our number of cores, we have an idea of the ideal speedup we could achieve.

\easyin{Ideal speedup we aim for}{x 20}

Now let's measure the actual speedup achieved. Let's switch to Release mode, rebuild and rerun

\begin{minted}{shell}
  cmake -DCMAKE_BUILD_TYPE=Release ..
  make -j 24
  time ./FakeLHCb LHCbbig.mdf ../geoData.bin
\end{minted}

\easyin{Time in multi-threaded case}{39.19s user 193.24s system 1876\% cpu 12.389 total}
\easyin{Actual Speedup}{x 1.61}

Clearly not exactly meeting our expectations...
In order to understand what is happening, look at htop while running if you did not yet.

\mintinline{shell}{htop} tells you which cores are running with a color code explaining you what was running on each of them. From the documentation, here is the color code meaning :
\begin{itemize}
    \item Blue: low priority processes
    \item Green: normal (user) processes
    \item Red: kernel time (kernel, iowait, irqs...)
    \item Orange: virt time (steal time + guest time)
\end{itemize}

So basically you want green. If you have red, this means you are spending your time in the kernel, typically switching context as the running thread on that core could not go further, being stuck on a lock. If you have black, it means the given core is not used

\easyin{How much green do you see roughly ?}{20-25 \%}

We are most probably paying our naive locking here. But which one ? Although you may already have guessed, let's use \mintinline{shell}{mutrace} to find out. \mintinline{shell}{mutrace} is able to trace the mutexes of your code and tell you how many times they were locked and for how long. Its usage is trivial and no recompilation is needed

\begin{minted}{shell}
 mutrace ./FakeLHCb LHCbbig.mdf ../geoData.bin
\end{minted}

You got a list of mutexes with full stack trace of where they are used and a summary table telling what they costed.
\vspace{1em}

\renewcommand{\arraystretch}{2}
\begin{tabularx}{15.5cm}{|X|Y|Y|}
  \hline
  Algorithm concerned & nb locks & time spent(ms) \\
  \hline
  PrPixelTracking & \pq{q1}{16352320} & \pq{q2}{7763} \\
  \hline
  FetchDataFromFile & \pq{q3}{8690} & \pq{q4}{1004} \\
  \hline
\end{tabularx}
\vspace{1em}

In case you have troubles reading the stack trace, remember that \mintinline{shell}{c++filt} can demangle the C++ symbols for you, e.g. :

\begin{minted}{shell}
  c++filt _ZNK15PrPixelTrackingclERKN4LHCb8RawEventE
  # -> PrPixelTracking::operator()(LHCb::RawEvent const&) const
\end{minted}
     
\easyin{Which lock is problematic ?}{The one in \mintinline{cpp}{PrPixelTracking}}

Now let's remove the problematic lock and solve the original race condition properly. First remove the mutex, the lock and the \mintinline{cpp}{mutable} statements you should have added. Then understand the usage of the member that creates the race condition :
\easyin{Which member is problematic ?}{\mintinline{cpp}{m_stack}}
\easyin{Where is it used ?}{only in \mintinline{cpp}{buildHitsFromRawBank}}
\easyin{What's the scope of each usage ?}{local : there is a call to clear at the begining}
Once you understood the scope, you should find an easy way to solve the race condition acting on that scope.
\easyin{What change solves the race condition ?}{make it local and drop the member}

Rebuild a last time, and see how fast it now runs

\begin{minted}{shell}
  make -j 24
  time ./FakeLHCb LHCbbig.mdf ../geoData.bin
\end{minted}

it's actually a bit too fast to measure the speedup precisely !
Let's put 10 times more data to it and rerun. Also look at htop concurrently from the other shell.

\begin{minted}{shell}
  for i in `seq 90`; do cat ../LHCbEvents.mdf >> LHCbbig.mdf; done
  time ./FakeLHCb LHCbbig.mdf ../geoData.bin
\end{minted}

\easyin{New time in multi-threaded case (10x more data)}{247.82s user 2.71s system 1873\% cpu 13.372 total}
\easyin{New Speedup (take care of the factor 10)}{x 14.98}

Still entirely satisfactory, although much better than before. We won't go further in this exercise but it already illustrates how difficult it is to make old code performant on recent hardware.

\end{document}

