#include <iostream>
#include <fstream>
#include <thread>

#include "VPDet/DeVP.h"
#include "Event/Track.h"
#include "PrPixel/PrPixelTracking.h"
#include "GaudiHive/FetchDataFromFile.h"

#define VERBOSE false
#define NBTHREADS 1

/// builds (partial) velo gemetry by hand from hard coded values in a local file
DeVP* buildDeVP(char* geoDataFile) {
  DeVP* vp = new DeVP();
  std::vector<DeVPSensor*>& sensors = vp->sensors();
  std::ifstream myFile (geoDataFile);
  if (!myFile) {
    throw std::string("Unable to open geoData.bin");
  }
  unsigned int nb;
  myFile.read((char*)&nb, sizeof(nb));
  for (unsigned int i = 0; i < nb; i++) {
    DeVPSensor* sensor = new DeVPSensor();
    double z;
    myFile.read((char*)&z, sizeof(z));
    sensor->setZ(z);
    bool isLeft;
    myFile.read((char*)&isLeft, sizeof(isLeft));
    sensor->setLeft(isLeft);
    unsigned int module;
    myFile.read((char*)&module, sizeof(module));
    sensor->setModule(module);
    unsigned int sensorNumber;
    myFile.read((char*)&sensorNumber, sizeof(sensorNumber));
    sensor->setNumber(sensorNumber);
    Gaudi::Transform3D* geo = new Gaudi::Transform3D();
    myFile.read((char*)geo, sizeof(*geo));
    sensor->setGeometry(geo);
    //std::cout << "Sensor z=" << z << ", isLeft=" << isLeft << ", module=" << module << ", number=" << sensorNumber << std::endl;
    sensors.push_back(sensor);
  }
  double pixelSize;
  myFile.read((char*)&pixelSize, sizeof(pixelSize));
  vp->sensors().front()->setPixelSize(pixelSize);
  std::array<double,VP::NSensorColumns>& xLocal = vp->sensors().front()->xLocal();
  myFile.read((char*)&xLocal, sizeof(xLocal));
  std::array<double,VP::NSensorColumns>& xPitch = vp->sensors().front()->xPitch();
  myFile.read((char*)&xPitch, sizeof(xPitch));
  myFile.close();
  return vp;
}

void freeDeVP(DeVP* vp) {
  for (auto sensor : vp->sensors()) {
    delete(sensor->geometry());
    delete(sensor);
  }
  delete vp;
}

void mainLoop(unsigned int threadNb,
              Gaudi::Hive::FetchDataFromFile* fetchData,
              PrPixelTracking* prpixel) {
  // main loop
  unsigned int evtNb = 0;
  while (true) {
    try {
      auto rawEvent = (*fetchData)();
      std::tuple<LHCb::Tracks, LHCb::VPLightClusters> tracksAndClusters = (*prpixel)(rawEvent);
      std::cout << "Thread " << threadNb << " : Nb Tracks found in Event " << evtNb << " : " << std::get<0>(tracksAndClusters).size() << "\n";
      if (VERBOSE) {
        for (auto& track : std::get<0>(tracksAndClusters)) {
          track->fillStream(std::cout);
        }
        std::cout << std::endl;
      }
      evtNb++;
    } catch (std::string& error) {
      std::cout << "Thread " << threadNb << " : " << error << std::endl;
      break;
    }
  }
}

int main(int argc, char *argv[]) {
  if (argc != 3) {
    std::cout << "Expected 2 arguments\n   usage : " << argv[0] << " <evtInputFile> <geoDataFile>" << std::endl;
    return -1;
  }
  try {
    // setup algorithms of the sequence and build geometry
    Gaudi::Hive::FetchDataFromFile fetchData{argv[1]};
    fetchData.initialize();
    DeVP* vp = buildDeVP(argv[2]);
    PrPixelTracking prpixel;
    prpixel.initialize(vp);
    // launch a pool of threads
    std::vector<std::thread> threadPool;
    for (unsigned int n = 0; n < NBTHREADS; n++) {
      threadPool.emplace_back(mainLoop, n, &fetchData, &prpixel);
    }
    for (auto& t : threadPool) {
      t.join();
    }
    // finalize and cleanup
    fetchData.finalize();
    freeDeVP(vp);
    vp = nullptr;
  } catch (std::string& error) {
    std::cout << error << std::endl;
  }
}
