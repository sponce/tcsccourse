#include <fstream>
#include "Event/RawEvent.h"
#include "GaudiKernel/StatusCode.h"

namespace Gaudi
{
  namespace Hive
  {
    class FetchDataFromFile {
    public:

      FetchDataFromFile(std::string fileName) : m_fileName(fileName) {}
      
      StatusCode initialize();
      StatusCode finalize();
      LHCb::RawEvent operator()();
      
    private:
      std::string m_fileName;
      std::ifstream m_stream;
      char* m_buffer{nullptr};
      unsigned long m_bufLen{0};
    };
  }
}
