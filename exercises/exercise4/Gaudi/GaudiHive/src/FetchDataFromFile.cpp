#include "GaudiHive/FetchDataFromFile.h"
#include "MDF/RawEventHelpers.h"
#include "MDF/MDFHeader.h"
#include <sstream>

StatusCode Gaudi::Hive::FetchDataFromFile::initialize() {
  // open input file
  m_stream.open (m_fileName, std::ios::in | std::ios::binary);
  if ( !m_stream )  {
    std::ostringstream s;
    s << "Unable to open input file " << m_fileName << ". Check it does exist";
    throw s.str();
  }
  return StatusCode::SUCCESS;
}

StatusCode Gaudi::Hive::FetchDataFromFile::finalize() {
  m_stream.close();
  if (m_buffer) {
    free(m_buffer);
  }
  return StatusCode::SUCCESS;
}

LHCb::RawEvent Gaudi::Hive::FetchDataFromFile::operator()() {
  // read header
  LHCb::MDFHeader h;
  m_stream.read((char*)&h, 5*sizeof(int));
  if ( !m_stream )  {
    throw std::string("Not able to read header. Probably reached end of input file");
  }
  // specific header
  unsigned int sh[7];
  m_stream.read((char*)&sh, 7*sizeof(int));
  if ( !m_stream )  {
    throw std::string("Not able to read specific header. Probably reached end of input file");
  }
  // check buffer is big enough, otherwise, reallocate
  int len = h.recordSize() - 12*sizeof(int);
  if (m_bufLen < len) {
    auto newBuffer = realloc(m_buffer, 2*len); // let's take quite some margin
    if (nullptr == newBuffer) {
      std::ostringstream s;
      s << "Not able to allocate enough memory : " << len << " bytes.";
      throw s.str();
    }
    m_buffer = (char*)newBuffer;
    m_bufLen = 2*len;
  }
  // read data and decrypt
  m_stream.read(m_buffer, len);
  if ( m_stream )  {
    LHCb::RawEvent event;
    LHCb::decodeRawBanks(m_buffer, m_buffer+len-1, &event, true);
    return event;
  } else {
    throw std::string("Not able to read more data. Probably reached end of input file");
  }
}
