#include "ParsersStandardMiscCommon.h"

StatusCode Gaudi::Parsers::parse( std::string& name, std::string& value, const std::string& input )
{
  Skipper skipper;
  KeyValueGrammar<IteratorT, Skipper>          g;
  KeyValueGrammar<IteratorT, Skipper>::ResultT result;
  auto iter         = input.begin();
  bool parse_result = qi::phrase_parse( iter, input.end(), g, skipper, result ) && ( iter == input.end() );
  if ( parse_result ) {
    name  = result.first;
    value = result.second;
    return StatusCode::SUCCESS;
  }
  return StatusCode::FAILURE;
}
