#ifndef VPDET_DEVP_H
#define VPDET_DEVP_H 1

// Local
#include "VPDet/DeVPSensor.h"

/** @class DeVP DeVP.h VPDet/DeVP.h
 *
 *  Detector element class for the VP as a whole.
 *
 *  @author Victor Coco Victor.Coco@cern.ch
 *  @date 20/5/2009
 */

#ifdef __INTEL_COMPILER         // Disable ICC remark
#pragma warning(disable : 177)  // variable was declared but never referenced
#endif
namespace DeVPLocation {
static const std::string& Default = "/dd/Structure/LHCb/BeforeMagnetRegion/VP";
}

class DeVP {

 public:

  /// Return the number of sensors.
  unsigned int numberSensors() const { return m_sensors.size(); }

  /// Return vector of sensors.
  std::vector<DeVPSensor*>& sensors() { return m_sensors; }

  /// Return pointer to sensor for a given channel ID.
  const DeVPSensor* sensorOfChannel(const LHCb::VPChannelID channel) const {
    return sensor(channel.sensor());
  }
  /// Return pointer to sensor for a given sensor number.
  const DeVPSensor* sensor(const unsigned int sensorNumber) const {
    return m_sensors[sensorNumber];
  }

  void* i_cast( const InterfaceID& ) const { return 0; }

  virtual std::vector<std::string> getInterfaceNames() const { return {}; }

  virtual long unsigned int refCount() const { return 0; }

 private:

  /// List of pointers to all sensors.
  std::vector<DeVPSensor*> m_sensors;

  /// Custom operator for sorting sensors by sensor number.
  struct less_SensorNumber {
    bool operator()(DeVPSensor* const& x, DeVPSensor* const& y) {
      return (x->sensorNumber() < y->sensorNumber());
    }
  };

};

#endif
