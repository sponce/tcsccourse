#ifndef VPDET_DEVPSENSOR_H
#define VPDET_DEVPSENSOR_H 1

#include <atomic>
#include <array>

// Gaudi
#include "GaudiKernel/MsgStream.h"
#include "GaudiKernel/Point3DTypes.h"
#include "GaudiKernel/Transform3DTypes.h"
#include "Kernel/VPChannelID.h"
#include "Kernel/VPConstants.h"

/** @class DeVPSensor DeVPSensor.h VPDet/DeVPSensor.h
 *
 *  Detector element class for a single VP sensor
 *  @author Victor Coco
 *  @date   2009-05-14
 */

class DeVPSensor {

 public:

  /// Return the pixel size.
  std::pair<double, double> pixelSize(const LHCb::VPChannelID channel) const;

  /// Return true if the pixel is elongated.
  bool isLong(const LHCb::VPChannelID channel) const;

  /// Determine if a local 3-d point is inside the sensor active area.
  bool isInActiveArea(const Gaudi::XYZPoint& point) const;

  /// Return the z position of the sensor in the global frame
  double z() const { return m_z; }
  void setZ(double z) { m_z = z; }

  /// Return the sensor number
  unsigned int sensorNumber() const { return m_sensorNumber; }
  void setNumber(unsigned int number) { m_sensorNumber = number; }
  /// Return the module number
  unsigned int module() const { return m_module; }
  void setModule(unsigned int module) { m_module = module; }
  /// Return station number (station comprises left and right module)
  unsigned int station() const { return m_module >> 1; }

  /// Return true for x < 0 side of the detector
  bool isRight() const { return !m_isLeft; }
  /// Return true for x > 0 side of the detector
  bool isLeft() const { return m_isLeft; }
  void setLeft(bool isLeft) { m_isLeft = isLeft; }

  /// Return sensor thickness in mm.
  double siliconThickness() const { return m_cache.thickness; }

  /// Return array of cached local x-coordinates by column
  std::array<double,VP::NSensorColumns>& xLocal() const { return m_cache.local_x; }

  /// Return array of cached x pitches by column
  std::array<double,VP::NSensorColumns>& xPitch() const { return m_cache.x_pitch; }

  void setPixelSize(double size) { m_cache.pixelSize = size; }
  const Gaudi::Transform3D* geometry() const { return m_geometry; }
  void setGeometry(Gaudi::Transform3D* geometry) { m_geometry = geometry; }
  
 private:

  Gaudi::Transform3D* m_geometry = nullptr;

  unsigned int m_sensorNumber;
  unsigned int m_module;
  bool m_isLeft;

  /// Global Z position
  double m_z;

  struct common_t {
      common_t() = default;
      /// Dimensions of the sensor active area
      double sizeX;
      double sizeY;
      double thickness;
      /// Number of chips per ladder
      unsigned int nChips;
      /// Length of chip active area
      double chipSize;
      /// Distance between two chips
      double interChipDist;
      /// Number of columns and rows
      unsigned int nCols;
      unsigned int nRows;
      /// Cell size of pixels
      double pixelSize;
      /// Cell size in column direction of elongated pixels
      double interChipPixelSize;
      /// Cache of local x-cooordinates
      std::array<double,VP::NSensorColumns> local_x;
      /// Cache of x-pitch
      std::array<double,VP::NSensorColumns> x_pitch;
      /// Cache validity, so we create it only once on startup
      std::atomic<bool> common_cache_valid { false };
  };

  static common_t m_cache;


  /// Output level flag
  bool m_debug = false;

  friend void init_cache(common_t&);
};
#endif
