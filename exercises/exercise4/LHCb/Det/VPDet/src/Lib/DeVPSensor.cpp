#include <mutex>
// Gaudi
#include "GaudiKernel/IUpdateManagerSvc.h"

// LHCb
// Det/DetDesc
#include "DetDesc/Condition.h"

// Local
#include "VPDet/DeVPSensor.h"

DeVPSensor::common_t DeVPSensor::m_cache{};

void init_cache(DeVPSensor::common_t& cache) {
    // Calculate the active area.
    cache.sizeX =
         cache.nChips * cache.chipSize +
        (cache.nChips - 1) * cache.interChipDist;
    cache.sizeY = cache.chipSize;

  for (unsigned int col = 0; col < VP::NSensorColumns; ++col) {
    // Calculate the x-coordinate of the pixel centre and the pitch.
    const double x0 =
        (col / VP::NColumns) * (cache.chipSize + cache.interChipDist);
    double x = x0 + (col % VP::NColumns + 0.5) * cache.pixelSize;
    double pitch = cache.pixelSize;
    switch (col) {
      case 256:
      case 512:
        // right of chip border
        x -= 0.5 * (cache.interChipPixelSize - cache.pixelSize);
        pitch =
            0.5 * (cache.interChipPixelSize + cache.pixelSize);
        break;
      case 255:
      case 511:
        // left of chip border
        x += 0.5 * (cache.interChipPixelSize - cache.pixelSize);
        pitch = cache.interChipPixelSize;
        break;
      case 254:
      case 510:
        // two left of chip border
        pitch =
            0.5 * (cache.interChipPixelSize + cache.pixelSize);
        break;
    }
    cache.local_x[col] = x;
    cache.x_pitch[col] = pitch;
  }

}

//==============================================================================
// Check if a local point is inside the active area of the sensor.
//==============================================================================
bool DeVPSensor::isInActiveArea(const Gaudi::XYZPoint& point) const {

  if (point.x() < 0. || point.x() > m_cache.sizeX) return false;
  if (point.y() < 0. || point.y() > m_cache.sizeY) return false;
  return true;

}

//==============================================================================
// Return the size of a pixel with given channel ID.
//==============================================================================
std::pair<double, double>
DeVPSensor::pixelSize(const LHCb::VPChannelID channel) const {

  return { isLong(channel) ? m_cache.interChipPixelSize
                           : m_cache.pixelSize,
           m_cache.pixelSize };

}

//==============================================================================
// Return true if a pixel with given channel ID is an elongated pixel.
//==============================================================================
bool DeVPSensor::isLong(const LHCb::VPChannelID channel) const {

  const unsigned int chip = channel.chip();
  const unsigned int col = channel.col();
  if ((col == 0 && chip > 0) ||
      (col == m_cache.nCols - 1 && chip < m_cache.nChips - 1)) {
    return true;
  }
  return false;

}
