
//   **************************************************************************
//   *                                                                        *
//   *                      ! ! ! A T T E N T I O N ! ! !                     *
//   *                                                                        *
//   *  This file was created automatically by GaudiObjDesc, please do not    *
//   *  delete it or edit it by hand.                                         *
//   *                                                                        *
//   *  If you want to change this file, first change the corresponding       *
//   *  xml-file and rerun the tools from GaudiObjDesc (or run make if you    *
//   *  are using it from inside a Gaudi-package).                            *
//   *                                                                        *
//   **************************************************************************

#ifndef TrackEvent_Measurement_H
#define TrackEvent_Measurement_H 1

// Include files
#include "Event/TrackTypes.h"
#include "Event/StateVector.h"
#include "Kernel/LHCbID.h"
#include "Kernel/Trajectory.h"
#include "GaudiKernel/VectorMap.h"
#include <vector>
#include <ostream>
#include <algorithm>

// Forward declarations
class DetectorElement;


namespace LHCb
{

  // Forward declarations

  /** @class Measurement Measurement.h
   *
   * Measurement is the base class for offline and online measurements.
   *
   * @author Jose Hernando, Eduardo Rodrigues
   * created Mon Apr  9 11:52:52 2018
   *
   */

  class Measurement
  {
  public:

    /// typedef for std::vector of Measurement
    typedef std::vector<Measurement*> Vector;
    typedef std::vector<const Measurement*> ConstVector;
  
  
    /// enumerator for the type of Measurement
    enum Type{ Unknown,
               VeloR,
               VeloPhi,
               VeloLiteR,
               VeloLitePhi,
               TT,
               IT,
               OT,
               Muon,
               TTLite,
               ITLite,
               VP,
               Calo,
               Origin,
               FT,
               UT=19,
               UTLite
      };
  
    /// Copy constructor
    Measurement(const LHCb::Measurement& other);
  
    /// Constructor
    Measurement(const LHCb::Measurement::Type& type,
                const LHCb::LHCbID& id,
                const DetectorElement* elem=0) : m_mtype(type),
                                                       m_z(0),
                                                       m_measure(0),
                                                       m_errMeasure(0),
                                                       m_lhcbID(id),
                                                       m_detectorElement(elem) {}
  
    /// Default Constructor
    Measurement() : m_mtype(0),
                    m_z(0.0),
                    m_measure(0.0),
                    m_errMeasure(0.0),
                    m_trajectory(),
                    m_lhcbID(),
                    m_detectorElement(0) {}
  
    /// Default Destructor
    virtual ~Measurement() {}
  
    /// conversion of string to enum for type Type
    static LHCb::Measurement::Type TypeToType (const std::string & aName);
  
    /// conversion to string for enum type Type
    static const std::string& TypeToString(int aEnum);
  
    /// Retrieve const  the measurement error squared
    double errMeasure2() const;
  
    /// Retrieve the measurement error
    virtual double resolution(const Gaudi::XYZPoint& /*point*/,
                              const Gaudi::XYZVector& /*vec*/) const;
  
    /// Retrieve the measurement error squared
    virtual double resolution2(const Gaudi::XYZPoint& /*point*/,
                               const Gaudi::XYZVector& /*vec*/) const;
  
    /// Retrieve the measurement type
    Measurement::Type type() const;
  
    /// Check the measurement type
    bool checkType(const LHCb::Measurement::Type& value) const;
  
    /// Clone the measurement
    virtual Measurement* clone() const = 0;
  
    /// Retrieve the trajectory representing the measurement
    const LHCb::Trajectory < double > & trajectory() const;
  
    /// Set the trajectory
    void setTrajectory(std::unique_ptr < LHCb::Trajectory < double > >& traj);
  
    /// Update measurement type
    void setType(const Type& value);
  
    /// Retrieve const  the z-position of the measurement
    double z() const;
  
    /// Update  the z-position of the measurement
    void setZ(double value);
  
    /// Retrieve const  the measurement value
    double measure() const;
  
    /// Update  the measurement value
    void setMeasure(double value);
  
    /// Retrieve const  the measurement error
    double errMeasure() const;
  
    /// Update  the measurement error
    void setErrMeasure(double value);
  
    /// Retrieve const  the corresponding LHCbID
    const LHCb::LHCbID& lhcbID() const;
  
    /// Update  the corresponding LHCbID
    void setLhcbID(const LHCb::LHCbID& value);
  
    /// Retrieve const  the corresponding detectorElement
    const DetectorElement* detectorElement() const;
  
  protected:

    /// Offsets of bitfield mtype
    enum mtypeBits{typeBits = 0};
  
    /// Bitmasks for bitfield mtype
    enum mtypeMasks{typeMask = 0x3fL
                   };
  
  
    unsigned int                                    m_mtype;           ///< the variety of measurement types
    double                                          m_z;               ///< the z-position of the measurement
    double                                          m_measure;         ///< the measurement value
    double                                          m_errMeasure;      ///< the measurement error
    std::unique_ptr < LHCb::Trajectory < double > > m_trajectory;      ///< the trajectory representing the measurement (owner)
    LHCb::LHCbID                                    m_lhcbID;          ///< the corresponding LHCbID
    const DetectorElement*                          m_detectorElement; ///< the corresponding detectorElement
  
  private:

    static const GaudiUtils::VectorMap<std::string,Type> & s_TypeTypMap();
  
  }; // class Measurement

  inline std::ostream & operator << (std::ostream & s, LHCb::Measurement::Type e) {
    switch (e) {
      case LHCb::Measurement::Unknown     : return s << "Unknown";
      case LHCb::Measurement::VeloR       : return s << "VeloR";
      case LHCb::Measurement::VeloPhi     : return s << "VeloPhi";
      case LHCb::Measurement::VeloLiteR   : return s << "VeloLiteR";
      case LHCb::Measurement::VeloLitePhi : return s << "VeloLitePhi";
      case LHCb::Measurement::TT          : return s << "TT";
      case LHCb::Measurement::IT          : return s << "IT";
      case LHCb::Measurement::OT          : return s << "OT";
      case LHCb::Measurement::Muon        : return s << "Muon";
      case LHCb::Measurement::TTLite      : return s << "TTLite";
      case LHCb::Measurement::ITLite      : return s << "ITLite";
      case LHCb::Measurement::VP          : return s << "VP";
      case LHCb::Measurement::Calo        : return s << "Calo";
      case LHCb::Measurement::Origin      : return s << "Origin";
      case LHCb::Measurement::FT          : return s << "FT";
      case LHCb::Measurement::UT          : return s << "UT";
      case LHCb::Measurement::UTLite      : return s << "UTLite";
      default : return s << "ERROR wrong value " << int(e) << " for enum LHCb::Measurement::Type";
    }
  }
  
  
} // namespace LHCb;

// -----------------------------------------------------------------------------
// end of class
// -----------------------------------------------------------------------------

// Including forward declarations

inline LHCb::Measurement::Measurement(const LHCb::Measurement& other) : m_mtype(other.m_mtype),
                                                                       m_z(other.m_z),
                                                                       m_measure(other.m_measure),
                                                                       m_errMeasure(other.m_errMeasure),
                                                                       m_lhcbID(other.m_lhcbID),
                                                                       m_detectorElement(other.m_detectorElement) 
{

	  if( other.m_trajectory )
            m_trajectory.reset( other.m_trajectory->clone().release() );
	
}

inline const GaudiUtils::VectorMap<std::string,LHCb::Measurement::Type> & LHCb::Measurement::s_TypeTypMap() {
  static const GaudiUtils::VectorMap<std::string,Type> m = {
       {"Unknown",Unknown},
       {"VeloR",VeloR},
       {"VeloPhi",VeloPhi},
       {"VeloLiteR",VeloLiteR},
       {"VeloLitePhi",VeloLitePhi},
       {"TT",TT},
       {"IT",IT},
       {"OT",OT},
       {"Muon",Muon},
       {"TTLite",TTLite},
       {"ITLite",ITLite},
       {"VP",VP},
       {"Calo",Calo},
       {"Origin",Origin},
       {"FT",FT},
       {"UT",UT},
       {"UTLite",UTLite}  };
  return m;
}

inline LHCb::Measurement::Type LHCb::Measurement::TypeToType(const std::string & aName) {
  auto iter =  s_TypeTypMap().find(aName);
  return iter != s_TypeTypMap().end() ? iter->second : Unknown;
}

inline const std::string& LHCb::Measurement::TypeToString(int aEnum) {
  static const std::string s_Unknown = "Unknown";
  auto iter = std::find_if(s_TypeTypMap().begin(),s_TypeTypMap().end(),
            [&](const std::pair<const std::string,Type>& i) {
                  return i.second == aEnum; });
  return iter != s_TypeTypMap().end() ? iter->first : s_Unknown;
}

inline void LHCb::Measurement::setType(const Type& value)
{
  unsigned int val = (unsigned int)value;
  m_mtype &= ~typeMask;
  m_mtype |= ((((unsigned int)val) << typeBits) & typeMask);
}

inline double LHCb::Measurement::z() const 
{
  return m_z;
}

inline void LHCb::Measurement::setZ(double value) 
{
  m_z = value;
}

inline double LHCb::Measurement::measure() const 
{
  return m_measure;
}

inline void LHCb::Measurement::setMeasure(double value) 
{
  m_measure = value;
}

inline double LHCb::Measurement::errMeasure() const 
{
  return m_errMeasure;
}

inline void LHCb::Measurement::setErrMeasure(double value) 
{
  m_errMeasure = value;
}

inline const LHCb::LHCbID& LHCb::Measurement::lhcbID() const 
{
  return m_lhcbID;
}

inline void LHCb::Measurement::setLhcbID(const LHCb::LHCbID& value) 
{
  m_lhcbID = value;
}

inline const DetectorElement* LHCb::Measurement::detectorElement() const 
{
  return m_detectorElement;
}

inline double LHCb::Measurement::errMeasure2() const 
{

  return m_errMeasure * m_errMeasure;
        
}

inline double LHCb::Measurement::resolution(const Gaudi::XYZPoint& /*point*/,
                                            const Gaudi::XYZVector& /*vec*/) const 
{

  return m_errMeasure;
        
}

inline double LHCb::Measurement::resolution2(const Gaudi::XYZPoint& /*point*/,
                                             const Gaudi::XYZVector& /*vec*/) const 
{

  return m_errMeasure * m_errMeasure;
        
}

inline LHCb::Measurement::Type LHCb::Measurement::type() const 
{

  return (Measurement::Type)((m_mtype & typeMask) >> typeBits);
        
}

inline bool LHCb::Measurement::checkType(const LHCb::Measurement::Type& value) const 
{

  return type() == value;
        
}

inline const LHCb::Trajectory < double > & LHCb::Measurement::trajectory() const 
{

  return *m_trajectory;
        
}

inline void LHCb::Measurement::setTrajectory(std::unique_ptr < LHCb::Trajectory < double > >& traj) 
{
  m_trajectory = std::move(traj) ; 
}



#endif ///TrackEvent_Measurement_H
