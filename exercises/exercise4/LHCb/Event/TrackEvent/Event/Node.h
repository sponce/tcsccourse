
//   **************************************************************************
//   *                                                                        *
//   *                      ! ! ! A T T E N T I O N ! ! !                     *
//   *                                                                        *
//   *  This file was created automatically by GaudiObjDesc, please do not    *
//   *  delete it or edit it by hand.                                         *
//   *                                                                        *
//   *  If you want to change this file, first change the corresponding       *
//   *  xml-file and rerun the tools from GaudiObjDesc (or run make if you    *
//   *  are using it from inside a Gaudi-package).                            *
//   *                                                                        *
//   **************************************************************************

#ifndef TrackEvent_Node_H
#define TrackEvent_Node_H 1

// Include files
#include "Event/State.h"
#include "Event/Measurement.h"
#include "Event/TrackParameters.h"
#include "Event/TrackTypes.h"
#include "LHCbMath/ValueWithError.h"
#include "GaudiKernel/VectorMap.h"
#include <ostream>
#include <algorithm>

// Forward declarations

namespace LHCb
{

  // Forward declarations

  /** @class Node Node.h
   *
   * Node is a base class for classes linking track states to measurements.
   *
   * @author Jose Hernando, Eduardo Rodrigues
   * created Mon Apr  9 11:52:52 2018
   *
   */

  class Node
  {
  public:

    /// enumerator for the type of Node
    enum Type{ Unknown,
               HitOnTrack,
               Outlier,
               Reference
      };
  
    /// Constructor from a Measurement
    Node(LHCb::Measurement* meas);
  
    /// Constructor from a z-position
    Node(double z);
  
    /// Constructor from a z-position and a location
    Node(double z,
         const LHCb::State::Location& location);
  
    /// Default Constructor
    Node() : m_type(),
             m_state(),
             m_refVector(),
             m_refIsSet(),
             m_measurement(0),
             m_residual(0.0),
             m_errResidual(0.0),
             m_errMeasure(0.0),
             m_projectionMatrix(),
             m_doca(0.0),
             m_pocaVector() {}
  
    /// destructor
    virtual ~Node();
  
    /// conversion of string to enum for type Type
    static LHCb::Node::Type TypeToType (const std::string & aName);
  
    /// conversion to string for enum type Type
    static const std::string& TypeToString(int aEnum);
  
    /// Update the state vector
    void setState(const Gaudi::TrackVector& stateVector,
                  const Gaudi::TrackSymMatrix& stateCovariance,
                  const double& z);
  
    /// Update the reference vector
    void setRefVector(const Gaudi::TrackVector& refVector);
  
    /// Update the reference vector
    void setRefVector(const LHCb::StateVector& refVector);
  
    /// Retrieve the local chi^2 
    virtual double chi2() const;
  
    /// Retrieve the state
    virtual const LHCb::State&  state() const;
  
    /// Retrieve the residual 
    virtual double residual() const;
  
    /// Retrieve the error in the residual 
    virtual double errResidual() const;
  
    /// Retrieve unbiased residual 
    double unbiasedResidual() const;
  
    /// Retrieve error on unbiased residual 
    double errUnbiasedResidual() const;
  
    /// Retrieve the reference to the measurement
    Measurement & measurement();
  
    /// Retrieve const  the reference to the measurement
    const LHCb::Measurement & measurement() const;
  
    /// Set the measurement
    void setMeasurement(LHCb::Measurement& meas);
  
    /// Clone the Node
    virtual Node* clone() const;
  
    /// Return the error on the residual squared
    double errResidual2() const;
  
    /// Return the measure error squared
    double errMeasure2() const;
  
    /// Return true if this Node has a valid pointer to measurement
    bool hasMeasurement() const;
  
    /// Remove measurement from the node
    void removeMeasurement();
  
    /// Retrieve the unbiased smoothed state at this position
    State unbiasedState() const;
  
    /// z position of Node
    double z() const;
  
    /// Position of the State on the Node
    Gaudi::XYZPoint position() const;
  
    /// Set the location of the state in this node.
    void setLocation(LHCb::State::Location& location);
  
    /// Retrieve const  type of node
    const Type& type() const;
  
    /// Update  type of node
    void setType(const Type& value);
  
    /// Update  state
    void setState(const LHCb::State& value);
  
    /// Retrieve const  the reference vector
    const LHCb::StateVector& refVector() const;
  
    /// Retrieve const  flag for the reference vector
    bool refIsSet() const;
  
    /// Update  the residual value
    void setResidual(double value);
  
    /// Update  the residual error
    void setErrResidual(double value);
  
    /// Retrieve const  the measure error
    double errMeasure() const;
  
    /// Update  the measure error
    void setErrMeasure(double value);
  
    /// Retrieve const  the projection matrix
    const Gaudi::TrackProjectionMatrix& projectionMatrix() const;
  
    /// Update  the projection matrix
    void setProjectionMatrix(const Gaudi::TrackProjectionMatrix& value);
  
    /// Retrieve const  Signed doca (of ref-traj). For ST/velo this is equal to minus (ref)residual
    double doca() const;
  
    /// Update  Signed doca (of ref-traj). For ST/velo this is equal to minus (ref)residual
    void setDoca(double value);
  
    /// Retrieve const  Unit vector perpendicular to state and measurement
    const Gaudi::XYZVector& pocaVector() const;
  
    /// Update  Unit vector perpendicular to state and measurement
    void setPocaVector(const Gaudi::XYZVector& value);
  
  protected:

    Type                         m_type;             ///< type of node
    LHCb::State                  m_state;            ///< state
    LHCb::StateVector            m_refVector;        ///< the reference vector
    bool                         m_refIsSet;         ///< flag for the reference vector
    LHCb::Measurement*           m_measurement;      ///< pointer to the measurement (not owner)
    double                       m_residual;         ///< the residual value
    double                       m_errResidual;      ///< the residual error
    double                       m_errMeasure;       ///< the measure error
    Gaudi::TrackProjectionMatrix m_projectionMatrix; ///< the projection matrix
    double                       m_doca;             ///< Signed doca (of ref-traj). For ST/velo this is equal to minus (ref)residual
    Gaudi::XYZVector             m_pocaVector;       ///< Unit vector perpendicular to state and measurement
  
  private:

    static const GaudiUtils::VectorMap<std::string,Type> & s_TypeTypMap();
  
  }; // class Node

  inline std::ostream & operator << (std::ostream & s, LHCb::Node::Type e) {
    switch (e) {
      case LHCb::Node::Unknown    : return s << "Unknown";
      case LHCb::Node::HitOnTrack : return s << "HitOnTrack";
      case LHCb::Node::Outlier    : return s << "Outlier";
      case LHCb::Node::Reference  : return s << "Reference";
      default : return s << "ERROR wrong value " << int(e) << " for enum LHCb::Node::Type";
    }
  }
  
  
} // namespace LHCb;

// -----------------------------------------------------------------------------
// end of class
// -----------------------------------------------------------------------------

// Including forward declarations

inline LHCb::Node::Node(LHCb::Measurement* meas) : m_type(Type::HitOnTrack),
                                                  m_refIsSet(false),
                                                   m_measurement(meas),
                                                   m_residual(0.0),
                                                   m_errResidual(0.0),
                                                   m_errMeasure(0.0),
                                                   m_projectionMatrix() 
{

  m_refVector.setZ(meas->z()) ;
        
}

inline LHCb::Node::Node(double z) : m_type(Type::Reference),
                                   m_refIsSet(false),
                                    m_measurement(0),
                                    m_residual(0.0),
                                    m_errResidual(0.0),
                                    m_errMeasure(0.0),
                                    m_projectionMatrix() 
{

  m_refVector.setZ(z) ;
        
}

inline LHCb::Node::Node(double z,
                        const LHCb::State::Location& location) : m_type(Type::Reference),
                                                                m_state(location),
                                                                m_refIsSet(false),
                                                                 m_measurement(0),
                                                                 m_residual(0.0),
                                                                 m_errResidual(0.0),
                                                                 m_errMeasure(0.0),
                                                                 m_projectionMatrix() 
{

  m_refVector.setZ(z) ;
        
}

inline LHCb::Node::~Node() 
{

        
}

inline const GaudiUtils::VectorMap<std::string,LHCb::Node::Type> & LHCb::Node::s_TypeTypMap() {
  static const GaudiUtils::VectorMap<std::string,Type> m = {
       {"Unknown",Unknown},
       {"HitOnTrack",HitOnTrack},
       {"Outlier",Outlier},
       {"Reference",Reference}  };
  return m;
}

inline LHCb::Node::Type LHCb::Node::TypeToType(const std::string & aName) {
  auto iter =  s_TypeTypMap().find(aName);
  return iter != s_TypeTypMap().end() ? iter->second : Unknown;
}

inline const std::string& LHCb::Node::TypeToString(int aEnum) {
  static const std::string s_Unknown = "Unknown";
  auto iter = std::find_if(s_TypeTypMap().begin(),s_TypeTypMap().end(),
            [&](const std::pair<const std::string,Type>& i) {
                  return i.second == aEnum; });
  return iter != s_TypeTypMap().end() ? iter->first : s_Unknown;
}

inline const LHCb::Node::Type& LHCb::Node::type() const 
{
  return m_type;
}

inline void LHCb::Node::setType(const Type& value) 
{
  m_type = value;
}

inline void LHCb::Node::setState(const LHCb::State& value) 
{
  m_state = value;
}

inline const LHCb::StateVector& LHCb::Node::refVector() const 
{
  return m_refVector;
}

inline bool LHCb::Node::refIsSet() const 
{
  return m_refIsSet;
}

inline void LHCb::Node::setResidual(double value) 
{
  m_residual = value;
}

inline void LHCb::Node::setErrResidual(double value) 
{
  m_errResidual = value;
}

inline double LHCb::Node::errMeasure() const 
{
  return m_errMeasure;
}

inline void LHCb::Node::setErrMeasure(double value) 
{
  m_errMeasure = value;
}

inline const Gaudi::TrackProjectionMatrix& LHCb::Node::projectionMatrix() const 
{
  return m_projectionMatrix;
}

inline void LHCb::Node::setProjectionMatrix(const Gaudi::TrackProjectionMatrix& value) 
{
  m_projectionMatrix = value;
}

inline double LHCb::Node::doca() const 
{
  return m_doca;
}

inline void LHCb::Node::setDoca(double value) 
{
  m_doca = value;
}

inline const Gaudi::XYZVector& LHCb::Node::pocaVector() const 
{
  return m_pocaVector;
}

inline void LHCb::Node::setPocaVector(const Gaudi::XYZVector& value) 
{
  m_pocaVector = value;
}

inline void LHCb::Node::setState(const Gaudi::TrackVector& stateVector,
                                 const Gaudi::TrackSymMatrix& stateCovariance,
                                 const double& z) 
{

  m_state.setState(stateVector);
  m_state.setCovariance(stateCovariance);
  m_state.setZ(z);
        
}

inline void LHCb::Node::setRefVector(const Gaudi::TrackVector& refVector) 
{

  m_refIsSet  = true;
  m_refVector.parameters() = refVector;     
        
}

inline void LHCb::Node::setRefVector(const LHCb::StateVector& refVector) 
{

  m_refIsSet  = true;
  m_refVector = refVector;
        
}

inline const LHCb::State&  LHCb::Node::state() const 
{

  return m_state;
        
}

inline double LHCb::Node::residual() const 
{

  return m_residual;
        
}

inline double LHCb::Node::errResidual() const 
{

  return m_errResidual;
        
}

inline double LHCb::Node::unbiasedResidual() const 
{

  double r = residual();
  return (type() == Type::HitOnTrack) ? (r * errMeasure2() / (m_errResidual*m_errResidual)) : r;
        
}

inline double LHCb::Node::errUnbiasedResidual() const 
{

  double errRes = errResidual();
  return (type() == Type::HitOnTrack) ? (errMeasure2() / errRes) : errRes;
        
}

inline LHCb::Measurement & LHCb::Node::measurement() 
{

  return *m_measurement;
        
}

inline const LHCb::Measurement & LHCb::Node::measurement() const 
{

  return *m_measurement;
        
}

inline void LHCb::Node::setMeasurement(LHCb::Measurement& meas) 
{

  m_measurement = &meas;
        
}

inline double LHCb::Node::errResidual2() const 
{

  double x = errResidual();
  return x*x;
        
}

inline double LHCb::Node::errMeasure2() const 
{

  return m_errMeasure * m_errMeasure;
        
}

inline bool LHCb::Node::hasMeasurement() const 
{

  return m_measurement != nullptr;
        
}

inline void LHCb::Node::removeMeasurement() 
{

  m_measurement = 0;
  // for a node without measurement the chi2 = 0 ;-)
  m_residual    = 0.;
  m_errResidual = 0.;
  m_errMeasure  = 0.;
        
}

inline LHCb::State LHCb::Node::unbiasedState() const 
{

    // we can redo this routine by smoothing the unfiltered states

    // This performs an inverse kalman filter step.
    // First calculate the gain matrix
    const auto& H = projectionMatrix();
    const auto& biasedC = state().covariance();
    double r = residual();
    double R = errResidual2();

    ROOT::Math::SMatrix<double,5,1> K = (biasedC * Transpose(H)) / R;

    // Forwarddate the state vectors
    Gaudi::TrackVector unbiasedX=state().stateVector() - K.Col(0) * r;
    
    // Forwarddate the covariance matrix
    static const Gaudi::TrackSymMatrix unit = ROOT::Math::SMatrixIdentity();
    Gaudi::TrackSymMatrix unbiasedC;
    ROOT::Math::AssignSym::Evaluate(unbiasedC, (unit + K*H)*biasedC);
    return LHCb::State {unbiasedX, unbiasedC, z(), state().location()};
        
}

inline double LHCb::Node::z() const 
{

  return m_refVector.z() ;
        
}

inline Gaudi::XYZPoint LHCb::Node::position() const 
{

  return m_state.position() ;
        
}

inline void LHCb::Node::setLocation(LHCb::State::Location& location) 
{

  m_state.setLocation(location);
        
}



#endif ///TrackEvent_Node_H
