
//   **************************************************************************
//   *                                                                        *
//   *                      ! ! ! A T T E N T I O N ! ! !                     *
//   *                                                                        *
//   *  This file was created automatically by GaudiObjDesc, please do not    *
//   *  delete it or edit it by hand.                                         *
//   *                                                                        *
//   *  If you want to change this file, first change the corresponding       *
//   *  xml-file and rerun the tools from GaudiObjDesc (or run make if you    *
//   *  are using it from inside a Gaudi-package).                            *
//   *                                                                        *
//   **************************************************************************

#ifndef TrackEvent_TrackFitResult_H
#define TrackEvent_TrackFitResult_H 1

// Include files
#include "Measurement.h"
#include "Event/ChiSquare.h"
#include "Kernel/STLExtensions.h"
#include "GaudiKernel/SerializeSTL.h"
#include <ostream>

// Forward declarations

namespace LHCb
{

  // Forward declarations
  class Node;
  class LHCbID;
  using GaudiUtils::operator<<;
  

  /** @class TrackFitResult TrackFitResult.h
   *
   * TrackFitResult holds transient information from the track fit
   *
   * @author Jose Hernando, Eduardo Rodrigues, Wouter Hulsbergen, Daniel Campora
   * created Mon Apr  9 11:52:52 2018
   *
   */

  class TrackFitResult
  {
  public:

    /// Container for LHCb::Measurements on track
    typedef std::vector<LHCb::Measurement*> MeasurementContainer;
    /// Container for LHCb::Nodes on track
    typedef std::vector<LHCb::Node*> NodeContainer;
  
    /// Constructor
    TrackFitResult();
  
    /// Destructor
    virtual ~TrackFitResult();
  
    /// Fill the ASCII output stream
    virtual std::ostream& fillStream(std::ostream& s) const;
  
    /// Track chi square
    virtual const LHCb::ChiSquare&  chi2() const;
  
    /// VELO segment chi square
    virtual const LHCb::ChiSquare&  chi2Velo() const;
  
    /// Upstream segment chi square
    virtual const LHCb::ChiSquare&  chi2Upstream() const;
  
    /// Velo-TT-T segment chi square
    virtual const LHCb::ChiSquare&  chi2Long() const;
  
    /// Muon segment chi square
    virtual const LHCb::ChiSquare&  chi2Muon() const;
  
    /// Downstream segment chi square
    virtual const LHCb::ChiSquare&  chi2Downstream() const;
  
    /// Upstream versus downstream segment chi square
    virtual ChiSquare chi2Match() const;
  
    /// Clone the track (you take ownership of the pointer)
    virtual TrackFitResult* clone() const;
  
    /// Get the number of outliers on the track
    unsigned int nOutliers() const;
  
    /// Get the number of active (non-outlier) measurements on the track
    unsigned int nActiveMeasurements() const;
  
    /// Add a Measurement to the list associated to the track
    void addToMeasurements(const LHCb::Measurement& meas);
  
    /// Add a set of measurements to the track. Track takes ownership.
    void addToMeasurements(span<LHCb::Measurement* const> measurements);
  
    /// Remove a Measurement from the list of Measurements associated to the track
    void removeFromMeasurements(const LHCb::Measurement* value);
  
    /// Clear all measurements on track
    void clearMeasurements();
  
    /// Add a Node to the list of Nodes (note: track will take the ownership of this pointer!!)
    void addToNodes(Node* node);
  
    /// Clear all nodes on track
    void clearNodes();
  
    /// Remove a Node from the list of Nodes associated to the track
    void removeFromNodes(Node* value);
  
    /// Retrieve the number of Measurements on the track
    unsigned int nMeasurements() const;
  
    /// Retrieve the number of measurements of a certain type
    unsigned int nMeasurements(const LHCb::Measurement::Type& type) const;
  
    /// Retrieve the number of active measurements of a certain type
    unsigned int nActiveMeasurements(const LHCb::Measurement::Type& type) const;
  
    /// Return the measurement on the track corresponding to the input LHCbID. Call first the "isMeasurementOnTrack" method before calling this one, as it throws an exception if the LHCbID is not present! (ONLY for tracking code, not for general use.)
    const LHCb::Measurement* measurement(const LHCb::LHCbID& value) const;
  
    /// Clear the track before re-use
    virtual void reset();
  
    /// Retrieve const  Number of iterations in track fit
    int nIter() const;
  
    /// Update  Number of iterations in track fit
    void setNIter(int value);
  
    /// Retrieve const  Momentum used for computing material corrections
    double pScatter() const;
  
    /// Update  Momentum used for computing material corrections
    void setPScatter(double value);
  
    /// Update  Track chi square
    void setChi2(const LHCb::ChiSquare& value);
  
    /// Update  VELO segment chi square
    void setChi2Velo(const LHCb::ChiSquare& value);
  
    /// Update  Upstream segment chi square
    void setChi2Upstream(const LHCb::ChiSquare& value);
  
    /// Update  Velo-TT-T segment chi square
    void setChi2Long(const LHCb::ChiSquare& value);
  
    /// Update  Muon segment chi square
    void setChi2Muon(const LHCb::ChiSquare& value);
  
    /// Update  Downstream segment chi square
    void setChi2Downstream(const LHCb::ChiSquare& value);
  
    /// Retrieve const  Container of Measurements
    const std::vector<LHCb::Measurement*>& measurements() const;
  
    /// Retrieve const  Container of Nodes
    const std::vector<LHCb::Node*>& nodes() const;
  
    /// Retrieve  Container of Nodes
    std::vector<LHCb::Node*>& nodes();
  
  protected:

    /// Copy constructor (hidden). Use clone method instead.
    TrackFitResult(const LHCb::TrackFitResult& rhs);
  
    /// Copy the info from the argument track into this track
    void copy(const LHCb::TrackFitResult& rhs);
  
    LHCb::ChiSquare m_chi2;           ///< Track chi square
    LHCb::ChiSquare m_chi2Velo;       ///< VELO segment chi square
    LHCb::ChiSquare m_chi2Upstream;   ///< Upstream segment chi square
    LHCb::ChiSquare m_chi2Long;       ///< Velo-TT-T segment chi square
    LHCb::ChiSquare m_chi2Muon;       ///< Muon segment chi square
    LHCb::ChiSquare m_chi2Downstream; ///< Downstream segment chi square
  
  private:

    int                             m_nIter;        ///< Number of iterations in track fit
    double                          m_pScatter;     ///< Momentum used for computing material corrections
    std::vector<LHCb::Measurement*> m_measurements; ///< Container of Measurements
    std::vector<LHCb::Node*>        m_nodes;        ///< Container of Nodes
  
  }; // class TrackFitResult

  inline std::ostream& operator<< (std::ostream& str, const TrackFitResult& obj)
  {
    return obj.fillStream(str);
  }
  
} // namespace LHCb;

// -----------------------------------------------------------------------------
// end of class
// -----------------------------------------------------------------------------

// Including forward declarations

inline std::ostream& LHCb::TrackFitResult::fillStream(std::ostream& s) const
{
  s << "{ " << "nIter :	" << m_nIter << std::endl
            << "pScatter :	" << (float)m_pScatter << std::endl
            << "chi2 :	" << m_chi2 << std::endl
            << "chi2Velo :	" << m_chi2Velo << std::endl
            << "chi2Upstream :	" << m_chi2Upstream << std::endl
            << "chi2Long :	" << m_chi2Long << std::endl
            << "chi2Muon :	" << m_chi2Muon << std::endl
            << "chi2Downstream :	" << m_chi2Downstream << std::endl
            << "measurements :	" << m_measurements << std::endl
            << "nodes :	" << m_nodes << std::endl << " }";
  return s;
}


inline int LHCb::TrackFitResult::nIter() const 
{
  return m_nIter;
}

inline void LHCb::TrackFitResult::setNIter(int value) 
{
  m_nIter = value;
}

inline double LHCb::TrackFitResult::pScatter() const 
{
  return m_pScatter;
}

inline void LHCb::TrackFitResult::setPScatter(double value) 
{
  m_pScatter = value;
}

inline void LHCb::TrackFitResult::setChi2(const LHCb::ChiSquare& value) 
{
  m_chi2 = value;
}

inline void LHCb::TrackFitResult::setChi2Velo(const LHCb::ChiSquare& value) 
{
  m_chi2Velo = value;
}

inline void LHCb::TrackFitResult::setChi2Upstream(const LHCb::ChiSquare& value) 
{
  m_chi2Upstream = value;
}

inline void LHCb::TrackFitResult::setChi2Long(const LHCb::ChiSquare& value) 
{
  m_chi2Long = value;
}

inline void LHCb::TrackFitResult::setChi2Muon(const LHCb::ChiSquare& value) 
{
  m_chi2Muon = value;
}

inline void LHCb::TrackFitResult::setChi2Downstream(const LHCb::ChiSquare& value) 
{
  m_chi2Downstream = value;
}

inline const std::vector<LHCb::Measurement*>& LHCb::TrackFitResult::measurements() const 
{
  return m_measurements;
}

inline const std::vector<LHCb::Node*>& LHCb::TrackFitResult::nodes() const 
{
  return m_nodes;
}

inline std::vector<LHCb::Node*>& LHCb::TrackFitResult::nodes() 
{
  return m_nodes;
}

inline const LHCb::ChiSquare&  LHCb::TrackFitResult::chi2() const 
{

  return m_chi2;
        
}

inline const LHCb::ChiSquare&  LHCb::TrackFitResult::chi2Velo() const 
{

  return m_chi2Velo;
        
}

inline const LHCb::ChiSquare&  LHCb::TrackFitResult::chi2Upstream() const 
{

  return m_chi2Upstream;
        
}

inline const LHCb::ChiSquare&  LHCb::TrackFitResult::chi2Long() const 
{

  return m_chi2Long;
        
}

inline const LHCb::ChiSquare&  LHCb::TrackFitResult::chi2Muon() const 
{

  return m_chi2Muon;
        
}

inline const LHCb::ChiSquare&  LHCb::TrackFitResult::chi2Downstream() const 
{

  return m_chi2Downstream;
        
}

inline LHCb::ChiSquare LHCb::TrackFitResult::chi2Match() const 
{

  return m_chi2 - m_chi2Upstream - m_chi2Downstream;
        
}

inline void LHCb::TrackFitResult::addToNodes(Node* node) 
{

  m_nodes.push_back( node );
        
}

inline unsigned int LHCb::TrackFitResult::nMeasurements() const 
{

  return m_measurements.size();
        
}



#endif ///TrackEvent_TrackFitResult_H
