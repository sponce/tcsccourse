
//   **************************************************************************
//   *                                                                        *
//   *                      ! ! ! A T T E N T I O N ! ! !                     *
//   *                                                                        *
//   *  This file was created automatically by GaudiObjDesc, please do not    *
//   *  delete it or edit it by hand.                                         *
//   *                                                                        *
//   *  If you want to change this file, first change the corresponding       *
//   *  xml-file and rerun the tools from GaudiObjDesc (or run make if you    *
//   *  are using it from inside a Gaudi-package).                            *
//   *                                                                        *
//   **************************************************************************

#ifndef LHCbKernel_FTChannelID_H
#define LHCbKernel_FTChannelID_H 1

// Include files
#include <ostream>

// Forward declarations

namespace LHCb
{

  // Forward declarations

  /** @class FTChannelID FTChannelID.h
   *
   * Channel ID for the Fibre Tracker (LHCb Upgrade)
   *
   * @author FT software team
   * created Mon Apr  9 11:52:37 2018
   *
   */

  class FTChannelID final
  {
  public:

    /// Explicit constructor from the geometrical location
    FTChannelID(unsigned int station,
                unsigned int layer,
                unsigned int quarter,
                unsigned int module,
                unsigned int mat,
                unsigned int sipm,
                unsigned int channel);
  
    /// Explicit constructor from the geometrical location
    FTChannelID(unsigned int station,
                unsigned int layer,
                unsigned int quarter,
                unsigned int module,
                unsigned int channelInModule);
  
    /// Constructor from int
    FTChannelID(unsigned int id) : m_channelID(id) {}
  
    /// Default Constructor
    FTChannelID() : m_channelID(0) {}
  
    /// Operator overload, to cast channel ID to unsigned int.                         Used by linkers where the key (channel id) is an int
    operator unsigned int() const;
  
    /// Comparison equality
    bool operator==(const FTChannelID& aChannel) const;
  
    /// Comparison <
    bool operator<(const FTChannelID& aChannel) const;
  
    /// Comparison >
    bool operator>(const FTChannelID& aChannel) const;
  
    /// Increment the channelID 
    void next();
  
    /// Increment the channelID 
    void addToChannel(const int offset);
  
    /// Return the SiPM number within the module (0-15)
    unsigned int sipmInModule() const;
  
    /// Return the die number (0 or 1)
    unsigned int die() const;
  
    /// Return true if channelID is in x-layer
    bool isX() const;
  
    /// Return true if channelID is in bottom part of detector
    bool isBottom() const;
  
    /// Return true if channelID is in top part of detector
    bool isTop() const;
  
    /// Print this FTChannelID in a human readable way
    std::ostream& fillStream(std::ostream& s) const;
  
    /// Retrieve const  FT Channel ID
    unsigned int channelID() const;
  
    /// Update  FT Channel ID
    void setChannelID(unsigned int value);
  
    /// Retrieve Channel in the 128 channel SiPM
    unsigned int channel() const;
  
    /// Retrieve ID of the SiPM in the mat
    unsigned int sipm() const;
  
    /// Retrieve ID of the mat in the module
    unsigned int mat() const;
  
    /// Retrieve Module id (0 - 5 or 0 - 6)
    unsigned int module() const;
  
    /// Retrieve Quarter ID (0 - 3)
    unsigned int quarter() const;
  
    /// Retrieve Layer id
    unsigned int layer() const;
  
    /// Retrieve Station id
    unsigned int station() const;
  
    /// Retrieve unique layer
    unsigned int uniqueLayer() const;
  
    /// Retrieve unique quarter
    unsigned int uniqueQuarter() const;
  
    /// Retrieve unique module
    unsigned int uniqueModule() const;
  
    /// Retrieve unique mat
    unsigned int uniqueMat() const;
  
    /// Retrieve unique SiPM
    unsigned int uniqueSiPM() const;
  
  protected:

  private:

    /// Offsets of bitfield channelID
    enum channelIDBits{channelBits       = 0,
                       sipmBits          = 7,
                       matBits           = 9,
                       moduleBits        = 11,
                       quarterBits       = 14,
                       layerBits         = 16,
                       stationBits       = 18};
  
    /// Bitmasks for bitfield channelID
    enum channelIDMasks{channelMask       = 0x7fL,
                        sipmMask          = 0x180L,
                        matMask           = 0x600L,
                        moduleMask        = 0x3800L,
                        quarterMask       = 0xc000L,
                        layerMask         = 0x30000L,
                        stationMask       = 0xc0000L,
                        uniqueLayerMask   = layerMask + stationMask,
                        uniqueQuarterMask = quarterMask + layerMask + stationMask,
                        uniqueModuleMask  = moduleMask + quarterMask + layerMask + stationMask,
                        uniqueMatMask     = matMask + moduleMask + quarterMask + layerMask + stationMask,
                        uniqueSiPMMask    = sipmMask + matMask + moduleMask + quarterMask + layerMask + stationMask
                       };
  
  
    unsigned int m_channelID; ///< FT Channel ID
  
  }; // class FTChannelID

  inline std::ostream& operator<< (std::ostream& str, const FTChannelID& obj)
  {
    return obj.fillStream(str);
  }
  
} // namespace LHCb;

// -----------------------------------------------------------------------------
// end of class
// -----------------------------------------------------------------------------

// Including forward declarations

inline LHCb::FTChannelID::FTChannelID(unsigned int station,
                                      unsigned int layer,
                                      unsigned int quarter,
                                      unsigned int module,
                                      unsigned int mat,
                                      unsigned int sipm,
                                      unsigned int channel) 
{
    
          m_channelID = (station  <<  stationBits  ) + 
                        (layer    <<  layerBits) + 
                        (quarter  <<  quarterBits) + 
                        (module   <<  moduleBits ) + 
                        (mat      <<  matBits ) + 
                        (sipm     <<  sipmBits ) + 
                        (channel  <<  channelBits  );
        
}

inline LHCb::FTChannelID::FTChannelID(unsigned int station,
                                      unsigned int layer,
                                      unsigned int quarter,
                                      unsigned int module,
                                      unsigned int channelInModule) 
{
    
          m_channelID = (station  <<  stationBits  ) + 
                        (layer    <<  layerBits) + 
                        (quarter  <<  quarterBits) + 
                        (module   <<  moduleBits ) + 
                        (channelInModule  <<  channelBits );
        
}

inline unsigned int LHCb::FTChannelID::channelID() const 
{
  return m_channelID;
}

inline void LHCb::FTChannelID::setChannelID(unsigned int value) 
{
  m_channelID = value;
}

inline unsigned int LHCb::FTChannelID::channel() const
{
  return (unsigned int)((m_channelID & channelMask) >> channelBits);
}

inline unsigned int LHCb::FTChannelID::sipm() const
{
  return (unsigned int)((m_channelID & sipmMask) >> sipmBits);
}

inline unsigned int LHCb::FTChannelID::mat() const
{
  return (unsigned int)((m_channelID & matMask) >> matBits);
}

inline unsigned int LHCb::FTChannelID::module() const
{
  return (unsigned int)((m_channelID & moduleMask) >> moduleBits);
}

inline unsigned int LHCb::FTChannelID::quarter() const
{
  return (unsigned int)((m_channelID & quarterMask) >> quarterBits);
}

inline unsigned int LHCb::FTChannelID::layer() const
{
  return (unsigned int)((m_channelID & layerMask) >> layerBits);
}

inline unsigned int LHCb::FTChannelID::station() const
{
  return (unsigned int)((m_channelID & stationMask) >> stationBits);
}

inline unsigned int LHCb::FTChannelID::uniqueLayer() const
{
  return (unsigned int)((m_channelID & uniqueLayerMask) >> layerBits);
}

inline unsigned int LHCb::FTChannelID::uniqueQuarter() const
{
  return (unsigned int)((m_channelID & uniqueQuarterMask) >> quarterBits);
}

inline unsigned int LHCb::FTChannelID::uniqueModule() const
{
  return (unsigned int)((m_channelID & uniqueModuleMask) >> moduleBits);
}

inline unsigned int LHCb::FTChannelID::uniqueMat() const
{
  return (unsigned int)((m_channelID & uniqueMatMask) >> matBits);
}

inline unsigned int LHCb::FTChannelID::uniqueSiPM() const
{
  return (unsigned int)((m_channelID & uniqueSiPMMask) >> sipmBits);
}

inline LHCb::FTChannelID::operator unsigned int() const 
{
 return m_channelID; 
}

inline bool LHCb::FTChannelID::operator==(const FTChannelID& aChannel) const 
{
 return (this->channelID() == aChannel.channelID()); 
}

inline bool LHCb::FTChannelID::operator<(const FTChannelID& aChannel) const 
{
 return (this->channelID() < aChannel.channelID()); 
}

inline bool LHCb::FTChannelID::operator>(const FTChannelID& aChannel) const 
{
 return (this->channelID() > aChannel.channelID()); 
}

inline void LHCb::FTChannelID::next() 
{
 ++m_channelID; 
}

inline void LHCb::FTChannelID::addToChannel(const int offset) 
{
 m_channelID+=offset; 
}

inline unsigned int LHCb::FTChannelID::sipmInModule() const 
{
 return (m_channelID & (matMask + sipmMask)) >> sipmBits; 
}

inline unsigned int LHCb::FTChannelID::die() const 
{
 return (m_channelID & 0x40) >> 6; 
}

inline bool LHCb::FTChannelID::isX() const 
{
 return (layer() == 0 || layer() == 3); 
}

inline bool LHCb::FTChannelID::isBottom() const 
{
 return (quarter() == 0 || quarter() == 1); 
}

inline bool LHCb::FTChannelID::isTop() const 
{
 return (quarter() == 2 || quarter() == 3); 
}

inline std::ostream& LHCb::FTChannelID::fillStream(std::ostream& s) const 
{

return s 
<< "{ FTChannelID : "
<< " channel =" << channel()
<< " sipm ="    << sipm()
<< " mat ="     << mat()
<< " module="   << module()
<< " quarter="  << quarter()
<< " layer="    << layer()
<< " station="  << station()
<< " }";
        
}



#endif ///LHCbKernel_FTChannelID_H
