
//   **************************************************************************
//   *                                                                        *
//   *                      ! ! ! A T T E N T I O N ! ! !                     *
//   *                                                                        *
//   *  This file was created automatically by GaudiObjDesc, please do not    *
//   *  delete it or edit it by hand.                                         *
//   *                                                                        *
//   *  If you want to change this file, first change the corresponding       *
//   *  xml-file and rerun the tools from GaudiObjDesc (or run make if you    *
//   *  are using it from inside a Gaudi-package).                            *
//   *                                                                        *
//   **************************************************************************

#ifndef LHCbKernel_HCCellID_H
#define LHCbKernel_HCCellID_H 1

// Include files
#include <ostream>

// Forward declarations

namespace LHCb
{

  // Forward declarations

  /** @class HCCellID HCCellID.h
   *
   * This class identifies a single PMT in the HC
   *
   * @author Victor Coco
   * created Mon Apr  9 11:52:37 2018
   *
   */

  class HCCellID
  {
  public:

    /// Constructor with crate and channel
    HCCellID(unsigned int crate,
             unsigned int channel);
  
    /// Constructor with cellID
    HCCellID(int id) : m_cellID(id) {}
  
    /// Default Constructor
    HCCellID() : m_cellID(0) {}
  
    /// Copy Constructor
    HCCellID(const HCCellID & rh);
  
    /// Default Destructor
     ~HCCellID() {}
  
    /// Cast
    operator unsigned int() const;
  
    /// Special serializer to ASCII stream
    std::ostream& fillStream(std::ostream& s) const;
  
    /// Retrieve const  HC Cell ID
    unsigned int cellID() const;
  
    /// Update  HC Cell ID
    void setCellID(unsigned int value);
  
    /// Retrieve channel number
    unsigned int channel() const;
  
    /// Update channel number
    void setChannel(unsigned int value);
  
    /// Retrieve crate number
    unsigned int crate() const;
  
    /// Update crate number
    void setCrate(unsigned int value);
  
  protected:

  private:

    /// Offsets of bitfield cellID
    enum cellIDBits{channelBits = 0,
                    crateBits   = 6};
  
    /// Bitmasks for bitfield cellID
    enum cellIDMasks{channelMask = 0x3fL,
                     crateMask   = 0x7c0L
                    };
  
  
    unsigned int m_cellID; ///< HC Cell ID
  
  }; // class HCCellID

  inline std::ostream& operator<< (std::ostream& str, const HCCellID& obj)
  {
    return obj.fillStream(str);
  }
  
} // namespace LHCb;

// -----------------------------------------------------------------------------
// end of class
// -----------------------------------------------------------------------------

// Including forward declarations

inline LHCb::HCCellID::HCCellID(unsigned int crate,
                                unsigned int channel) 
{

  m_cellID = (crate << crateBits) | channel;
        
}

inline LHCb::HCCellID::HCCellID(const LHCb::HCCellID & rh) : 
   m_cellID( rh.m_cellID )
   {}

inline unsigned int LHCb::HCCellID::cellID() const 
{
  return m_cellID;
}

inline void LHCb::HCCellID::setCellID(unsigned int value) 
{
  m_cellID = value;
}

inline unsigned int LHCb::HCCellID::channel() const
{
  return (unsigned int)((m_cellID & channelMask) >> channelBits);
}

inline void LHCb::HCCellID::setChannel(unsigned int value)
{
  unsigned int val = (unsigned int)value;
  m_cellID &= ~channelMask;
  m_cellID |= ((((unsigned int)val) << channelBits) & channelMask);
}

inline unsigned int LHCb::HCCellID::crate() const
{
  return (unsigned int)((m_cellID & crateMask) >> crateBits);
}

inline void LHCb::HCCellID::setCrate(unsigned int value)
{
  unsigned int val = (unsigned int)value;
  m_cellID &= ~crateMask;
  m_cellID |= ((((unsigned int)val) << crateBits) & crateMask);
}

inline LHCb::HCCellID::operator unsigned int() const 
{
return m_cellID;
}



#endif ///LHCbKernel_HCCellID_H
