
//   **************************************************************************
//   *                                                                        *
//   *                      ! ! ! A T T E N T I O N ! ! !                     *
//   *                                                                        *
//   *  This file was created automatically by GaudiObjDesc, please do not    *
//   *  delete it or edit it by hand.                                         *
//   *                                                                        *
//   *  If you want to change this file, first change the corresponding       *
//   *  xml-file and rerun the tools from GaudiObjDesc (or run make if you    *
//   *  are using it from inside a Gaudi-package).                            *
//   *                                                                        *
//   **************************************************************************

#ifndef LHCbKernel_OTChannelID_H
#define LHCbKernel_OTChannelID_H 1

// Include files
#include <ostream>

// Forward declarations

namespace LHCb
{

  // Forward declarations

  /** @class OTChannelID OTChannelID.h
   *
   * Simple class that represents a straw and the tdc time of a hit
   *
   * @author Jeroen van Tilburg and Jacopo Nardulli
   * created Mon Apr  9 11:52:37 2018
   *
   */

  class OTChannelID
  {
  public:

    /// Number of unique sequential IDs
    enum NumUniqueIDEnum{ NumSequentialLayer = 12,
                          NumSequentialQuarter = 48,
                          NumSequentialModule = 432,
                          NumSequentialOtis = 432*4,
                          NumSequentialStraw = 432*128
      };
  
    /// Explicit constructor from the geometrical location of the straw and the tdc time of the hit
    OTChannelID(unsigned int station,
                unsigned int layer,
                unsigned int quarter,
                unsigned int module,
                unsigned int straw,
                unsigned int tdcTime=0u);
  
    /// Constructor from int
    OTChannelID(unsigned int id) : m_channelID(id) {}
  
    /// Default Constructor
    OTChannelID() : m_channelID(0) {}
  
    /// Copy Constructor
    OTChannelID(const OTChannelID & rh);
  
    /// Default Destructor
     ~OTChannelID() {}
  
    /// Comparison equality
    bool operator==(const OTChannelID& aChannel) const;
  
    /// Comparison <
    bool operator<(const OTChannelID& aChannel) const;
  
    /// Comparison >
    bool operator>(const OTChannelID& aChannel) const;
  
    /// Operator overload, to cast channel ID to unsigned int. Used by linkers where the key (channel id) is an int
    operator unsigned int() const;
  
    /// Returns the geometrical part of the channelID
    unsigned int geometry() const;
  
    /// True if same geometric channel
    bool sameGeometry(const OTChannelID& testChannel) const;
  
    /// true if same tdc counts
    bool sameTime(const OTChannelID& testChannel) const;
  
    /// Returns a layer id in range [0,11]
    unsigned int sequentialUniqueLayer() const;
  
    /// Returns a quarter id in range [0,47]
    unsigned int sequentialUniqueQuarter() const;
  
    /// Returns a module id in range [0,431]
    unsigned int sequentialUniqueModule() const;
  
    /// Returns an otis id in range [0,1727]
    unsigned int sequentialUniqueOtis() const;
  
    /// Returns a straw id in range [0,55295]
    unsigned int sequentialUniqueStraw() const;
  
    /// Print this OTChannelID in a human readable way
    std::ostream& fillStream(std::ostream& s) const;
  
    /// Retrieve const  OT Channel ID of straw
    unsigned int channelID() const;
  
    /// Update  OT Channel ID of straw
    void setChannelID(unsigned int value);
  
    /// Retrieve TDC-time of hit
    unsigned int tdcTime() const;
  
    /// Update TDC-time of hit
    void setTdcTime(unsigned int value);
  
    /// Retrieve Straw id
    unsigned int straw() const;
  
    /// Retrieve Module id
    unsigned int module() const;
  
    /// Retrieve Quarter id
    unsigned int quarter() const;
  
    /// Retrieve Layer id
    unsigned int layer() const;
  
    /// Retrieve Station id
    unsigned int station() const;
  
    /// Retrieve Unique layer id, i.e. this layer belong to this station
    unsigned int uniqueLayer() const;
  
    /// Retrieve Unique quarter id, i.e. this quarter belongs to this layer and station
    unsigned int uniqueQuarter() const;
  
    /// Retrieve Unique module id, i.e. this module belongs to this quarter, layer and station
    unsigned int uniqueModule() const;
  
    /// Retrieve Unique straw, i.e. this straw belongs to this module, quarter, layer and station
    unsigned int uniqueStraw() const;
  
  protected:

  private:

    /// Offsets of bitfield channelID
    enum channelIDBits{tdcTimeBits       = 0,
                       strawBits         = 8,
                       moduleBits        = 16,
                       quarterBits       = 20,
                       layerBits         = 22,
                       stationBits       = 24};
  
    /// Bitmasks for bitfield channelID
    enum channelIDMasks{tdcTimeMask       = 0xffL,
                        strawMask         = 0xff00L,
                        moduleMask        = 0xf0000L,
                        quarterMask       = 0x300000L,
                        layerMask         = 0xc00000L,
                        stationMask       = 0x3000000L,
                        uniqueLayerMask   = layerMask + stationMask,
                        uniqueQuarterMask = quarterMask + layerMask + stationMask,
                        uniqueModuleMask  = moduleMask + quarterMask + layerMask + stationMask,
                        uniqueStrawMask   = strawMask + moduleMask + quarterMask + layerMask + stationMask
                       };
  
  
    unsigned int m_channelID; ///< OT Channel ID of straw
  
  }; // class OTChannelID

  inline std::ostream& operator<< (std::ostream& str, const OTChannelID& obj)
  {
    return obj.fillStream(str);
  }
  
  inline std::ostream & operator << (std::ostream & s, LHCb::OTChannelID::NumUniqueIDEnum e) {
    switch (e) {
      case LHCb::OTChannelID::NumSequentialLayer   : return s << "NumSequentialLayer";
      case LHCb::OTChannelID::NumSequentialQuarter : return s << "NumSequentialQuarter";
      case LHCb::OTChannelID::NumSequentialModule  : return s << "NumSequentialModule";
      case LHCb::OTChannelID::NumSequentialOtis    : return s << "NumSequentialOtis";
      case LHCb::OTChannelID::NumSequentialStraw   : return s << "NumSequentialStraw";
      default : return s << "ERROR wrong value " << int(e) << " for enum LHCb::OTChannelID::NumUniqueIDEnum";
    }
  }
  
  
} // namespace LHCb;

// -----------------------------------------------------------------------------
// end of class
// -----------------------------------------------------------------------------

// Including forward declarations

inline LHCb::OTChannelID::OTChannelID(unsigned int station,
                                      unsigned int layer,
                                      unsigned int quarter,
                                      unsigned int module,
                                      unsigned int straw,
                                      unsigned int tdcTime) 
{
    
          m_channelID = (station << stationBits) + 
                        (layer   << layerBits  ) + 
                        (quarter << quarterBits) + 
                        (module  << moduleBits ) + 
                        (straw   << strawBits  ) +
                        (tdcTime << tdcTimeBits);
        
}

inline LHCb::OTChannelID::OTChannelID(const LHCb::OTChannelID & rh) : 
   m_channelID( rh.m_channelID )
   {}

inline unsigned int LHCb::OTChannelID::channelID() const 
{
  return m_channelID;
}

inline void LHCb::OTChannelID::setChannelID(unsigned int value) 
{
  m_channelID = value;
}

inline unsigned int LHCb::OTChannelID::tdcTime() const
{
  return (unsigned int)((m_channelID & tdcTimeMask) >> tdcTimeBits);
}

inline void LHCb::OTChannelID::setTdcTime(unsigned int value)
{
  unsigned int val = (unsigned int)value;
  m_channelID &= ~tdcTimeMask;
  m_channelID |= ((((unsigned int)val) << tdcTimeBits) & tdcTimeMask);
}

inline unsigned int LHCb::OTChannelID::straw() const
{
  return (unsigned int)((m_channelID & strawMask) >> strawBits);
}

inline unsigned int LHCb::OTChannelID::module() const
{
  return (unsigned int)((m_channelID & moduleMask) >> moduleBits);
}

inline unsigned int LHCb::OTChannelID::quarter() const
{
  return (unsigned int)((m_channelID & quarterMask) >> quarterBits);
}

inline unsigned int LHCb::OTChannelID::layer() const
{
  return (unsigned int)((m_channelID & layerMask) >> layerBits);
}

inline unsigned int LHCb::OTChannelID::station() const
{
  return (unsigned int)((m_channelID & stationMask) >> stationBits);
}

inline unsigned int LHCb::OTChannelID::uniqueLayer() const
{
  return (unsigned int)((m_channelID & uniqueLayerMask) >> layerBits);
}

inline unsigned int LHCb::OTChannelID::uniqueQuarter() const
{
  return (unsigned int)((m_channelID & uniqueQuarterMask) >> quarterBits);
}

inline unsigned int LHCb::OTChannelID::uniqueModule() const
{
  return (unsigned int)((m_channelID & uniqueModuleMask) >> moduleBits);
}

inline unsigned int LHCb::OTChannelID::uniqueStraw() const
{
  return (unsigned int)((m_channelID & uniqueStrawMask) >> strawBits);
}

inline bool LHCb::OTChannelID::operator==(const OTChannelID& aChannel) const 
{
 return (this->channelID() == aChannel.channelID()); 
}

inline bool LHCb::OTChannelID::operator<(const OTChannelID& aChannel) const 
{
 return (this->channelID() < aChannel.channelID()); 
}

inline bool LHCb::OTChannelID::operator>(const OTChannelID& aChannel) const 
{
 return (this->channelID() > aChannel.channelID()); 
}

inline LHCb::OTChannelID::operator unsigned int() const 
{
 return m_channelID; 
}

inline unsigned int LHCb::OTChannelID::geometry() const 
{
 return (m_channelID & uniqueStrawMask); 
}

inline bool LHCb::OTChannelID::sameGeometry(const OTChannelID& testChannel) const 
{
 return (geometry() == testChannel.geometry()); 
}

inline bool LHCb::OTChannelID::sameTime(const OTChannelID& testChannel) const 
{
 return (tdcTime() == testChannel.tdcTime()); 
}

inline unsigned int LHCb::OTChannelID::sequentialUniqueLayer() const 
{
 return (station()-1) * 4 + layer() ;
}

inline unsigned int LHCb::OTChannelID::sequentialUniqueQuarter() const 
{
 return sequentialUniqueLayer()*4 + quarter() ;
}

inline unsigned int LHCb::OTChannelID::sequentialUniqueModule() const 
{
 return sequentialUniqueQuarter()*9 + module() - 1 ;
}

inline unsigned int LHCb::OTChannelID::sequentialUniqueOtis() const 
{
 return sequentialUniqueModule()*4 + (straw()-1)/32 ;
}

inline unsigned int LHCb::OTChannelID::sequentialUniqueStraw() const 
{
 return sequentialUniqueModule()*128 + (straw()-1) ;
}



#endif ///LHCbKernel_OTChannelID_H
