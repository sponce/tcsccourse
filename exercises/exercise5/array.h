#include <vectorclass.h>
#include <array>

/**
 * This defines an array of items of a given VCL vector type
 * and implements a number of operators and useful method to
 * handle it as if we were using a single vector element
 */
template<typename VectorType, int NbElem = N>
  struct Array : std::array<VectorType, NbElem> {
  using BaseType = decltype(std::declval<VectorType>().extract(0));
  using BoolType = decltype(std::declval<VectorType>() > std::declval<BaseType>());
  using BoolArrayType = Array<BoolType, NbElem>;
  Array() = default;
  Array(BaseType v) {
    for (int i = 0; i < NbElem; i++) (*this)[i] = v;
  }
  Array(BaseType minx, BaseType dx) {
    for (int i = 0; i < NbElem; i++) {
      (*this)[i] = {minx+i*8*dx, minx+(1+i*8)*dx, minx+(2+i*8)*dx, minx+(3+i*8)*dx, minx+(4+i*8)*dx, minx+(5+i*8)*dx, minx+(6+i*8)*dx, minx+(7+i*8)*dx};
    }
  }
  void store(BaseType* buffer) {
    for (int i = 0; i < NbElem; i++) {  
      (*this)[i].store(buffer+8*i);
    }
  }
  Array operator+=(Array& arg) {
    for (int i = 0; i < NbElem; i++) (*this)[i] += arg;
  }
  Array operator+=(BaseType arg) {
    for (int i = 0; i < NbElem; i++) (*this)[i] += arg;
    return *this;
  }
  Array operator+(Array const & arg) {
    Array res;
    for (int i = 0; i < NbElem; i++) res[i] = (*this)[i] + arg[i];
    return res;
  }
  Array operator+(VectorType arg) {
    Array res;
    for (int i = 0; i < NbElem; i++) res[i] = (*this)[i] + arg;
    return res;
  }
  Array operator-(Array const& arg) {
    Array res;
    for (int i = 0; i < NbElem; i++) res[i] = (*this)[i] - arg[i];
    return res;
  }
  Array operator*(Array const & arg) {
    Array res;
    for (int i = 0; i < NbElem; i++) res[i] = (*this)[i] * arg[i];
    return res;
  }
  Array operator*(BaseType arg) {
    Array res;
    for (int i = 0; i < NbElem; i++) res[i] = (*this)[i] * arg;
    return res;
  }
  BoolArrayType operator>(BaseType arg) {
    BoolArrayType res;
    for (int i = 0; i < NbElem; i++) res[i] = (*this)[i] > arg;
    return res;
  }
};

template<int NbElem>
  struct Array<Vec8fb, NbElem> : std::array<Vec8fb, NbElem> {
  using BaseType = decltype(std::declval<Vec8fb>().extract(0));
  Array() = default;
  Array(BaseType v) {
    for (int i = 0; i < NbElem; i++) (*this)[i] = v;
  }
  bool horizontal_and() {
    bool b = true;
    for (int i = 0; i < N; i++) b = b && ::horizontal_and((*this)[i]);
    return b;
  }

};
