#include <chrono>
#include <iostream>
#include <numeric>
#include <experimental/simd>

extern "C" {

  namespace stdx = std::experimental;
  using float_v = stdx::native_simd<float>;
  using int_v = stdx::fixed_size_simd<int, float_v::size()>;
  using mask_v = stdx::fixed_size_simd_mask<int, float_v::size()>;

  int_v kernel(float_v ax, float_v ay) {
    float_v x(0);
    float_v y(0);
    int_v res(-1);
    mask_v cmp{false};
    for (int n = 1; n <= 100; n++) {
      float_v newx = x*x - y*y + ax;
      float_v newy = 2*x*y + ay;
      // The reason why the comparison is made with integers here is
      // that we do not have a way to simd_cast a mask<flaot> into a
      // mask<int> in the current implementation of simd and we need
      // a mask<int> to fill res as `where` wants a mask fittin the
      // data
      mask_v newcmp = 4 < stdx::static_simd_cast<int>(newx*newx + newy*newy);
      if (stdx::any_of(newcmp)) {
        where(!cmp && newcmp, res) = n;
        cmp |= newcmp;
      }
      if (stdx::all_of(newcmp)) {
        break;
      }
      x = newx;
      y = newy;
    }
    return res;
  }
  
  void mandel(int* buffer,
              const float minx,
              const float dx,
              const unsigned int nx,
              const float miny,
              const float dy,
              const unsigned int ny) {
    auto start = std::chrono::steady_clock::now();
    float_v ay(miny);
    for (unsigned int any = 0; any < ny; ay += dy, any++) {
      float_v ax([](int i) { return i; });
      ax = ax * float_v(dx);
      ax = ax + float_v(minx);
      for (unsigned int anx = 0; anx < nx; ax += float_v::size()*dx, anx += float_v::size()) {
        kernel(ax, ay).copy_to(buffer+any*nx+anx, stdx::vector_aligned);
      }
    }
    auto end = std::chrono::steady_clock::now();
    std::cout << "Elapsed time in microseconds : " 
              << std::chrono::duration_cast<std::chrono::microseconds>(end - start).count()
              << " us" << std::endl;
  }
}
