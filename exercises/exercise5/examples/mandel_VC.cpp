#include <chrono>
#include <iostream>
#include <Vc/vector.h>

extern "C" {

  Vc::int_v kernel(Vc::float_v ax, Vc::float_v ay) {
    Vc::float_v x(0);
    Vc::float_v y(0);
    Vc::int_v res(-1);
    Vc::int_m cmp{false};
    for (int n = 1; n <= 100; n++) {
      Vc::float_v newx = x*x - y*y + ax;
      Vc::float_v newy = 2*x*y + ay;
      Vc::int_m newcmp = simd_cast<Vc::int_m>(4 < newx*newx + newy*newy);
      if (newcmp.isNotEmpty()) {
        res(cmp && !newcmp) = n;
        cmp = cmp | newcmp;
      }
      if (newcmp.isFull()) {
        break;
      }
      x = newx;
      y = newy;
    }
    return res;
  }
  
  void mandel(int* buffer,
              const float minx,
              const float dx,
              const unsigned int nx,
              const float miny,
              const float dy,
              const unsigned int ny) {
    auto start = std::chrono::steady_clock::now();
    Vc::float_v ay(miny);
    for (unsigned int any = 0; any < ny; ay += dy, any++) {
      Vc::float_v ax(Vc::IndexesFromZero);
      ax = ax * Vc::float_v(dx);
      ax = ax + Vc::float_v(minx);
      for (unsigned int anx = 0; anx < nx; ax += Vc::float_v::Size*dx, anx += Vc::float_v::Size) {
        kernel(ax, ay).store(buffer+any*nx+anx);
      }
    }
    auto end = std::chrono::steady_clock::now();
    std::cout << "Elapsed time in microseconds : " 
              << std::chrono::duration_cast<std::chrono::microseconds>(end - start).count()
              << " us" << std::endl;
  }
}
