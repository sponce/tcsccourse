#include <chrono>
#include <cstring>
#include <iostream>
#include <string.h>
#include <immintrin.h>

typedef float Vec8f __attribute__ ((vector_size (32)));
typedef int Vec8i __attribute__ ((vector_size (32)));

extern "C" {

  Vec8i kernel(Vec8f ax, Vec8f ay) {
    Vec8f x{0};
    Vec8f y{0};
    Vec8i res{-1};
    Vec8i cmp{0};
    for (int n = 1; n <= 100; n++) {
      Vec8f newx = x*x - y*y + ax;
      Vec8f newy = 2*x*y + ay;
      Vec8i cmpmask = (4 < newx*newx + newy*newy);
      int newcmp = _mm256_movemask_ps((__m256)cmpmask);
      if (newcmp != 0) {
        res = (cmpmask && (cmp == 0)) ? n : res;
        cmp = cmp | cmpmask;
      }
      if ((int)0xff == newcmp) {
        break;
      }
      x = newx;
      y = newy;
    }
    return res;
  }
  
  void mandel(int* buffer,
              const float minx,
              const float dx,
              const unsigned int nx,
              const float miny,
              const float dy,
              const unsigned int ny) {
    auto start = std::chrono::steady_clock::now();
    Vec8f ay{miny, miny, miny, miny, miny, miny, miny, miny};
    for (unsigned int any = 0; any < ny; ay += dy, any++) {
      Vec8f ax{minx, minx+dx, minx+2*dx, minx+3*dx, minx+4*dx, minx+5*dx, minx+6*dx, minx+7*dx};
      for (unsigned int anx = 0; anx < nx; ax += 8*dx, anx += 8) {
        Vec8i res = kernel(ax, ay);
        memcpy(buffer+any*nx+anx, &res, 32);
      }
    }
    auto end = std::chrono::steady_clock::now();
    std::cout << "Elapsed time in microseconds : " 
              << std::chrono::duration_cast<std::chrono::microseconds>(end - start).count()
              << " us" << std::endl;
  }
}
