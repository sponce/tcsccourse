#include <chrono>
#include <iostream>
#include <immintrin.h>

extern "C" {

  __m256i kernel(__m256 ax, __m256 ay) {
    __m256 two = _mm256_set1_ps(2.0);
    __m256 four = _mm256_set1_ps(4.0);
    __m256i minusone = _mm256_set1_epi32(-1);
    __m256 x = _mm256_setzero_ps();
    __m256 y = _mm256_setzero_ps();
    __m256i res = minusone;
    __m256 cmp = _mm256_setzero_ps();
    for (int n = 1; n <= 100; n++) {
      __m256 newx = _mm256_add_ps(_mm256_sub_ps(_mm256_mul_ps(x,x), _mm256_mul_ps(y,y)), ax);
      __m256 newy = _mm256_add_ps(_mm256_mul_ps(two,_mm256_mul_ps(x,y)), ay);
      __m256 norm = _mm256_add_ps(_mm256_mul_ps(newx, newx),_mm256_mul_ps(newy, newy));
      __m256 cmpmask = _mm256_cmp_ps(four, norm, _CMP_LT_OS); // < comparison
      int newcmp = _mm256_movemask_ps(cmpmask);
      if (0 != newcmp) {
        __m256 nm = _mm256_andnot_ps(cmp, cmpmask);
        res = _mm256_castps_si256(_mm256_blendv_ps(_mm256_castsi256_ps(res), _mm256_castsi256_ps(_mm256_set1_epi32(n)), nm));
        cmp = _mm256_or_ps(cmp, cmpmask);
      }
      if ((int)0xff == newcmp) {
        return res;
      }
      x = newx;
      y = newy;
    }
    return res;
  }

  void mandel(int* buffer,
              const float minx,
              const float dx,
              const unsigned int nx,
              const float miny,
              const float dy,
              const unsigned int ny) {
    auto start = std::chrono::steady_clock::now();
    __m256 dx256 = _mm256_set1_ps(8*dx);
    __m256 dy256 = _mm256_set1_ps(dy);
    __m256 ay = _mm256_set1_ps(miny);
    __m256 minax = _mm256_set_ps(minx+7*dx, minx+6*dx, minx+5*dx, minx+4*dx, minx+3*dx, minx+2*dx, minx+dx, minx);
    for (unsigned int any = 0; any < ny; ay = _mm256_add_ps(ay, dy256), any++) {
      __m256 ax = minax;
      for (unsigned int anx = 0; anx < nx; ax = _mm256_add_ps(ax, dx256), anx += 8) {
        _mm256_storeu_si256((__m256i *)(buffer+any*nx+anx), kernel(ax, ay));
      }
    }
    auto end = std::chrono::steady_clock::now();
    std::cout << "Elapsed time in microseconds : " 
              << std::chrono::duration_cast<std::chrono::microseconds>(end - start).count()
              << " us" << std::endl;
  }
}
