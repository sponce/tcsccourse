#include <cstdlib>
#include <vector>

extern "C" 
{
  
void mandel(int* buffer,
             const float minx,
             const float dx,
             const unsigned int nx,
             const float miny,
             const float dy,
             const unsigned int ny);
}

int main() {
  constexpr unsigned int nx = 2560;
  constexpr unsigned int ny = 2000;
  constexpr float minx = -2.0;
  constexpr float maxx = 0.5;
  constexpr float miny = -1.0;
  constexpr float maxy = 1.0;
  auto dx = (maxx - minx) / nx;
  auto dy = (maxy - miny) / ny;
  std::vector<int> buf;
  buf.reserve(nx*ny);
  mandel(buf.data(), minx, dx, nx, miny, dy, ny);
}
