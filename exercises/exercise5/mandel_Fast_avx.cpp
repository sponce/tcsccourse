#include <chrono>
#include <iostream>
#include <vectorclass.h>

#define N 2
#include "array.h"

extern "C" {

  Array<Vec8i> kernel(Array<Vec8f> ax, Vec8f ay) {
    // future results, all to -1 initially
    Array<Vec8i> res{-1};
    // remember which pixels are already diverging
    Array<Vec8fb> cmp{false};

    // --------- begin of part you have to work on
    Vec8f x{0};
    Vec8f y{0};
    for (int n = 1; n <= 100; n++) {
      auto newx = x*x - y*y + ax;
      auto newy = x*y*2 + ay;
      // --------- end of part you have to work on

      // comparison now gives a vector of booleans
      Array<Vec8fb> newcmp = (newx*newx + newy*newy > 4);
      // gather new items that reached divergence
      for (int i = 0; i < N; i++) {
        if (horizontal_or(newcmp[i])) {
          // we fill res for the pixels reaching divergence on this round
          res[i] = select(andnot(newcmp[i], cmp[i]), Vec8i{n}, res[i]);
          // and update our map of all pixels already diverging
          cmp[i] = cmp[i] | newcmp[i];
        }
      }
      // no need to continue if all pixels are over
      if (newcmp.horizontal_and()) {
        break;
      }
      x = newx;
      y = newy;
    }
    return res;
  }
  
  void mandel(int* buffer,
              const float minx,
              const float dx,
              const unsigned int nx,
              const float miny,
              const float dy,
              const unsigned int ny) {
    auto start = std::chrono::steady_clock::now();
    Vec8f ay{miny}; // vector of 8 x miny value
    // loop one pixel at a time in y
    for (unsigned int any = 0; any < ny; ay += dy, any++) {
      // first N vectors of 8 pixels in x
      Array<Vec8f> ax{minx, dx};
      // loop 8*N by 8*N pixels in x
      for (unsigned int anx = 0; anx < nx; anx += N*8) {
        // compute and store back 8 pixels in one go
        auto res = kernel(ax, ay);
        res.store(buffer+any*nx+anx);
        ax += N*8*dx;
      }
    }
    auto end = std::chrono::steady_clock::now();
    std::cout << "Elapsed time in microseconds : " 
              << std::chrono::duration_cast<std::chrono::microseconds>(end - start).count()
              << " us" << std::endl;
  }
}
