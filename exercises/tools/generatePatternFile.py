#!/usr/bin/python
import sys

def usage(msg=None):
    if msg:
        print msg
    print "%s <fileName>" % (sys.argv[0])
    sys.exit(1)

if len(sys.argv) != 2:
    usage('Wrong nb of arguments')

f = open(sys.argv[1], 'w')
for i in range(10000):
    f.write(str(i))
    f.write(' ')
f.close()
