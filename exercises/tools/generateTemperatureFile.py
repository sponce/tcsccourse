#!/usr/bin/python
import sys, numpy, struct
from scipy.interpolate import interp1d

# the format of the data is binary with the following structure
#   - bytes 0 to 3 : nbCaptors
#   - bytes 4 to 4+4*nbCaptors : values for the first point of each captor (floats)
#   - repeated bunches of 4*nbCaptors bytes

def usage(msg=None):
    if msg:
        print msg
    print "%s <nbCaptors> <nbPoints> <fileName>" % (sys.argv[0])
    sys.exit(1)

if len(sys.argv) != 4:
    usage('Wrong nb of arguments')

try:
    nbCaptors = int(sys.argv[1])
    if nbCaptors < 1: raise ValueError
except ValueError:
    usage('Invalid nb of captors : "%s"' % sys.argv[1])

try:
    nbPoints = int(sys.argv[2])
    if nbPoints < 1: raise ValueError
except ValueError:
    usage('Invalid nb of points : "%s"' % sys.argv[2])

if nbPoints%20 != 0:
    nbPoints += (20-nbPoints%20)
    print "File will actually contain %d points" % nbPoints

f = open(sys.argv[3], 'w')
f.write(struct.pack('I', nbCaptors))
# create mean temperature of all captors for each 20th point
# we use a standard distribution centered on 20 with sigma of 5
sigma = 5
mean = 20
pointMeans = sigma * numpy.random.randn(nbPoints/20+1) + mean

# create captor shifts as a standard distribution centered
# on 0 and with a sigma of 5
captorsShift = sigma * numpy.random.randn(nbCaptors)

# create the values for each 20th point
# using a distribution centered on the pointMeans shifted
# by the captor shift and with a sigma of 5
distMeans = [[pointMeans[i]+captorsShift[j] for j in range(nbCaptors)] for i in range(nbPoints/20+1)]
values20 = sigma * numpy.random.randn(nbPoints/20+1, nbCaptors) + distMeans

# create interpolation functions for each captor
# based on the 20th values
interpol = [interp1d(range(0,nbPoints+20,20), [values20[i][p] for i in range(nbPoints/20+1)], kind='linear') for p in range(nbCaptors)]

# Compute all values by extrapolating the values of every 20th point
for i in range(nbPoints):
    f.write(struct.pack(str(nbCaptors)+'f', *[inter(i) for inter in interpol]))
f.close()
