#!/usr/bin/python
import sys, struct

# the format of the data is binary with the following structure
#   - bytes 0 to 3 : nbCaptors
#   - bytes 4 to 4+4*nbCaptors : values for the first point of each captor (floats)
#   - repeated bunches of 4*nbCaptors bytes

def usage(msg=None):
    if msg:
        print msg
    print "%s <fileName> [<pointNb>[,...]]" % (sys.argv[0])
    sys.exit(1)

if len(sys.argv) not in (2,3):
    usage('Wrong nb of arguments')

points = None
if len(sys.argv) == 3:
    try:
        points = [int(arg) for arg in sys.argv[2].split(',')]
    except ValueError:
        usage('Invalid point nb : "%s"' % sys.argv[2])

# open data file
f = None
try:
    f = open(sys.argv[1])
except IOError:
    print "File %s does not exist" % sys.argv[1]
    sys.exit(1)
nbCaptors = struct.unpack('I', f.read(4))[0]

# read data
n = 0
while True:
    rawData = f.read(4*nbCaptors)
    if not rawData:
        break
    if not points or n in points:
        data = struct.unpack(str(nbCaptors)+'f', rawData)
        print 'Captor %d : %s' % (n, str(data))
    n += 1

# check points validity
notExistingPoints = [p for p in points if p >= n]
if notExistingPoints:
    print 'WARNING : the following points did not exist :'
    print '  %s' % str(notExistingPoints)
