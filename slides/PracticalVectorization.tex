\include{header}

\usepackage{hyperref}
\usepackage{multicol}
\usepackage{minted}
\usepackage{fancyvrb}
\newcommand{\parnoteintercmd}{\hspace{0.2em plus 0.3em minus 0.2em}}
\usepackage{parnotes}

\setbeamersize{description width=1.2cm}

\newcommand\vectorP[5]{
  #5
  \draw[shift={(#3,#4)}] (0,0) grid (1,3);
  \foreach[count=\y] \coorda in #2 {
    \node at (#3+.5,#4+\y-.5) {\footnotesize $#1_{\coorda}$};
  }
}

\newcommand\vectorF[5]{
  #5
  \draw[shift={(#3,#4)}] (0,0) grid (1,-4);
  \foreach[count=\y] \coorda in #2 {
    \node at (#3+.5,#4-\y+.5) {\footnotesize $#1_{\coorda}$};
  }
}

\newcommand\vectorG[5]{
  #5
  \draw[shift={(#3,#4)}] (0,0) grid (1,-4);
  \foreach[count=\y] \coorda in #2 {
    \node at (#3+.5,#4-\y+.5) {\footnotesize $#1^{\coorda}$};
  }
}

\newcommand\matrixT[3]{
  #3
  \draw[shift={(#1,#2)}] (0,0) grid (3,3);
  \foreach[count=\y] \coorda in {Z,Y,X} {
    \foreach[count=\x] \coordb in {X,Y,Z} {
      \node at (#1+\x-.5,#2+\y-.5) {\footnotesize $T_{\coorda\coordb}$};
    }
  }
}

\newcommand\profile[5]{
  \begin{scope}[shift={(#1,#2)}]
    \filldraw[fill=#4!#5!white] (0,0) -- (1,0) -- (3.4,2.4) -- (3.4,3.4) -- (2.4,3.4) -- (0,1) -- (0,0);
    \draw (0,0) rectangle (1,1);
    \draw (1,1) -- (3.4,3.4);
    \node at (.5,.5) {$#3$};
  \end{scope}
}

\title{Practical vectorization}
\author[S. Ponce]{S\'ebastien Ponce\\ \texttt{sebastien.ponce@cern.ch}}
\institute{CERN}
\date{tCSC May \the\year}

\AtBeginSection[] {
  \begin{frame}
    \frametitle{\insertsection}
    \tableofcontents[sectionstyle=show/shaded,subsectionstyle=show/show/hide]
  \end{frame}
}

%%%%%%%%%%%%%%
% The slides %
%%%%%%%%%%%%%%

\begin{document}

\begin{frame}
  \titlepage
\end{frame}

\begin{frame}
  \frametitle{Outline}
  \tableofcontents[sectionstyle=show,subsectionstyle=show]
\end{frame}

\section[Intro]{Introduction}

\begin{frame}
  \frametitle{Goal of this course}
  \begin{itemize}
  \item Make the theory concerning SIMD and vectorization more concrete
  \item Detail the impact of vectorization on your code
    \begin{itemize}
    \item on your data model
    \item on actual \cpp code
    \end{itemize}
  \item Give an idea of what to expect from vectorized code
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{SIMD - Single Instruction Multiple Data}
  \begin{block}{Concept}
    \begin{itemize}
    \item Run the same operation in parallel on multiple data
    \item Operation is as fast as in single data case
    \item The data leave in a ``vector''
    \end{itemize}  
  \end{block}  
  \begin{exampleblock}{Practically}
    \center
    \begin{tikzpicture}[scale=.6]
      \node (rect) at (-8,-2)[draw,thick,fill=orange!30!white] {\footnotesize $A$};
      \node at (-7,-2) {+};
      \node (rect) at (-6,-2)[draw,thick,fill=green!30!white] {\footnotesize $B$};
      \node at (-5,-2) {=};
      \node (rect) at (-4,-2)[draw,thick,fill=blue!30!white] {\footnotesize $R$};
      \draw[double,->,very thick] (-2.5,-2) -- (-1.5,-2);
      \vectorG{A}{{1,2,3,4}}{0}{0}{
        \fill[orange!30!white] (0,0) rectangle (1,-4);
      }
      \node at (1.5,-2) {+};
      \vectorG{B}{{1,2,3,4}}{2}{0}{
        \fill[green!30!white] (2,0) rectangle (3,-4);
      }
      \node at (3.5,-2) {=};
      \vectorG{R}{{1,2,3,4}}{4}{0}{
        \fill[blue!30!white] (4,0) rectangle (5,-4);
      }
    \end{tikzpicture}
  \end{exampleblock}
\end{frame}

\begin{frame}
  \frametitle{Promises of vectorization}
  \begin{block}{Theoretical gains}
    \begin{itemize}
    \item Computation speed up corresponding to vector width
    \item Note that it's dependant on the type of data
      \begin{itemize}
      \item float vs double
      \item shorts versus ints
      \end{itemize}
    \end{itemize}
  \end{block}
  \begin{block}{Various units for various vector width}
    \center
    \begin{tabular}{|l|c|c|c|c|}
      \hline
      Name & Arch & nb bits & nb floats/int & nb doubles/long \\
      \hline
      SSE\parnote{Streaming SIMD Extensions} 4 & X86 & 128 & 4 & 2 \\
      AVX\parnote{Advanced Vector eXtension} & X86 & 256 & 8 & 4 \\
      AVX\textsuperscript{2} 2 (FMA) & X86 & 256 & 8 & 4 \\
      AVX\textsuperscript{2} 512 & X86 & 512 & 16 & 8 \\
      SVE\parnote{Scalable Vector Extension} & ARM & 128-2048 & 4-64 & 2-32 \\
      \hline
    \end{tabular}\par
    \parnotes
  \end{block}
\end{frame}

\begin{frame}[fragile]
  \frametitle{How to now what you can use}
  \begin{block}{Manually}
    Look for sse, avx, etc in your processor flags
    \vspace{.2cm}
    \begin{Verbatim}[commandchars=\\\{\}]
  \textcolor{green!60!black}{lscpu} | egrep ``mmx|sse|avx''
  Flags: fpu vme de pse tsc msr pae mce cx8 apic sep
         mtrr pge mca cmov pat pse36 clflush dts acpi
         \textcolor{red!80!black}{mmx} fxsr \textcolor{red!80!black}{sse sse2} ss ht tm pbe syscall nx
         rdtscp lm constant_tsc arch_perfmon pebs bts
         rep_good nopl xtopology nonstop_tsc cpuid
         aperfmperf pni pclmulqdq dtes64 monitor ds_cpl
         vmx smx est tm2 \textcolor{red!80!black}{ssse3} cx16 xtpr pdcm pcid
         \textcolor{red!80!black}{sse4_1 sse4_2} x2apic popcnt tsc_deadline_timer
         aes xsave \textcolor{red!80!black}{avx} f16c rdrand lahf_lm cpuid_fault
         epb pti ibrs ibpb stibp tpr_shadow vnmi
         flexpriority ept vpid fsgsbase smep erms
         xsaveopt dtherm ida arat pln pts
    \end{Verbatim}
  \end{block}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Situation for Intel processors}
  \center
  \includegraphics[scale=0.35]{SIMD.png}
\end{frame}

\section[Measure]{Measuring vectorization}

\begin{frame}
  \frametitle{Am I using vector registers ?}
  \pause
  \begin{block}{Yes you are}
    \begin{itemize}
    \item As vector registers are used for scalar operations
    \item But not so efficiently
      \center
      \begin{tikzpicture}
        \draw[fill=red!20] (0,0) rectangle node {Wasted} (7.5,1);
        \draw[fill=green!20] (7.5,0) rectangle node[rotate=90] {Used} (8,1);
      \end{tikzpicture}
    \end{itemize}
  \end{block}
  \pause
  \begin{block}{Am I efficiently using vector registers ?}
    \begin{itemize}
    \item Here we have to look at the generated assembly code
    \item Looking for specific intructions
    \item Or for the use of specific names of registers
    \end{itemize}
  \end{block}
\end{frame}

\begin{frame}
  \frametitle{Side note : what to look at ?}
  \begin{alertblock}{What you should look at}
    \begin{itemize}
    \item Specific, CPU intensive pieces of code
    \item The most time consuming functions
    \item Very small subset of your code (often $<$ 5\%)
    \end{itemize}
  \end{alertblock}
  \begin{exampleblock}{Where you should not waste your time}
    \begin{itemize}
    \item Try to have an overall picture of vectorization in your application
    \item As most of the code won't use vectors anyway
    \end{itemize}
  \end{exampleblock}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Crash course in SIMD assembly}
  \begin{block}{Register names}
    \begin{itemize}
    \item SSE : xmm0 to xmm15 (128 bits)
    \item AVX2 : ymm0 to ymm15 (256 bits)
    \item AVX512 : zmm0 to zmm31 (512 bits)
    \end{itemize}
    In scalar mode, SSE registers are used
  \end{block}
  \begin{block}{floating point instruction names}
    \begin{verbatim}
      <op><simd or not><raw type>
    \end{verbatim}%
    \vspace{-1cm}
    where
    \begin{itemize}
    \item \verb|<op>| is something like {\texttt vmul}, {\texttt vadd}, {\texttt vmov} or {\texttt vfmadd}
    \item \verb|<simd or not>| is either 's' for scalar or 'p' for packed (i.e. vector)
    \item \verb|<raw type>| is either 's' for single precision or 'd' for double precision
    \end{itemize}
    Typically :
    \begin{minted}{as}
      vmulss, vmovaps, vaddpd, vfmaddpd
    \end{minted}
  \end{block}
\end{frame}

\begin{frame}
  \frametitle{Practical look at assembly}
  \begin{block}{Extract assembly code}
    \begin{itemize}
    \item Run \textcolor{green!60!black}{{\texttt objdump -d -C}} on your executable or library
    \item Search for your function name
    \end{itemize}
  \end{block}
  \pause
  \begin{block}{Check for vectorization}
    \begin{itemize}
    \item For avx2, look for ymm
    \item For avx512, look for zmm
    \item Otherwise look for instructions with ps or pd at the end
      \begin{itemize}
      \item but ignore mov operations
      \item only concentrate on arithmetic ones
      \end{itemize}
    \end{itemize}
  \end{block}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Exercise 1}
  \begin{exampleblock}{Code}
    \begin{minted}{objdump}
     d18:  c5 fc 59 d8      vmulps %ymm0,%ymm0,%ymm3
     d1c:  c5 fc 58 c0      vaddps %ymm0,%ymm0,%ymm0
     d20:  c5 e4 5c de      vsubps %ymm6,%ymm3,%ymm3
     d24:  c4 c1 7c 59 c0   vmulps %ymm8,%ymm0,%ymm0
     d29:  c4 c1 64 58 da   vaddps %ymm10,%ymm3,%ymm3
     d2e:  c4 41 7c 58 c3   vaddps %ymm11,%ymm0,%ymm8
     d33:  c5 e4 59 d3      vmulps %ymm3,%ymm3,%ymm2
     d37:  c4 c1 3c 59 f0   vmulps %ymm8,%ymm8,%ymm6
     d3c:  c5 ec 58 d6      vaddps %ymm6,%ymm2,%ymm2
    \end{minted}
  \end{exampleblock}
  \pause
  \begin{block}{Solution}
    \begin{itemize}
    \item Presence of ymm
    \item Vectorized, AVX level
    \end{itemize}
    \end{block}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Exercise 2}
  \begin{exampleblock}{Code}
    \begin{minted}{objdump}
     b97:  0f 28 e5         movaps %xmm5,%xmm4
     b9a:  f3 0f 59 e5      mulss  %xmm5,%xmm4
     b9e:  f3 0f 58 ed      addss  %xmm5,%xmm5
     ba2:  f3 0f 59 ee      mulss  %xmm6,%xmm5
     ba6:  f3 0f 5c e7      subss  %xmm7,%xmm4
     baa:  0f 28 f5         movaps %xmm5,%xmm6
     bad:  f3 41 0f 58 e0   addss  %xmm8,%xmm4
     bb2:  f3 0f 58 f2      addss  %xmm2,%xmm6
     bb6:  0f 28 ec         movaps %xmm4,%xmm5
    \end{minted}
  \end{exampleblock}
  \pause
  \begin{block}{Solution}
    \begin{itemize}
    \item Presence of xmm but ps only in mov
    \item Not vectorized
    \end{itemize}
    \end{block}
\end{frame}

\begin{frame}
  \frametitle{For small pieces of code : \href{http://godbolt.org}{godbolt}}
  \begin{block}{what is it}
    \begin{itemize}
    \item Online, on the fly compilation
    \item Annotated, colorized assembler
    \item Supports many platforms and compilers
    \end{itemize}
  \end{block}
  \pause
  \includegraphics[trim={0 3cm 0 0},clip,width=\textwidth]{godbolt.png}
\end{frame}

\section[Prereq]{Vectorization Prerequisite}

\begin{frame}
  \frametitle{Data format for vectorization}
  \begin{block}{Some constraints}
    \begin{itemize}
    \item Loading/storing a vector from/to non contiguous data is very inefficient
      \begin{itemize}
      \item even worse than n data loads
      \end{itemize}
    \item Converting data format is also expensive
      \begin{itemize}
      \item and will ruin your vectorization gains
      \end{itemize}
    \item So you need proper data format from scratch
    \end{itemize}
  \end{block}
  \begin{alertblock}{Which data layout to choose ?}
    \begin{itemize}
    \item Depends on your algorithm
    \item Also depends on your CPU !
    \end{itemize}
  \end{alertblock}
\end{frame}

\begin{frame}
  \frametitle{First Vectorization Attempt}
  Simple, standard matrix times vector \visible<2-|handout:1>{ - Let's adopt a row first storage}
  \begin{center}
    \begin{tikzpicture}[scale=0.6]
      \vectorP{V}{{Z,Y,X}}{-2}{0}{}
      \node at (0,1.5) {$=$};
      \matrixT{1}{0}{
        \fill<2-|handout:1>[blue!30!white] (1,0) rectangle (4,1);
        \fill<2-|handout:1>[red!30!white] (1,1) rectangle (4,2);
        \fill<2-|handout:1>[green!30!white] (1,2) rectangle (4,3);
      }
      \node at (4.5,1.5) {$\cdot$};
      \vectorP{P}{{Z,Y,X}}{5}{0}{
        \fill<2-|handout:1>[orange!30!white] (5,0) rectangle (6,3);
      }
      \node at (7,1.5) {$=$};
      \fill<2-|handout:1>[green!15!white] (8,2) rectangle (15,3);
      \fill<2-|handout:1>[blue!15!white] (8,1) rectangle (15,2);
      \fill<2-|handout:1>[red!15!white] (8,0) rectangle (15,1);
      \draw[shift={(8,0)},xstep=7] (0,0) grid (7,3);
      \node at (11.5,2.5) {\footnotesize $T_{XX}.P_x+T_{XY}.P_Y+T_{XZ}.P_Z$};
      \node at (11.5,1.5) {\footnotesize $T_{YX}.P_x+T_{YY}.P_Y+T_{YZ}.P_Z$};
      \node at (11.5,.5) {\footnotesize $T_{ZX}.P_x+T_{ZY}.P_Y+T_{ZZ}.P_Z$};
    \end{tikzpicture}
  \end{center}
  \pause \pause
  Will actually be something like :
  \begin{center}
    \begin{tikzpicture}[scale=0.6]
      \matrixT{1}{0}{
        \fill[blue!30!white] (1,0) rectangle (4,1);
        \fill[red!30!white] (1,1) rectangle (4,2);
        \fill[green!30!white] (1,2) rectangle (4,3);
      }
      \node(mul) at (4.5,1.5) {$\cdot$};
      \vectorP{P}{{Z,Y,X}}{5}{0}{
        \fill[orange!30!white] (5,0) rectangle (6,3);
      }
      \node at (6.5,1.5) {$=$};
      \fill[green!15!white] (7,2.2) rectangle (13,3);
      \draw[shift={(7,2.2)},xstep=2,ystep=0.8] (0,0) grid (6,.8);
      \node at (8,2.6) {\footnotesize $T_{XX}.P_x$};
      \node at (10,2.6) {\footnotesize $T_{XY}.P_Y$};
      \node at (12,2.6) {\footnotesize $T_{XZ}.P_Z$};
      \fill[red!15!white] (7,1.1) rectangle (13,1.9);
      \draw[shift={(7,1.1)},xstep=2,ystep=0.8] (0,0) grid (6,.8);
      \node at (8,1.5) {\footnotesize $T_{yX}.P_x$};
      \node at (10,1.5) {\footnotesize $T_{yY}.P_Y$};
      \node at (12,1.5) {\footnotesize $T_{yZ}.P_Z$};
      \fill[blue!15!white] (7,0) rectangle (13,.8);
      \draw[shift={(7,0)},xstep=2,ystep=0.8] (0,0) grid (6,.8);
      \node at (8,.4) {\footnotesize $T_{zX}.P_x$};
      \node at (10,.4) {\footnotesize $T_{zY}.P_Y$};
      \node at (12,.4) {\footnotesize $T_{zZ}.P_Z$};
      \draw[double,->,very thick] (13.5,1.5) -- (14.5,1.5);
      \vectorP{V}{{Z,Y,X}}{15}{0}{}
      \visible<4-|handout:1> {
        \node(mulcost) at (4.5,-1) {3 mul $\sim$ 6 cycles};
        \draw[thick,->] (mulcost) -> (mul);
        \node(shufflecost) at (13.5,-1) {6 hadd, 3 mov $\sim$ 60 cycles};
        \draw[thick,->] (shufflecost) -> (14,1);
      }
    \end{tikzpicture}
  \end{center}
  \pause \pause
  Scalar case : 9 mul, 6 adds $\sim$ 30 cycles
\end{frame}

\begin{frame}
  \frametitle{Second Vectorization Attempt}
  Let's adopt a column first storage and use Broadcast
  \begin{center}
    \begin{tikzpicture}[scale=0.6]
      \matrixT{1}{0}{
        \fill[blue!30!white] (1,0) rectangle (2,3);
        \fill[red!30!white] (2,0) rectangle (3,3);
        \fill[green!30!white] (3,0) rectangle (4,3);
      }
      \node at (4.5,1.5) {$\cdot$};
      \vectorP{P}{{Z,Y,X}}{5}{0}{
        \fill[orange!30!white] (5,0) rectangle (6,3);
      }
      \node at (6.5,1.5) {$=$};
      \vectorP{T}{{ZX,YX,XX}}{7}{0}{
        \fill[blue!30!white] (7,0) rectangle (8,3);
      }
      \node at (8.5,1.5) {$\cdot$};
      \vectorP{P}{{X,X,X}}{9}{0}{
        \fill[orange!20!white] (9,0) rectangle (10,3);
      }
      \node at (10.5,1.5) {$+$};
      \vectorP{T}{{ZY,YY,XY}}{11}{0}{
        \fill[red!30!white] (11,0) rectangle (12,3);
      }
      \node at (12.5,1.5) {$\cdot$};
      \vectorP{P}{{Y,Y,Y}}{13}{0}{
        \fill[orange!20!white] (13,0) rectangle (14,3);
      }
      \node at (14.5,1.5) {$+$};
      \vectorP{T}{{ZZ,YZ,XZ}}{15}{0}{
        \fill[green!30!white] (15,0) rectangle (16,3);
      }
      \node at (16.5,1.5) {$\cdot$};
      \vectorP{P}{{Z,Z,Z}}{17}{0}{
        \fill[orange!20!white] (17,0) rectangle (18,3);
      }
    \end{tikzpicture}
  \end{center}
  \pause
  Costs :
  \begin{itemize}
  \item 3 broadcasts $\sim$ 3 cycles
  \item 3 mul and 2 adds $\sim$ 10 cycles
  \end{itemize}
  Twice better than scalar case !\\
  \visible<3-|handout:1>{Wait a minute... only twice ?!?}
\end{frame}

\begin{frame}
  \frametitle{Vertical vs Horizontal vectorization}
  \begin{alertblock}{Vertical vectorization}
    \begin{itemize}
    \item Previous attempts are examples of {\texttt vertical} vectorization
    \item They use parallelism within the given data
    \item Speedup is limited by data size
      \begin{itemize}
      \item was 3 numbers here, while our vector was 8 or 16 items long !
      \end{itemize}
    \end{itemize}
  \end{alertblock}
  \begin{exampleblock}{Horizontal vectorization}
    \begin{itemize}
    \item Aims at using parallelism between independent (but similar) computations
    \item In our example several (many ?) products Matrix by Vector
    \item Allows to fully use the vector sizes
    \end{itemize}
  \end{exampleblock}
\end{frame}

\begin{frame}
  \frametitle{Horizontal vectorization example}
  Let's compute n products in one go (n = 4 on the picture)\\
  Let's have vectors in the new dimension, one color = one vector
  \begin{center}
    \begin{tikzpicture}[scale=0.5,font=\footnotesize]
      \foreach \N in {3,2,1,0} {
        \foreach \P in {10,20,30} {
          \fill[blue!\P!white] (.8*\N+\P/10,.8*\N+0) rectangle (.8*\N+\P/10+1,.8*\N+1);
          \fill[red!\P!white] (.8*\N+\P/10,.8*\N+1) rectangle (.8*\N+\P/10+1,.8*\N+2);
          \fill[green!\P!white] (.8*\N+\P/10,.8*\N+2) rectangle (.8*\N+\P/10+1,.8*\N+3);
        }
        \draw[shift={(.8*\N,.8*\N)}] (1,0) grid (4,3);
        \foreach[count=\y] \coorda in {Z,Y,X} {
          \foreach[count=\x] \coordb in {X,Y,Z} {
            \node at (.8*\N+\x+.5,.8*\N+\y-.5) {$T_{\coorda\coordb}^{\N}$};
          }
        }
      }
      \node at (7,2.5) {$\cdot$};
      \foreach \N in {3,2,1,0} {
        \foreach \P in {10,20,30} {
          \fill[orange!\P!white] (.8*\N+7.5,.8*\N+\P/10-1) rectangle (.8*\N+8.5,.8*\N+\P/10);
        }
        \draw[shift={(.8*\N-.5,.8*\N)}] (8,0) grid (9,3);
        \foreach[count=\y] \coorda in {Z,Y,X} {
          \node at (.8*\N+8,.8*\N+\y-.5) {$P_{\coorda}^{\N}$};
        }
      }
      \node at (11.5,2.5) {$=$};
      \foreach \y/\coorda/\color/\int in {0/Z/blue/10,1/Y/red/20,2/X/green/30} {
        \foreach \x/\coordb/\intensity in {0/X/10,1/Y/20,2/Z/30} {
          \profile{12+3.5*\x}{1.5*\y}{T_{\coorda\coordb}}{\color}{\intensity};
          \node at (13.25+3.5*\x,.5+1.5*\y) {$\cdot$};
          \profile{13.5+3.5*\x}{1.5*\y}{P_{\coordb}}{orange}{\intensity};
        }
        \node at (15,.5+1.5*\y) {$+$};
        \node at (18.5,.5+1.5*\y) {$+$};
      }
    \end{tikzpicture}  
  \end{center}
  We compute as if we were scalar, using vectors to do n at a time\\
  \pause
  Cost :
  \begin{itemize}
  \item 9 mul + 6 add $\sim$ 30 cycles for n products
  \item n is typically 8 or 16 $ \to $ 4 to 2 cycles per product
  \item Perfect speedup !
  \end{itemize}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Vertical vectorization allows AoS}
  \begin{block}{AoS = {\bf A}rray {\bf o}f {\bf S}tructures}
    Basically you use standard structures\\
    So you can write very natural code :
    \begin{minted}[gobble=2]{cpp}
      struct Vector { float x; float y; float z; };
      using Matrix = std::array<float, 12>; // padded
      std::array<Vector,N> Ps = ...;
      std::array<Matrix,N> Ts = ...;
      auto V0 = multiply(Ts[0], Ps[0]);
    \end{minted}
  \end{block}
  \begin{columns}
    \begin{column}{.6\textwidth}
      \begin{alertblock}{Drawback}
        \begin{itemize}
        \item It does not scale with vector width
        \item It needs adaption of your math code
          \begin{itemize}
          \item a dedicated, vectorized multiply method
          \end{itemize}
        \end{itemize}
      \end{alertblock}
    \end{column}
    \begin{column}{.3\textwidth}
      \begin{tikzpicture}[scale=.6]
        \vectorF{A^1}{{X,Y,Z}}{0}{0}{
          \fill[orange!30!white] (0,0) rectangle (1,-4);
        }
        \node at (1.5,-2) {+};
        \vectorF{B^1}{{X,Y,Z}}{2}{0}{
          \fill[green!30!white] (2,0) rectangle (3,-4);
        }
        \node at (3.5,-2) {=};
        \vectorF{R^1}{{X,Y,Z}}{4}{0}{
          \fill[blue!30!white] (4,0) rectangle (5,-4);
        }
      \end{tikzpicture}
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Horizontal vectorization requires SoA}
  \begin{block}{SoA = {\bf S}tructures {\bf o}f {\bf A}rray}
    That is standard structures where each element became a vector\\
    Thus you loose the concept of elements
    \begin{minted}{cpp}
      using floats = std::array<float,N>;
      struct Vectors { floats xs, ys, zs; };
      using Matrices = std::array<floats, 9>;
      Vectors Ps = ...;
      Matrices Ts = ...;
      auto Vs = multiply(Ts, Ps);
      // no Ts[0] or Ps[0]
    \end{minted}
  \end{block}
  \begin{columns}
    \begin{column}{.6\textwidth}
      \begin{exampleblock}{Advantages}
        \begin{itemize}
        \item Scales perfectly with vector width
        \item Code similar (identical ?) to scalar one
        \end{itemize}
      \end{exampleblock}
    \end{column}
    \begin{column}{.3\textwidth}
      \begin{tikzpicture}[scale=.6]
        \vectorG{A_X}{{1,2,3,4}}{0}{0}{
          \fill[orange!30!white] (0,0) rectangle (1,-4);
        }
        \node at (1.5,-2) {+};
        \vectorG{B_X}{{1,2,3,4}}{2}{0}{
          \fill[green!30!white] (2,0) rectangle (3,-4);
        }
        \node at (3.5,-2) {=};
        \vectorG{R_X}{{1,2,3,4}}{4}{0}{
          \fill[blue!30!white] (4,0) rectangle (5,-4);
        }
      \end{tikzpicture}
    \end{column}
  \end{columns}
\end{frame}

\section[Techniques]{Vectorizing techniques in C++}

\begin{frame}[fragile]
  \frametitle{Example Code}
  \begin{block}{Mandelbrot kernel}
    Given $(ax, ay)$ a point in 2D space, compute $n$ :
    \small
    \begin{minted}[gobble=4]{cpp}
      int kernel(float ax, float ay) {
        float x = 0; float y = 0;
        for (int n = 1; n <= 100; n++) {
          float newx = x*x - y*y + ax;
          float newy = 2*x*y + ay;
          if (4 < newx*newx + newy*newy) {
            return n;
          }
          x = newx; y = newy;
        }
        return -1;
      }
    \end{minted}
  \end{block}
  \begin{tikzpicture}[remember picture,overlay]
    \node[xshift=-2.5cm,yshift=5cm] at (current page.south east) {\includegraphics[width=3.5cm]{mandel.png}};
  \end{tikzpicture}
\end{frame}

\subsection[Auto]{Autovectorization}

\begin{frame}
  \frametitle{Why is autovectorization not so easy ?}
  \begin{block}{Summary of previous slides}
    Main issues with autovectorization :
    \begin{itemize}
    \item Aliasing, alignment, data dependencies, branching, ...
    \item In general lack of knowledge of the compiler
    \end{itemize}
    Ways to solve (some of) them
    \begin{itemize}
    \item {\texttt restrict}, {\texttt align}, ternary operator, ... aka give knowledge to compiler
    \item And proper data structures (SoA)
    \end{itemize}
  \end{block}
  \begin{exampleblock}{Still worth trying it}
    \begin{itemize}
    \item It's (almost) a free lunch !
    \item 100\% portable code
    \item No dependencies
    \end{itemize}
  \end{exampleblock}
\end{frame}

\begin{frame}
  \frametitle{How to autovectorize ?}
  \begin{block}{Compiler flags}
    \begin{itemize}
    \item Optimization ones
      \begin{itemize}
      \item For gcc, clang : {\texttt -O3} or {\texttt -O2} {\texttt -ftree-vectorize}
      \item For icc : {\texttt -O2} or {\texttt -O3}. Use {\texttt -no-vec to disable it}
      \end{itemize}
    \item Architecture ones
      \begin{itemize}
      \item For avx2 : {\texttt -mavx2} on gcc/clang, {\texttt -axAVX2 -xAVX2} on icc
      \item For avx512 on gcc/clang : {\texttt -march=skylake-avx512}
      \item For avx512 on icc: {\texttt -xCORE-AVX512}
      \item For optimal vectorization depending on your CPU :\\ {\texttt -march=native} on gcc/clang, {\texttt -xHOST} on icc
      \end{itemize}
    \end{itemize}
  \end{block}
\end{frame}

\begin{frame}
  \frametitle{How to debug autovectorization}
  Ask the compiler about its choices
  \begin{block}{For icc}
    \begin{itemize}
    \item Use {\texttt -vec-report=5} to ``tell the vectorizer to report on non-vectorized loops and the reason why they were not vectorized''
    \end{itemize}
  \end{block}
  \begin{block}{For clang}
    \begin{itemize}
    \item Use {\texttt -Rpass-missed=loop-vectorize} to ``identify loops that failed to vectorize''
    \item Use {\texttt -Rpass-analysis=loop-vectorize} to ``show the statements that caused the vectorization to fail''
    \end{itemize}
  \end{block}
  \begin{block}{For gcc}
    \begin{itemize}
    \item Use {\texttt -fopt-info-vec-missed} to get ``detailed info about loops not being vectorized''
    \end{itemize}
  \end{block}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Autovectorizing Mandelbrot}
  \begin{block}{code}
    \begin{minted}[gobble=4,linenos,firstnumber=8,numbersep=-5pt]{cpp}
      int kernel(float ax, float ay) {
        float x = 0; float y = 0;
        for (int n = 1; n <= 100; n++) {
          float newx = x*x - y*y + ax;
          float newy = 2*x*y + ay;
          if (4 < newx*newx + newy*newy) {
    \end{minted}
  \end{block}
  \begin{block}{compiler output (gcc)}
    \small
    \begin{minted}[autogobble]{shell}
    ...
    mandel.cpp:10:21: note: not vectorized: control flow in loop.
    mandel.cpp:10:21: note: bad loop form.
    ...
    \end{minted}
  \end{block}
\end{frame}

\begin{frame}
  \frametitle{For small code, godbolt is again your friend}
  \begin{itemize}
  \item Use clang, click ``Add new...'' in assembly pane
  \item Select ``optimization output''
  \item Move mouse on the side of the interesting code
  \end{itemize}
  \includegraphics[width=\textwidth]{godboltMandel.png}
\end{frame}

\begin{frame}
  \frametitle{Autovectorization is still not enough}
  \begin{block}{It's not fully mature}
    \begin{itemize}
    \item Still very touchy despite improvements
    \item Only able to vectorize loops (or almost)
    \item Hardly able to handle branching via masks
    \item No abstract knowledge of the application (yet ?)
    \end{itemize}
  \end{block}
  \begin{block}{It will probably never be good enough}
    \begin{itemize}
    \item As it cannot know as much as the developer
    \item Especially concerning input data such as
      \begin{itemize}
      \item average number of tracks reconstructed
      \item average energy in that data sample
      \end{itemize}
    \item And the optimal code depends on this
    \end{itemize}
  \end{block}
  So we need to vectorize by hand from time to time
\end{frame}

\subsection[asm]{Inline assembly}

\begin{frame}
  \frametitle{Why inline assembly should not be used}
  \begin{itemize}
  \item Hard to write/read
  \item Not portable at all
    \begin{itemize}
    \item processor specific AND compiler specific
    \end{itemize}
  \item Almost completely superseeded by intrinsics
  \end{itemize}
  So just don't do it
\end{frame}

\subsection{Intrinsics}

\begin{frame}
  \frametitle{The intrinsics layer}
  \begin{block}{Principles}
    \begin{itemize}
    \item Intrinsics are functions that the compiler replaces with the proper assembly instructions
    \item It hides nasty assembly code but maps 1 to 1 to SIMD assembly instructions
    \end{itemize}
  \end{block}
  \begin{exampleblock}{Pros}
    \begin{itemize}
    \item Easy to use
    \item Full power of SIMD can be achieved
    \end{itemize}  
  \end{exampleblock}
  \begin{alertblock}{Cons}
    \begin{itemize}
    \item Very verbose, very low level
    \item Processor specific
    \end{itemize}  
  \end{alertblock}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Intrinsics crash course}
  naming convention :
  \begin{verbatim}
    _mm<S><mask>_<op>_<suffix>(data_type param1, ...)
  \end{verbatim}
  where
  \begin{itemize}
  \item \verb|<S>| is empty for SSE, 256 for AVX2 and 512 for AVX512
  \item \verb|<mask>| is empty or {\texttt \_mask} or {\texttt \_maskz} (AVX512 only)
  \item \verb|<op>| is the operator ({\texttt add}, {\texttt mul}, ...)
  \item \verb|<suffix>| describes the data in the vector
  \end{itemize}
  Example :\\
  \verb|_mm256_mul_ps|, \verb|_mm512_maskz_add_pd| \\
  \small
  \vfill
  see \url{https://software.intel.com/sites/landingpage/IntrinsicsGuide}
\end{frame}

\begin{frame}
  \begin{tikzpicture}[remember picture,overlay]
    \node[xshift=5.9cm, yshift=-1cm] {
      \includegraphics[width=\paperwidth]{IntelIntrinsics.png}
    };
  \end{tikzpicture}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Practically for Mandelbrot}
  \small
  \begin{block}{Code}
    \begin{minted}[gobble=5]{cpp}
      __m256i kernel(__m256 ax, __m256 ay) {
        __m256 x = _mm256_setzero_ps();
        __m256 y = _mm256_setzero_ps();
        for (int n = 1; n <= 100; n++) {
          __m256 newx = _mm256_add_ps
          (_mm256_sub_ps(_mm256_mul_ps(x,x), _mm256_mul_ps(y,y)), ax);
          __m256 newy = _mm256_add_ps
          (_mm256_mul_ps(two,_mm256_mul_ps(x,y)), ay);
          __m256 norm = _mm256_add_ps(_mm256_mul_ps(newx, newx),
          _mm256_mul_ps(newy, newy));
          __m256 cmpmask = _mm256_cmp_ps(four, norm, _CMP_LT_OS);
  \end{minted}
  \end{block}
  \begin{alertblock}{}
    \begin{itemize}
    \item A bit too verbose to my taste !
    \item Hard to understand what's going on
    \end{itemize}
  \end{alertblock}
\end{frame}

\subsection[Compiler]{Compiler extensions}

\begin{frame}
  \frametitle{Vector compiler extensions}
  \begin{block}{Principle}
    \begin{itemize}
    \item Compiler extended syntax to write SIMD code
    \item Compiler specific, mostly (clang and gcc are close)
    \item Allows to use vector types naturally
    \end{itemize}
  \end{block}
  \begin{exampleblock}{Pros}
    \begin{itemize}
    \item Easy to use
    \item (Almost) independent of processor
    \end{itemize}  
  \end{exampleblock}
  \begin{alertblock}{Cons}
    \begin{itemize}
    \item Limited instruction set
    \item Compiler specific
    \end{itemize}  
  \end{alertblock}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Practically for Mandelbrot}
  \small
  \begin{block}{Code}
    \begin{minted}[gobble=4]{cpp}
      typedef float Vec8f __attribute__ ((vector_size (32)));
      typedef int Vec8i __attribute__ ((vector_size (32)));
      Vec8i kernel(Vec8f ax, Vec8f ay) {
        Vec8f x{0};
        Vec8f y{0};
        for (int n = 1; n <= 100; n++) {
          Vec8f newx = x*x - y*y + ax;
          Vec8f newy = 2*x*y + ay;
          Vec8i cmpmask = (4 < newx*newx + newy*newy);
    \end{minted}
  \end{block}
  \begin{exampleblock}{}
    \begin{itemize}
    \item Syntax very close to scalar case
    \item Only change : the comparison is returning a mask rather than a boolean
    \end{itemize}
  \end{exampleblock}
\end{frame}


\subsection{Libraries}

\begin{frame}
  \frametitle{The library way}
  \begin{block}{Expectations}
    \begin{itemize}
    \item Write compiler agnostic code
    \item With natural syntax, a la compiler extensions
    \item Evolve with technologies without modifying the code
    \end{itemize}
  \end{block}
  \begin{block}{Many available libraries}
    {\center {\bf VC}, {\bf xSIMD}, {\bf VCL}, {\bf UME::SIMD}, {\bf E.V.E.}, {\bf VecCore}, ...}
    \begin{itemize}
    \item a proposal has been made to \cpp standard comitee
      \begin{itemize}
      \item vector support is coming in the standard (\href{https://en.cppreference.com/w/cpp/experimental/simd}{\color{blue}cppref})
      \end{itemize}
    \item {\bf VCL} is header only library, so easy to use
    \item {\bf E.V.E} has support for AVX512 and masked operations
    \item {\bf VecCore} is an attempt to rule them all
      \begin{itemize}
      \item basically a common wrapper on top on the rest
      \end{itemize}
    \end{itemize}
  \end{block}
\end{frame}

\begin{frame}[fragile]
  \frametitle{VCL - Practically for Mandelbrot}
  \small
  \begin{block}{Code}
    \begin{minted}[gobble=4]{cpp}
      #include <vectorclass.h>
      Vec8i kernel(Vec8f ax, Vec8f ay) {
        Vec8f x{0};
        Vec8f y{0};
        for (int n = 1; n <= 100; n++) {
          Vec8f newx = x*x - y*y + ax;
          Vec8f newy = 2*x*y + ay;
          Vec8fb newcmp = (4 < newx*newx + newy*newy);
    \end{minted}
  \end{block}
  \begin{exampleblock}{}
    \begin{itemize}
    \item Code very close to vector extensions' one, but compiler agnostic
    \item Still using mask obviously
    \end{itemize}
  \end{exampleblock}
\end{frame}

\begin{frame}[fragile]
  \frametitle{STL - Practically for Mandelbrot}
  \small
  \begin{block}{Code}
    \begin{minted}[gobble=4]{cpp}
      #include <experimental/simd>
      namespace stdx = std::experimental;
      using float_v = stdx::native_simd<float>;
      using int_v = stdx::fixed_size_simd<int, float_v::size()>;
      int_v kernel(float_v ax, float_v ay) {
        float_v x(0);
        float_v y(0);
        for (int n = 1; n <= 100; n++) {
          float_v newx = x*x - y*y + ax;
          float_v newy = 2*x*y + ay;
          auto newcmp = 4 < newx*newx + newy*newy;
    \end{minted}
  \end{block}
  \begin{exampleblock}{}
    \begin{itemize}
    \item Note that the code is vector width agnostic this time !
    \item Still experimental and needs polishing, gcc only
    \end{itemize}
  \end{exampleblock}
\end{frame}

\section[Expectations]{What to expect ?}

\begin{frame}
  \frametitle{Amdahl strikes back}
  Remember the talk on parallelisation ? I revisited it slightly
  \begin{columns}
    \begin{column}{.45\textwidth}
      \begin{alertblock}{}
        Applies to a fixed size problem, speedup in terms of time
      \end{alertblock}
      \begin{exampleblock}{Maximum achievable speedup}
      {
        \center \scalebox{1.3}{
          $Speedup_{max} = \frac{1}{(1 - p) + \frac{p}{n}}$
        }
      }
      \begin{itemize}
      \item $p$ the vectorizable portion, $p \in [0,1]$
      \item $n$ vector width (floats)
      \end{itemize}
      \end{exampleblock}
    \end{column}
    \begin{column}{.5\textwidth}
      \begin{tikzpicture}[scale=0.65]
        \begin{semilogxaxis}[grid=major, xmin=1, xmax=1000, ymin=0, ymax=20, xlabel=vector width (nb floats), ylabel=Speedup, scale=1, log basis x=2, log ticks with fixed point, xtick={1,2,4,8,16,32,64,128,256,512}, domain=1:1024, ytick={0,2,...,20}, legend pos=north west]
          \addlegendimage{empty legend}
          \plot[blue] plot[smooth] expression{1/((1-.5)+.5/x))};
          \plot[red] plot[smooth] expression{1/((1-.75)+.75/x))};
          \plot[purple] plot[smooth] expression{1/((1-.9)+.9/x))};
          \plot[green!50!black] plot[smooth] expression{1/((1-.95)+.95/x))};
          \addlegendentry{\hspace{-.6cm}\textbf{Vectorizable part}}
          \addlegendentry{$50\%$}
          \addlegendentry{$75\%$}
          \addlegendentry{$90\%$}
          \addlegendentry{$95\%$}
        \end{semilogxaxis}
      \end{tikzpicture}
    \end{column}
  \end{columns}
  \vspace{4mm}
  “… the effort expended on achieving high parallel
  processing rates is wasted unless it is accompanied by
  achievements in sequential processing rates of very
  nearly the same magnitude.” - me, 2019
\end{frame}

\begin{frame}
  \frametitle{When does vectorization bring speed ?}
  \begin{exampleblock}{Vectorization will bring speed if}
    \begin{itemize}
    \item The code in computing intensive
    \item You have enough parallelism (think horizontal vectorization)
    \item The code has few branches
    \item The data have proper format (SoA)
    \end{itemize}
  \end{exampleblock}
  \begin{alertblock}{Vectorization won't necessarily work}
    \begin{itemize}
    \item If you do not have SoA and conversion is too costly
      \begin{itemize}
      \item you lose back what you won (or more ?)
      \end{itemize}
    \item For specific algorithms
      \begin{itemize}
      \item typically standard sorting algorithm (std::sort)
      \item for that case SoA is even to be avoided
      \end{itemize}
    \end{itemize}
  \end{alertblock}
  A matter of testing and experience. You'll be surprised for sure
\end{frame}

\begin{frame}
  \frametitle{A word on Vectorization and IO}
  \begin{block}{The Problem}
    \begin{itemize}
    \item When you optimize one piece, you put more pressure on the others
    \item Speeding up CPU may lead to memory bandwidth issues
    \end{itemize}
  \end{block}
  \begin{block}{Typical scenario}
    \begin{itemize}
    \item Suppose you get x16 speedup on your matrix - vector code
    \item So you'll use 16x more input data than you used to
    \item Do you have the memory bandwidth for that ?
    \end{itemize}
  \end{block}
\end{frame}

\begin{frame}
  \frametitle{Roofline Model}
  \begin{block}{Definition}
    Let's define for a given piece of code (aka kernel) :
    \begin{description}
    \item[$W$] work, number of operations performed
    \item[$Q$] memory traffic, number of bytes of memory transfers
    \item[$I=\frac{W}{Q}$] the arithmetic intensity
    \end{description}
    And let's define for a given hardware :
    \begin{description}
    \item[$\beta$] the peak bandwidth in bytes/s, usually obtained via benchmarks
    \item[$\pi$] the peak performance in flops/s, derived from the architecture
    \end{description}
    All this is plotted in a log-log graph of flops/s versus arithmetic intensity
  \end{block}
\end{frame}

\begin{frame}
  \frametitle{Roofline plot}
  \includegraphics[width=\textwidth]{Roofline.png}
  \begin{block}{}
    \begin{itemize}
    \item Shows what you can expect for a given arithmetic intensity
    \item And whether you are ultimately CPU or I/O bound
    \end{itemize}
  \end{block}
  {\tiny Source : Giu.natale \href{https://creativecommons.org/licenses/by-sa/4.0}{CC BY-SA 4.0}, via Wikimedia Commons}
\end{frame}

\begin{frame}
  \frametitle{Realistic Roofline plot}
  \begin{block}{}
    \begin{itemize}
    \item Multiple lines for different levels of caches
    \item Multiple lines for vectorization and FMA
    \end{itemize}
  \end{block}
  \begin{center}
    \includegraphics[width=9cm]{RealisticRoofline.png}
  \end{center}
  \vspace{-1cm}
  {\tiny Source : \href{https://www.researchgate.net/figure/depicts-a-Roofline-model-7-for-an-Intel-Haswell-processor-A-Roofline-model-relates-the_fig4_321234914}{researchgate.net} \href{https://creativecommons.org/licenses/by/3.0}{CC BY 3.0}}
\end{frame}

\begin{frame}
  \frametitle{Conclusion}
  \begin{exampleblock}{Key messages of the day}
    \begin{itemize}
    \item Vectorization requires a suitable data model, typically SoA
      \begin{itemize}
      \item And always prefer horizontal vectorization when you can
      \end{itemize}
    \item There are several ways to vectorize
      \begin{itemize}
      \item Check whether autovectorization works for you
      \item Otherwise choose between intrinsics, compiler extensions and libraries
      \end{itemize}
    \item Do not build wrong expectations on the overall speedup
      \begin{itemize}
      \item Amdahl's law is really stubborn
      \item And you may hit other limitations, like I/O
      \end{itemize}
    \end{itemize}
  \end{exampleblock}
\end{frame}

\end{document}
