\include{header}

\usepackage{hyperref}
\usepackage{multicol}
\usepackage{minted}

\title{Writing efficient code}
\author[S. Ponce]{S\'ebastien Ponce\\ \texttt{sebastien.ponce@cern.ch}}
\institute{CERN}
\date{tCSC June \the\year}

\AtBeginSection[] {
}
\AtBeginSubsection[] {
  \begin{frame}
    \frametitle{\insertsection}
    \tableofcontents[sectionstyle=show/shaded,subsectionstyle=show/shaded/hide]
  \end{frame}
}

\newcommand\classbox[3][]{
  \def\temp{#3}
  \ifx\temp\empty
    \draw[thick] node (#2) [#1]
         [rectangle,rounded corners,draw] {#2};
  \else
    \draw[thick] node (#2) [#1]
         [rectangle,rounded corners,draw] {
      \begin{tabular}{l}
        \multicolumn{1}{c}{#2} \\
        \hline
        #3
      \end{tabular}
    };
  \fi
}

%%%%%%%%%%%%%%
% The slides %
%%%%%%%%%%%%%%

\begin{document}

\begin{frame}
  \titlepage
\end{frame}

\begin{frame}
  \frametitle{Outline}
  \tableofcontents[sectionstyle=show,subsectionstyle=show]
\end{frame}

\section[Functional]{Virtues of functional programming}

\subsection[theory]{Theory using haskell}

\begin{frame}
  \frametitle{Functional programming}
  \begin{block}{Definition (Wikipedia) and rules}
    a programming paradigm where programs are constructed by applying and composing functions. Functions have
    \begin{itemize}
    \item no side effects
    \item no modification of the input
    \item new return values
    \end{itemize}
  \end{block}
  \begin{exampleblock}{Consequences}
    \begin{itemize}
    \item no state, no globals, no members
    \item guaranteed thread-safety !
    \end{itemize}
  \end{exampleblock}
  \begin{block}{Usage}
    \begin{itemize}
    \item dedicated languages : Haskell, Erlang, Lisp, ...
    \item modern \cpp (lambdas, move, copy elision, ...)
    \end{itemize}
  \end{block}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Crash course in functional programing}
  \begin{exampleblockGB}{haskell syntax already tells a lot}{Y9TGTGrWh}{func prog basics}
    \begin{haskellcode}
      -- declaration
      add :: Int -> Int -> Int
      -- implementation
      add x y = x + y
      -- usage
      five = add 3 2
      add_42 = add 42
    \end{haskellcode}
    \begin{itemize}
    \item everything is a function
    \item functions always take one single paremeter
    \item functions with more arguments are functions returning a function 
    \end{itemize}
  \end{exampleblockGB}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Crash course in functional programing [2]}
  \begin{exampleblockGB}{Functions are regular objects}{bzaM9ad38}{func prog apply}
    \begin{haskellcode}
      apply_operator :: (a -> a -> a) -> a -> a -> a
      apply_operator op x y = op x y
      inc = apply_operator add 1
      double = apply_operator (\ x y -> x * y) 2
    \end{haskellcode}
  \end{exampleblockGB}
  \begin{exampleblockGB}{They can replace loops}{P7s8dGxhj}{func prog no loops}
    \begin{haskellcode*}{firstnumber=last}
      a = [1,2,3,4,5]
      b = map double a
      -- b is [2,4,6,8,10]
      fact5 = fold (*) 1 a -- 120
    \end{haskellcode*}
  \end{exampleblockGB}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Crash course in functional programing [3]}
  \begin{exampleblockGB}{Actual looping is done via recursion}{fPhhKWqG1}{func prog map/fold}
    \begin{haskellcode}
      -- map definition
      map :: (a -> b) -> [a] -> [b]
      map f [] = []
      map f (x:xs) = f x : map f xs
      
      -- fold definition
      fold :: (a -> b -> b) -> b -> [a] -> b
      fold f z []     = z      
      fold f z (x:xs) = f x (fold f z xs) 
    \end{haskellcode}
  \end{exampleblockGB}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Crash course in functional programing [4]}
  \begin{exampleblockGB}{Evaluation is lazy}{5jaW3zzfd}{func prog lazy}
    \begin{haskellcode}
      allEven = filter even [2..]
      a = take 5 allEven
      -- a is [2 4 6 8 10]
    \end{haskellcode}
  \end{exampleblockGB}
  \pause
  \begin{exampleblockGB}{Prime computation}{abPcso6Tz}{func prog primes}
    \begin{haskellcode*}{firstnumber=last}
      filterPrime :: [Int] -> [Int]
      filterPrime (p:xs) =
        p : filterPrime (filter (\ x -> x `mod` p /= 0) xs)
      allPrimes = filterPrime [2..]
      a = take 10 allPrimes
      -- a is [2 3 5 7 11 13 17 23 29 31]
    \end{haskellcode*}
  \end{exampleblockGB}
\end{frame}

\begin{frame}[fragile]
  \frametitle{What to conclude ?}
  \Large
  \begin{itemize}
  \item Nice, very elegant, but not so practical...\\
    \pause
  \item can we use that in python and \cpp ?
  \item and is all that really efficient ?
  \end{itemize}
  \pause
  \vspace{1cm}
  \begin{center}
    {Short answers : Yes, and double Yes}
  \end{center}
\end{frame}

\section[Practice]{Practical usage and efficiency}

\subsection[\cpp]{Functional programming in \cpp}

\begin{frame}[fragile]
  \frametitle{Everything is a function, also in \cpp}
  \begin{block}{Concept}
    \begin{itemize}
    \item a class can implement \mintinline{cpp}{operator()}
    \item it's then a ``functor'' (no relation to functors in math)
    \item allows to use objects in place of functions
    \item with constructors and data members
    \end{itemize}
  \end{block}
  \begin{exampleblockGB}{First functor}{4nPf1MrY3}{\cpp functor}
    \begin{cppcode}
    struct Adder {
      int m_increment;
      Adder(int increment) : m_increment(increment) {}
      int operator()(int a) { return a + m_increment; }
    };
    Adder add_42{42};
    int i = add_42(3); // 45
    \end{cppcode}
  \end{exampleblockGB}
\end{frame}

\begin{frame}[fragile]
  \frametitle{And we also have lambdas}
  \begin{block}{Definition}
    a lambda is a function with no name
  \end{block}
  \begin{block}{Syntax}
    \begin{cppcode*}{linenos=false}
      [captures] (args) -> type { code; }
    \end{cppcode*}
    The type specification is optional
  \end{block}
  \begin{exampleblockGB}{Usage example}{an5G993rh}{\cpp lambdas}
    \begin{cppcode}
      auto add = [](int a, int b) { return a+b; };
      auto add_42 = [&add](int b) { return add(42, b); };
    \end{cppcode}
  \end{exampleblockGB}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Lambdas are essentially syntactic sugar}
  \begin{exampleblock}{Lambdas}
    \begin{cppcode}
      [&sum, offset](int x) { sum += x + offset; }
    \end{cppcode}
  \end{exampleblock}
  \begin{alertblock}{Are just functors - \cppinsightLink{https://cppinsights.io/s/0f788e60}}
    \begin{cppcode}
      struct MyFunc {
        int& m_sum;
        int m_offset;
        MyFunc(int& s, int o) : m_sum(s), m_offset(o) {}
        void operator(int x) { m_sum += x + m_offset; }
      };
      MyFunc(sum, offset)
    \end{cppcode}
  \end{alertblock}
  \small By the way, as lambdas are functors, they can inherit from each other !\\
  And this can be super useful.
\end{frame}

\begin{frame}[fragile]
  \frametitle{Functions are regular objects}
  \begin{exampleblockGB}{They can be passed around}{fzG151WT8}{C++ func}
    \begin{cppcode}
      auto apply = [](auto f, auto a, auto b)
        { return f(a, b); };
      auto inc = [&](auto a) { return apply( add, 1, a ); };
      auto doubleF = [&](auto a) {
        return apply([](auto x, auto y) {return x*y;}, 2, a);
      };
    \end{cppcode}
  \end{exampleblockGB}
  \begin{exampleblockGB}{They can replace loops}{K8jM1eYr6}{C++ no loop}
    \begin{cppcode*}{firstnumber=last}
      std::vector a{1,2,3,4,5};
      auto fact =
        std::ranges::fold_right(a, 1, std::multiplies());
      std::ranges::transform
        (begin(a), end(a), begin(a), doubleF);
      -- a is [2,4,6,8,10]
    \end{cppcode*}
  \end{exampleblockGB}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Ranges}
  \begin{block}{Reason of being}
    \begin{itemize}
    \item provide easy manipulation of sets of data via views
    \item simplify the horrible iterator syntax
    \end{itemize}
  \end{block}
  \begin{block}{Syntax}
    Based on Unix like pipes, and used in range based loops
  \end{block}
  \begin{exampleblockGB}{Example code}{zf7jGcWM4}{C++ ranges}
    \begin{cppcode}
      std::vector<int> numbers{1, 2, 3, 4, 5, 6};
      auto results =
        numbers | filter([](int n){ return n % 2 == 0; })
                | transform([](int n){ return n * 2; });                         
      for (auto v: results) std::cout << v << " ";
    \end{cppcode}
  \end{exampleblockGB}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Ranges are lazy evaluated}
  \begin{exampleblockGB}{Example code}{6h4cMrsYn}{C++ primes}
    \small
    \begin{cppcode}
      using Gen = std::generator<unsigned int>;
      Gen source() { for (unsigned x = 2; ; x++) co_yield x; }
      Gen filter(Gen& g, int prime) {
        for (unsigned x : g) { if ((x % prime) != 0) co_yield x; }
      }
      Gen prime(Gen& g) {
        auto p = *g.begin(); co_yield p;
        auto ng = filter(g, p);
        for (auto n : prime(ng)) co_yield n;
      }
      auto numbers = source();
      for (unsigned p : prime(numbers) | std::views::take(10)) {
        std::cout << p << std::endl;
      }
    \end{cppcode}
  \end{exampleblockGB}
\end{frame}

\subsection{Efficiency}

\begin{frame}[fragile]
  \frametitle{Is this efficient ?}
  \begin{exampleblockGB}{Let's compile this code}{zxG78h33E}{transform efficiency}
    \small
    \begin{cppcode}
      std::array<int, 6> numbers{1, 2, 3, 4, 5, 6};
      auto results = numbers |
        std::ranges::views::transform([](int n){ return n * 23; });
      for (auto n : results) std::cout << n << " ";
    \end{cppcode}
  \end{exampleblockGB}
  \begin{block}{Generated code with \mintinline{cpp}{-O3}}
    \small
    \begin{asmcode}
    .L2:  imul    esi, DWORD PTR [rbx], 23
          mov     edi, OFFSET FLAT:_ZSt4cout
          add     rbx, 4
          call    std::basic_ostream::operator<<(int)
          cmp     rbp, rbx
          jne     .L2
    .LC0: .long   1
          .long   2
          ...
    \end{asmcode}
  \end{block}
  \pause
  \begin{tikzpicture}[remember picture,overlay]
    \LARGE
    \visible<2-|handout:0>{
      \node[above=-.5cm of current page.center, draw, thick, minimum width=4cm, minimum height=1.5cm, rotate=30, rounded rectangle, color=red!40!black, fill=white] { \bf $\sim$Optimized };
    }
  \end{tikzpicture}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Is that efficient ?}
  \begin{exampleblockGB}{Another bit of \cpp code}{r6o3MT4os}{fold efficiency}
    \small
    \begin{cppcode}
      int foo() {
        std::vector<int> a{1,2,3,4,5};
        return std::ranges::fold_right(a, 1, std::multiplies());
      }
      std::cout << foo() << std::endl;
    \end{cppcode}
  \end{exampleblockGB}
  \begin{block}{We get (gcc trunk}
    \small
    \begin{asmcode}
      foo():
        ...
        call    operator new(unsigned long)
        movdqa  xmm0, XMMWORD PTR .LC0[rip]
        mov     esi, 20
        mov     DWORD PTR [rax+16], 5
        mov     rdi, rax
        movups  XMMWORD PTR [rax], xmm0
        call    operator delete(void*, unsigned long)
        mov     eax, 120
    \end{asmcode}
  \end{block}
  \pause
  \begin{tikzpicture}[remember picture,overlay]
    \LARGE
    \visible<2-|handout:0>{
      \node[above=-.5cm of current page.center, draw, thick, minimum width=4cm, minimum height=1.5cm, rotate=30, rounded rectangle, color=red!40!black, fill=white] { \bf Weird };
    }
  \end{tikzpicture}
\end{frame}

\begin{frame}[fragile]
  \frametitle{How do we explain that ?}
  \begin{block}{Facts}
    \begin{itemize}
    \item what could be precomputed was precomputed
    \item all functional code is gone
    \item but vector still created, filled and dropped
      \begin{itemize}
      \item not even used !
      \end{itemize}
    \end{itemize}
  \end{block}
  \begin{exampleblock}{Hypothesis}
    \begin{itemize}
    \item creation/deletion of the vector had to be kept in case of side effects
    \item compiler did not know whether there would be some
    \item Bottom line :
      \begin{itemize}
      \item no enough information given to the compiler
      \item a.k.a. compiler not clever enough
      \end{itemize}
    \end{itemize}
  \end{exampleblock}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Switching from vector to array}
  \begin{exampleblockGB}{Another bit of \cpp code}{nz531zcKo}{array vs vector}
    \small
    \begin{cppcode}
      int foo() {
        std::array<int, 5> a{1,2,3,4,5};
        return std::ranges::fold_right(a, 1, std::multiplies());
      }
      foo();
    \end{cppcode}
  \end{exampleblockGB}
  \begin{block}{We get (gcc trunk)}
    \begin{asmcode}
    foo():
        mov     eax, 120
    \end{asmcode}
  \end{block}
  \pause
  \begin{tikzpicture}[remember picture,overlay]
    \LARGE
    \visible<2-|handout:0>{
      \node[above=-.5cm of current page.center, draw, thick, minimum width=4cm, minimum height=1.5cm, rotate=30, rounded rectangle, color=red!40!black, fill=white] { \bf Optimized };
    }
  \end{tikzpicture}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Using a more clever compiler}
  \begin{exampleblockGB}{Another bit of \cpp code}{cPqYjq6hz}{better compiler}
    \small
    \begin{cppcode}
      int foo() {
        std::vector<int> a{1,2,3,4,5};
        return std::ranges::fold_right(a, 1, std::multiplies());
      }
      foo();
    \end{cppcode}
  \end{exampleblockGB}
  \begin{block}{We get (clang 18)}
    \begin{asmcode}
    foo():
        mov     eax, 120
    \end{asmcode}
  \end{block}
  \pause
  \begin{tikzpicture}[remember picture,overlay]
    \LARGE
    \visible<2-|handout:0>{
      \node[above=-.5cm of current page.center, draw, thick, minimum width=4cm, minimum height=1.5cm, rotate=30, rounded rectangle, color=red!40!black, fill=white] { \bf Optimized };
    }
  \end{tikzpicture}
\end{frame}

\section[Compiler]{It's all about helping the compilers}

\subsection[const/noexcept]{constness, exceptions}

\begin{frame}[fragile]
  \frametitle{Constness}
  \begin{block}{The \texttt{const} keyword}
    \begin{itemize}
    \item indicates that the variable is constant
    \item this is all checked at compile time
    \item and used by the compiler for optimization
    \end{itemize}
  \end{block}
  \begin{cppcode}
    int const i = 6;
    const int i = 6; // equivalent
    // error: i is constant
    i = 5;
    auto const j = i; // works with auto
  \end{cppcode}
\end{frame}

\begin{frame}[fragile]
  \frametitle{constness}
  \begin{columns}
    \begin{column}{.45\textwidth}
      \begin{exampleblockGB}{Non \mintinline{cpp}{const} code}{T8hvhrP4z}{missing const}
        \small
        \begin{cppcode}
          void foo( int& k );
          int bar() {
            int k = 10; 
            foo(k);
            return k*k+2*k+1;
          }
        \end{cppcode}
      \end{exampleblockGB}
    \end{column}
    \begin{column}{.45\textwidth}
      \begin{block}{We get}
        \small
        \begin{asmcode}
        bar():
          sub  rsp, 24
          lea  rdi, [rsp+12]
          mov  DW PTR [rsp+12], 10
          call foo(int&)
          mov  eax, DW PTR [rsp+12]
          add  rsp, 24
          mov  edx, eax
          imul edx, eax
          lea  eax, [rdx+1+rax*2]
        \end{asmcode}
      \end{block}
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}[fragile]
  \frametitle{constness}
  \begin{columns}
    \begin{column}{.45\textwidth}
      \begin{exampleblockGB}{\mintinline{cpp}{const} aware code}{4c948qP6a}{const usage}
        \small
        \begin{cppcode}
          void foo( const int& k );
          int bar() {
            const int k = 10; 
            foo(k);
            return k*k+2*k+1;
          }
        \end{cppcode}
      \end{exampleblockGB}
    \end{column}
    \begin{column}{.45\textwidth}
      \begin{block}{We get}
        \small
        \begin{asmcode}
        bar():
          sub  rsp, 24
          lea  rdi, [rsp+12]
          mov  DW PTR [rsp+12], 10
          call foo(int const&)
          mov  eax, 121
          add  rsp, 24
        \end{asmcode}
      \end{block}
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Data replication can also help}
  \begin{columns}
    \begin{column}{.45\textwidth}
      \begin{exampleblockGB}{data replication}{K797cGPeY}{data replication}
        \small
        \begin{cppcode}
          void foo( int k );
          int bar() {
            int k = 10; 
            foo(k);
            return k*k+2*k+1;
          }
        \end{cppcode}
      \end{exampleblockGB}
    \end{column}
    \begin{column}{.45\textwidth}
      \begin{block}{We get}
        \small
        \begin{asmcode}
        bar():
          sub     rsp, 8
          mov     edi, 10
          call    foo(int)
          mov     eax, 121
          add     rsp, 8
        \end{asmcode}
      \end{block}
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}[fragile]
  \frametitle{\cpp exception support}
  \begin{block}{}
    After a lot of thinking and experiencing, the conclusions of the community on exception handling are :
    \begin{itemize}
    \item Never write an exception specification
    \item Except possibly an empty one
    \end{itemize}
  \end{block}
  \pause
  \begin{alertblock}{one of the reasons : performance}
    \begin{itemize}
    \item does not allow compiler optimizations
    \item on the contrary forces extra checks
    \end{itemize}
  \end{alertblock}
  \pause
  \begin{exampleblock}{Introducing noexcept}
      \begin{cppcode}
        int f() noexcept;
      \end{cppcode}
    \begin{itemize}
    \item somehow equivalent to throw()
    \item meaning no exception can go out of the function
    \item but is checked at compile time
    \item thus allowing compiler optimizations
    \end{itemize}
  \end{exampleblock}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Impact on generated code - exceptions}
  \begin{columns}
    \begin{column}{.5\textwidth}
      \begin{exampleblockGB}{\cpp code}{WbM4xzE8a}{exception}
        \begin{cppcode}
          struct MyExcept{};
          int f(int a); // may throw
          
          int foo() {
            try {
              int a = 23;
              return f(a) + f(-a);
            } catch (MyExcept& e) {
              return 0;
            }
          }
        \end{cppcode}
      \end{exampleblockGB}
    \end{column}
    \begin{column}{.4\textwidth}
      \begin{block}{Generated code}
        \tiny
        \begin{asmcode}
        foo():
          push    rbx
          mov     edi, 23
          call    f(int)
          mov     edi, -23
          mov     ebx, eax
          call    f(int)
          add     eax, ebx
        .L1:
          pop     rbx
          ret
          mov     rdi, rax
          mov     rax, rdx
          jmp     .L2
          foo() [clone .cold]:
        .L2:
          sub     rax, 1
          jne     .L8
          call    __cxa_begin_catch
          call    __cxa_end_catch
          xor     eax, eax
          jmp     .L1
        .L8:
          call    _Unwind_Resume
        \end{asmcode}
      \end{block}
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Impact on generated code - noexcept }
  \begin{columns}
    \begin{column}{.5\textwidth}
      \begin{exampleblockGB}{\cpp code}{xx75Md7of}{noexcept}
        \begin{cppcode}
          struct MyExcept{};
          int f(int a) noexcept;
          
          int foo() {
            try {
              int a = 23;
              return f(a) + f(-a);
            } catch (MyExcept& e) {
              return 0;
            }
          }
        \end{cppcode}
      \end{exampleblockGB}
    \end{column}
    \begin{column}{.4\textwidth}
      \begin{block}{Generated code}
        \small
        \begin{asmcode}
        foo():
          push    rbx
          mov     edi, 23
          call    f(int)
          mov     edi, -23
          mov     ebx, eax
          call    f(int)
          add     eax, ebx
          pop     rbx
        \end{asmcode}
      \end{block}
    \end{column}
  \end{columns}
\end{frame}

\subsection[copy]{Avoiding useless copying}

\begin{frame}[fragile]
  \frametitle{Useless copying: the problem}
  \begin{exampleblock}{Inefficient code}
    \begin{cppcode*}{}
      void swap(std::vector<int> &a,
                std::vector<int> &b) {
        std::vector<int> c = a;
        a = b;
        b = c;
      }
      std::vector<int> v(10000), w(10000);
      ...
      swap(v, w);
    \end{cppcode*}
  \end{exampleblock}
  \pause
  \begin{alertblock}{What happens during swap}
    \begin{itemize}
    \item one allocation and one release for 10k \mintinline{cpp}{int}s
    \item a copy of 30k \mintinline{cpp}{int}s
    \end{itemize}
  \end{alertblock}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Move semantics}
  \begin{block}{The idea}
    \begin{itemize}
      \item a new type of reference : rvalue references
      \begin{itemize}
      \item used for ``moving'' objects
      \item denoted by \&\&
      \end{itemize}
      \item 2 new members in every class, with move semantic :
      \begin{description}
      \item[a move constructor] similar to copy constructor
      \item[a move assignment operator] similar to assignment operator (now called copy assignment operator)
      \end{description}
      \begin{itemize}
      \item used when original object can be reused
      \end{itemize}      
    \end{itemize}
  \end{block}
  \pause
  \begin{exampleblock}{Practically}
    \begin{cppcode}
      T(const T&  other); // copy construction
      T(      T&& other); // move construction
      T& operator=(const T&  other); // copy assignment
      T& operator=(      T&& other); // move assignment
    \end{cppcode}
  \end{exampleblock}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Move semantics}
  \begin{block}{A few important points concerning move semantic}
    \begin{itemize}
    \item move assignment operator is allowed to destroy source
      \begin{itemize}
      \item so do not reuse source afterward
      \end{itemize}
    \item if not implemented, move falls back to copy version
    \item move is called by the compiler whenever possible
      \begin{itemize}
      \item and can be forced via \mintinline{cpp}{std::move}
      \end{itemize}
    \end{itemize}
  \end{block}
  \pause
  \begin{exampleblock}{Practically}
    \begin{cppcode}
      void swap(T &a, T &b) {
        T c = std::move(a);  // move construct
        a = std::move(b);    // move assign
        b = std::move(c);    // move assign
      }
    \end{cppcode}
    No allocation, no release, 3x3 pointers copied
  \end{exampleblock}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Guaranteed copy elision}
  \begin{block}{What is copy elision}
    \begin{cppcode}
      struct Foo { ... };
      Foo f() {
        return Foo();
      }
      int main() {
        // No copy here, compiler must elude the copy
        Foo foo = f();
      }
    \end{cppcode}
  \end{block}
  \begin{exampleblock}{From \cpp17 on}
    The elision is guaranteed.
    \begin{itemize}
    \item superseeds move semantic in some cases
    \item so do not hesitate anymore to return plain objects in generators
      \begin{itemize}
      \item and ban pointers for good
      \end{itemize}
    \end{itemize}
  \end{exampleblock}
\end{frame}

\section[CompileTime]{Do more at compile time}

\subsection{Templates}

\begin{frame}[fragile]
  \frametitle{Templates}
  \begin{block}{Concept}
    \begin{itemize}
    \item The \cpp way to write reusable code
      \begin{itemize}
        \item aka macros on steroids
      \end{itemize}
    \item Applicable to functions and objects
    \end{itemize}
  \end{block}
  \begin{cppcode}
    template<typename T>
    const T & max(const T &A, const T &B) {
      return A > B ? A : B;
    }

    template<typename T>
    struct Vector {
      int m_len;
      T* m_data;
    };
  \end{cppcode}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Templates}
  \begin{exampleblock}{Warning}
    These are really like macros
    \begin{itemize}
      \item they need to be defined before used
      \begin{itemize}
        \item so all templated code has to be in headers
      \end{itemize}
      \item they are compiled n times
      \item and thus each version is optimized individually !
    \end{itemize}
  \end{exampleblock}
  \newsavebox{\codepiece}
  \begin{lrbox}{\codepiece}
    \begin{minipage}{.37\linewidth}
      \small
      \begin{cppcode}
        template<typename T>
        T func(T a) {
          return a;
        }
      \end{cppcode}
    \end{minipage}
  \end{lrbox}
  \newsavebox{\codepiecea}
  \begin{lrbox}{\codepiecea}
    \begin{minipage}{.41\linewidth}
      \small
      \begin{cppcode}
        int func(int a) {
          return a;
        }
      \end{cppcode}
    \end{minipage}
  \end{lrbox}
  \newsavebox{\codepieceb}
  \begin{lrbox}{\codepieceb}
    \begin{minipage}{.41\linewidth}
      \small
      \begin{cppcode}
        double func(double a) {
          return a;
        }
      \end{cppcode}
    \end{minipage}
  \end{lrbox}
  \begin{tikzpicture}[rectangle,rounded corners]
    \draw node (template) [draw] {\usebox{\codepiece}}
          node (templatea) [draw] at (6.5cm,+1cm) {\usebox{\codepiecea}}
          node (templateb) [draw] at (6.5cm,-1cm) {\usebox{\codepieceb}};
    \draw[->,thick] (template) -- (templatea) node [above,midway,sloped] {\small func(3)};
    \draw[->,thick] (template) -- (templateb) node [below,midway,sloped] {\small func(5.2)};
  \end{tikzpicture}
\end{frame}

\begin{frame}[fragile]
  \frametitle{A realistic template usage}
  \begin{exampleblockGB}{Generic printing}{5eqMK3dbj}{Generic printing}
    \small
    \begin{cppcode}
      template<typename T> struct Print {
        Print(T const& obj) {
          std::cout << "(" << typeid(T).name()
                    << ") " << obj << "\n";
        }
      };
      Print{5};                       // (i) 5
      Print("a text string");         // (A14_c) a text string
    \end{cppcode}
  \end{exampleblockGB}
\end{frame}

\begin{frame}[fragile]
  \frametitle{A realistic template usage}
  \begin{exampleblock}{Generic printing - \href{https://godbolt.org/z/5eqMK3dbj}{\color{blue!50!white} godbolt}}
    \small
    \begin{cppcode}
      template<typename T>
      concept Container = requires( T v1 ) {
        begin(v1);
        end(v1);
      };
      template<Container T> struct Printer<T> {
        Printer(T const& obj) {
          std::cout << "(" << typeid(T).name() << ") ";
          for( auto& item: obj) { std::cout << item << " "; }
          std::cout << "\n";
        }
      };
      Print(std::vector<int>{1,2,3});
      // (St6vectorIiSaIiEE) 1 2 3
      Printer(std::array<double, 3>{2,3,4});
      // (St5arrayIdLm3EE) 2 3 4
    \end{cppcode}
  \end{exampleblock}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Pros and cons of templates}
  \begin{exampleblock}{Gains}
    \begin{itemize}
    \item more generic code, thus less code
    \item better compiler optimization
    \item allows some compile time processing
      \begin{itemize}
      \item also known as template metaprograming
      \end{itemize}
    \end{itemize}
  \end{exampleblock}
  \begin{alertblock}{The drawbacks}
    \begin{itemize}
    \item not so trivial syntax, especially when variadic
    \item heavy for the compiler if abused
    \item only usable for what you know at compile time
    \end{itemize}
  \end{alertblock}
\end{frame}

\subsection[constexpr]{\mintinline{cpp}{constexpr} and \mintinline{cpp}{if constexpr} }

\begin{frame}[fragile]
  \frametitle{Generalized Constant Expressions}
  \begin{block}{Reason of being}
    \begin{itemize}
    \item use functions to compute constant expressions at compile time
    \end{itemize}
  \end{block}
  \pause
  \begin{exampleblock}{Example}
    \begin{cppcode*}{}
      constexpr int f(int x) {
        if (x > 1) return x * f(x - 1);
        return 1;
      }
      constexpr int a = f(5); // computed at compile-time
      int b = f(5); // maybe computed at compile-time
      int n = ...;  // runtime value
      int c = f(n); // computed at runtime
    \end{cppcode*}
  \end{exampleblock}
\end{frame}

\begin{frame}[fragile]
  \frametitle{\mintinline{cpp}{if constexpr}}
  \begin{block}{Compile time if statement}
    \begin{itemize}
    \item takes a generalized constant expression
    \item decides which branch to keep at compile time
    \item drops the other branch
    \end{itemize}
  \end{block}
  \pause
  \begin{exampleblockGB}{Generic printing}{Tvqbh5q7Y}{if constexpr}
    \small
    \begin{cppcode*}{}
      template<typename T> struct Print {
        Print(T const& obj) {
          std::cout << "(" << typeid(T).name() << ") ";
          if constexpr( Container<T> ) {
            for( auto& item: obj) { std::cout << item << " "; }
          } else {
            std::cout << obj;
          }
          std::cout << "\n";
        }
      };
    \end{cppcode*}
  \end{exampleblockGB}
\end{frame}

\subsection[virtual]{Virtuality : a counter example}

\begin{frame}[fragile]
  \frametitle{Virtuality in a nutshell}
  \begin{block}{Principle}
    \begin{itemize}
    \item a base class (aka interface) declares some method virtual
    \item children can overload these methods (as any other)
    \item for these method, late binding is applied
    \item that is most precise type is used
    \end{itemize}
  \end{block}
  \pause
  \begin{multicols}{2}
    \begin{cppcode}
      Polygon p;
      p.draw(); // Polygon.draw
      
      Shape & s = p;
      s.draw(); // Shape.draw
    \end{cppcode}
    \columnbreak
    \center
    \begin{tikzpicture}[node distance=1.5cm]
      \classbox{Shape}{
        void draw();
      }
      \classbox[below of=Shape]{Polygon}{
        void draw();
      }
      \draw[very thick,->] (Polygon) -- (Shape);
    \end{tikzpicture}
  \end{multicols}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Virtuality in a nutshell}
  \begin{block}{Principle}
    \begin{itemize}
    \item a base class (aka interface) declares some method virtual
    \item children can overload these methods (as any other)
    \item for these method, late binding is applied
    \item that is most precise type is used
    \end{itemize}
  \end{block}
  \begin{multicols}{2}
    \begin{cppcode}
      Polygon p;
      p.draw(); // Polygon.draw
      
      Shape & s = p;
      s.draw(); // Polygon.draw
    \end{cppcode}
    \columnbreak
    \center
    \begin{tikzpicture}[node distance=1.5cm]
      \classbox{Shape}{
        virtual void draw() = 0;
      }
      \classbox[below of=Shape]{Polygon}{
        void draw();
      }
      \draw[very thick,->] (Polygon) -- (Shape);
    \end{tikzpicture}
  \end{multicols}
\end{frame}

\begin{frame}
  \frametitle{The price of virtuality}
  \begin{block}{Actual implementation}
    \begin{itemize}
    \item each object has an extra pointer
    \item to a ``virtual table'' object in memory
    \item where each virtual function points to the right overload
    \end{itemize}
  \end{block}
  \begin{block}{Cost}
    \begin{itemize}
    \item extra virtual table in memory, per type
    \item each virtual call does
      \begin{itemize}
      \item retrieve virtual table pointer
      \item load virtual table into memory
      \item lookup right call
      \item effectively call
      \end{itemize}
    \item and is thus much more costful than standard function call
    \item up to 20\% difference in terms of nb of instructions
    \end{itemize}
  \end{block}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Actual price of virtuality}
  \begin{block}{Comparison with templates - \godboltLink{https://godbolt.org/z/ETEneP8K7}{Counter virtual} / \godboltLink{https://godbolt.org/z/9P9hshMqh}{Counter templates} }
    \scriptsize
    \vspace{-3mm}
    \begin{columns}
      \begin{column}{.45\textwidth}
        \begin{cppcode}
          struct Interface {
            virtual void tick(float n) = 0;
          };
          struct Counter : Interface {
            float sum{0};
            void tick(float v) override
              { sum += v; }
          };
          void foo(Interface& c) {
            for (int i = 0; i < 80000; ++i) {
              for (int j = 0; j < i; ++j) {
                c.tick(j);
              }
            }
          }
          int main() {
            auto obj =
              std::make_unique<Counter>();
            foo(*obj);
          }
        \end{cppcode}
      \end{column}
      \begin{column}{.45\textwidth}
        \begin{cppcode*}{firstnumber=21}
          struct Counter {
            float sum{0};
            void tick(float v) { sum += v; }
          };
          template<typename CounterType>
          void foo(CounterType& c) {
            for (int i = 0; i < 80000; ++i) {
              for (int j = 0; j < i; ++j) {
                c.tick(j);
              }
            }
          }
        \end{cppcode*}
        \pause
        \normalsize
        \vspace{2mm}
        \begin{tabular}{ r|c|c }
          {\bf Timing} & Time(s) & Nb instr(G) \\
          \hline
          virtual  & 10.8 & 35.2 \\
          \hline
          templ & 2.97 & 8.9 \\
        \end{tabular}
        \begin{itemize}
          \tiny
        \item measured on EPYC 7552, with gcc 9.1 and perf
        \end{itemize}
      \end{column}
    \end{columns}
  \end{block}
\end{frame}

\begin{frame}[fragile]
  \frametitle{A few explanations}
  \begin{alertblock}{Some consequences of virtuality}
    \begin{itemize}
    \item more branching, killing the pipeline
      \begin{itemize}
      \item here 6.4M vs 0.8M branches !
      \item as virtual calls are branches
      \end{itemize}
    \item lack of inlining possibilities
    \item lack of optimizations after inlining
      \begin{itemize}      
      \item e.g. auto vectorization
      \end{itemize}        
    \end{itemize}
  \end{alertblock}
  \begin{exampleblock}{Note that the compiler is trying hard to help}
    \begin{itemize}
    \item when it can, when it knows so give it all the knowledge !
    \item typical on my example
      \pause
      \begin{itemize}
      \item declare \mintinline{cpp}{obj} on the stack and the compiler will ``drop'' virtuality
        \begin{itemize}
        \item again : drop pointers !
        \end{itemize}
      \item gcc 10/12 does much better : 22/16G instructions and 3s
      \end{itemize}
    \end{itemize}
  \end{exampleblock}
\end{frame}

\begin{frame}
  \frametitle{Should I use virtuality ?}
  {\bf Yes, when you cannot know anything at compile time}
  \begin{block}{Typical cases}
    \begin{itemize}
    \item you have no knowledge of the implementations of an interface
      \begin{itemize}
      \item new ones may even be loaded dynamically via shared libraries
      \end{itemize}
    \item you mix various implementations in a container
      \begin{itemize}
      \item e.g. \mintinline{cpp}{std::vector<MyInterface>}
      \item and there is no predefined set of implementations
      \end{itemize}
    \end{itemize}
  \end{block}
  \pause
  \begin{block}{Typical alternatives}
    \begin{itemize}
    \item templates when everything is compile time
      \begin{itemize}
      \item allows full optimization of each case
      \item and even static polymorphism through CRTP
        \begin{itemize}
        \item {\color{blue} \href{https://en.wikipedia.org/wiki/Curiously_recurring_template_pattern}{Curiously recurring template pattern}}
        \end{itemize}
      \end{itemize}
    \item \mintinline{cpp}{std::variant}, \mintinline{cpp}{std::any} and visitor
      \begin{itemize}
      \item when type definitions are known at compile type
      \item but not necessary their usage
      \end{itemize}
    \end{itemize}    
  \end{block}
\end{frame}


\begin{frame}[fragile]
  \frametitle{std::variant, std::any}
  \begin{block}{Purpose}
    \begin{itemize}
    \item type safe union and ``void*''
    \item with visitor pattern
    \end{itemize}
  \end{block}
  \pause
  \begin{exampleblockGB}{Example code}{Pnqd9PoW5}{Visitor}
    \scriptsize
    \begin{cppcode}
      using Message = std::variant<int, std::string>;
      Message createMessage(bool error) {
        if (error) return "Error"; else return 42;
      }
      int i = std::get<int>(createMessage(false));
      struct Visitor {
        void operator()(int n) const {
          std::cout << "Int " << n << std::endl;
        }
        void operator()(const std::string &s) const {
          std::cout << "String \"" << s << "\"" << std::endl;
        }
      };
      std::visit(Visitor{}, createMessage(true));
    \end{cppcode}
  \end{exampleblockGB}
\end{frame}

\begin{frame}[fragile]
  \frametitle{std::variant, std::any}
  \begin{exampleblockGB}{Or you use lambdas and their inheritance}{4cKv6GnK5}{Lambda inheritance}
    \small
    \begin{cppcode}
      template <class ... P> struct Combine : P... {
        using P::operator()...;
      };
      template <class ... F> Combine<F...> combine(F... fs) {
        return { fs ... };
      }
      using Message = std::variant<int, std::string>;
      Message createMessage(bool error) {
        if (error) return "Error"; else return 42;
      }
      auto f = combine(
          [](int n) { std::cout << "Int " << n << std::endl; },
          [](string const &s) {
            std::cout << "String \"" << s << "\"" << std::endl;
          });
      std::visit(f, createMessage(true));
    \end{cppcode}
  \end{exampleblockGB}
\end{frame}


\begin{frame}[fragile]
  \frametitle{A Visitor example}
  \begin{exampleblockGB}{Virtual vs variant}{W4jGP4nYd}{Virtual vs variant}
    \scriptsize
    \begin{cppcode}
      struct Point { virtual float getR() = 0; };
      struct XYZPoint : Point {
        float x, y, z;
        float getR() override { return std::sqrt(x*x+y*y+z*z); }; };
      struct RTPPoint : Point {
        float r, theta, phi;
        float getR() override { return r; } };
      float sumR(std::vector<std::unique_ptr<Point>>& v) {
        return std::accumulate(begin(v), end(v), 0.0f,
        [&](float s, std::unique_ptr<Point>& p) { return s + p->getR();} );
      }

      struct XYZPoint { float x,y,z; };  struct RTPPoint { float r, theta, phi; };
      using Point=std::variant<XYZPoint, RTPPoint>;
      float sumR(std::vector<Point>& v) {
        auto getR = combine(
        [](XYZPoint& p) { return std::sqrt(p.x*p.x+p.y*p.y+p.z*p.z); },
        [](RTPPoint& p) { return p.r; });
        return std::accumulate(begin(v), end(v), 0.0f,
        [&](float s, Point& p) { return s + std::visit(getR, p);} );
      }
    \end{cppcode}
  \end{exampleblockGB}
  \pause
  \begin{tikzpicture}[remember picture,overlay]
    \LARGE
    \visible<2-|handout:2->{
      \node (1) [above=.5cm of current page.center, draw, thick, minimum width=4cm, minimum height=1.5cm, rotate=30, rounded rectangle, color=red!40!black, fill=white] { \bf took 3500$\mu$s};
      \node [below=2.5cm of 1, minimum width=4cm, minimum height=1.5cm, rotate=30, thick, draw, rounded rectangle, color=red!40!black, fill=white] { \bf took 2050$\mu$s};
    }
  \end{tikzpicture}
\end{frame}

\section{Conclusion}

\begin{frame}
  \frametitle{Conclusion}
  \begin{exampleblock}{Key messages of the day}
    \begin{itemize}
    \item functional programming is useful
      \begin{itemize}
      \item can simplify the code
      \item ensures thread safety
      \item helps the compiler to optimize better
      \end{itemize}
    \item the key to performance is the compiler
      \begin{itemize}
      \item help it by providing information
      \item constness, noexcept, fixed lengths, ...
      \end{itemize}
    \item a lot can be done at compile time
      \begin{itemize}
      \item find out what you know at compile time
      \item make best use of it (templates, variants, ...)
      \end{itemize}
    \end{itemize}
  \end{exampleblock}
\end{frame}


\begin{frame}
  \frametitle{Index of Godbolt code examples}
  \listofgodbolt
\end{frame}


\end{document}
