\include{header}

%%%%%%%%%%%%%%%%%%
% Document setup %
%%%%%%%%%%%%%%%%%%

\title{Preserving data}
\author[S. Ponce]{S\'ebastien Ponce\\ \texttt{sebastien.ponce@cern.ch}}
\institute{CERN}
\date{tCSC May \the\year}

%%%%%%%%%%%%%%
% The slides %
%%%%%%%%%%%%%%

\begin{document}

\begin{frame}
  \titlepage
\end{frame}

\begin{frame}
  \begin{block}{In the previous episodes...}
    \begin{itemize}
    \item We've found out how to store data efficiently
    \item And how to distribute it
    \item And even how to distribute the computation
    \end{itemize}
  \end{block}
  \pause
  \begin{block}{Today}
  Let's make sure we do not lose or corrupt our nice data !
  \end{block}
\end{frame}

\begin{frame}
  \frametitle{Outline}
  \vspace{-0.5cm}
  \begin{scriptsize}
    \tableofcontents[sectionstyle=show,subsectionstyle=show]
  \end{scriptsize}
\end{frame}

\section[risks]{Risks of data loss and corruption}

\begin{frame}
  \frametitle{Risks for my data - Hardware}
  \begin{block}{some numbers for disks}
    \begin{itemize}
    \item probability of losing a disk per year : few \%, up to 10\%
      \begin{itemize}
      \item with 60K disks, it's around 10 per day
      \item and all files are lost
      \end{itemize}
    \item one unrecoverable bit error in $10^{14}$ bits read/written
      \begin{itemize}
      \item for 1GB files, that's one file corrupted per 10K files written
      \end{itemize}
    \end{itemize}
  \end{block}
  \begin{block}{some numbers for tapes}
    \begin{itemize}
    \item probability of losing a tape per year : ~$10^{-4}$
      \begin{itemize}
      \item and you recover most of the data on it
      \item net result is ~$10^{-7}$ file loss per year
      \end{itemize}
    \item one unrecoverable bit error in $10^{19}$ bits read/written 
      \begin{itemize}
      \item for 1GB files, that's one file corrupted per 1G files written
      \end{itemize}
    \end{itemize}
  \end{block}
\end{frame}

\begin{frame}
  \frametitle{Risks for my data - Software}
  \begin{alertblock}{BUGS !}
    \begin{itemize}
    \item in your software
      \begin{itemize}
      \item e.g. scheduling twice a transfer, not receiving data on the second run and overwriting the correct file with an empty one
      \end{itemize}
    \item in your dependencies
      \begin{itemize}
      \item e.g. the transfer protocol used does not support checksum and data may be corrupted by TCP (checksum is only \SI{16}{bit}, one corrupted packet in $65536$ will go through)
      \end{itemize}
    \item in the OS or common libraries
      \begin{itemize}
      \item e.g. libc locks not being atomic
      \end{itemize}
    \item in the hardware - that is in the micro code running inside
      \begin{itemize}
      \item e.g. RAID controllers
      \end{itemize}
    \item in your admin tools
      \begin{itemize}
      \item e.g. recycling a tape that was not empty
      \end{itemize}
    \end{itemize}
  \end{alertblock}
\end{frame}

\begin{frame}
  \frametitle{Risks for my data - Human factor}
  \begin{alertblock}{Real life cases that went wrong}
    \begin{itemize}
    \item reinstall (and wipe) old machine p23425a4752
      \begin{itemize}
      \item Oh no, I actually meant p42532a8779... bad cut and paste
      \end{itemize}
    \item rm -rf /top/data/alltimes /2015/04/crap
      \begin{itemize}
      \item one space too much and all data are gone....
      \end{itemize}
    \item activate garbage collection on pool XYZ, it's full
      \begin{itemize}
      \item wasn't it tape backed up ? no ? oups....
      \end{itemize}    
    \end{itemize}
  \end{alertblock}
\end{frame}

\begin{frame}
  \frametitle{Risks for my data - conclusion}
  \pause
  {\Huge \bf \center \color{red!80!black}{You will lose/corrupt data !}}
  \pause
  \vspace{1cm}
  \begin{itemize}
  \item better to be able to know when and what
  \item even better if you can repair
  \end{itemize}
\end{frame}

\section[consistency]{Data consistency}

\subsection[cksum]{Checksums}

\begin{frame}
  \frametitle{Checksum}
  \begin{block}{Definition}
    \vbox {
      \center
      ``small-size datum from a block of digital data for the purpose of detecting errors``
    }
    \center
    \begin{tikzpicture}
      \draw[blue] (0,0) grid[step=0.5] (2,0.5);
      \draw[dotted,blue] (2,0) grid[step=0.5] (3.5,0.5);
      \node at (0.25,0.25) {$a_1$};
      \node at (0.75,0.25) {$a_2$};
      \node at (1.25,0.25) {$a_3$};
      \node at (1.75,0.25) {$a_4$};
      \node at (3.75,0.25) {$a_{n}$};
      \draw[blue] (3.4999,0) grid[step=0.5] (4.0001,0.5);
      \node[blue] at (2.75, 0.25) {... $a_i$ ...};
      \draw[->,thick] (4.5,0.25) -- (5,0.25);
      \draw[red] (5.5,0) rectangle node{CS} (7,0.5);
      \draw[<->,blue] (0,1.5) -- (4,1.5) node[midway,above] {n blocks};
      \draw[<->,blue] (0,0.75) -- (0.5,0.75) node[midway,above] {b};
      \draw[<->,red] (5.5,1) -- (7,1) node[midway,above] {w};
    \end{tikzpicture}
  \end{block}
\end{frame}

\begin{frame}
  \frametitle{Most basic checksum : data size}
  \begin{block}{Computation}
    \center
    \begin{tikzpicture}
      \draw[blue] (0,0) grid[step=0.5] (2,0.5);
      \draw[dotted,blue] (2,0) grid[step=0.5] (3.5,0.5);
      \node at (0.25,0.25) {$a_1$};
      \node at (0.75,0.25) {$a_2$};
      \node at (1.25,0.25) {$a_3$};
      \node at (1.75,0.25) {$a_4$};
      \node at (3.75,0.25) {$a_{n}$};
      \draw[blue] (3.4999,0) grid[step=0.5] (4.001,0.5);
      \node[blue] at (2.75, 0.25) {... $a_i$ ...};
      \draw[->,thick] (4.5,0.25) -- (5,0.25);
      \draw[red] (5.5,0) rectangle node{$n$} (7,0.5);
    \end{tikzpicture}
    \begin{equation*}
      b = \SI{8}{\bit} \,\,\,\,\, w = \SI{64}{bit}  \,\,\,\,\, CS = n
    \end{equation*}
  \end{block}
  \begin{exampleblock}{Pros and Contra}
    \begin{itemize}
    \item easy to compute
    \item detects erasures and additions
    \item does not detect any corruption
    \end{itemize}
  \end{exampleblock}
\end{frame}

\begin{frame}
  \frametitle{Basic checksum : sum/xor}
  \begin{block}{Computation}
    \center
    \begin{tikzpicture}
      \draw[blue] (0,0) grid[step=0.5] (2,0.5);
      \draw[dotted,blue] (2,0) grid[step=0.5] (3.5,0.5);
      \node at (0.25,0.25) {$a_1$};
      \node at (0.75,0.25) {$a_2$};
      \node at (1.25,0.25) {$a_3$};
      \node at (1.75,0.25) {$a_4$};
      \node at (3.75,0.25) {$a_{n}$};
      \draw[blue] (3.4999,0) grid[step=0.5] (4.001,0.5);
      \node[blue] at (2.75, 0.25) {... $a_i$ ...};
      \draw[->,thick] (4.5,0.25) -- (5,0.25);
      \draw[red] (5.5,0) rectangle node{$\sum a_i$} (7,0.5);
    \end{tikzpicture}
    \begin{equation*}
      b \,\,\,\,\, w = b  \,\,\,\,\, CS = \sum\limits_{i=1}^n a_i
    \end{equation*}
  \end{block}
  \begin{exampleblock}{Pros and Contra}
    \begin{itemize}
    \item easy to compute
    \item detects most corruptions
    \item does not detect any inversions/change of order
    \end{itemize}
  \end{exampleblock}
\end{frame}

\begin{frame}
  \frametitle{Adler like checksums}
  \begin{block}{Computation}
    \center
    \begin{tikzpicture}
      \draw[blue] (0,0) grid[step=0.5] (2,0.5);
      \draw[dotted,blue] (2,0) grid[step=0.5] (3.5,0.5);
      \node at (0.25,0.25) {$a_1$};
      \node at (0.75,0.25) {$a_2$};
      \node at (1.25,0.25) {$a_3$};
      \node at (1.75,0.25) {$a_4$};
      \node at (3.75,0.25) {$a_{n}$};
      \draw[blue] (3.4999,0) grid[step=0.5] (4.001,0.5);
      \node[blue] at (2.75, 0.25) {... $a_i$ ...};
      \draw[->,thick] (4.5,0.25) -- (5,0.25);
      \draw[red] (5.5,0) rectangle node{$\sum a_i$} (6.5,0.5);
      \draw[red] (6.5,0) rectangle node{$\sum i a_i$} (7.5,0.5);
    \end{tikzpicture}
    \begin{equation*}
      b = \SI{8}{\bit} \,\,\,\,\, w = \SI{32}{\bit} \,\,\,\,\, CS_{high} = \sum\limits_{i=1}^n a_i \,\,\,\,\, CS_{low} = \sum\limits_{i=1}^n {i a_i}
    \end{equation*}
  \end{block}
  \begin{exampleblock}{Pros and Contra}
    \begin{itemize}
    \item easy to compute
    \item detects most corruptions and inversions
    \item weak for small files
    \item easy to fake in case of intentional corruption
    \end{itemize}
  \end{exampleblock}
\end{frame}

\begin{frame}
  \frametitle{(Crypt)Analysis of adler}
  \begin{block}{Weaknesses}
    \begin{itemize}
    \item 32 bits is short
      \begin{itemize}
      \item one per 4 billion corruption will go through
      \end{itemize}
    \item it's actually worse for small files
      \begin{itemize}
      \item all bits of the sum are not even used for less than 256 bytes
      \end{itemize}
    \item they can be easily bypassed
      \begin{itemize}
      \item one can easily change the last 16 bytes and reach any checksum
      \item so intentional corruptions are not covered
      \end{itemize}
    \end{itemize}
  \end{block}
\end{frame}

\begin{frame}
  \frametitle{Cryptographic checksums}
  \begin{exampleblock}{What is it ?}
    \begin{itemize}
    \item checksums that cannot be faked (easily)
    \item they are based on non reversible cryptographic functions
    \end{itemize}
  \end{exampleblock}
  \begin{block}{Most used ones}
    \begin{description}
    \item[md5] 1991, 128 bits, by Rivest. Not considered secure anymore as complete collisions have been discovered.
    \item[sha1] 1995, 160 bits, by NSA. Collision in $2^{61}$ operations
    \item[sha256] 2001, 256 bits, by NSA. Collision in $2^{128}$ operations
    \item[sha512] 2001, 512 bits, by NSA. Collision in $2^{256}$ operations
    \end{description}
  \end{block}
  \begin{alertblock}{Drawback}
    \begin{itemize}
    \item more costful to compute
    \item although modern processors have dedicated instructions
    \end{itemize}
  \end{alertblock}
\end{frame}

\begin{frame}
  \frametitle{Comparison of main checksums}
  \center
  \begin{tabular}{|l|r|r|}
    \hline
    Name & MB/s on intel core 2 & Cycles Per Byte \\
    \hline
    \hline
    Adler32 & 920 & 1.9  \\
    \hline
    MD5	    & 255 & 6.8  \\
    \hline
    SHA-1   & 153 & 11.4 \\
    \hline
    SHA-256 & 111 & 15.8 \\
    \hline
    SHA-512 & 99  & 17.7 \\
    \hline
  \end{tabular}
\end{frame}

\subsection[block]{Block checksums}

\begin{frame}
  \frametitle{Practical usage of checksums}
  \begin{block}{Simple approach}
    \begin{itemize}
    \item compute checksum in memory when creating/writing file
    \item store checksum in a DB
    \item check it in memory on full file reads
    \end{itemize}
  \end{block}
  \begin{alertblock}{Problems}
    \begin{itemize}
    \item corrupted data only found when read back, unnoticed otherwise
    \item one needs to fully read the file to be able to check
    \item file updates not supported. Need to read back the whole file
    \item file append suffers the same limitation
    \item multi stream, out of order writing not supported
    \item losing the DB loses all checksums
    \item renaming a file implies changing the entry in the DB, with double commit issue
    \end{itemize}
  \end{alertblock}
\end{frame}

\begin{frame}
  \frametitle{Recommendations}
  \begin{block}{When to compute checksums}
    \begin{itemize}
    \item opportunistically
    \item plus regular scans of the whole data set
    \end{itemize}
  \end{block}
  \begin{block}{How to store checksums}
    \begin{itemize}
    \item always close to the file
    \item next to it on the filesystem
    \item in most cases, using external attributes
    \end{itemize}
  \end{block}
\end{frame}

\begin{frame}
  \frametitle{Block checksumming}
  \begin{block}{One checksum per file piece}
    \begin{itemize}
    \item split the file into smaller pieces
    \item compute one checksum per piece
    \end{itemize}
  \end{block}
  \begin{exampleblock}{Advantages}
    \begin{itemize}
    \item updates only need recompilation of modified blocks
    \item adding blocks is trivial
    \item concurrent write to different blocks can be handled
    \end{itemize}    
  \end{exampleblock}
  \begin{alertblock}{Disadvantages}
    \begin{itemize}
    \item the metadata management becomes complex
    \end{itemize}
  \end{alertblock}
\end{frame}

\begin{frame}
  \frametitle{Combining checksums}
  \begin{block}{Idea}
    \begin{itemize}
    \item allow to combine checksums of subparts of a file
    \item allows to handle updates (if block checksums is used)
    \item allows easy support of parallel writing into a file
    \end{itemize}
  \end{block}
  \pause
  \begin{exampleblock}{}
    \begin{itemize}
    \item supported by trivial checksums (size, xor) and adler32
    \end{itemize}
  \end{exampleblock}
  \begin{alertblock}{}
    \begin{itemize}
    \item not supported by strong checksums obviously
    \end{itemize}
  \end{alertblock}
\end{frame}

\begin{frame}
  \frametitle{The adler case}
  \vspace{-.5cm}
  \begin{equation*}
    Adler_{1,n} = (A_{1,n}, B_{1,n}) \qquad with \qquad A_{1,n} = \sum_{i=1}^n{a_i}\qquad B_{1,n} = \sum_{i=1}^n{i a_i}
  \end{equation*}
  \pause
  Cutting at p, we have :
  \begin{align*}
      A_{1,n} = A_{1,p} + A_{p+1,n} \qquad B_{1,n} &= \sum_{i=1}^n{i a_i} \\
      &= B_{1,p} + \sum_{i=p+1}^n{(i-p) a_i} + p\sum_{i=p+1}^n{a_i} \\
      &= B_{1,p} + B_{p+1,n} + p~A_{p+1,n}
  \end{align*}
  \pause
  \vspace{-.5cm}
  \begin{exampleblock}{Practically}
    \begin{itemize}
    \item you need the length on top of the adler32 checksum
    \end{itemize}
  \end{exampleblock}
\end{frame}

\section[safety]{Data safety}

\subsection[redundancy]{Redundancy}

\begin{frame}
  \frametitle{How to correct corrupted data ?}
  \begin{block}{So far}
    \begin{itemize}
    \item checksum allow corruption detection
    \item but no correction is possible
    \item erasure of data is not covered either
    \item for this we need some data redundancy
    \end{itemize}
  \end{block}
  \begin{block}{Existing techniques}
    \begin{itemize}
    \item mirroring
    \item parity and RAID systems
    \item erasure coding
    \end{itemize}
  \end{block}
\end{frame}

\begin{frame}
  \frametitle{Context}
  \center
  \vspace{-1cm}
  \begin{tikzpicture}
    \foreach \x in {1,2,3}{
      \foreach \y in {1,2}{
        \draw[red!70!white] (\x,\y) rectangle node (D\x\y) {$D_{\x,\y}$} (\x+.8, \y+.8);
      }
    }
    \foreach \x in {1,2,3}{
      \foreach \y in {1,2,3}{
        \node[disk,draw,fill=blue!20] (N\x\y) at (7+\x*1.2,\y){$N_{\x,\y}$};
      }
    }
    \draw[<->,line width=3] (4.5,2) -- (6.7,2);
    \node at (2.5,4.2) {\huge \textcolor{red}{k} pieces of data};
    \node at (2.5,3.5) {\huge to be stored};
    \node at (9.3,4.2) {\huge \textcolor{blue}{n} nodes};
    \foreach \n in {N11,N23,N32}{    
      \node<2-> [cross out,draw=red,minimum width=1.1cm,line width=2] at (\n) {};
    }
  \end{tikzpicture}
  \begin{block}{Goal}
    \begin{itemize}
    \item some nodes will fail/disappear
    \item we want our data back anyway
    \item we want the cheapest price
    \end{itemize}
  \end{block}
\end{frame}

\begin{frame}<1-|handout:2,3>
  \frametitle{Replication}
  \begin{block}{Idea}
    \begin{itemize}
    \item replicate each piece of data on $p$ disks
    \item you can afford losing $p-1$ disks
    \item you pay $p$ times the original price
    \end{itemize}
  \end{block}
  \onslide<all:2-> {
    \begin{block}{Replication - RAID 1}
      \center
      \begin{tikzpicture}
        \onslide<all:2>{
          \raid[nb disks=3, nb blocks=3]
          \raidblock{red}{C}{3}{3}
          \raidblock{red}{C}{2}{3}
          \raidblock{red}{C}{1}{3}
          \raidblock{green}{B}{3}{2}
          \raidblock{green}{B}{2}{2}
          \raidblock{green}{B}{3}{2}
          \raidblock{green}{B}{1}{2}
          \raidblock{orange}{A}{3}{1}
          \raidblock{orange}{A}{2}{1}
          \raidblock{orange}{A}{1}{1}
        }
        \only<all:3>{
          \raid[nb disks=5, nb blocks=3]
          \raidblock{red}{C}{5}{3}
          \raidblock{red}{C}{4}{3}
          \raidblock{red}{C}{2}{3}
          \raidblock{green}{B}{5}{2}
          \raidblock{green}{B}{3}{2}
          \raidblock{green}{B}{1}{2}
          \raidblock{orange}{A}{3}{1}
          \raidblock{orange}{A}{2}{1}
          \raidblock{orange}{A}{1}{1}
        }
      \end{tikzpicture}
    \end{block}
  }
\end{frame}

\begin{frame}
  \frametitle{Limitations of replication}
  \begin{block}{With 2 replicas}
    \begin{itemize}
    \item expensive : effective disk space divided by 2
    \item when corruption occur, no way to know which copy is corrupted
      \begin{itemize}
      \item unless you have checksums on top
      \end{itemize}
    \end{itemize}
  \end{block}
  \begin{block}{With 3 and more replicas}
    \begin{itemize}
    \item horribly expensive, actually unaffordable in general
    \end{itemize}
  \end{block}
\end{frame}

\subsection[parity]{Parity}

\begin{frame}<1-|handout:2,3>
  \frametitle{Parity}
  \begin{block}{Idea}
    \begin{itemize}
    \item create a parity piece of data for each $k$ pieces (xor)
    \item store it as an extra piece of data
    \item you can afford losing $1$ disk
    \item you pay $1+\frac{1}{k}$ times the original price
    \end{itemize}
  \end{block}
  \pause
  \onslide<all:2-> {
    \begin{block}{Parity Node - \only<all:2>{RAID 4}\only<all:3>{RAID 5}}
      \center
      \begin{tikzpicture}
        \onslide<all:2> {
          \raid[nb disks=4, nb blocks=3]
          \raidblock{red}{$P_C$}{4}{3}
          \raidblock{red}{$C_3$}{3}{3}
          \raidblock{red}{$C_2$}{2}{3}
          \raidblock{red}{$C_1$}{1}{3}
          \raidblock{green}{$P_B$}{4}{2}
          \raidblock{green}{$B_3$}{3}{2}
          \raidblock{green}{$B_2$}{2}{2}
          \raidblock{green}{$B_1$}{1}{2}
          \raidblock{orange}{$P_A$}{4}{1}
          \raidblock{orange}{$A_3$}{3}{1}
          \raidblock{orange}{$A_2$}{2}{1}
          \raidblock{orange}{$A_1$}{1}{1}
        }
        \only<all:3> {
          \raid[nb disks=4, nb blocks=3]
          \raidblock{red}{$P_C$}{2}{3}
          \raidblock{red}{$C_3$}{1}{3}
          \raidblock{red}{$C_2$}{4}{3}
          \raidblock{red}{$C_1$}{3}{3}
          \raidblock{green}{$P_B$}{1}{2}
          \raidblock{green}{$B_3$}{4}{2}
          \raidblock{green}{$B_2$}{3}{2}
          \raidblock{green}{$B_1$}{2}{2}
          \raidblock{orange}{$P_A$}{4}{1}
          \raidblock{orange}{$A_3$}{3}{1}
          \raidblock{orange}{$A_2$}{2}{1}
          \raidblock{orange}{$A_1$}{1}{1}
        }
      \end{tikzpicture}
    \end{block}
  }
\end{frame}

\begin{frame}
  \frametitle{Main issues}
  \begin{block}{}
    \begin{itemize}
    \item writes are slightly more costful
    \item small updates are costful
      \begin{itemize}
      \item one has to read back the old data to recompute parity
      \end{itemize}
    \item only one corruption/erasure allowed
      \begin{itemize}
      \item take care that losing a disk drastically increases the probability to lose another one
      \item especially in RAID due to locality. RAIN is much better from this point of view
      \item reconstructing the lost disk can take days\\e.g. \SI{6}{\tera\byte} at \SI{100}{\mega\byte\per\second} is 17 hours
      \end{itemize}
    \end{itemize}
  \end{block}
\end{frame}

\begin{frame}
  \frametitle{Double parity}
  \begin{block}{Idea : same spirit}
    \begin{itemize}
    \item compute 2 parity pieces of data for each $k$ pieces
      \setlength{\belowdisplayskip}{0pt}
      \begin{equation*}
        P = \sum{D_i} \qquad Q = \sum{g^i D_i}
      \end{equation*}
    \item you can lose $2$ disks and still recover (less easily though)
    \item you pay $1+\frac{2}{k}$ times the original price
    \end{itemize}
  \end{block}
  \onslide<2>{
    \begin{block}{Double parity - RAID 6}
      \center
      \begin{tikzpicture}
        \raid[nb disks=5, nb blocks=3]
        \raidblock{red}{$Q_C$}{2}{3}
        \raidblock{red}{$P_C$}{1}{3}
        \raidblock{red}{$C_3$}{5}{3}
        \raidblock{red}{$C_2$}{4}{3}
        \raidblock{red}{$C_1$}{3}{3}
        \raidblock{green}{$Q_B$}{1}{2}
        \raidblock{green}{$P_B$}{5}{2}
        \raidblock{green}{$B_3$}{4}{2}
        \raidblock{green}{$B_2$}{3}{2}
        \raidblock{green}{$B_1$}{2}{2}
        \raidblock{orange}{$Q_A$}{5}{1}
        \raidblock{orange}{$P_A$}{4}{1}
        \raidblock{orange}{$A_3$}{3}{1}
        \raidblock{orange}{$A_2$}{2}{1}
        \raidblock{orange}{$A_1$}{1}{1}
      \end{tikzpicture}
    \end{block}
  }
\end{frame}

\begin{frame}
  \frametitle{Cost vs Risk computations}
  \begin{block}{Cost of parity}
    if we call $r$ the ratio of disk space compared to data size\\
    for k blocks with one parity, we have
    \begin{equation*}
      r = 1+\frac{1}{k}
    \end{equation*}
  \end{block}
  \pause
  \begin{block}{Risk reduction}
    \begin{itemize}
    \item if $p$ the probability to lose a given disk within a given period
    \item we call $p_{n,i}$ the probability of losing any $i$ disks among $n$
      \begin{align}
        p_{n,0} &= (1-p)^n \\
        p_{n,1} &= n p (1-p)^{n-1} \\
        p_{n,i} &= \binom{n}{i} p^i (1-p)^{n-i}
      \end{align}
    \end{itemize}
  \end{block}
\end{frame}

\begin{frame}
  \frametitle{Graphically, for disks within a year}
  \center
  \begin{tikzpicture}[scale=0.8, every node/.style={transform shape}]
    \begin{semilogyaxis}[xlabel={$i$}, ylabel={$p_{n,i}$},
        xmin=0, xmax=7, ymin=1e-5,
        domain={0:6},
        samples=7,
        grid=major,smooth,
        legend pos=outer north east]
      \foreach \n in {16,32,48} {
        \foreach \p in {0.01, 0.03} {
          \addplot {factorial(\n)/factorial(\n-x)/factorial(x)*(\p^x)*((1-\p)^(\n-x))};
        }
      }
      \legend{
        {$n=16$, $p=1\%$},{$n=16$, $p=3\%$},
        {$n=32$, $p=1\%$},{$n=32$, $p=3\%$},
        {$n=48$, $p=1\%$},{$n=48$, $p=3\%$}}
    \end{semilogyaxis}
  \end{tikzpicture}
\end{frame}

\subsection[erasure]{Erasure coding}

\begin{frame}
  \frametitle{Generic case (erasure coding)}
  \begin{block}{Ideas}
    \begin{itemize}
    \item compute more ``parities'' : $m$ for each $k$ blocks
    \item work on $r$ rows of $k$ blocks at a time
    \end{itemize}
  \end{block}
  \begin{exampleblock}{Goal}
    \begin{itemize}
    \item be able to lose any $m$ disks
    \item be able to reconstruct from any $k$ disks
    \end{itemize}
  \end{exampleblock}
  \center \Large
   ``Maximum Distance Separable'' code (MDS)
\end{frame}

\begin{frame}
  \frametitle{First example}
  \center
  \vbox{\huge Systematic code
    \vspace{.3cm}
  }
  \begin{tikzpicture}
    \node[draw=black, rectangle, fill=green!20, align=left] at (-0.1,2) {k=6\\m=2\\n=8\\r=4};
    \foreach \k in {1,2,3,4,5,6} {
      \foreach \r in {1,2,3,4} {
        \draw[fill=red!20] (\k,\r) rectangle node (d\k\r) {$d_{\k,\r}$} (\k+.8,\r+1);
      };
      \node[disk,fill=red!20,minimum width=1] (D\k) at (\k+0.4,-0.5) {$D\k$};
      \draw[->, line width=2] (\k+0.4,0.8) to (\k+0.4,0.2);
    };
    \draw[->, line width=2] (7.2,3) to (8.2,3);
    \foreach \k in {1,2} {
      \foreach \r in {1,2,3,4} {
        \draw[fill=blue!20] (7.5+\k,\r) rectangle node (c\k\r) {$c_{\k,\r}$} (\k+8.3,\r+1);
      };
      \node[disk,fill=blue!20,minimum width=1] (D\k) at (\k+7.9,-0.5) {$C\k$};
      \draw[->, line width=2] (\k+7.9,0.8) to (\k+7.9,0.2);
    };
    \draw[<->, line width=1] (1,5.3) -- node[fill=white,anchor=center] {$k$} (6.8,5.3);
    \draw[<->, line width=1] (8.5,5.3) -- node[fill=white,anchor=center] {$m$} (10.5,5.3);
    \draw[<->, line width=1] (.7,1) -- node[fill=white,anchor=center] {$r$} (.7,5);
  \end{tikzpicture}
\end{frame}

\begin{frame}
  \frametitle{More generic example}
  \center
  \vbox{\huge Non systematic code
    \vspace{.3cm}
  }
  \begin{tikzpicture}[scale=0.7, every node/.style={transform shape}]
    \node[draw=black, rectangle, fill=green!20, align=left]
    at (3.9,0) {\Large k=6  m=2  n=8  r=4};
    \foreach \k in {1,2,3,4,5,6} {
      \foreach \r in {1,2,3,4} {
        \draw[fill=red!20] (\k,\r) rectangle node (d\k\r) {$d_{\k,\r}$} (\k+.8,\r+1);
      };
    };
    \draw[->, line width=2] (7.2,3) to (8.2,3);
    \foreach \k in {1,2,3,4,5,6,7,8} {
      \foreach \r in {1,2,3,4} {
        \draw[fill=blue!20] (7.5+\k,\r) rectangle node (c\k\r) {$c_{\k,\r}$} (\k+8.3,\r+1);
      };
      \node[disk,fill=blue!20,minimum width=1] (D\k) at (\k+7.9,-0.5) {$C\k$};
      \draw[->, line width=2] (\k+7.9,0.8) to (\k+7.9,0.2);
    };
    \draw[<->, line width=1] (1,5.3) -- node[fill=white,anchor=center] {$k$} (6.8,5.3);
    \draw[<->, line width=1] (8.5,5.3) -- node[fill=white,anchor=center] {$n$} (16.5,5.3);
    \draw[<->, line width=1] (.7,1) -- node[fill=white,anchor=center] {$r$} (.7,5);
  \end{tikzpicture}
\end{frame}

\begin{frame}
  \frametitle{The matrix view}
  \center
  \vbox{\huge Generator Matrix $G$ is $nr$ x $kr$
    \vspace{.3cm}
  }
  \begin{tikzpicture}
    \draw[step=0.15,fill=green!20] (0,0) grid (3.6,4.8) rectangle (0,0);
    \draw[line width=1.5,step=.6] (0,0) grid (3.6,4.8);
    \node at (3.9,2.4) {$\times$};
    \draw[line width=1.5,fill=red!20] (4.2,4.2) rectangle (4.35,0.6);
    \draw[line width=1.5] (4.2,3.6) rectangle (4.35,1.2);
    \draw[line width=1.5] (4.2,3) rectangle (4.35,1.8);
    \draw[line width=1.5] (4.2,2.4) rectangle (4.35,2.4);
    \draw[step=0.15] (4.2,0.6) grid (4.35,4.2);
    \foreach \k in {1,2,3,4,5,6} {
      \node[anchor=west] at (4.4,\k*.6+.3) {\small $d_{\k,1}$-$d_{\k,r}$};
    }
    \node at (6.3,2.4) {$=$};
    \draw[line width=1.5,fill=blue!20] (6.75,4.8) rectangle (6.9,0);    
    \draw[line width=1.5] (6.75,4.2) rectangle (6.9,0.6);    
    \draw[line width=1.5] (6.75,3.6) rectangle (6.9,1.2);    
    \draw[line width=1.5] (6.75,3) rectangle (6.9,1.8);
    \draw[line width=1.5] (6.75,2.4) rectangle (6.9,2.4);
    \draw[step=0.15,fill=blue!20] (6.9,0) grid (6.75,4.8);
    \foreach \k in {1,2,3,4,5,6,7,8} {
      \node[anchor=west] (r\k) at (6.9,\k*.6-.35) {\tiny $c_{\k,1}$-$c_{\k,r}$};
      \node[disk,fill=blue!20,minimum height=.8,minimum width=1] (d\k) at (9.7,\k*.6-.35) {\small $C_\k$};
      \draw[->,line width=1.5] (r\k) -- (9,\k*.6-.35);
    }
    \onslide<2> {
      \draw[step=0.15,red,line width=1.5] (0,4.65) rectangle (3.6,4.8);
      \draw[step=0.15,red,line width=1.5] (4.2,0.6) rectangle (4.35,4.2);
      \draw[step=0.15,red,line width=1.5] (6.9,4.8) rectangle (6.75,4.65);
      \node[fill=yellow!40,draw=black,rectangle] (dp) at (5.2,5.2) {dot product};
      \draw[->,line width=1.5] (1.8,4.8) .. controls +(up:5mm) .. (dp.west);
      \draw[->,line width=1.5] (4.275,4.2) .. controls +(up:5mm) and +(down:5mm) .. (dp.south);
      \draw[->,line width=1.5] (dp.east) .. controls +(right:5mm) and +(up:5mm) .. (6.825,4.8);
    }
  \end{tikzpicture}
\end{frame}

\begin{frame}
  \frametitle{Back to systematic code}
  \center
  \vbox{\huge Systematic means $G$ includes identity
    \vspace{.3cm}
  }
  \begin{tikzpicture}
    \foreach \k in {1,2,...,24} {
      \draw[fill=green!50!black] (0.15*\k-0.15,4.95-0.15*\k) rectangle (0.15*\k,4.8-0.15*\k);
    }
    \draw[fill=green!20] (3.6,0) rectangle (0,1.2);
    \draw[step=0.15] (0,0) grid (3.6,4.8);
    \draw[line width=1.5,step=.6] (0,0) grid (3.6,4.8);
    \node at (3.9,2.4) {$\times$};
    \draw[line width=1.5,fill=red!20] (4.2,4.2) rectangle (4.35,0.6);
    \draw[line width=1.5] (4.2,3.6) rectangle (4.35,1.2);
    \draw[line width=1.5] (4.2,3) rectangle (4.35,1.8);
    \draw[line width=1.5] (4.2,2.4) rectangle (4.35,2.4);
    \draw[step=0.15] (4.2,0.6) grid (4.35,4.2);
    \foreach \k in {1,2,3,4,5,6} {
      \node[anchor=west] at (4.4,\k*.6+.3) {\small $d_{\k,1}$-$d_{\k,r}$};
    }
    \node at (6.3,2.4) {$=$};
    \draw[fill=blue!20] (6.75,1.2) rectangle (6.9,0);    
    \draw[fill=red!20] (6.75,4.8) rectangle (6.9,1.2);    
    \draw[line width=1.5] (6.75,4.8) rectangle (6.9,0);    
    \draw[line width=1.5] (6.75,4.2) rectangle (6.9,0.6);    
    \draw[line width=1.5] (6.75,3.6) rectangle (6.9,1.2);    
    \draw[line width=1.5] (6.75,3) rectangle (6.9,1.8);
    \draw[line width=1.5] (6.75,2.4) rectangle (6.9,2.4);
    \draw[step=0.15,fill=blue!20] (6.9,0) grid (6.75,4.8);
    \foreach \k in {1,2} {
      \node[anchor=west] (r\k) at (6.9,\k*.6-.35) {\tiny $c_{\k,1}$-$c_{\k,r}$};
      \node[disk,fill=blue!20,minimum height=.8,minimum width=.7cm] (d\k) at (9.7,\k*.6-.35) {\small $C_\k$};
      \draw[->,line width=1.5] (r\k) -- (9,\k*.6-.35);
    }
    \foreach \k [evaluate=\k as \m using \k-2] in {3,4,5,6,7,8} {
      \pgfmathtruncatemacro{\l}{\m}
      \node[anchor=west] (r\k) at (6.9,\k*.6-.35) {\tiny $d_{\l,1}$-$d_{\l,r}$};
      \node[disk,fill=red!20,minimum height=.8,minimum width=.7cm] (d\k) at (9.7,\k*.6-.35) {\small $D_\l$};
      \draw[->,line width=1.5] (r\k) -- (9,\k*.6-.35);
    }
  \end{tikzpicture}
\end{frame}

\begin{frame}
  \frametitle{Practical example : RAID 6}
  \center
  \begin{tikzpicture}
    \foreach \k [evaluate=\k as \coeff using 2^(6-\k)] in {1,2,3,4,5,6} {
      \draw[fill=green!20] (0.6*\k-0.6,5.4-0.6*\k) rectangle node {1} (0.6*\k,4.8-0.6*\k);
      \draw[fill=green!20] (0.6*\k-0.6,1.2) rectangle node {1} (0.6*\k,0.6);
      \draw[fill=green!20] (0.6*\k-0.6,0.6) rectangle node
           {\pgfmathtruncatemacro{\yet}{\coeff}\yet} (0.6*\k,0);
    }
    \draw[line width=1.5,step=.6] (0,0) grid (3.6,4.8);
    \node at (3.9,2.4) {$\times$};
    \foreach \k in {1,2,...,6} {
      \draw[line width=1.5,fill=red!20] (4.2,\k*.6) rectangle node {\small $d_{\k}$} (4.8,\k*.6+.6);
    }
    \node at (5.1,2.4) {$=$};
    \foreach \k in {1,2} {
      \draw[line width=1.5,fill=blue!20] (5.4,\k*.6) rectangle node (r\k) {\small $d_{\k}$} (6,\k*.6-.6);
      \node[disk,fill=blue!20,minimum height=.8,minimum width=.7cm] (d\k) at (7.5,\k*.6-.3) {\small $C_\k$};
      \draw[->,line width=1.5,shorten >=.2cm,shorten <=.2cm] (r\k) -- (d\k);
    }
    \foreach \k [evaluate=\k as \m using \k-2] in {3,4,5,6,7,8} {
      \pgfmathtruncatemacro{\l}{\m}
      \draw[line width=1.5,fill=red!20] (5.4,\k*.6) rectangle node (r\k) {\small $d_{\l}$} (6,\k*.6-.6);
      \node[disk,fill=red!20,minimum height=.8,minimum width=.7cm] (d\k) at (7.5,\k*.6-.3) {\small $D_\l$};
      \draw[->,line width=1.5,shorten >=.2cm,shorten <=.2cm] (r\k) -- (d\k);
    }
  \end{tikzpicture}
\end{frame}

\begin{frame}
  \frametitle{Systematic Reed-Solomon Codes}
  \begin{columns}
    \begin{column}{0.45\textwidth}
      \center
      \begin{tikzpicture}
        \foreach \k in {1,2,3,4,5,6} {
          \draw[fill=green!20] (0.7*\k-0.7,7-0.7*\k) rectangle node {1} (0.7*\k,6.3-0.7*\k);
          \foreach \r in {1,2,3} {
            \draw[fill=green!20] (0.7*\k-0.7,\r*0.7) rectangle node {$a_{\r,\k}$} (0.7*\k,\r*0.7-0.7);
          }
        }
        \draw[line width=1.5,step=0.7] (0,0) grid (4.2,6.3);
        \node[draw=black,rectangle,fill=green!20] at (2.1,-.5) {$k=6$ and $n=9$};
      \end{tikzpicture}
    \end{column}
    \begin{column}{0.55\textwidth}
      \begin{itemize}
      \item Create two sets $X$ and $Y$
      \item X has m elements: $x_0$ to $x_{m-1}$
      \item Y has k elements: $y_0$ to $y_{k-1}$
      \item Elements in $X \cup Y$ are distinct
      \item $a_{i,j} = \frac{1}{x_i + y_j}$ in $GF(2^w)$ \\
        (Galois Field)
      \end{itemize}
      \center
      \begin{tikzpicture}
        \node[draw=black,rectangle,anchor=west,fill=blue!20] at (0,1) {$X = {1,2,3}$};
        \node[draw=black,rectangle,anchor=west,fill=red!20] at (0,0) {$Y = {4,5,6,7,8,9}$};
      \end{tikzpicture}
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}<1-|handout:2,4>
  \frametitle{Generic reconstruction}
  \center
  \begin{tikzpicture}
    \foreach \n in {1} {
      \node[disk,fill=blue!20,minimum height=.8,minimum width=1] (d\n) at (9.7,\n*.6+.3) {\small $C_\n$};
    }
    \onslide<all:1-2>{
      \foreach \n in {2} {
        \node[disk,fill=blue!20,minimum height=.8,minimum width=1] (d\n) at (9.7,\n*.6+.3) {\small $C_\n$};
      }
    }    
    \foreach \n in {3} {
      \node[disk,fill=blue!20,minimum height=.8,minimum width=1] (d\n) at (9.7,\n*.6+.3) {\small $C_\n$};
    }
    \onslide<all:1-2>{
      \foreach \n in {4} {
        \node[disk,fill=blue!20,minimum height=.8,minimum width=1] (d\n) at (9.7,\n*.6+.3) {\small $C_\n$};
      }
    }    
    \foreach \n in {5,6,7,8} {
      \node[disk,fill=blue!20,minimum height=.8,minimum width=1] (d\n) at (9.7,\n*.6+.3) {\small $C_\n$};
    }
    \foreach \n in {1,3,5,6,7,8} {
      \onslide<all:1-4>{
        \draw[step=0.15,fill=green!20] (0,\n*.6) grid (3.6,\n*.6+.6) rectangle (0,\n*.6);
        \draw[line width=1.5,step=.6] (0,\n*.6-0.0001) grid (3.60001,\n*.6+.60001);
        \draw[step=0.15,fill=blue!20] (6.9,\n*.6) grid (6.75,\n*.6+.6) rectangle (6.9,\n*.6);
        \draw[line width=1.5,step=.6] (6.9,\n*.6) rectangle (6.75,\n*.6+.6);
        \node[anchor=west] at (7.1,\n*.6+.3) (r\n) {\small $c_{\n,1}$-$d_{\n,r}$};
        \draw[->,line width=1.5,shorten >=.2cm,shorten <=.2cm] (r\n) -- (d\n);
      }
    }
    \onslide<all:1-2>{
      \foreach \n in {2,4} {
        \draw[step=0.15,fill=green!20] (0,\n*.6) grid (3.6,\n*.6+.6) rectangle (0,\n*.6);
        \draw[line width=1.5,step=.6] (0,\n*.6) grid (3.6,\n*.6+.6);
        \draw[step=0.15,fill=blue!20] (6.9,\n*.6) grid (6.75,\n*.6+.6) rectangle (6.9,\n*.6);
        \draw[line width=1.5,step=.6] (6.9,\n*.6) rectangle (6.75,\n*.6+.6);
        \node[anchor=west] at (7.1,\n*.6+.3) (r\n) {\small $c_{\n,1}$-$d_{\n,r}$};
        \draw[->,line width=1.5,shorten >=.2cm,shorten <=.2cm] (r\n) -- (d\n);
      }
    }
    \node at (3.9,3) {$\times$};
    \foreach \n in {1,2,3,4,5,6} {
      \draw[step=0.15,fill=red!20] (4.2,\n*.6+.6) grid (4.35,\n*.6+1.2) rectangle (4.2,\n*.6+.6);
      \draw[line width=1.5,step=.6] (4.2,\n*.6+.6) rectangle (4.35,\n*.6+1.2);
      \node[anchor=west] at (4.4,\n*.6+.9) {\small $d_{\n,1}$-$d_{\n,r}$};
    }
    \node at (6.3,3) {$=$};
    \foreach \p in {2,4} {
      \node<all:2> [cross out,draw=red,minimum width=.8cm,line width=2] at (d\p) {};
    }
    \onslide<all:4> {
      \node at (1.8,0) {$G_{del}$};
      \node at (3,0) {$\times$};
      \node at (4.2,0) {$D_{del}$};
      \node at (5.5,0) {$=$};
      \node at (6.8,0) {$C_{del}$};
    }
  \end{tikzpicture}
\end{frame}

\begin{frame}
  \frametitle{Generic reconstruction}
  \center
  \begin{tikzpicture}[scale=0.95, every node/.style={transform shape}]
    \draw[fill=green!20] (0,0) rectangle node {$G_{del}$} (3.6,3.6);
    \node at (3.9,1.8) {$\times$};
    \draw[fill=red!20] (4.2,0) rectangle node {$D_{del}$} (5.0,3.6);
    \node at (5.3,1.8) {$=$};
    \draw[fill=blue!20] (5.6,0) rectangle node {$C_{del}$} (6.4,3.6);
    \node[align=center] at (8.4,1.8) {\Large ``Simple'' matrix \\ \Large inversion};
    \draw[fill=red!20] (4.2,-.4) rectangle node {$D_{del}$} (5,-4);
    \node at (5.3,-2.2) {$=$};
    \draw[fill=green!20] (5.6,-.4) rectangle node {$G_{del}^{-1}$} (9.2,-4);
    \node at (9.5,-2.2) {$\times$};
    \draw[fill=blue!20] (9.8,-.4) rectangle node {$C_{del}$} (10.6,-4);
  \end{tikzpicture}
\end{frame}

\begin{frame}
  \frametitle{Reconstruction cost}
  \begin{block}{Important parameters}
    \begin{itemize}
    \item systematic codes are very interesting for no error case
    \item parity is very cheap for a single erasure/corruption : simple xor
    \item galois fields are more costful, especially when m grows
    \end{itemize}
  \end{block}
  \pause
  \begin{exampleblock}{Improvements are possible}
    \begin{itemize}
    \item pyramid codes bring improvements of reconstruction time of errors in different subparts
    \item Cauchy Reed-Solomon : replaces Galois fields with pure xors
    \item evenodd, RDP : fast single error recovery 
    \end{itemize}
  \end{exampleblock}
\end{frame}

\section[c/c]{Conclusion}

\begin{frame}
  \frametitle{Conclusion}
  \begin{exampleblock}{Key messages of the day}
    \begin{itemize}
    \item {\bf You will lose and corrupt data} - better be prepared
    \item Checksums will allow you to know
    \item RAID, RAIN and erasure coding will allow to recover
      \begin{itemize}
      \item choose your security level, pay the price
      \end{itemize}
    \end{itemize}
  \end{exampleblock}
\end{frame}

\end{document}
