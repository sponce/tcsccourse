\include{header}

%%%%%%%%%%%%%%%%%%
% Document setup %
%%%%%%%%%%%%%%%%%%

\title{Many ways to store data}
\author[S. Ponce]{S\'ebastien Ponce\\ \texttt{sebastien.ponce@cern.ch}}
\institute{CERN}
\date{tCSC May \the\year}

%%%%%%%%%%%%%%
% The slides %
%%%%%%%%%%%%%%

\begin{document}

\begin{frame}
  \titlepage
\end{frame}

\begin{frame}
  \frametitle{Overall Course Structure}
  \begin{block}{Many ways to Store Data}
    \begin{itemize}
      \setlength\itemsep{.07cm}
    \item {\small Storage devices and their specificities}
    \item {\small Distributing and parallelizing storage}
    \end{itemize}
  \end{block}
  \begin{block}{Preserving data}
    \begin{itemize}
      \setlength\itemsep{.07cm}
    \item {\small Data consistency}
    \item {\small Data safety}
    \end{itemize}
  \end{block}
  \begin{block}{Key ingredients to achieve efficient I/O}
    \begin{itemize}
      \setlength\itemsep{.07cm}
    \item {\small Synchronous vs asynchronous I/O}
    \item {\small I/O optimizations and caching}
    \end{itemize}
  \end{block}
\end{frame}

\begin{frame}
  \frametitle{Outline}
  \vspace{-0.5cm}
  \begin{scriptsize}
    \tableofcontents[sectionstyle=show,subsectionstyle=show]
  \end{scriptsize}
\end{frame}

\section[devices]{Storage devices}

\subsection[zoo]{Existing devices}

\begin{frame}
  \frametitle{A variety of storage devices}
  \begin{block}{Main differences}
    \begin{itemize}
    \item Capacities from \SI{1}{\giga\byte} to \SI{10}{\tera\byte} per unit
    \item Prices from 1 to 300 for the same capacity
    \item Very different reliability
    \item Very different speeds
    \end{itemize}
  \end{block}
  \pause
  \begin{exampleblock}{Typical numbers in 2019}
    \center
    \begin{tabular}{|l|r|r|r|r|r|}
      \hline
           & \parbox{1.5cm}{Capacity \\ per unit} & Latency & \SI{}{\$\per\tera\byte} & Speed           & reliability \\
      \hline
      RAM  & \SI{16}{\giga\byte}  & \SI{10}{\nano\second}    & 7000 \$ & \SI{10}{\giga\byte\per\second}  &    volatile \\
      SSD  & \SI{500}{\giga\byte} & \SI{10}{\micro\second}  &  200 \$ & \SI{1}{\giga\byte\per\second} &        poor \\
      HD   & \SI{6}{\tera\byte}   & \SI{3}{\milli\second}   &   25 \$ & \SI{150}{\mega\byte\per\second} &     average \\
      Tape & \SI{20}{\tera\byte}  & \SI{100}{\second}       &   20 \$ & \SI{500}{\mega\byte\per\second} &        good \\
      \hline
    \end{tabular}
  \end{exampleblock}
\end{frame}

\begin{frame}
  \frametitle{A variety of storage devices}
  \begin{block}{You cannot have everything}
    \center
    \begin{tikzpicture}
      \coordinate (cheap) at (90:3);
      \coordinate (rely) at (210:3);
      \coordinate (speed) at (-30:3);
      \node[above] at (cheap) {cheap};
      \node[below left] at (rely) {reliability};
      \node[below right] at (speed) {speed};
      \draw[line width=1.3] (cheap.south) -- (rely.north east) -- (speed.north west) -- cycle;
      \node at (barycentric cs:cheap=.1 ,rely=.15 ,speed=1)  {RAM};
      \node at (barycentric cs:cheap=.3,rely=.3,speed=.6) {SSD};
      \node at (barycentric cs:cheap=.8 ,rely=.5,speed=.2) {HD};
      \node at (barycentric cs:cheap=1 ,rely=1 ,speed=.6) {Tape};
    \end{tikzpicture}
  \end{block}
\end{frame}

\begin{frame}
  \frametitle{Reliability in real world (CERN)}
  \begin{block}{For disks}
    \begin{itemize}
    \item probability of losing a disk per year : few \%, up to 10\%
      \begin{itemize}
      \item with 60K disks, it's around 10 per day
      \item and all files are lost
      \end{itemize}
    \item one unrecoverable bit error in $10^{14}$ bits read/written
      \begin{itemize}
      \item for 10GB files, that's one file corrupted per 1000 files written
      \end{itemize}
    \end{itemize}
  \end{block}
  \pause
  \begin{block}{For tapes}
    \begin{itemize}
    \item probability of losing a tape per year : ~$10^{-4}$
      \begin{itemize}
      \item and you recover most of the data on it
      \item net result is ~$10^{-7}$ file loss per year
      \end{itemize}
    \item one unrecoverable bit error in $10^{19}$ bits read/written 
      \begin{itemize}
      \item for 10GB files, that's one file corrupted per 100M files written
      \end{itemize}
    \end{itemize}
  \end{block}
\end{frame}

\subsection[HSM]{Hierarchical storage}

\begin{frame}
  \frametitle{Practical Mass Storage - Real Big Data}
  \Large when you count in 100s of PetaBytes...
  \begin{block}{The constraints}
    \begin{itemize}
    \item disks or tapes are the only possible solutions
    \item disks are unreliable at that scale, and need redundancy
      \begin{itemize}
      \item we'll see that extensively
      \end{itemize}
    \item tapes are cheaper long term storage by factor 2-2.5
    \item tape latency imposes data access on disk
    \end{itemize}
  \end{block}
\end{frame}

\begin{frame}
  \frametitle{Specificities of tape storage}
  \begin{block}{Key points}
    \begin{itemize}
    \item 500MB/s in sequential read/write
      \begin{itemize}
      \item 4x the speed of a disk
      \item who said tape is slow ?
      \end{itemize}
    \item latency/seek time in the order of minutes !
      \begin{itemize}
      \item due to mount time and robot arm moving
      \item due to positionning
      \end{itemize}
    \item storage is cheap, I/O is not
      \begin{itemize}
      \item 20\$/TB for storage capacity
      \item 25K\$ for each drive
      \end{itemize}
    \end{itemize}
  \end{block}
\end{frame}

\begin{frame}
  \frametitle{Tape efficiency}
  \begin{block}{Computation}
    \begin{align*}
      efficiency  &= \frac{I/O\ time}{mount\ time + I/O\ time} \\
      mount\ size &= mount\ time * drive\ speed \\
      efficiency  &= \frac{1}{1 + \frac{mount\ size}{data\ size}} \\
      mount\ size &\simeq \SI{50}{\giga\byte}
    \end{align*}
  \end{block}
\end{frame}

\begin{frame}
  \frametitle{Tape efficiency}
  \center
  \begin{tikzpicture}
    \begin{loglogaxis}[xlabel={$size (GB)$}, ylabel={$efficiency$},
        xmin=.001, xmax=10000, ymin=0.0005, ymax=1,
        domain={0.005:20000},grid=major,smooth,
        xtick={.01,1,200,10000},
        xticklabels={\SI{10}{\mega\byte}, \SI{1}{\giga\byte}, \SI{200}{\giga\byte}, \SI{1}{\tera\byte}},
        ytick={.001,.01,.1,.8},
        yticklabels={1\permil,1\%,10\%,80\%}]
      \addplot {1/(1+50/x)};
    \end{loglogaxis}
    \draw[->,line width=3,anchor=west] (-1,-1.5) -- (0,-1.5);
    \node[anchor=west] at (.5,-1.5) {\Large \color{red} No mount for less than \SI{100}{\giga\byte} !};
  \end{tikzpicture}
\end{frame}

\begin{frame}
  \frametitle{Hierarchical storage}
  \begin{block}{Layers}
    \begin{itemize}
    \item tape as primary storage
    \item disks used as a cache in front of the tape system
    \item SSD used as cache in front of disks (or inside them)
    \end{itemize}
  \end{block}
  \begin{exampleblock}{CERN's case (2018)}
    \begin{itemize}
    \item tape capacity : \SI{335}{\peta\byte}
    \item tape usage : \SI{325}{\peta\byte}
    \item disk raw capacity : \SI{280}{\peta\byte}
    \item disk usage : \SI{215}{\peta\byte} for \SI{112}{\peta\byte} of data
    \end{itemize}    
  \end{exampleblock}
\end{frame}

\begin{frame}
  \frametitle{Some consequences}
  \begin{block}{Disk cache management is needed}
    \begin{itemize}
    \item the disk cache needs garbage collection
    \item different algorithm used depending on usage
      \begin{itemize}
      \item FIFO - First In First Out
      \item LRU - Least Recently Used
      \end{itemize}
    \end{itemize}
  \end{block}
  \begin{block}{User need to adapt their usage}
    \begin{itemize}
    \item data need to be prefetched from tape before access
    \item preferably in bulk
    \end{itemize}
  \end{block}
\end{frame}
      
\section[distrib]{Distributed storage}

\begin{frame}
  \frametitle{Handling large distributed storage}
  \begin{block}{Standard issues}
    \begin{itemize}
    \item failures are very common
    \item data distribution is hard to balance
    \item congestions are frequent
    \end{itemize}
  \end{block}
\end{frame}

\subsection[distrib]{Data distribution}

\begin{frame}<1-|handout:2,4>
  \frametitle{Keeping balanced data distribution}
  \begin{block}{sounds initially easy}
    \begin{itemize}
    \item standard load balancing
    \item taking benefit of the numerous clients
    \end{itemize}
  \end{block}
  \onslide<all:3-> {
    \begin{block}{Growing the cluster}
      \begin{itemize}
      \item when you add disks, their are empty
      \item data need to be rebalanced
      \end{itemize}
    \end{block}
  }
  \center
  \onslide<all:2-> {  
    \begin{tikzpicture}
      \raid[nb disks=3, nb blocks=3]
      \onslide<all:2-> {
        \raidblock{blue}{$E$}{1}{2}
        \raidblock{pink}{$D$}{3}{2}
      }
      \raidblock{red}{$C$}{2}{1}
      \raidblock{green}{$B$}{3}{1}
      \raidblock{orange}{$A$}{1}{1}
      \onslide<all:3-> {
        \raid[first disk=4, nb disks=2, nb blocks=3]
      }
      \onslide<all:4-> {
        \raidblock{pink}{$D$}{5}{1}
        \raidblock{blue}{$E$}{4}{1}
        \node[cross out,draw=red,minimum width=1.5cm,line width=2] at (block-1-2) {};
        \node[cross out,draw=red,minimum width=1.5cm,line width=2] at (block-3-2) {};
      }
    \end{tikzpicture}
  }
\end{frame}

\begin{frame}
  \frametitle{Data congestions}
  \begin{block}{The problem of popular files}
    \begin{itemize}
    \item on writes, you can send a piece of data where you like
    \item on reads, you have to send the client where the data is
    \item but for popular pieces of data, the device holding it may become congested
    \end{itemize}
  \end{block}
  \center
  \onslide<2-> {
    \begin{tikzpicture}
      \raid[nb disks=3, nb blocks=2]
      \raidblock{red}{$C$}{2}{1}
      \raidblock{green}{$B$}{3}{1}
      \raidblock{orange}{$A$}{1}{1}
      \foreach \n in {1,2,3,4} {
        \draw[black,fill=blue!20] (\n*2-1.7,1) rectangle node (Client \n) {Client \n} (\n*2-.2,2);
      }
      \onslide<3-> {
        \draw[->,line width=1.2] (Client 1.south) -- (Disk 2.north);
        \draw[->,line width=1.2] (Client 2.south) -- (Disk 2.north);
        \draw[->,line width=1.2] (Client 3.south) -- (Disk 1.north);
        \draw[->,line width=1.2] (Client 3.south) -- (Disk 2.north);
        \draw[->,line width=1.2] (Client 4.south) -- (Disk 2.north);
        \draw[->,line width=1.2] (Client 4.south) -- (Disk 3.north);
      }
    \end{tikzpicture}
  }
\end{frame}

\begin{frame}<1-|handout:2,4>
  \frametitle{Can be solved using data temperature}
  \begin{block}{Principle}
    \begin{itemize}
    \item temperature is data popularity
    \item ``hot'' data is accessed more often
    \item data getting hotter than given threshold get replicated
    \item on cool down, replicas should be dropped
    \end{itemize}
  \end{block}
  \center
  \onslide<all:2-> {
    \begin{tikzpicture}
      \raid[nb disks=4, nb blocks=2]
      \raidblock{red}{$C$}{2}{1}
      \raidblock{green}{$B$}{3}{1}
      \raidblock{orange}{$A$}{1}{1}
      \foreach \n in {1,2,3,4} {
        \draw[black,fill=blue!20] (\n*2-.9,1) rectangle node (Client \n) {Client \n} (\n*2+.6,2);
      }
      \draw[->,line width=1.2] (Client 1.south) -- (Disk 2.north);
      \draw[->,line width=1.2] (Client 2.south) -- (Disk 2.north);
      \draw[->,line width=1.2] (Client 3.south) -- (Disk 1.north);
      \onslide<all:2-3> {
        \draw[->,line width=1.2] (Client 3.south) -- (Disk 2.north);
        \draw[->,line width=1.2] (Client 4.south) -- (Disk 2.north);
      }
      \draw[->,line width=1.2] (Client 4.south) -- (Disk 3.north);
      \onslide<all:3-> {
        \raidblock{red}{$C$}{4}{1}
      }
      \onslide<all:4-> {
        \draw[->,line width=1.2] (Client 3.south) -- (Disk 4.north);
        \draw[->,line width=1.2] (Client 4.south) -- (Disk 4.north);
      }
    \end{tikzpicture}
  }
\end{frame}

\subsection[federation]{Data federation}

\begin{frame}
  \frametitle{The global view}
  \begin{block}{A World distributed storage}
    \begin{itemize}
    \item large experiments' data are distributed around the world
    \item usually replicating the data at least once
    \item networks are now fast and you can access remote data through wide area network when local data in not available
    \item but you do not want to do that by default
    \item and you want to try the ``closest'' replica first
    \end{itemize}
  \end{block}
  \pause
  \begin{block}{Data geolocalization}
    \begin{itemize}
    \item a global central catalog lists all available replicas
    \item and their ``physical'' location
    \item with information on the topology of the network to reach them
    \end{itemize}
  \end{block}
\end{frame}

\begin{frame}
  \frametitle{Data federation}
  \begin{block}{Hiding the underlying complexity}
    \begin{itemize}
    \item some interfaces that hide this world wide distribution of data
    \item by redirecting clients dynamically to the closest data available
    \end{itemize}
  \end{block}
  \begin{block}{Xrootd federation}
    \begin{itemize}
    \item widely used by LHC experiments across the world
    \end{itemize}
  \end{block}
\end{frame}

\begin{frame}<1-|handout:3,5>
  \frametitle{Typical federation in an LHC experiment}
  \center
  \begin{tikzpicture}[scale=0.7, every node/.style={transform shape}]
    \tikzstyle{every node}=[align=center,draw]
    \node[fill=blue!20] (top) {Global\\redirector}
         [level distance=25mm, sibling distance=30mm]
         child {node[fill=green!20] {CERN\\redirector}
           child {node[fill=red!20] {Meyrin\\redirector}
             child {node[disk,fill=yellow!15] {Meyrin\\Storage}}
           }
           child {node[fill=red!20] {Budapest\\redirector}
             child {node[disk,fill=yellow!15] {Budapest\\Storage}}
           }
         }
         child {node[fill=green!20] {...}}
         child {node[fill=green!20] {T1\\Redirector}
           child {node[disk,fill=yellow!15] {T1\\Storage}}
           child {node[fill=red!20] {T2\\Redirector}
             child {node[disk,fill=yellow!15] {T2\\Storage}}
           }
         };
         \onslide<all:2-> {
           \node[draw,line width=1.5,left of=top-1-1,xshift=-1.5cm] (client) {Client};
         }
         \draw<all:2->[->,line width=1.5] (client) -- (top-1-1);
         \draw<all:3>[->,line width=1.5] (top-1-1) -- (top-1-1-1);
         \draw<all:4>[->,line width=1.5] (top-1-1) -- (top-1) -- (top-1-2) -- (top-1-2-1);
         \draw<all:5>[->,line width=1.5] (top-1-1) -- (top-1) -- (top) -- (top-3) -- (top-3-2) -- (top-3-2-1);
  \end{tikzpicture}
\end{frame}


\section[//]{Parallelizing files' storage}

\begin{frame}
  \frametitle{Why to parallelize storage ?}
  \begin{block}{to work around limitations}
    \begin{itemize}
    \item individual device speed (think disk)
      \begin{itemize}
      \item a file is typically stored on a single device
      \end{itemize}
    \item network cards' speed
      \begin{itemize}
      \item \SI{1}{\giga\bit} network still present
      \item network congestion on a node reduces bandwidth per stream
      \end{itemize}
    \item core network throughput
      \begin{itemize}
      \item switches / routers are expensive
      \item machines may have less throughput than their card(s) allow(s)
      \end{itemize}
    \item hot data congestions
      \begin{itemize}
      \item and the black hole it can generate
      \item as slower tranfers allow to accumulate more transfers
      \end{itemize}
    \end{itemize}
  \end{block}
\end{frame}

\subsection[striping]{Striping}

\begin{frame}
  \frametitle{Parallelizing through striping}
  \begin{block}{Main idea}
    \begin{itemize}
    \item use several devices in parallel for a single stream
    \item moving the limitations up by summing performances
    \end{itemize}
  \end{block}
  \begin{block}{Basic striping : Divide and conquer for storage}
    \begin{itemize}
    \item split data into chunks aka stripes on different devices
    \item access in parallel
    \end{itemize}
  \end{block}
  \pause
  \center
  \begin{tikzpicture}
    \raid[nb disks=5, nb blocks=3]
    \raidblock{red}{File C.6}{3}{3}
    \raidblock{red}{File C.5}{2}{3}
    \raidblock{red}{File C.4}{1}{3}
    \raidblock{red}{File C.3}{5}{2}
    \raidblock{red}{File C.2}{4}{2}
    \raidblock{red}{File C.1}{3}{2}
    \raidblock{green}{File B.3}{2}{2}
    \raidblock{green}{File B.2}{1}{2}
    \raidblock{green}{File B.1}{5}{1}
    \raidblock{orange}{File A.4}{4}{1}
    \raidblock{orange}{File A.3}{3}{1}
    \raidblock{orange}{File A.2}{2}{1}
    \raidblock{orange}{File A.1}{1}{1}
  \end{tikzpicture}
\end{frame}

\begin{frame}
  \frametitle{RAID 0}
  \begin{block}{RAID}
    \begin{itemize}
    \item stands to ``Redundant Array of Inexpensive Disks''
    \item set of configurations that employ the techniques of striping, mirroring, or parity to create large reliable data stores from multiple general-purpose computer hard disk drives (Wikipedia)
    \end{itemize}
  \end{block}
  \begin{exampleblock}{Useful RAID levels}
    \begin{tikzpicture}
      \node[anchor=east] (n) {
        \parbox{4.8cm}{
          \begin{description}
          \item[RAID 0] striping
          \item[RAID 1] mirroring
          \item[RAID 5] parity
          \item[RAID 6] double parity
          \end{description}
        }
      };
      \onslide<2-> {
        \draw[thick,snake=brace]
        ($(n.east)+(0,.6)$) -- node[midway, right=4pt] {See data preservation talk} ($(n.east)-(0,.9)$);
      }
    \end{tikzpicture}
  \end{exampleblock}
  Can be implemented in hardware or software
\end{frame}

\begin{frame}
  \frametitle{RAID versus RAIN}
  \begin{block}{RAIN}
    \begin{itemize}
    \item Redundant Array of Inexpensive Nodes
    \item similar to RAID but across nodes
    \end{itemize}
  \end{block}
  \begin{exampleblock}{Main interest}
    \begin{itemize}
    \item tackle also the network limitations
    \item when used for redundancy, improves reliability
    \item more on this in subsequent lecture
    \end{itemize}
  \end{exampleblock}
\end{frame}

\begin{frame}
  \frametitle{Practical striping - the stripe size}
  \begin{block}{Desired picture}
    \center
    \begin{tikzpicture}
      \raid[nb disks=5, nb blocks=3]
      \raidblock{red}{File C.6}{3}{3}
      \raidblock{red}{File C.5}{2}{3}
      \raidblock{red}{File C.4}{1}{3}
      \raidblock{red}{File C.3}{5}{2}
      \raidblock{red}{File C.2}{4}{2}
      \raidblock{red}{File C.1}{3}{2}
      \raidblock{green}{File B.3}{2}{2}
      \raidblock{green}{File B.2}{1}{2}
      \raidblock{green}{File B.1}{5}{1}
      \raidblock{orange}{File A.4}{4}{1}
      \raidblock{orange}{File A.3}{3}{1}
      \raidblock{orange}{File A.2}{2}{1}
      \raidblock{orange}{File A.1}{1}{1}
    \end{tikzpicture}
  \end{block}
\end{frame}

\begin{frame}
  \frametitle{Practical striping - the stripe size}
  \begin{block}{Stripes too big}
    \center
    \begin{tikzpicture}
      \raid[nb disks=5, nb blocks=1, size y=2.8cm]
      \raidblock[block size=.7cm]{red}{File C.2}{4}{1}
      \raidblock[block size=2.8cm]{red}{File C.1}{3}{1}
      \raidblock[block size=2.1cm]{green}{File B.1}{2}{1}
      \raidblock[block size=2.8cm]{orange}{File A.1}{1}{1}
    \end{tikzpicture}    
  \end{block}
  RAID has practically not effect
\end{frame}

\begin{frame}
  \frametitle{Practical striping - the stripe size}
  \begin{block}{Striped too small}
    \center
    \begin{tikzpicture}
      \raid[nb disks=5, nb blocks=9, block size=.175cm, size y=.175cm, font size=\tiny]
      \raidblock{red}{File C.20}{2}{10}
      \raidblock{red}{File C.19}{2}{10}
      \raidblock{red}{File C.18}{1}{10}
      \raidblock{red}{File C.17}{5}{9}
      \raidblock{red}{File C.16}{4}{9}
      \raidblock{red}{File C.15}{3}{9}
      \raidblock{red}{File C.14}{2}{9}
      \raidblock{red}{File C.13}{1}{9}
      \raidblock{red}{File C.12}{5}{8}
      \raidblock{red}{File C.11}{4}{8}
      \raidblock{red}{File C.10}{3}{8}
      \raidblock{red}{File C.9}{2}{8}
      \raidblock{red}{File C.8}{1}{8}
      \raidblock{red}{File C.7}{5}{7}
      \raidblock{red}{File C.6}{4}{7}
      \raidblock{red}{File C.5}{3}{7}
      \raidblock{red}{File C.4}{2}{7}
      \raidblock{red}{File C.3}{1}{7}
      \raidblock{red}{File C.2}{5}{6}
      \raidblock{red}{File C.1}{4}{6}
      \raidblock{green}{File B.12}{3}{6}
      \raidblock{green}{File B.11}{2}{6}
      \raidblock{green}{File B.10}{1}{6}
      \raidblock{green}{File B.9}{5}{5}
      \raidblock{green}{File B.8}{4}{5}
      \raidblock{green}{File B.7}{3}{5}
      \raidblock{green}{File B.6}{2}{5}
      \raidblock{green}{File B.5}{1}{5}
      \raidblock{green}{File B.4}{5}{4}
      \raidblock{green}{File B.3}{4}{4}
      \raidblock{green}{File B.2}{3}{4}
      \raidblock{green}{File B.1}{2}{4}
      \raidblock{orange}{File A.16}{1}{4}
      \raidblock{orange}{File A.15}{5}{3}
      \raidblock{orange}{File A.14}{4}{3}
      \raidblock{orange}{File A.13}{3}{3}
      \raidblock{orange}{File A.12}{2}{3}
      \raidblock{orange}{File A.11}{1}{3}
      \raidblock{orange}{File A.10}{5}{2}
      \raidblock{orange}{File A.9}{4}{2}
      \raidblock{orange}{File A.8}{3}{2}
      \raidblock{orange}{File A.7}{2}{2}
      \raidblock{orange}{File A.6}{1}{2}
      \raidblock{orange}{File A.5}{5}{1}
      \raidblock{orange}{File A.4}{4}{1}
      \raidblock{orange}{File A.3}{3}{1}
      \raidblock{orange}{File A.2}{2}{1}
      \raidblock{orange}{File A.1}{1}{1}
    \end{tikzpicture}    
  \end{block}
  RAID will only kill performance by forcing disk to seek far too often
\end{frame}

\begin{frame}
  \frametitle{How to choose the stripe size}
  \begin{block}{size of the stripe}
    \begin{itemize}
    \item must be as small as possible to let small reads benefit from parallelization
    \item must not be too small
      \begin{itemize}
      \item to avoid having to deal with too much metadata
      \item to avoid too much disk seeking
      \end{itemize}      
    \end{itemize}
  \end{block}
\end{frame}

\begin{frame}
  \frametitle{A generic solution for the stripe size}
  \begin{block}{Idea}
    \begin{itemize}
    \item disentangle ``stripe size'' from ``object size''
    \item ``stripe size'' is the size of one slice of data
    \item ``object size'' is the size of one block of data on disk
    \item several stripes are put together into one bigger object
    \end{itemize}
  \end{block}
\end{frame}

\begin{frame}
  \frametitle{Ceph striping}
  \center
  \begin{tikzpicture}[scale=0.57, every node/.style={transform shape}]
    \foreach \objset in {0,1} {
      \pgfmathparse{\objset*5+4}\let\lastobjnbf\pgfmathresult
      \pgfmathtruncatemacro{\lastobjnb}{\lastobjnbf}
      \foreach \obj in {0,1,2,3,4} {
        \pgfmathparse{\objset*5+\obj}\let\objnbf\pgfmathresult
        \pgfmathtruncatemacro{\objnb}{\objnbf}
        \draw[fill=black!40] (\obj*3,-\objset*6.6) rectangle node (top\objnb) {Object \objnb} (\obj*3+2,-\objset*6.6-1);
        \foreach \unit in {0,1,2} {
          \pgfmathparse{\objset*15+\unit*5+\obj}\let\unitnbf\pgfmathresult
          \pgfmathtruncatemacro{\unitnb}{\unitnbf}
          \draw[draw] (\obj*3,-\objset*6.6-1.1-\unit*1.1) rectangle node (unit\unitnb) {Unit \unitnb} (\obj*3+2,-\objset*6.6-2.1-\unit*1.1);
        }
        \draw[fill=black!40] (\obj*3,-\objset*6.6-4.4) rectangle node (bottom\objnb) {Object \objnb} (\obj*3+2,-\objset*6.6-5.4);
      }
    }
    \onslide<2-> {
      \draw[draw=orange,line width=1] (11.95,-1.05) rectangle (18,-2.15);
      \node[orange,thick,anchor=east] at (17.8,-1.6) {\LARGE Stripe Unit 4};
    }
    \onslide<3-> {
      \draw[draw=red,line width=1] (-.05,-3.25) rectangle (18,-4.35);
      \node[red,thick,anchor=east] at (17.8,-3.8) {\LARGE Stripe 2};
    }
    \onslide<4-> {
      \draw[draw=black,line width=1] (-0.15,0.95) rectangle (2.15,-5.55);
      \node[black,thick,anchor=south] at (1,0.1) {\LARGE Object 0};
    }
    \onslide<5-> {
      \draw[draw=green!70!black,line width=1] (8.85,.15) rectangle (11.15,-13);
      \node[green!70!black,thick,anchor=north] at (10,-12.2) {\LARGE Disk 3};
    }
    \onslide<6-> {
      \draw[draw=blue,line width=1] (-.05,-6.55) rectangle (18,-12.05);
      \node[blue,thick,anchor=east] at (17.8,-9.3) {\LARGE Object Set 1};
    }
  \end{tikzpicture}
\end{frame}

\begin{frame}
  \frametitle{Practical striping - number of disks}
  \begin{exampleblock}{Why to have many}
    \begin{itemize}
    \item to increase parallelism
    \item to get better performances
    \end{itemize}
  \end{exampleblock}
  \begin{alertblock}{Why to have few}
    \begin{itemize}
    \item to limit the risk of losing files
    \item as losing a disk now means losing all files of all disks
    \item if $p$ is the probability to lose a disk \\
      the probability to lose one in $n$ is $p_n = n p (1-p)^{n-1} \sim n p$
    \end{itemize}
  \end{alertblock}
\end{frame}

\begin{frame}
  \frametitle{A generic solution for the number of disks}
  \begin{block}{Idea}
    \begin{itemize}
    \item disentangle ``nb disks'' from ``nb stripes''
    \item do not use all disks for all files
    \item adapt your number of disks to each file
      \begin{itemize}
      \item more disks for high performance files
      \item less disks for more safety
      \end{itemize}
    \end{itemize}
  \end{block}
  \pause
  \center
  \begin{tikzpicture}
    \raid[nb disks=5, nb blocks=3, size y=.7cm]
    \raidblock[block size=1.9cm]{green}{Safe2.2}{4}{2}
    \raidblock[block size=1.9cm]{green}{Safe2.1}{3}{2}
    \raidblock[block size=1.9cm]{orange}{Safe1.2}{2}{2}
    \raidblock[block size=1.9cm]{orange}{Safe1.1}{1}{2}
    \raidblock[block size=.7cm]{red}{HP.5}{5}{1}
    \raidblock[block size=.7cm]{red}{HP.4}{4}{1}
    \raidblock[block size=.7cm]{red}{HP.3}{3}{1}
    \raidblock[block size=.7cm]{red}{HP.2}{2}{1}
    \raidblock[block size=.7cm]{red}{HP.1}{1}{1}
  \end{tikzpicture}
\end{frame}

\subsection[mapreduce]{Introduction to Map/Reduce}

\begin{frame}
  \frametitle{Going further : Map/Reduce}
  \begin{block}{What do we have with striping ?}
    \begin{itemize}
    \item striping allows to distribute server I/O on several devices
    \item but client still faces the total I/O
    \item and CPU is not distributed
    \end{itemize}
  \end{block}
  \begin{block}{Map/Reduce Idea}
    \begin{itemize}
    \item send computation to the data nodes
    \item ``the most efficient network I/O is the one you don't do''
    \end{itemize}
  \end{block}
\end{frame}

\begin{frame}
  \frametitle{Map/Reduce Introduction}
  \begin{block}{Schema}
    \begin{description}
    \item[Map] processes data locally and returns key/value pairs
    \item[Shuffle] sends output to $Reduce$ step based on output key
    \item[Reduce] merges partial results to final output
    \end{description}
  \end{block}
  \center
  \tikzstyle data=[draw=black, thick, rectangle,
    minimum height=.8cm,minimum width=1cm]
  \begin{tikzpicture}
    \node[data,fill=blue!20,minimum height=3cm] (data) {Data};
    \node[disk,fill=red!20] at (2,-1.5) (data3) {\begin{tikzpicture}
        \node[data,fill=blue!20] {data3}; \end{tikzpicture}};
    \node[disk,fill=red!20] at (2,0) (data2) {\begin{tikzpicture}
        \node[data,fill=blue!20] {data2}; \end{tikzpicture} };
    \node[disk,fill=red!20] at (2,1.5) (data1) {\begin{tikzpicture}
        \node[data,fill=blue!20] {data1}; \end{tikzpicture} };
    \draw[->,line width=1] (data) -- (data1);
    \draw[->,line width=1] (data) -- (data2);
    \draw[->,line width=1] (data) -- (data3);
    \node[rectangle,draw,fill=green!20,minimum height=1.5] at (4,1.5) (map1) {Map1};
    \node[rectangle,draw,fill=green!20,minimum height=1.5] at (4,0) (map2) {Map2};
    \node[rectangle,draw,fill=green!20,minimum height=1.5] at (4,-1.5) (map3) {Map3};    
    \draw[->,line width=1] (data1) -- (map1);
    \draw[->,line width=1] (data2) -- (map2);
    \draw[->,line width=1] (data3) -- (map3);
    \node[rectangle,draw,fill=orange!20,minimum height=1.5] at (7,.75) (reduce1) {Reduce1};
    \node[rectangle,draw,fill=orange!20,minimum height=1.5] at (7,-.75) (reduce2) {Reduce2};
    \draw[->,line width=1] (map1) -- (reduce1);
    \draw[->,line width=1] (map1) -- (reduce2);
    \draw[->,line width=1] (map2) -- (reduce1);
    \draw[->,line width=1] (map2) -- (reduce2);
    \draw[->,line width=1] (map3) -- (reduce1);
    \draw[->,line width=1] (map3) -- (reduce2);
    \node[data,fill=blue!20,minimum height=3cm] at(9.5,0) (results) {Results};
    \draw[->,line width=1] (reduce1) -- (results);
    \draw[->,line width=1] (reduce2) -- (results);
  \end{tikzpicture}
\end{frame}

\begin{frame}
  \frametitle{Example : usage of protocols from logs}
  \center
  \begin{enumerate}
  \item<2-> split logs on different nodes
  \item<3-> parse and extract key/value ($Protocol/Date$) in Map
  \item<4-> count accesses and output $Date/nb~accesses$ in Reduce
  \item<4-> build graph
  \end{enumerate}
  \tikzstyle data=[draw=black, thick, rectangle, minimum height=.8cm,minimum width=1cm]
  \begin{tikzpicture}[scale=0.7, every node/.style={transform shape}]
    \node[data,fill=blue!20,minimum height=3cm,align=left] (data) {open\\protoA\\close\\open\\protoB\\close\\stat\\protoB\\protoA};
    \onslide<2-> {
      \node[disk,fill=red!20,minimum width=1.5cm] at (2,-2) (data3) {\begin{tikzpicture}
          \node[data,fill=blue!20,align=left] {stat\\protoB\\protoA}; \end{tikzpicture}};
      \node[disk,fill=red!20,minimum width=1.5cm] at (2,0) (data2) {\begin{tikzpicture}
          \node[data,fill=blue!20,align=left] {open\\protoB\\close}; \end{tikzpicture} };
      \node[disk,fill=red!20,minimum width=1.5cm] at (2,2) (data1) {\begin{tikzpicture}
          \node[data,fill=blue!20,align=left] {open\\protoA\\close}; \end{tikzpicture} };
      \draw[->,line width=1] (data) -- (data1.west);
      \draw[->,line width=1] (data) -- (data2.west);
      \draw[->,line width=1] (data) -- (data3.west);
    }
    \onslide<3-> {
      \node[rectangle,draw,fill=green!20,minimum height=1.5] at (4,2) (map1) {Map1};
      \node[rectangle,draw,fill=green!20,minimum height=1.5] at (4,0) (map2) {Map2};
      \node[rectangle,draw,fill=green!20,minimum height=1.5] at (4,-2) (map3) {Map3};    
      \draw[->,line width=1] (data1) -- (map1);
      \draw[->,line width=1] (data2) -- (map2);
      \draw[->,line width=1] (data3) -- (map3);
      \node[rectangle,draw,fill=orange!20,minimum height=1.5] at (7,1) (reduce1) {Reduce1};
      \node[rectangle,draw,fill=orange!20,minimum height=1.5] at (7,-1) (reduce2) {Reduce2};
      \draw[->,line width=1] (map1.east) -- node[above,midway] {A:$t_1$} (reduce1.north west);
      \draw[->,line width=1] (map2.east) -- node[above,near start,sloped] {B:$t_2$} (reduce2.north west);
      \draw[->,line width=1] (map3.east) -- node[below,midway,sloped] {B:$t_3$} (reduce2.south west);
      \draw[->,line width=1] (map3.east) -- node[above,near start,sloped] {A:$t_4$} (reduce1.south west);
    }
    \onslide<4-> {
      \node[draw,] at(10.5,0) (results) {
        \begin{tikzpicture}[scale=0.6, every node/.style={transform shape}]
          \coordinate (p1) at (.5,1);
          \coordinate (p2) at (1.5,2);
          \coordinate (p3) at (2.5,1);
          \coordinate (p4) at (3.5,2);
          \coordinate (q1) at (.75,.5);
          \coordinate (q2) at (2,1.5);
          \coordinate (q3) at (3.25,.5);
          \draw [thick,blue] (p1) to[out=70,in=180] (p2) to[out=0,in=180] (p3) to[out=0,in=270] (p4);
          \draw [thick,red!80!black] (q1) to[out=70,in=180] (q2) to[out=0,in=290] (q3);
        \end{tikzpicture}
      };
      \draw[->,line width=1] (reduce1.east) -- node[above,midway,sloped] {A/$p_1$:$n_1$}  node[below,midway,sloped] {A/$p_2$:$n_2$} (results);
      \draw[->,line width=1] (reduce2.east) -- node[above,midway,sloped] {B/$p_1$:$m_1$} node[below,midway,sloped] {B/$p_2$:$m_2$} (results);
    }
  \end{tikzpicture}
\end{frame}

\begin{frame}
  \frametitle{The case of Hadoop, HDFS}
  \begin{block}{HDFS : the Hadoop Distributed FileSystem}
    \begin{itemize}
    \item Distributed
      \begin{itemize}
      \item files split in blocks spread over the cluster
      \item blocks are typically 128MB
      \end{itemize}
    \item Redundant
      \begin{itemize}
      \item mirroring, 3 copies by default
      \item erasure coding from version 3.0 on
      \end{itemize}
    \item Hardware aware
    \end{itemize}
  \end{block}
\end{frame}

\begin{frame}<1-|handout:4-5,8>
  \frametitle{Parallel Read/Write in HDFS}
  \begin{tikzpicture}
    \node[shape=rectangle,draw,fill=blue!20,minimum width=2cm,minimum height=1cm] at (5.5,0) (Client) {Client};
    \node[shape=rectangle,draw,fill=green!20,minimum width=2cm,minimum height=1cm] at (10,0) (NameNode) {NameNode};
    \foreach \n in {1,2,3,4} {
      \node[draw,anchor=north west,minimum width=2cm,minimum height=1.2cm] (DataNode\n) at (\n*3-3,-2.5) {};
      \node[anchor=north] at (DataNode\n.south) {DataNode \n};
    }
    \onslide<all:2-4> {
      \node[shape=rectangle,draw,fill=yellow!20,minimum width=2cm,minimum height=1cm] at (1,0) (InputFile) {InputFile};
    }
    \onslide<all:2-4> {
      \draw[line width=1.5,->] (InputFile.east) -- node[above,midway]{3 blocks} (Client.west);
    }
    \onslide<all:3-4> {
      \draw[line width=1.5,->] (Client) -- node[above,midway]{where to go ?} (NameNode);
      \draw[line width=1.5,->] (NameNode) -- node[below,midway]{1,2,3} (Client);
    }
    \onslide<all:4> {
      \foreach \n in {1,2,3} {
        \draw[line width=1.5,->] (Client) -- (DataNode\n.north);
      }
    }
    \onslide<all:4-> {
      \node[disk,fill=yellow!20,minimum width=.3,minimum height=1,anchor=west] at (0.2,-3.2) (D11) {1};
      \node[disk,fill=yellow!20,minimum width=.3,minimum height=1,anchor=west] at (3.2,-3.2) (D21) {2};
      \node[disk,fill=yellow!20,minimum width=.3,minimum height=1,anchor=west] at (6.2,-3.2) (D31) {3};
    }
    \onslide<all:5-> {
      \node[disk,fill=yellow!20,minimum width=.3,minimum height=1,anchor=west] at (3.7,-3.2) (D12) {1};
      \node[disk,fill=yellow!20,minimum width=.3,minimum height=1,anchor=west] at (6.7,-3.2) (D22) {2};
      \node[disk,fill=yellow!20,minimum width=.3,minimum height=1,anchor=west] at (9.2,-3.2) (D32) {3};
      \node[disk,fill=yellow!20,minimum width=.3,minimum height=1,anchor=west] at (7.2,-3.2) (D13) {1};
      \node[disk,fill=yellow!20,minimum width=.3,minimum height=1,anchor=west] at (9.7,-3.2) (D23) {2};
      \node[disk,fill=yellow!20,minimum width=.3,minimum height=1,anchor=west] at (0.7,-3.2) (D33) {3};
    }
    \onslide<all:5> {
      \foreach \n in {1,2,3} {
        \draw[line width=1.5,->] (D\n1.north) .. controls +(up:2) and +(up:2) .. (D\n2.north);
      }
      \foreach \n in {1,2,3} {
        \draw[line width=1.5,->] (D\n1.north) .. controls +(up:\n/3+.3) and +(up:\n/3+.3) .. (D\n3.north);
      }
    }
    \onslide<all:7|handout:8> {
      \draw[line width=1.5,->] (Client) -- node[above,midway]{where is file ?} (NameNode);
      \draw[line width=1.5,->] (NameNode) -- node[below,midway]{1:2,2:4,3:1} (Client);      
    }
    \onslide<all:8-> {
      \foreach \n in {1,2,4} {
        \draw[line width=1.5,->] (DataNode\n.north) -- (Client);
      }
      \node[shape=rectangle,draw,fill=red!20,minimum width=2cm,minimum height=1cm] at (1,0) (OutputFile) {OutputFile};
      \draw[line width=1.5,->] (Client.west) -- node[above,midway]{3 blocks} (OutputFile.east);
    }
  \end{tikzpicture}
\end{frame}

\begin{frame}<1-| handout:4-6>
  \frametitle{Parallel Map/Reduce}
  \begin{tikzpicture}
    \node[shape=rectangle,draw,fill=blue!20,minimum width=2cm,minimum height=1cm] at (1,0) (Client) {Client};
    \node[shape=rectangle,draw,fill=green!20,minimum width=2cm,minimum height=1cm] at (5.5,0) (JobTracker) {JobTracker};
    \node[shape=rectangle,draw,fill=green!20,minimum width=2cm,minimum height=1cm] at (10,0) (NameNode) {NameNode};
    \foreach \n in {1,2,3,4} {
      \node[draw,anchor=north west,minimum width=2cm,minimum height=1.2cm] (DataNode\n) at (\n*3-3,-2.5) {};
      \node[anchor=north] at (DataNode\n.south) {DataNode \n};
    }
    \node[disk,fill=yellow!20,minimum width=.3,minimum height=1,anchor=west] at (0.2,-3.2) (D11) {1};
    \node[disk,fill=yellow!20,minimum width=.3,minimum height=1,anchor=west] at (3.2,-3.2) (D21) {2};
    \node[disk,fill=yellow!20,minimum width=.3,minimum height=1,anchor=west] at (6.2,-3.2) (D31) {3};
    \node[disk,fill=yellow!20,minimum width=.3,minimum height=1,anchor=west] at (3.7,-3.2) (D12) {1};
    \node[disk,fill=yellow!20,minimum width=.3,minimum height=1,anchor=west] at (6.7,-3.2) (D22) {2};
    \node[disk,fill=yellow!20,minimum width=.3,minimum height=1,anchor=west] at (9.2,-3.2) (D32) {3};
    \node[disk,fill=yellow!20,minimum width=.3,minimum height=1,anchor=west] at (7.2,-3.2) (D13) {1};
    \node[disk,fill=yellow!20,minimum width=.3,minimum height=1,anchor=west] at (9.7,-3.2) (D23) {2};
    \node[disk,fill=yellow!20,minimum width=.3,minimum height=1,anchor=west] at (0.7,-3.2) (D33) {3};
    \onslide<all:2-4> {
      \draw[line width=1.5,->] (Client.east) -- node[above,midway]{.jar} (JobTracker.west);
    }
    \onslide<all:3-4> {
      \draw[line width=1.5,->] (JobTracker) -- node[above,midway]{where ?} (NameNode);
      \draw[line width=1.5,->] (NameNode) -- node[below,midway]{1:2,2:4,3:1} (JobTracker);
    }
    \onslide<all:4-5> {
      \foreach \n/\p in {1/2,2/4,3/1} {
        \node[anchor=south,shape=rectangle,draw,fill=red!20,minimum width=2cm] at (DataNode\p.north) (Map\n) {Map \n};
      }
    }
    \onslide<all:4> {
      \foreach \n/\p in {1/2,2/4,3/1} {
        \draw[line width=1.5,->] (JobTracker) -- (Map\n.north);
      }
    }
    \onslide<all:5-6> {
      \node[anchor=south,shape=rectangle,draw,fill=red!20,minimum width=2cm] at (DataNode3.north) (Reduce) {Reduce};
    }
    \onslide<all:5> {
      \foreach \n/\p in {1/D12,2/D23,3/D33} {
        \draw[line width=1.5,->] (Map\n.north) .. controls +(up:1) and +(up:1) .. (Reduce.north);
        \draw[line width=1.5,->] (\p.north) -- (\p.north |- Map\n.south);
      }
    }
    \onslide<all:6-> {
      \node[disk,fill=yellow!20,minimum width=.3,minimum height=1,anchor=west] at (4.2,-3.2) (DR1) {4};
    }
    \onslide<all:6> {
      \draw[line width=1.5,->] (Reduce.north) .. controls +(up:2) and +(up:2) .. (DR1.north);      
    }
    \onslide<all:7-|handout:6> {
      \node[disk,fill=yellow!20,minimum width=.3,minimum height=1,anchor=west] at (10.2,-3.2) (DR2) {4};
      \node[disk,fill=yellow!20,minimum width=.3,minimum height=1,anchor=west] at (1.2,-3.2) (DR3) {4};
      \draw[line width=1.5,->] (DR1.north) .. controls +(up:2) and +(up:2) .. (DR2.north);
      \draw[line width=1.5,->] (DR1.north) .. controls +(up:2) and +(up:2) .. (DR3.north);
    }
  \end{tikzpicture}
\end{frame}

\section[c/c]{Conclusion}

\begin{frame}
  \frametitle{Conclusion}
  \begin{exampleblock}{Key messages of the day}
    \begin{itemize}
    \item Take benefit of all existing devices to optimize your efficiency
    \item Do not underestimate the problem of data distribution
    \item Striping solves many problems. Use it but think twice
    \item Map/Reduce avoids a lot of I/O when results are small
    \end{itemize}
  \end{exampleblock}
\end{frame}

\end{document}
