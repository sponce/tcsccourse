\include{header}

%%%%%%%%%%%%%%%%%%
% Document setup %
%%%%%%%%%%%%%%%%%%

\title{Structuring data for efficient I/O}
\author[S. Ponce]{S\'ebastien Ponce\\ \texttt{sebastien.ponce@cern.ch}}
\institute{CERN}
\date{tCSC May \the\year}

%%%%%%%%%%%%%%
% The slides %
%%%%%%%%%%%%%%

\begin{document}

\begin{frame}
  \titlepage
\end{frame}

\begin{frame}
  \frametitle{Overall Course Structure}
  \begin{block}{Structuring Data for efficient I/O}
    \begin{itemize}
      \setlength\itemsep{.07cm}
    \item {\small Data formats, data compression}
    \item {\small Data addressing}
    \end{itemize}
  \end{block}
  \begin{block}{Many ways to Store Data}
    \begin{itemize}
      \setlength\itemsep{.07cm}
    \item {\small Storage devices and their specificities}
    \item {\small Distributing and parallelizing storage}
    \end{itemize}
  \end{block}
  \begin{block}{Preserving data}
    \begin{itemize}
      \setlength\itemsep{.07cm}
    \item {\small Data consistency}
    \item {\small Data safety}
    \end{itemize}
  \end{block}
  \begin{block}{Key ingredients to achieve efficient I/O}
    \begin{itemize}
      \setlength\itemsep{.07cm}
    \item {\small Synchronous vs asynchronous I/O}
    \item {\small I/O optimizations and caching}
    \end{itemize}
  \end{block}
\end{frame}

\begin{frame}
  \frametitle{Outline}
  \vspace{-0.5cm}
  \begin{scriptsize}
    \tableofcontents[sectionstyle=show,subsectionstyle=show]
  \end{scriptsize}
\end{frame}

\section[format]{Data format}

\subsection[row/col]{Row vs Column}

\begin{frame}
  \frametitle{Data structure by example - scenario}
  \begin{block}{Scenario}
    \begin{itemize}
    \item You are measuring temperatures within a piece of detector
    \item You have 10K captors and you take one measure every minute
    \item After a month, you got 432M measures
    \item That is 1.6GB if you take single precision floats (32bits)
    \end{itemize}
  \end{block}
\end{frame}

\begin{frame}
  \frametitle{Data structure by example - row storage}
  \begin{block}{Naive structure}
    \begin{itemize}
    \item You arrange your captors in a sequential order according to the detector geometry
    \item Each minute, you create a new ``row'' of data, with 10K floats representing temperatures given by the captors, in that order
    \end{itemize}
  \end{block}
  \pause
  \center
  \begin{tabular}{lrrrr}
    \toprule
    Time (mn) & Captor 1 & Captor 2 & ... & Captor c \\
    \midrule
    0   & $a_0$ & $b_0$ & ... & $z_0$ \\
    \pause
    \color{blue!70!black}{1}   & \color{blue!70!black}{$a_1$} & \color{blue!70!black}{$b_1$} & ... & \color{blue!70!black}{$z_1$} \\
    \pause
    ... & ...   & ...   & ... & ...  \\
    \color{green!50!black}{n}   & \color{green!50!black}{$a_n$} & \color{green!50!black}{$b_n$} & ... & \color{green!50!black}{$z_n$} \\
    \bottomrule
  \end{tabular}
  \pause
  \begin{exampleblock}{File content}
    \color{black}{$a_0~b_0~...~z_0$}~\color{blue!70!black}{$a_1~b_1~...~z_1$}~...~\color{green!50!black}{$a_n~b_n~...~z_n$} \\
  \end{exampleblock}
\end{frame}

\begin{frame}
  \frametitle{Data structure by example - access}
  \begin{block}{Find out overheated devices at a given time}
    \begin{itemize}
    \item find the offset of that time in the file
    \item read 10K numbers
    \item apply simple filter
    \end{itemize}
  \end{block}
  \center
  \begin{tikzpicture}[line width=1.5]
    \draw[fill=red!20] (2.6,0) rectangle (7.4,.6);
    \draw (0,0) -- (10,0) (0,.6) -- (10,.6);
    \foreach \n in {0,1,...,16} {
      \draw (\n*.6+.2,0) -- (\n*.6+.2,.6);
    }
    \node at (2.6,-.75) {seek} edge[->] (2.6,0);
    \draw[->] (2.6,-.2) -- node[midway,below] {read} (7.4,-.2);
  \end{tikzpicture}
  \pause
  \begin{exampleblock}{Cost}
    \begin{itemize}
    \item one seek
    \item one read of 10K ints
    \end{itemize}
    This is efficient !
  \end{exampleblock}
\end{frame}

\begin{frame}
  \frametitle{Data structure by example - access (2)}
  \begin{block}{Graph the temperature evolution of a given device}
    \begin{itemize}
    \item read 43.2K numbers from the file, every 40K bytes
    \item graph them
    \end{itemize}
  \end{block}
  \center
  \begin{tikzpicture}[line width=1.5]
    \foreach \n in {3,8,13} {
      \draw[fill=red!20] (\n*.6+.2,0) rectangle (\n*.6+.8,.6);
    }
    \draw (0,0) -- (10,0) (0,.6) -- (10,.6);
    \foreach \n in {0,1,...,16} {
      \draw (\n*.6+.2,0) -- (\n*.6+.2,.6);
    }
    \foreach \n in {3,8,13} {
      \node at (\n*.6+.2,-.75) {seek} edge[->] (\n*.6+.2,0);
      \draw[->] (\n*.6+.2,-.2) -- node[near end,below] {read} (\n*.6+.8,-.2);
    }
  \end{tikzpicture}
  \pause
  \begin{exampleblock}{Cost}
    \begin{itemize}
    \item 43.2K reads of 4 bytes and 43.2K seeks !
    \item on top typical block size in a filesystem is 8k
    \item you will probably read effectively 20\% of the file !
    \item actually reading the whole file will be more efficient
    \end{itemize}
    Here the structure of our data is a killer
  \end{exampleblock}
\end{frame}

\begin{frame}
  \frametitle{Column storage}
  \center
  \begin{tabular}{lrrrr}
    \toprule
    Time (mn) & Captor 1 & Captor 2 & ... & Captor c \\
    \midrule
    0   & $a_0$ & \color{blue!70!black}{$b_0$} & ... & \color{green!50!black}{$z_0$} \\
    1   & $a_1$ & \color{blue!70!black}{$b_1$} & ... & \color{green!50!black}{$z_1$} \\
    ... & ...   & ...   & ... & ...  \\
    n   & $a_n$ & \color{blue!70!black}{$b_n$} & ... & \color{green!50!black}{$z_n$} \\
    \bottomrule
  \end{tabular}
  \pause
  \begin{exampleblock}{File content}
    \color{black}{$a_0~a_1~...~a_n$}~\color{blue!70!black}{$b_0~b_1~...~b_n$}~...~\color{green!50!black}{$z_0~z_1~...~z_n$}
  \end{exampleblock}
  \pause
  \begin{exampleblock}{Back to efficient read}
    \center
    \begin{tikzpicture}[line width=1.5]
      \draw[fill=red!20] (2.6,0) rectangle (7.4,.6);
      \draw (0,0) -- (10,0) (0,.6) -- (10,.6);
      \foreach \n in {0,1,...,16} {
        \draw (\n*.6+.2,0) -- (\n*.6+.2,.6);
      }
      \node at (2.6,-.75) {seek} edge[->] (2.6,0);
      \draw[->] (2.6,-.2) -- node[midway,below] {read} (7.4,-.2);
    \end{tikzpicture}
  \end{exampleblock}
\end{frame}

\begin{frame}
  \frametitle{Row vs column storage}
  \begin{exampleblock}{Definition}
    \begin{description}
    \item[Row storage] respects internal structure of the data and puts the different items one next to the other in a sequence
    \item[Column storage] breaks the internal structure of the data to collate similar pieces
    \end{description}
  \end{exampleblock}
  \pause
  \begin{block}{Why to use column ?}
    \begin{itemize}
    \item to optimize I/O in general and avoid scattered reads
    \item to optimize data compression
    \item to optimize parallelization of processing
    \end{itemize}
  \end{block}
  \pause
  \begin{alertblock}{Drawback of column storage}
    \begin{itemize}
    \item a column organized file cannot be updated easily
    \item column storage is usually created from row storage in a postprocessing phase.
    \end{itemize}
  \end{alertblock}
\end{frame}

\begin{frame}
  \frametitle{The parallelization view}
  \begin{columns}
    \begin{column}{0.4\textwidth}
      \begin{block}{Row/Column}
        \begin{itemize}
        \item naming
          \begin{itemize}
          \item Row storage
          \item Column storage
          \end{itemize}
        \item goal
          \begin{itemize}
          \item Storage efficiency, data on disk
          \end{itemize}
        \end{itemize}
      \end{block}
    \end{column}
    \begin{column}{0.5\textwidth}
      \begin{block}{AoS/SoA}
        \begin{itemize}
        \item naming
          \begin{itemize}
          \item Array of structs (AoS)
          \item Struct of arrays (SoA)
          \end{itemize}
        \item goal
          \begin{itemize}
          \item Algorithmic efficiency,\\ data in RAM
          \end{itemize}
        \end{itemize}
      \end{block}
    \end{column}
  \end{columns}
  \begin{block}{A lot in common}
    \begin{itemize}
    \item Same advantages/disadvantages
    \item Same compromises
    \item Same ultimate solution ? : column per block / AoSoA
    \end{itemize}
  \end{block}
\end{frame}

\begin{frame}
  \frametitle{Column per block storage}
  \center
  \begin{tabular}{lrrrr}
    \toprule
    Time (mn) & Captor 1 & Captor 2 & ... & Captor c \\
    \midrule
    0   & $a_0$ & \color{blue!70!black}{$b_0$} & ... & \color{green!50!black}{$z_0$} \\
    ... & ...   & ...   & ... & ...  \\
    p-1   & $a_{p-1}$ & \color{blue!70!black}{$b_{p-1}$} & ... & \color{green!50!black}{$z_{p-1}$} \\
    \midrule
    p   & $a_p$ & \color{blue!70!black}{$b_p$} & ... & \color{green!50!black}{$z_p$} \\
    ... & ...   & ...   & ... & ...  \\
    \bottomrule
  \end{tabular}
  \begin{exampleblock}{File content}
    \color{black}{$a_0~a_1~...~a_{p-1}$}~\color{blue!70!black}{$b_0~b_1~...~b_{p-1}$}~...~\color{green!50!black}{$z_0~z_1~...~z_{p-1}$}~
    \color{black}{$a_p~a_{p+1}~...~a_{2p-1}$}~\color{blue!70!black}{$b_p~b_{p+1}~...~b_{2p-1}$}~...
  \end{exampleblock}
  \pause
  \begin{exampleblock}{Advantages}
    \begin{itemize}
    \item limits number of seeks per data to $\frac{1}{p}$
    \item allows updates, providing a small cache
    \end{itemize}
  \end{exampleblock}
\end{frame}


\section[compress]{Compressing data}

\subsection[algos]{Compression algorithms}

\begin{frame}
  \frametitle{Data compression}
  \begin{block}{Main idea}
    \begin{itemize}
    \item eliminate redundancy (e.g. LZ77)
    \item optimize encoding (Huffman coding)
    \item to squeeze more information into less bytes
    \end{itemize}
  \end{block}
\end{frame}

\begin{frame}
  \frametitle{Eliminate redundancy}
  \begin{block}{LZ77}
    Replace redundancy with pointers
    \center
    \begin{tikzpicture}
      \node[anchor=west] at(0,0) (raw) {qwerABCDhhABCDzABCDhhz};
      \node[anchor=west] at(0,-1.5cm) (zip) {qwerABCDhh\color{blue}{(6,4)}\color{black}{z}\color{green!50!black}{(11,6)}\color{black}{z}};
      \draw[thick,draw=blue,snake=brace,line width=1.5] (.95,.3) -- node(b1){} (1.9,.3) (2.4,.3) -- node(b2){} (3.35,.3);
      \draw[thick,draw=green!50!black,snake=brace,line width=1.5] (2.4,-.2) -- node(b3){} (.85,-.2) (5,-.2) -- node(b4){} (3.55,-.2);
      \draw[<->,blue,line width=1.5] (b1.north) .. controls +(up:5mm) and +(up:5mm) .. (b2.north);
      \draw[<->,green!50!black,line width=1.5] (b3) .. controls +(down:.8cm) and +(down:.8cm) .. (b4);
    \end{tikzpicture}
  \end{block}
\end{frame}

\begin{frame}
  \frametitle{Optimize encoding}
  \begin{block}{Huffman coding idea}
    \begin{itemize}
    \item use short codes for symbols repeated often
    \item build an optimal code set depending on symbols' frequencies
    \end{itemize}
  \end{block}
\end{frame}

\definecolor{bggreen}{RGB}{230,239,230}

\begin{frame}
  \frametitle{Optimize encoding}
  \begin{exampleblock}{Encoding ``faeaebacdeeabadeacdfbdaddadcafdacbaababbaaccaa''}
    \begin{tikzpicture}[level distance=10mm,
        level 1/.style={sibling distance=30mm},
        level 2/.style={sibling distance=30mm},
        level 3/.style={sibling distance=15mm},
        level 4/.style={sibling distance=15mm}
      ]
      \usetikzlibrary{shapes}
      \tikzstyle{c} = [shape=circle]
      \tikzstyle{r} = [shape=rectangle,minimum width=10mm]
      \only<3-> { \tikzstyle{colef} = [draw=black,text=black] }
      \only<1-2|handout:0> { \tikzstyle{colef} = [draw=bggreen,text=bggreen] }
      \only<5-> { \tikzstyle{colbc} = [draw=black,text=black] }
      \only<1-4|handout:0> { \tikzstyle{colbc} = [draw=bggreen,text=bggreen] }
      \only<6-> { \tikzstyle{cold} = [draw=black,text=black] }
      \only<1-5|handout:0> { \tikzstyle{cold} = [draw=bggreen,text=bggreen] }
      \only<7-> { \tikzstyle{colbcd} = [draw=black,text=black] }
      \only<1-6|handout:0> { \tikzstyle{colbcd} = [draw=bggreen,text=bggreen] }
      \only<8-> { \tikzstyle{cola} = [draw=black,text=black] }
      \only<1-7|handout:0> { \tikzstyle{cola} = [draw=bggreen,text=bggreen] }      
      \node[c,cola]{45}[edge from parent]
      child {node[r,cola] {a:17} edge from parent[cola] coordinate (ea); }
      child {node[c,colbcd]{28}
        child {node[c,colbc]{12}
          child{node[r,colbc]{c:5} edge from parent[colbc] coordinate (ec);}
          child{node[r,colbc]{b:7} edge from parent[colbc] coordinate (eb);}
          edge from parent[colbcd] coordinate (e25);
        }
        child {node[c,cold]{16}
          child{node[c,colef]{8}
            child{node[r,colef]{f:3} edge from parent[colef] coordinate (ef);}
            child{node[r,colef]{e:5} edge from parent[colef] coordinate (ee);}
            edge from parent[cold] coordinate (e14);
          }
          child{node[r,cold]{d:8} edge from parent[cold] coordinate (e16);}
          edge from parent[colbcd] coordinate (e30);
        }
        edge from parent[cola] coordinate (e55);
      };
      \onslide<9-> {
        \node at([xshift=-2mm,yshift=2mm]ea)  {0};
        \node at([xshift=2mm,yshift=2mm]e55) {1};
        \node at([xshift=-2mm,yshift=2mm]e25) {0};
        \node at([xshift=2mm,yshift=2mm]e30) {1};
        \node at([xshift=-2mm,yshift=1mm]ec) {0};
        \node at([xshift=2mm,yshift=1mm]eb) {1};
        \node at([xshift=-2mm,yshift=1mm]e14) {0};
        \node at([xshift=2mm,yshift=1mm]e16) {1};
        \node at([xshift=-2mm,yshift=1mm]ef) {0};
        \node at([xshift=2mm,yshift=1mm]ee) {1};
      }
      \onslide<2-> {
        \node at (6.5cm, 0) {\Large Frequencies};
        \node at (5cm, -1.1cm) (LD) {\parbox{3cm}{
            \begin{description}
            \item[\only<1-7,9->{a}\only<8|handout:0>{all}] \only<1-7,9->{$17$}\only<8|handout:0>{$45$}
            \item[\only<1-4,9->{b}\only<5-6|handout:0>{bc}] \only<1-4,9->{$7$}\only<5-6|handout:0>{$12$}
            \item[\only<1-4,9->{c}] \only<1-4,9->{$5$}
            \end{description}
        }};
        \node at (7cm, -1cm) {\parbox{3cm}{
            \begin{description}
            \item[\only<1-5,9->{d}\only<6|handout:0>{def}\only<7|handout:0>{bcdef}] \only<1-5,9->{$8$}\only<6|handout:0>{$16$}\only<7|handout:0>{$28$}
            \item[\only<1-3,9->{e}\only<4-5|handout:0>{ef}] \only<1-3,9->{$5$}\only<4-5|handout:0>{$8$}
            \item[\only<1-3,9->{f}] \only<1-3,9->{$3$}
            \end{description}
        }};
      }
      \onslide<10-> {
        \node at (6.8cm, -2.2cm) {\Large Codes};
        \node at (5cm, -3.3cm) {\parbox{3cm}{
            \begin{description}
            \item[a] $0$
            \item[b] $101$
            \item[c] $100$
            \end{description}
        }};
        \node at (7cm, -3.3cm) {\parbox{3cm}{
            \begin{description}
            \item[d] $111$
            \item[e] $1101$
            \item[f] $1100$
            \end{description}
        }};
      }
      \onslide<11-> {
        \draw[->,line width=2.5,red!80!black] (-1,-5) -- (1,-5);
        \node[align=left] at (4,-5) {$f6b6a7dd57d4ff7bf78fe94ab490$\\28+6 bytes instead of 46};
      }
    \end{tikzpicture}
  \end{exampleblock}
\end{frame}

\begin{frame}
  \frametitle{Classical compression algorithms}
  \begin{block}{Best compression first}
    \begin{description}
    \item[bzip2] high quality, size within 10\% to 15\% to best compressors and twice faster to compress, six times faster to decompress
    \item[gzip] classical ziping with {\it deflate} algorithm, combination of LZ77 and Huffman Coding
    \item[LZO] fast - twice faster than gzip - but less good - 50\% bigger result. Composed of small blocks of \SI{256}{\kilo\byte} of compressed data
    \item[Snappy] even faster - 250MB/s encoding, 500MB/s decoding on single core i7 - but even less good. Use internally by Google
    \end{description}
  \end{block}
\end{frame}

\begin{frame}
  \frametitle{A word on lossy compression}
  \begin{alertblock}{What does it mean, lossy ?}
    \begin{itemize}
    \item that you will lose part of your data !
    \item but that you may not see the difference
    \item and it may allow to compress MUCH better
    \end{itemize}
  \end{alertblock}
  \pause
  \begin{exampleblock}{Classical examples}
    \begin{description}
    \item[jpg] did you notice the images may be slightly blured ?
    \item[mp3] removes all the frequencies you cannot hear
    \end{description}
  \end{exampleblock}
\end{frame}

\begin{frame}
  \frametitle{A word on lossy compression}
  \begin{block}{The main ideas}
    \begin{enumerate}
    \item tranformation of the data, usually based on fourier transforms
    \item sensibility (nb bits) is reduced on less important parts
      \begin{itemize}
      \item e.g. high frequencies
      \end{itemize}
    \item thanks to the previous cuts, a lot of similar values (0) appear
    \item use standard compression
    \end{enumerate}
  \end{block}
  \pause
  \begin{block}{Usage in scientific computing}
    \begin{itemize}
    \item mostly when encoding the data, that is within a subdetector
    \item very seldom at the level of complete data files
    \end{itemize}
  \end{block}
\end{frame}
    
\subsection[efficiency]{Efficiency and use cases}

\begin{frame}
  \frametitle{Compression efficiency factors}
  \begin{block}{Algorithm - a small factor}
    \begin{itemize}
    \item always a tradeoff between speed and compression
    \item best algorithms will gain maximum a factor 2 in size compared to fast ones
    \end{itemize}
  \end{block}
  \begin{block}{Data - the main factor}
    \begin{description}
    \item[content] some contents are most compressable than others - zeros compress very well, while already compressed data don't
    \item[format] human readable formats (XML) tend to compress very well as they are full of redundant markers
    \item[structure] column storage compresses better than raw storage, as identical data types are close to each other
    \end{description}
  \end{block}
\end{frame}


\begin{frame}
  \frametitle{Globally, is compression interesting ?}
  \begin{exampleblock}{Yes if}
    \begin{itemize}
    \item your data are very cold, that is not frequently accessed
      \begin{itemize}
      \item and the gain of space is worth the annoyance
      \item tapes are compressing systematically
      \end{itemize}
    \item you are really I/O bound and compression factor is high
    \item you use a fast compression algorithm (snappy) and gain space for little price
    \end{itemize}
  \end{exampleblock}
  \begin{alertblock}{No if}
    \begin{itemize}
    \item you become CPU bound due to (de)compression
      \begin{itemize}
      \item typical with bzip2
      \end{itemize}
    \item you do frequent partial reads
    \item you need to split and cannot use LZO or bzip2
    \end{itemize}
  \end{alertblock}
\end{frame}

\section[addr]{Data addressing}

\begin{frame}
  \frametitle{Data addressing}
  \center
  {\bf The most important part of your data is your metadata}
  \vspace{1cm}
  \begin{block}{Addressing options}
    \begin{itemize}
    \item good old hierarchical namespace
    \item databases
    \item object store approach
    \end{itemize}
  \end{block}
\end{frame}

\subsection[treens]{Hierarchical namespaces}

\begin{frame}
  \frametitle{Traditionnal, hierarchical data addressing}
  \begin{block}{Directory tree}
    \begin{columns}
      \begin{column}{0.6\textwidth}
        \begin{itemize}
        \item structured via ``directories''
        \item containing ``files'' and subdirectories
        \item filesystem like          
        \end{itemize}
        \onslide<2> {
          \center \Large \color{green!50!black} /atlas/raw/file
        }
      \end{column}
      \begin{column}{0.4\textwidth}
        \tikzstyle{every node}=[draw=black,thick,anchor=west]
        \tikzstyle{selected node}=[style=every node]
        \only<2>{\tikzstyle{selected node}+=[fill=green!40]}
        \begin{tikzpicture}[%
            grow via three points={one child at (0.5,-0.7) and
              two children at (0.5,-0.7) and (0.5,-1.4)},
            edge from parent path={(\tikzparentnode.south) |- (\tikzchildnode.west)}]
          \node[selected node] {root}
          child { node {alice}}		
          child { node[selected node] {atlas}
            child { node[selected node] {raw}
              child {node[selected node] {file}}
            }
            child [missing] {}
            child { node {esd}}
            child { node {other}}
          }
          child [missing] {}
          child [missing] {}
          child [missing] {}
          child [missing] {}
          child { node {cms}}
          child { node {lhcb}};
        \end{tikzpicture}
      \end{column}
    \end{columns}
  \end{block}
\end{frame}

\begin{frame}
  \frametitle{Hierarchy pros and contras}
  \begin{exampleblock}{Pros}
    \begin{itemize}
    \item easiness of use (for humans)
    \item helps to structure the data sets
      \begin{itemize}
      \item easy to list/erase/deny access to a whole subtree
      \item allows to implement quotas per directory
      \end{itemize}
    \item atomic operations
      \begin{itemize}
      \item in particular moves, removals
      \item change of permissions
      \end{itemize}
    \item always consistent
    \end{itemize}
  \end{exampleblock}
  \begin{alertblock}{Contras}
    \begin{itemize}
    \item becomes slow for deep hierarchies
    \item in general scales badly
    \end{itemize}
  \end{alertblock}
\end{frame}

\subsection[limits]{Limitations}

\begin{frame}
  \frametitle{Influence of depth}
  \begin{columns}
    \begin{column}{0.6\textwidth}
      \begin{itemize}
      \item Suppose you access '/A/B/C/.../Z'
      \item The following will happen :
        \begin{itemize}
        \item<2-> find $root$
        \item<2-> check you have execution right
        \item<3-> find $A$ within $root$
        \item<3-> check you have execution right
        \item<4-> find $B$ within $A$
        \item<4-> check you have execution right
        \item<5-> ... repeat until $Y$ ...
        \item<6-> find $Z$ within $Y$
        \item<6-> check you have read/write rights
        \end{itemize}
      \end{itemize}
    \end{column}
    \begin{column}{0.4\textwidth}
      \tikzstyle{every node}=[draw=black,thick,anchor=west]
      \tikzstyle{selRoot}=[style=every node]
      \tikzstyle{selA}=[style=every node]
      \tikzstyle{selB}=[style=every node]
      \tikzstyle{selDots}=[style=every node]
      \tikzstyle{selZ}=[style=every node]
      \only<2>{\tikzstyle{selRoot}+=[fill=green!40]}
      \only<3>{\tikzstyle{selA}+=[fill=green!40]}
      \only<4>{\tikzstyle{selB}+=[fill=green!40]}
      \only<5>{\tikzstyle{selDots}+=[fill=green!40]}
      \only<6>{\tikzstyle{selZ}+=[fill=green!40]}
      \begin{tikzpicture}[%
          grow via three points={one child at (0.5,-0.7) and
            two children at (0.5,-0.7) and (0.5,-1.4)},
          edge from parent path={(\tikzparentnode.south) |- (\tikzchildnode.west)}]
        \node[selRoot] {root}
        child { node[selA] {A}
          child { node[selB] {B}
            child { node[selDots] {...}
              child { node[selZ] {Z} }
            }
          }
        };
      \end{tikzpicture}
    \end{column}
  \end{columns}
  \vspace{.5cm}
  \onslide<7> {
    \begin{tikzpicture}
      \draw[->,line width=3,anchor=west] (0,0) -- (1,0);
      \node[anchor=west] at (1.5,0) {\Large \color{red} $2 n +1$ operations for depth $n$};
      \node[anchor=west] at (0,-.7) {\large depth seen at CERN (EOS, CASTOR) : 20-25};
    \end{tikzpicture}
  }
\end{frame}

\begin{frame}
  \frametitle{Atomicity and consistency consequences}
  \begin{block}{$root$ is a bottle neck}
    \begin{itemize}
    \item every single requests starts at root
    \item no easy parallelization of this part because of atomicity
    \end{itemize}
  \end{block}
  \pause
  \begin{block}{consistent recursive listing/searching is a nightmare}
    \begin{itemize}
    \item you would need to lock all subtree for the whole listing time
    \item makes full listing impossible
    \end{itemize}
  \end{block}
\end{frame}

\begin{frame}
  \frametitle{Some partial solutions - rights}
  \begin{columns}
    \begin{column}{.6\textwidth}
      \begin{block}{Precomputation}
        \begin{itemize}
        \item precompute effective rights/size/... at every level
        \end{itemize}
      \end{block}
      \onslide<3>{
        \begin{alertblock}{But...}
          \begin{itemize}
          \item updating a top level right means full recomputation !
          \item the complete expansion for each file may be extremely heavy
          \item especially if complex ACLs are used at directory levels
          \end{itemize}
        \end{alertblock}
      }
    \end{column}
    \begin{column}{.4\textwidth}
      \tikzstyle{every node}=[draw=black,thick]
      \begin{tikzpicture}[remember picture,
          grow via three points={one child at (.2,-1.5)
            and two children at (0,-1.5) and (0,-3)},
          align=right,
          edge from parent path={(\tikzparentnode.east) .. controls +(right:1) and +(right:1) .. (\tikzchildnode.east)},->,line width=1.5]
        \node (root) {root 90 rwxr-xr-x \\
          {\onslide<2->{\color{red!70!black}2270 rwxr-xr-x}}}
        child { node (atlas) {atlas 90 rwxr-xr-- \\
            {\onslide<2->{\color{red!50!black}2180 rwxr-xr--}}}
          child { node (raw) {raw 90 rwxrwxr-x \\
              {\onslide<2->{\color{red!50!black}2090 rwxr-x---}}}
            child {node (file) {file 2000 r-xr-xr-x \\
                {\onslide<2->{\color{red!50!black}2000 r-xr-x---}}}}
          }
        };
      \end{tikzpicture}
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}
  \frametitle{Some partial solutions - rights}
  \begin{exampleblock}{Avoiding full recomputation}
    \begin{itemize}
    \item one can mark a subtree as ``dirty''
    \item can then be recomputed offline
    \item in case we hit a dirty tree, we go back to tree scanning
    \end{itemize}
  \end{exampleblock}
  \pause
  \begin{alertblock}{But...}
    \begin{itemize}
    \item tree scanning is needed to check for the dirty flag !
    \item actually, structures like bit map indexes can help speeding it up
    \end{itemize}
  \end{alertblock}
\end{frame}

\begin{frame}
  \frametitle{Some partial solutions - root bottleneck}
  \begin{block}{Path indexing}
    \begin{itemize}
    \item keep an index of full paths and their locations
    \end{itemize}
  \end{block}
  \center
  \begin{columns}
    \begin{column}{.72\textwidth}
      \tikzstyle{every node}=[draw=black,thick,anchor=west]
      \begin{tikzpicture}[%
          grow via three points={one child at (0.5,-0.7) and
            two children at (0.5,-0.7) and (0.5,-1.4)},
          edge from parent path={(\tikzparentnode.south) |- (\tikzchildnode.west)}]
        \node(root) at(0,2) {root}
        child { node (alice) {alice}}		
        child { node (atlas) {atlas}
          child { node (raw) {raw}
            child {node (file) {file}}
          }
          child [missing] {}
          child { node (esd) {esd}}
          child { node (other) {other}}
        }
        child [missing] {}
        child [missing] {}
        child [missing] {}
        child [missing] {}
        child { node (cms) {cms}}
        child { node (lhcb) {lhcb}};
        \onslide<2->{
          \draw[draw=black,fill=yellow!50] (5,-2.7) rectangle (8,2);
          \foreach \n/\p [count=\xi] in {root/{/}, alice/{/alice/}, atlas/{/atlas/}, raw/{/atlas/raw/}, file/{/atlas/raw/file}, esd/{/atlas/esd/}, other/{/atlas/other/}, cms/{/cms/}, lhcb/{/lhcb/}} {
            \node[draw=none,anchor=south west,align=left] (db\n) at (5,1.8-\xi*0.5) {\p} rectangle (8,1.3-\xi*0.5);
            \draw[->] (db\n.west) .. controls +(left:1) and +(right:1) .. (\n);
          }
        }
      \end{tikzpicture}
    \end{column}
    \begin{column}{.28\textwidth}
      \onslide<3->{
        \begin{alertblock}{But...}
          \begin{itemize}
          \item just think of a rename of atlas into titan...
          \end{itemize}
        \end{alertblock}
      }
      \onslide<4->{
        \begin{exampleblock}{Ideas}
          \begin{itemize}
          \item use redirections, dirty flags, ...
          \end{itemize}
        \end{exampleblock}
      }
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}
  \frametitle{Some partial solutions - conclusion}
  \center
  \begin{tikzpicture}
    \draw[->,line width=3,anchor=west] (0,0) -- (1,0);
    \node[anchor=west] at (1.5,0) {\Large \color{red} very high complexity};
    \node[anchor=west] at (0,-.7) {\large no scalable solution on the market so far};
  \end{tikzpicture}
\end{frame}
      
\subsection[flatns]{Flat namespaces}

\begin{frame}
  \frametitle{Radical change : flat namespaces}
  \begin{block}{The idea}
    \begin{itemize}
    \item drop the hierarchy
    \item only have a sea of objects with a unique identifier
    \item forbid object modifications (only appends)
    \item forbid renames
      \begin{itemize}
      \item or implement them as copy + drop
      \end{itemize}
    \end{itemize}
  \end{block}
  \pause
  \center
  \tikzstyle{every node}=[draw=black,thick,anchor=west]
  \begin{tikzpicture}[%
      grow via three points={one child at (0.5,-0.7) and
        two children at (0.5,-0.7) and (0.5,-1.4)},
      edge from parent path={(\tikzparentnode.south) |- (\tikzchildnode.west)}]
    \node(root) at(0,2) {root}
    child { node (alice) {alice}}
    child { node (atlas) {atlas}
      child { node (raw) {raw}
        child {node (file) {file}}
      }
      child [missing] {}
      child { node (esd) {esd}}
    };
    \draw[->,line width=1.5] (4,0.5) -- (5,0.5);
    \foreach \n/\pos in {{alice/(7,1.1)}, {atlas/(8,0.6)}, {raw/(8.5,0)}, {file/(7.5,-0.1)}, {esd/(6.5,0.2)}} {
      \node[draw=none] at \pos {\n};
    }
    \node[cloud,cloud puffs=7,cloud puff arc=120, aspect=2, inner ysep=1.8em] at (6,0.5) {};
  \end{tikzpicture}  
\end{frame}

\begin{frame}
  \frametitle{Radical change : flat namespaces}
  \begin{exampleblock}{Infinitely scalable namespace}
    \begin{itemize}
    \item client can hash the entry path
    \item and connect accordingly to the right server
    \end{itemize}
  \end{exampleblock}
  \pause
  \begin{alertblock}{You lose}
    \begin{itemize}
    \item data management aspect, left to user
    \item listing facilities
      \begin{itemize}
      \item can be implemented in a Map/Reduce way
      \end{itemize}
    \end{itemize}
  \end{alertblock}  
\end{frame}

\begin{frame}
  \frametitle{About hashing on the client}
  \begin{alertblock}{Large systems are dynamic}
    \begin{itemize}
    \item i.e. they change over time
      \begin{itemize}
      \item adding/removing hardware
      \item changing network topology
      \end{itemize}
    \item data may need to be rebalanced (see later)
    \item the client hashing algorithm needs to deal with that
    \end{itemize}
  \end{alertblock}
  \pause
  \begin{exampleblock}{CRUSH : the ceph's hash}
    \begin{itemize}
    \item achieves pseudo-random but deterministic data distribution
    \item supports replication and erasure coding (see later)
    \item taking into account storage structure (machines, racks, rows, ...)
    \item minimizing the data movements in case of cluster modifications
    \end{itemize}
  \end{exampleblock}
\end{frame}

\section[state]{Stateful interfaces}

\subsection[posix]{POSIX}

\begin{frame}[fragile]
  \frametitle{The traditional POSIX interface for I/O}
  \begin{block}{Based on file descriptors}
    \begin{minted}[gobble=4]{c}
      int open(const char *pathname, int flags);
      int socket(int domain, int type, int protocol);
      int connect(int sockfd,
                  const struct sockaddr *addr,
                  socklen_t addrlen);
      ssize_t read(int fd, void *buf, size_t count);
      ssize_t write(int fd,
                    const void *buf, size_t count);
      off_t lseek(int fd, off_t offset, int whence);
      int close (int fd);
      int stat(const char *pathname, struct stat *buf);
      DIR *opendir(const char *name);
      struct dirent *readdir(DIR *dirp);
      int closedir(DIR *dirp);
    \end{minted}
  \end{block}
\end{frame}

\subsection[limits]{Limitations}

\begin{frame}
  \frametitle{Key features and their consequences}
  \begin{block}{hierarchical namespace}
    \begin{itemize}
    \item can be a limitation
    \end{itemize} 
  \end{block}
  \begin{block}{strong coherency}
    \begin{itemize}
    \item last writer wins, metadata updated
    \item not suited for distributed environment, out of order concurrent writes
    \item lack of explicit atomicity
    \item metadata maintenance (e.g. last access time) is costly
    \end{itemize}
  \end{block}
  \begin{block}{file oriented}
    \begin{itemize}
    \item no bulk interfaces for metadata
    \item stateful, requires server to maintain a state per client
      \begin{itemize}
      \item e.g. opendir / readdir
      \end{itemize}
    \end{itemize}
  \end{block}
\end{frame}

\begin{frame}
  \frametitle{Limitations of stateful interfaces}
  \begin{block}{Server resources}
    \begin{itemize}
    \item any stateful client call means allocation of server resources
    \item these resources will accumulate with the number of clients
    \item limiting single server scalability 
    \end{itemize}
  \end{block}
  \pause
  \begin{block}{Parallelization issues}
    \begin{itemize}
    \item multiple servers in parallel would need to share resources
      \begin{itemize}
      \item which creates a single point of faillure and a possible contention
      \end{itemize}
    \item alternatively you can dedicate servers to clients
      \begin{itemize}
      \item but you lose server fail over
      \item and load balancing
      \end{itemize}
    \end{itemize}
  \end{block}
  \pause
  \center \bf Practically stateful interfaces do not scale
\end{frame}

\subsection[stateless]{Stateless interfaces}

\begin{frame}[fragile]
  \frametitle{Stateless interfaces}
  \begin{block}{Approach}
    \begin{itemize}
    \item each client request must bring enough information to complete it without context
    \item simply the state is stored on the client
    \end{itemize}
  \end{block}
  \pause
  \begin{exampleblock}{Example of listing}
    \begin{itemize}
    \item stateful
      \begin{minted}[gobble=6]{c}
        DIR *opendir(const char *name);
        struct dirent *readdir(DIR *dirp);
        int closedir(DIR *dirp);
      \end{minted}
    \item stateless
      \begin{minted}[gobble=6]{c}
        struct dirent *readdir(const char *name);
        struct dirent *readdir(struct dirent* current);
      \end{minted}
    \item providing you have a strict order defined
    \end{itemize}
  \end{exampleblock}
\end{frame}

\section[c/c]{Conclusion}

\begin{frame}
  \frametitle{Conclusion}
  \begin{exampleblock}{Key messages of the day}
    \begin{itemize}
    \item Choose well your data format
    \item Think twice before compressing
    \item Value your metadata
    \item Think stateless for scalability
    \end{itemize}
  \end{exampleblock}
\end{frame}

\end{document}
